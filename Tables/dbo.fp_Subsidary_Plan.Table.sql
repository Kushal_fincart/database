USE [FP]
GO
/****** Object:  Table [dbo].[fp_Subsidary_Plan]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_Subsidary_Plan](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NULL,
	[email] [nvarchar](max) NULL,
	[mobile] [nvarchar](max) NULL,
	[currrentage] [nvarchar](max) NULL,
	[retirementage] [nvarchar](max) NULL,
	[lifeexpentency] [nvarchar](max) NULL,
	[status] [nvarchar](max) NULL,
	[Risk_Ability] [nvarchar](max) NULL,
	[E_Fund_Amount] [nvarchar](max) NULL,
	[E_Fund_SIP] [nvarchar](max) NULL,
	[Risk_T_Insurance] [nvarchar](max) NULL,
	[Risk_FF_Insurance] [nvarchar](max) NULL,
	[Retire] [bit] NULL,
	[Retire_Amount] [nvarchar](max) NULL,
	[Retire_SIP] [nvarchar](max) NULL,
	[Vacation] [bit] NULL,
	[Vacation_Amount] [nvarchar](max) NULL,
	[Vacation_SIP] [nvarchar](max) NULL,
	[Asset] [bit] NULL,
	[Asset_Amount] [nvarchar](max) NULL,
	[Asset_SIP] [nvarchar](max) NULL,
	[other] [bit] NULL,
	[other_Amount] [nvarchar](max) NULL,
	[other_SIP] [nvarchar](max) NULL,
	[ChildE] [bit] NULL,
	[ChildE_Amount] [nvarchar](max) NULL,
	[ChildE_SIP] [nvarchar](max) NULL,
	[ChildM] [bit] NULL,
	[ChildM_Amount] [nvarchar](max) NULL,
	[ChildM_SIP] [nvarchar](max) NULL,
	[chartdata] [nvarchar](max) NULL,
	[illness] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[planStatus] [int] NULL,
	[Schemes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

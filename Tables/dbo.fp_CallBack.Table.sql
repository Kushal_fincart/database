USE [FP]
GO
/****** Object:  Table [dbo].[fp_CallBack]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_CallBack](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[CBName] [varchar](50) NULL,
	[CBMobile] [varchar](12) NULL,
	[CountryCode] [varchar](5) NULL,
	[CBEmail] [varchar](50) NULL,
	[CBStatus] [char](1) NULL,
	[EntryDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

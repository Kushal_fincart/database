USE [FP]
GO
/****** Object:  Table [dbo].[tmpfin_Cart]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmpfin_Cart](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[userId] [nvarchar](max) NOT NULL,
	[memberId] [nvarchar](max) NOT NULL,
	[profileId] [nvarchar](max) NULL,
	[CartId] [nvarchar](max) NULL,
	[goalCode] [nvarchar](max) NULL,
	[fundCode] [nvarchar](max) NULL,
	[schemeCode] [nvarchar](max) NULL,
	[type] [nvarchar](max) NULL,
	[entryDate] [datetime] NULL,
	[dividendOpt] [nvarchar](max) NULL,
	[folioNo] [nvarchar](max) NULL,
	[amount] [nvarchar](max) NULL,
	[startDate] [datetime] NULL,
	[endDate] [datetime] NULL,
	[noOfInstallment] [int] NULL,
	[frequency] [nvarchar](max) NULL,
	[bankCode] [nvarchar](max) NULL,
	[accountNo] [nvarchar](max) NULL,
	[mandateId] [nvarchar](max) NULL,
	[firstSipDate] [datetime] NULL,
	[mDate] [nvarchar](max) NULL,
	[status] [int] NULL,
	[UserGoalId] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

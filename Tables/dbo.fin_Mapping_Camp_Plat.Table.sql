USE [FP]
GO
/****** Object:  Table [dbo].[fin_Mapping_Camp_Plat]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_Mapping_Camp_Plat](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Camp_Id] [bigint] NOT NULL,
	[Platform_Id] [bigint] NOT NULL,
	[Track_Code] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_fin_Mapping_Camp_Plat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[fin_Mapping_Camp_Plat] ADD  CONSTRAINT [Default_status_map]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[fin_Mapping_Camp_Plat]  WITH CHECK ADD  CONSTRAINT [FK_fin_Mapping_Camp_Plat_fin_Campaign] FOREIGN KEY([Camp_Id])
REFERENCES [dbo].[fin_Campaign] ([ID])
GO
ALTER TABLE [dbo].[fin_Mapping_Camp_Plat] CHECK CONSTRAINT [FK_fin_Mapping_Camp_Plat_fin_Campaign]
GO
ALTER TABLE [dbo].[fin_Mapping_Camp_Plat]  WITH CHECK ADD  CONSTRAINT [FK_fin_Mapping_Camp_Plat_fin_Campaign_Platform] FOREIGN KEY([Platform_Id])
REFERENCES [dbo].[fin_Campaign_Platform] ([ID])
GO
ALTER TABLE [dbo].[fin_Mapping_Camp_Plat] CHECK CONSTRAINT [FK_fin_Mapping_Camp_Plat_fin_Campaign_Platform]
GO
ALTER TABLE [dbo].[fin_Mapping_Camp_Plat]  WITH CHECK ADD  CONSTRAINT [FK_fin_Mapping_Camp_Plat_fin_Mapping_Camp_Plat] FOREIGN KEY([ID])
REFERENCES [dbo].[fin_Mapping_Camp_Plat] ([ID])
GO
ALTER TABLE [dbo].[fin_Mapping_Camp_Plat] CHECK CONSTRAINT [FK_fin_Mapping_Camp_Plat_fin_Mapping_Camp_Plat]
GO

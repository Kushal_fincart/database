USE [FP]
GO
/****** Object:  Table [dbo].[fp_Prospects_Trash]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_Prospects_Trash](
	[Id] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[Code] [numeric](18, 0) NULL,
	[Salutation] [varchar](10) NULL,
	[Name] [varchar](50) NULL,
	[UserID] [varchar](50) NULL,
	[Pwd] [varchar](20) NULL,
	[Category] [varchar](20) NULL,
	[ClientStatus] [varchar](5) NULL,
	[Gender] [char](1) NULL,
	[DOB] [datetime] NULL,
	[Mobile1] [varchar](10) NULL,
	[Mobile2] [varchar](10) NULL,
	[Email1] [varchar](50) NULL,
	[Email2] [varchar](50) NULL,
	[PAN] [varchar](10) NULL,
	[Occuption] [varchar](20) NULL,
	[Company] [varchar](50) NULL,
	[Designation] [numeric](10, 0) NULL,
	[OfficeAddress] [varchar](150) NULL,
	[ResAddress] [varchar](150) NULL,
	[City] [varchar](25) NULL,
	[State] [varchar](20) NULL,
	[Country] [varchar](25) NULL,
	[Pin] [varchar](6) NULL,
	[Active] [char](1) NULL,
	[GroupCode] [numeric](10, 0) NULL,
	[Relation] [numeric](10, 0) NULL,
	[FinancialDependent] [char](1) NULL,
	[HealthHistory] [varchar](50) NULL,
	[UserKey] [uniqueidentifier] NULL,
	[MaritalStatus] [varchar](10) NULL,
	[AnniversaryDate] [datetime] NULL,
	[Source1] [varchar](20) NULL,
	[Source2] [varchar](20) NULL,
	[OpType1] [varchar](10) NULL,
	[OpType2] [varchar](10) NULL,
	[SiteAC] [char](1) NULL,
	[TxnAC] [char](1) NULL,
	[RiskCode] [numeric](10, 0) NULL,
	[Remarks] [varchar](100) NULL,
	[PlannerCode] [numeric](10, 0) NULL,
	[PLock] [char](1) NULL,
	[OperatorCode] [numeric](10, 0) NULL,
	[AllocateDate] [datetime] NULL,
	[SystemDate] [datetime] NULL,
	[EditDate] [datetime] NULL,
	[EditBy] [varchar](30) NULL,
	[LeadType] [numeric](10, 0) NULL,
	[CountryCode] [varchar](5) NULL,
	[GrossIncome] [numeric](10, 0) NULL,
	[FTimePeriod] [varchar](20) NULL,
	[FAmount] [numeric](10, 0) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

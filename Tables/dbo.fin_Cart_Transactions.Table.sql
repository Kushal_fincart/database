USE [FP]
GO
/****** Object:  Table [dbo].[fin_Cart_Transactions]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fin_Cart_Transactions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[InvestTxnId] [bigint] NOT NULL,
	[userId] [nvarchar](max) NOT NULL,
	[memberId] [nvarchar](max) NOT NULL,
	[profileId] [nvarchar](max) NULL,
	[transDate] [datetime] NULL,
	[status] [int] NULL,
	[transType] [varchar](500) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE [FP]
GO
/****** Object:  Table [dbo].[fin_landingpage_usergoals]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_landingpage_usergoals](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[userid] [nvarchar](max) NULL,
	[campaign_key] [nvarchar](max) NULL,
	[goalcode] [nvarchar](200) NULL,
	[status] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

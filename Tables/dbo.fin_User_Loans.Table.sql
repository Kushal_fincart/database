USE [FP]
GO
/****** Object:  Table [dbo].[fin_User_Loans]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fin_User_Loans](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UUID] [nvarchar](max) NULL,
	[UserID] [nvarchar](200) NULL,
	[LoanID] [varchar](5) NULL,
	[CreatedByEmail] [nvarchar](200) NULL,
	[CreatedByUUID] [nvarchar](max) NULL,
	[CreatedDatetime] [datetime] NULL,
	[UpdatedByEmail] [nvarchar](200) NULL,
	[UpdatedByUUID] [nvarchar](max) NULL,
	[UpdatedDatetime] [datetime] NULL,
	[Status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

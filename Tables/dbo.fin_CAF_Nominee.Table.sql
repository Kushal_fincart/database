USE [FP]
GO
/****** Object:  Table [dbo].[fin_CAF_Nominee]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_CAF_Nominee](
	[nominee_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[basic_ID] [bigint] NOT NULL,
	[Nominee_name] [nvarchar](max) NULL,
	[Relation] [nvarchar](max) NULL,
	[Nominee_Dob] [date] NULL,
	[percent] [int] NULL,
	[Guardian_name] [nvarchar](max) NULL,
	[Guardian_pan] [nvarchar](max) NULL,
	[CAF_Nominee_Status] [bit] NULL,
	[createdDate] [datetime] NULL,
	[updatedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

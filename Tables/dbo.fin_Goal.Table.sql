USE [FP]
GO
/****** Object:  Table [dbo].[fin_Goal]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_Goal](
	[GID] [int] IDENTITY(1,1) NOT NULL,
	[GCode] [nvarchar](max) NULL,
	[GName] [nvarchar](max) NULL,
	[GDesc] [nvarchar](max) NULL,
	[GType] [int] NULL,
	[GBrowserId] [nvarchar](500) NULL,
	[GBrowserIp] [nvarchar](500) NULL,
	[GCreatedByEmail] [nvarchar](500) NULL,
	[GCreatedByMobile] [nvarchar](50) NULL,
	[GCreatedDatetime] [datetime] NULL,
	[GUpdatedByEmail] [nvarchar](500) NULL,
	[GUpdatedByMobile] [nvarchar](50) NULL,
	[GUpdatedDatetime] [datetime] NULL,
	[GStatus] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

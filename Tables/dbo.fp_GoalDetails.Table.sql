USE [FP]
GO
/****** Object:  Table [dbo].[fp_GoalDetails]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_GoalDetails](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[GroupCode] [numeric](10, 0) NULL,
	[GoalCode] [numeric](10, 0) NULL,
	[FamilyMemberCode] [numeric](10, 0) NULL,
	[GoalName] [varchar](50) NULL,
	[Priority] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Amount] [numeric](18, 0) NULL,
	[ActualInvestment] [numeric](10, 0) NULL,
	[ExlCode] [varchar](6) NULL,
	[RetirementAge] [numeric](3, 0) NULL,
	[CurrentExpenditure] [numeric](10, 0) NULL,
	[RequiredInflow] [numeric](10, 0) NULL,
	[LoanAmount] [numeric](10, 0) NULL,
	[LifeExpectancy] [numeric](10, 0) NULL,
	[EntryDate] [datetime] NULL,
	[Remarks] [varchar](150) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

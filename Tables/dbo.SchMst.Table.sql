USE [FP]
GO
/****** Object:  Table [dbo].[SchMst]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SchMst](
	[FCODE] [varchar](6) NULL,
	[SCODE] [varchar](6) NULL,
	[SNAME] [varchar](100) NULL,
	[ExlCode] [int] NULL,
	[CAMSCode] [varchar](15) NULL,
	[IILCode] [varchar](15) NULL,
	[AmfiCode] [varchar](20) NULL,
	[Objective] [varchar](50) NULL,
	[ACT_INACT] [varchar](1) NULL,
	[SCHEMENAME] [varchar](50) NULL,
	[Type] [varchar](5) NULL,
	[CreateDate] [datetime] NULL,
	[EditDate] [smalldatetime] NULL,
	[REINVESTSAMEDAY] [char](1) NULL,
	[DivFreq] [char](1) NULL,
	[Remark] [varchar](200) NULL,
	[Div_Up_Date] [smalldatetime] NULL,
	[NAV_Up_Date] [smalldatetime] NULL,
	[Open_Date] [smalldatetime] NULL,
	[Close_Date] [smalldatetime] NULL,
	[OLDSTATUS] [char](15) NULL,
	[FSCode] [varchar](10) NULL,
	[obj] [varchar](50) NULL,
	[bench] [varchar](50) NULL,
	[facevalue] [float] NULL,
	[AFCode] [varchar](6) NULL,
	[NAV] [numeric](18, 0) NULL,
	[NJCode] [varchar](10) NULL,
	[EquityPer] [numeric](6, 2) NULL,
	[EquityDebt] [numeric](6, 2) NULL,
	[on] [char](1) NULL,
	[register] [char](1) NULL,
	[tax_stat] [char](1) NULL,
	[SchGroupCode] [int] NULL,
	[Org_Scheme] [varchar](150) NULL,
	[Expens_Ratio] [numeric](5, 4) NULL,
	[BenchMark] [varchar](20) NULL,
	[Maturity_Date] [datetime] NULL,
	[Close_Ended] [char](1) NULL,
	[CategoryCode] [int] NULL,
	[CurrExitLoad] [varchar](500) NULL,
	[GoldPer] [numeric](15, 2) NULL,
	[OthPer] [numeric](15, 2) NULL,
	[Manager] [varchar](50) NULL,
	[HighNAV] [numeric](18, 5) NULL,
	[HighNAVDt] [datetime] NULL,
	[LowNAV] [numeric](18, 5) NULL,
	[LowNAVDt] [datetime] NULL,
	[SchBenchMark] [varchar](8) NULL,
	[Risk] [char](1) NULL,
	[FECode] [varchar](10) NULL,
	[ViewStatus] [char](1) NULL,
	[DirPlan] [char](1) NULL,
	[ISINNO] [varchar](30) NULL,
	[SuperObjective] [varchar](50) NULL,
	[BSECODE] [varchar](25) NULL,
	[NSECODE] [varchar](25) NULL,
	[BSE_SERIES] [varchar](5) NULL,
	[NSE_SERIES] [varchar](5) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE [FP]
GO
/****** Object:  Table [dbo].[statelist]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[statelist](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[state] [nvarchar](max) NULL,
	[code] [nvarchar](200) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

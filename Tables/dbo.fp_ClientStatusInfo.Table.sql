USE [FP]
GO
/****** Object:  Table [dbo].[fp_ClientStatusInfo]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_ClientStatusInfo](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[ClientCode] [numeric](10, 0) NULL,
	[MyDetails] [char](1) NULL,
	[FamilyDetail] [char](1) NULL,
	[MyReq] [char](1) NULL,
	[Assets] [char](1) NULL,
	[Liabilities] [char](1) NULL,
	[Income] [char](1) NULL,
	[Expense] [char](1) NULL,
	[Insurance] [char](1) NULL,
	[Investments] [char](1) NULL,
	[OperationType1] [varchar](20) NULL,
	[OperationType2] [varchar](20) NULL,
	[Source1] [varchar](20) NULL,
	[Source2] [varchar](20) NULL,
	[EntryDate] [datetime] NULL,
	[EditDate] [datetime] NULL,
	[EditBy] [varchar](30) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE [FP]
GO
/****** Object:  Table [dbo].[fin_LeadActivityMonitor]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_LeadActivityMonitor](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[leadCode] [bigint] NOT NULL,
	[empCode] [nvarchar](max) NULL,
	[comment] [nvarchar](max) NULL,
	[activityId] [bigint] NULL,
	[actionDate] [datetime] NULL,
	[nextActionDate] [datetime] NULL,
	[status] [bit] NULL,
	[isReminder] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

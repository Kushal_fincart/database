USE [FP]
GO
/****** Object:  Table [dbo].[fin_ModuleAssignToEmployee]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fin_ModuleAssignToEmployee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmpCode] [varchar](50) NULL,
	[ModuleID] [int] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[fin_ModuleAssignToEmployee] ADD  CONSTRAINT [DF__fin_Modul__Statu__0D4FE554]  DEFAULT ((1)) FOR [Status]
GO

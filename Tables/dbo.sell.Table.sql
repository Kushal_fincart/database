USE [FP]
GO
/****** Object:  Table [dbo].[sell]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sell](
	[bid] [varchar](8) NOT NULL,
	[cid] [varchar](8) NULL,
	[fcode] [varchar](6) NULL,
	[scode] [varchar](6) NULL,
	[amount] [float] NULL,
	[nav] [float] NULL,
	[units] [float] NULL,
	[navdt] [datetime] NULL,
	[cancelled] [varchar](3) NULL,
	[foliono] [varchar](40) NULL,
	[divrein] [char](1) NULL,
	[bcode] [char](2) NULL,
	[tran_no] [varchar](20) NULL,
	[seqno] [int] NULL,
	[grplead] [varchar](20) NULL,
	[flag] [char](1) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

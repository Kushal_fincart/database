USE [FP]
GO
/****** Object:  Table [dbo].[fp_Expense]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_Expense](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[ClientCode] [numeric](10, 0) NULL,
	[Source] [numeric](10, 0) NULL,
	[SourceSub] [numeric](10, 0) NULL,
	[ExpenseMonthly] [numeric](18, 0) NULL,
	[ExpenseYearly] [numeric](18, 0) NULL,
	[MiscExpenseName] [varchar](30) NULL,
	[ExpensePeriod] [varchar](20) NULL,
	[EntryDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE [FP]
GO
/****** Object:  Table [dbo].[fp_Investments]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_Investments](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[ClientCode] [numeric](10, 0) NULL,
	[ProductName] [varchar](50) NULL,
	[SchemeName] [varchar](50) NULL,
	[InvestmentType] [varchar](20) NULL,
	[InvestedAmount] [numeric](10, 0) NULL,
	[CurrentValue] [numeric](10, 0) NULL,
	[SchemeOption] [varchar](20) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[MaturityDate] [datetime] NULL,
	[NoShare] [numeric](10, 0) NULL,
	[PurchasePrice] [numeric](10, 2) NULL,
	[CurrentPrice] [numeric](10, 2) NULL,
	[GainLoss] [numeric](10, 2) NULL,
	[IssuingEntity] [varchar](50) NULL,
	[ROI] [numeric](18, 0) NULL,
	[HolderName] [numeric](10, 0) NULL,
	[Remarks] [varchar](50) NULL,
	[EntryDate] [datetime] NULL,
	[EditDate] [datetime] NULL,
	[EditBy] [varchar](30) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

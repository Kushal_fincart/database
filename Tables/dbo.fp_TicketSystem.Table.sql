USE [FP]
GO
/****** Object:  Table [dbo].[fp_TicketSystem]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_TicketSystem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TicketId] [nvarchar](max) NOT NULL,
	[Query] [nvarchar](max) NULL,
	[Attachment] [nvarchar](max) NULL,
	[DepId] [nvarchar](500) NULL,
	[RoleId] [nvarchar](500) NULL,
	[CreatorEmail] [nvarchar](1000) NOT NULL,
	[AssignerRoleId] [nvarchar](500) NULL,
	[AssignByEmail] [nvarchar](1000) NOT NULL,
	[AssignToEmail] [nvarchar](1000) NOT NULL,
	[ReportToEmail] [nvarchar](1000) NULL,
	[UpdatedByEmail] [nvarchar](1000) NULL,
	[Status] [int] NOT NULL,
	[Priority] [int] NOT NULL,
	[OpenDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[Subject] [nvarchar](max) NULL,
	[TAT] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

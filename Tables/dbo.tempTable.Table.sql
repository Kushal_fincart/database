USE [FP]
GO
/****** Object:  Table [dbo].[tempTable]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tempTable](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UUID] [nvarchar](max) NULL,
	[UserID] [nvarchar](200) NULL,
	[Name] [nvarchar](200) NULL,
	[Mobile] [nvarchar](200) NULL,
	[Age] [int] NULL,
	[Gender] [varchar](5) NULL,
	[MaritialStatus] [varchar](5) NULL,
	[AnnualIncome] [nvarchar](200) NULL,
	[MonthlyExpence] [nvarchar](200) NULL,
	[RetirementAge] [int] NULL,
	[LifeExpentency] [int] NULL,
	[Spouse] [bit] NULL,
	[ChildCount] [int] NULL,
	[Emi] [bit] NULL,
	[EmiAmount] [nvarchar](200) NULL,
	[Loans] [bit] NULL,
	[CreatedByEmail] [nvarchar](200) NULL,
	[CreatedByUUID] [nvarchar](max) NULL,
	[CreatedDatetime] [datetime] NULL,
	[UpdatedByEmail] [nvarchar](200) NULL,
	[UpdatedByUUID] [nvarchar](max) NULL,
	[UpdatedDatetime] [datetime] NULL,
	[Status] [int] NULL,
	[GroupLeader] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

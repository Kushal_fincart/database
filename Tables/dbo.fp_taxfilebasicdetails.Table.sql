USE [FP]
GO
/****** Object:  Table [dbo].[fp_taxfilebasicdetails]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_taxfilebasicdetails](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[client_id] [nvarchar](max) NULL,
	[email_id] [nvarchar](max) NULL,
	[password] [nvarchar](max) NULL,
	[first_name] [nvarchar](max) NULL,
	[middle_name] [nvarchar](max) NULL,
	[last_name] [nvarchar](max) NULL,
	[pan] [nvarchar](max) NULL,
	[dob] [nvarchar](max) NULL,
	[mobile_no] [nvarchar](max) NULL,
	[flat_Door] [nvarchar](max) NULL,
	[building] [nvarchar](max) NULL,
	[road_Street] [nvarchar](max) NULL,
	[area_Locality] [nvarchar](max) NULL,
	[city] [nvarchar](max) NULL,
	[state] [nvarchar](max) NULL,
	[pincode] [nvarchar](max) NULL,
	[country] [nvarchar](max) NULL,
	[occupation] [nvarchar](max) NULL,
	[nominee] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[Status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

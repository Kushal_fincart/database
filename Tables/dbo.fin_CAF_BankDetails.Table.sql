USE [FP]
GO
/****** Object:  Table [dbo].[fin_CAF_BankDetails]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_CAF_BankDetails](
	[bankdtls_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[basic_ID] [bigint] NOT NULL,
	[Bank_name] [nvarchar](max) NULL,
	[NameAsPerBank] [nvarchar](max) NULL,
	[Bank_Address] [nvarchar](max) NULL,
	[Acc_No] [nvarchar](max) NULL,
	[Branch] [nvarchar](max) NULL,
	[Bankcity] [nvarchar](max) NULL,
	[Acc_Type] [nvarchar](max) NULL,
	[Micr] [nvarchar](max) NULL,
	[Ifsc] [nvarchar](max) NULL,
	[createdDate] [datetime] NULL,
	[updatedDate] [datetime] NULL,
	[CAF_Bank_Status] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

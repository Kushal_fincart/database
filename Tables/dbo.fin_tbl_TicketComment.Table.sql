USE [FP]
GO
/****** Object:  Table [dbo].[fin_tbl_TicketComment]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_tbl_TicketComment](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TicketId] [nvarchar](10) NULL,
	[Comment] [nvarchar](max) NULL,
	[CommentBy] [nvarchar](100) NULL,
	[CommentDate] [datetime] NULL,
	[Attachment] [nvarchar](100) NULL,
	[IsDelete] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

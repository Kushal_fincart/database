USE [FP]
GO
/****** Object:  Table [dbo].[fp_PlanMaster]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_PlanMaster](
	[Code] [numeric](10, 0) NOT NULL,
	[PlanTitle] [varchar](50) NULL,
	[PlannerCode] [numeric](10, 0) NULL,
	[DateOfPlanning] [datetime] NULL,
	[ReviewDate] [datetime] NULL,
	[ClientCode] [numeric](10, 0) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

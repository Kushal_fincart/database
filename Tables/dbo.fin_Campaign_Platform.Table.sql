USE [FP]
GO
/****** Object:  Table [dbo].[fin_Campaign_Platform]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_Campaign_Platform](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Platform_Name] [nvarchar](max) NULL,
	[Platform_Desc] [nvarchar](max) NULL,
	[StaticURL] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[Status] [bit] NULL,
	[IsDripMktEnable] [bit] NULL,
	[IsAutoAllocate] [bit] NULL,
 CONSTRAINT [PK_fin_Campaign_Platform] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[fin_Campaign_Platform] ADD  CONSTRAINT [Default_status_plat]  DEFAULT ((1)) FOR [Status]
GO

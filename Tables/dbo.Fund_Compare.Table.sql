USE [FP]
GO
/****** Object:  Table [dbo].[Fund_Compare]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Fund_Compare](
	[ExlCode] [int] NOT NULL,
	[Curr_Date] [datetime] NULL,
	[Curr_NAV] [float] NULL,
	[I_Corpus] [int] NULL,
	[Curr_Corpus] [int] NULL,
	[Growth1] [float] NULL,
	[Growth2] [float] NULL,
	[Growth3] [float] NULL,
	[Growth4] [float] NULL,
	[Growth5] [float] NULL,
	[Growth6] [float] NULL,
	[Growth7] [float] NULL,
	[Growth8] [float] NULL,
	[Growth9] [float] NULL,
	[Growth10] [float] NULL,
	[Growth11] [float] NULL,
	[Growth12] [float] NULL,
	[Beta] [float] NULL,
	[SD] [float] NULL,
	[SR] [float] NULL,
	[Rank] [int] NULL,
	[EditDate] [datetime] NULL,
	[D1Rank] [float] NULL,
	[D7Rank] [float] NULL,
	[D15Rank] [float] NULL,
	[D30Rank] [float] NULL,
	[M3Rank] [float] NULL,
	[M6Rank] [float] NULL,
	[Y1Rank] [float] NULL,
	[Y2Rank] [float] NULL,
	[Y3Rank] [float] NULL,
	[Y5Rank] [float] NULL,
	[Y10Rank] [float] NULL,
	[D1Quar] [char](1) NULL,
	[D7Quar] [char](1) NULL,
	[D15Quar] [char](1) NULL,
	[D30Quar] [char](1) NULL,
	[M3Quar] [char](1) NULL,
	[M6Quar] [char](1) NULL,
	[Y1Quar] [char](1) NULL,
	[Y2Quar] [char](1) NULL,
	[Y3Quar] [char](1) NULL,
	[Y5Quar] [char](1) NULL,
	[Y10Quar] [char](1) NULL,
	[Objective] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

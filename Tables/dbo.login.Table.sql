USE [FP]
GO
/****** Object:  Table [dbo].[login]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[login](
	[userid] [varchar](20) NULL,
	[password] [varchar](20) NULL,
	[category] [char](1) NULL,
	[name] [varchar](100) NULL,
	[lname] [varchar](70) NULL,
	[llgn] [varchar](20) NULL,
	[email] [varchar](100) NULL,
	[grp_lead] [varchar](20) NULL,
	[Arn_no] [varchar](16) NULL,
	[auth] [char](1) NULL,
	[limit] [numeric](18, 0) NULL,
	[rmcode] [varchar](15) NULL,
	[sbcode] [varchar](15) NULL,
	[ExpandWatchList] [varchar](1) NULL,
	[foliochoice] [varchar](1) NULL,
	[brokerip] [varchar](16) NULL,
	[website] [varchar](100) NULL,
	[blockclient] [varchar](1) NULL,
	[rmlimit] [numeric](18, 0) NULL,
	[initial_inv] [varchar](20) NULL,
	[portflag] [char](1) NULL,
	[porttime] [datetime] NULL,
	[hni] [char](1) NULL,
	[remarks] [varchar](50) NULL,
	[inv_date] [varchar](12) NULL,
	[start_time] [datetime] NULL,
	[end_time] [datetime] NULL,
	[phoneno] [varchar](15) NULL,
	[createdate] [datetime] NULL,
	[showmf] [char](1) NULL,
	[PURCHCOUNT] [int] NULL,
	[SALECOUNT] [int] NULL,
	[port_show] [char](1) NULL,
	[port_arrange] [char](1) NULL,
	[mobile] [char](1) NULL,
	[lgnflag] [char](1) NULL,
	[P3Flag] [char](1) NULL,
	[BrokerMobile] [char](1) NULL,
	[AskReference] [char](1) NULL,
	[ValidUpTo] [datetime] NULL,
	[OnlineInvest] [varchar](10) NULL,
	[AlternateEmailId] [varchar](100) NULL,
	[dob] [smalldatetime] NULL,
	[Address1] [varchar](150) NULL,
	[Address2] [varchar](150) NULL,
	[Address3] [varchar](150) NULL,
	[City] [varchar](150) NULL,
	[State] [varchar](150) NULL,
	[Pin_No] [varchar](30) NULL,
	[Tel_No] [varchar](60) NULL,
	[MobileNo] [varchar](40) NULL,
	[pan_no] [varchar](15) NULL,
	[ARNNO] [varchar](10) NULL,
	[EUIN] [varchar](10) NULL,
	[UserKEY] [uniqueidentifier] NULL,
	[ResetPwd] [char](1) NULL,
	[TLCode] [varchar](12) NULL,
	[LoginSendOn] [datetime] NULL,
	[sel_model] [numeric](6, 0) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE [FP]
GO
/****** Object:  Table [dbo].[fin_Cart_Transactions_Details]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_Cart_Transactions_Details](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[InvestTxnID] [bigint] NOT NULL,
	[CartId] [nvarchar](max) NULL,
	[goalCode] [nvarchar](max) NULL,
	[fundCode] [nvarchar](max) NULL,
	[schemeCode] [nvarchar](max) NULL,
	[type] [nvarchar](max) NULL,
	[entryDate] [datetime] NULL,
	[dividendOpt] [nvarchar](max) NULL,
	[folioNo] [nvarchar](max) NULL,
	[amount] [nvarchar](max) NULL,
	[billdesk_code] [nvarchar](max) NULL,
	[billamc_code] [nvarchar](max) NULL,
	[scheme_name] [nvarchar](max) NULL,
	[invest_profile] [nvarchar](max) NULL,
	[startDate] [datetime] NULL,
	[endDate] [datetime] NULL,
	[noOfInstallment] [int] NULL,
	[frequency] [nvarchar](max) NULL,
	[bankCode] [nvarchar](max) NULL,
	[accountNo] [nvarchar](max) NULL,
	[mandateId] [nvarchar](max) NULL,
	[firstSipDate] [datetime] NULL,
	[mDate] [nvarchar](max) NULL,
	[minPurchase] [nvarchar](max) NULL,
	[bankName] [nvarchar](max) NULL,
	[bankBranch] [nvarchar](max) NULL,
	[PGRefID] [nvarchar](max) NULL,
	[BankRefN] [nvarchar](max) NULL,
	[BillDeskTxnDate] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

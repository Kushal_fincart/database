USE [FP]
GO
/****** Object:  Table [dbo].[fin_tbl_TicketActivityLog]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_tbl_TicketActivityLog](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TicketId] [nvarchar](10) NULL,
	[ActivityId] [bigint] NULL,
	[ActivityBy] [nvarchar](100) NULL,
	[ActivityDate] [datetime] NULL
) ON [PRIMARY]

GO

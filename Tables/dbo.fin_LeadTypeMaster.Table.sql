USE [FP]
GO
/****** Object:  Table [dbo].[fin_LeadTypeMaster]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_LeadTypeMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[leadTypeId] [int] NULL,
	[Name] [nvarchar](max) NULL,
	[status] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

USE [FP]
GO
/****** Object:  Table [dbo].[fp_ActivityMonitor]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_ActivityMonitor](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[PlannerCode] [numeric](10, 0) NULL,
	[ClientCode] [numeric](10, 0) NULL,
	[Comment] [nvarchar](max) NULL,
	[Activity] [numeric](10, 0) NULL,
	[ActionDate] [datetime] NULL,
	[NextActionDate] [datetime] NULL,
	[AMStatus] [varchar](10) NULL,
	[EnteredBy] [varchar](30) NULL,
	[ActionTime] [varchar](5) NULL,
	[EntryDate] [datetime] NULL,
	[EditDate] [datetime] NULL,
	[EditBy] [varchar](30) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE [FP]
GO
/****** Object:  Table [dbo].[NavExl]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NavExl](
	[ExlCode] [int] NOT NULL,
	[NavDate] [datetime] NOT NULL,
	[Nav] [float] NULL,
	[SysDate] [datetime] NULL
) ON [PRIMARY]

GO

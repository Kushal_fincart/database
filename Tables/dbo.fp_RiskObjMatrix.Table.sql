USE [FP]
GO
/****** Object:  Table [dbo].[fp_RiskObjMatrix]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_RiskObjMatrix](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[RiskCode] [numeric](10, 0) NULL,
	[FromMonth] [numeric](4, 0) NULL,
	[ToMonth] [numeric](4, 0) NULL,
	[Category] [varchar](30) NULL,
	[Bid] [numeric](10, 0) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

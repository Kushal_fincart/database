USE [FP]
GO
/****** Object:  Table [dbo].[fp_GoalMaster]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_GoalMaster](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[GoalName] [varchar](30) NULL,
	[GoalImage] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

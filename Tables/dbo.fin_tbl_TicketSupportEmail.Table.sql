USE [FP]
GO
/****** Object:  Table [dbo].[fin_tbl_TicketSupportEmail]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_tbl_TicketSupportEmail](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DeptCode] [nvarchar](100) NULL,
	[SupportUserID] [nvarchar](100) NULL
) ON [PRIMARY]

GO

USE [FP]
GO
/****** Object:  Table [dbo].[fin_CAF_FATCA]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_CAF_FATCA](
	[fatca_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[basic_ID] [bigint] NOT NULL,
	[Place_of_Birth] [nvarchar](max) NULL,
	[Country_of_Birth] [nvarchar](max) NULL,
	[Pol_Exp_Person] [nvarchar](max) NULL,
	[Country_Tax_Res1] [nvarchar](max) NULL,
	[CountryPayer_Id_no] [nvarchar](max) NULL,
	[Idenif_Type] [nvarchar](max) NULL,
	[Country_Tax_Res2] [nvarchar](max) NULL,
	[CountryPayer_Id_no2] [nvarchar](max) NULL,
	[Idenif_Type2] [nvarchar](max) NULL,
	[Country_Tax_Res3] [nvarchar](max) NULL,
	[CountryPayer_Id_no3] [nvarchar](max) NULL,
	[Idenif_Type3] [nvarchar](max) NULL,
	[Source_Wealth] [nvarchar](max) NULL,
	[CAF_Fatca_Status] [bit] NULL,
	[isTaxOther] [nvarchar](max) NULL,
	[createdDate] [datetime] NULL,
	[updatedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

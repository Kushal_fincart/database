USE [FP]
GO
/****** Object:  Table [dbo].[fin_Exclusive_Company]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_Exclusive_Company](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[userId] [nvarchar](max) NULL,
	[companyCode] [nvarchar](max) NULL,
	[companyName] [nvarchar](max) NULL,
	[status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

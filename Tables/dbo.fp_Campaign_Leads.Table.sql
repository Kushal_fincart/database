USE [FP]
GO
/****** Object:  Table [dbo].[fp_Campaign_Leads]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_Campaign_Leads](
	[CL_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CL_UserId] [nvarchar](max) NULL,
	[CL_Campaign_Key] [nvarchar](max) NULL,
	[CL_CreatedDate] [datetime] NULL,
	[CL_UpdatedDate] [datetime] NULL,
	[CL_Status] [int] NULL,
	[CL_IP] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

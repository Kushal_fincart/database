USE [FP]
GO
/****** Object:  Table [dbo].[fin_InvestProfile]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_InvestProfile](
	[invst_id] [bigint] IDENTITY(1,1) NOT NULL,
	[profileid] [bigint] NULL,
	[FirstApplicant] [nvarchar](max) NULL,
	[SecondApplicant] [nvarchar](max) NULL,
	[ThirdApplicant] [nvarchar](max) NULL,
	[GroupLeader] [nvarchar](max) NULL,
	[FirstPAN] [nvarchar](max) NULL,
	[SecondPAN] [nvarchar](max) NULL,
	[ThirdPAN] [nvarchar](max) NULL,
	[MOH] [nvarchar](max) NULL,
	[Invst_Status] [bit] NULL,
	[createdDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

USE [FP]
GO
/****** Object:  Table [dbo].[fin_Referral]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_Referral](
	[RID] [int] IDENTITY(1,1) NOT NULL,
	[FROMEMAIL] [nvarchar](max) NULL,
	[TONAME] [nvarchar](max) NULL,
	[TOEMAIL] [nvarchar](max) NULL,
	[TOMOBILE] [nvarchar](max) NULL,
	[RCODE] [nvarchar](max) NULL,
	[CREATEDDATETIME] [datetime] NULL,
	[UPDATEDDATETIME] [datetime] NULL,
	[STATUS] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

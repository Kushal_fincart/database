USE [FP]
GO
/****** Object:  Table [dbo].[fin_CAF_BasicDetails]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_CAF_BasicDetails](
	[basic_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[userid] [nvarchar](500) NOT NULL,
	[memberId] [nvarchar](max) NULL,
	[userPass] [nvarchar](500) NULL,
	[groupLeader] [nvarchar](max) NULL,
	[title] [nvarchar](500) NULL,
	[ClientName] [nvarchar](max) NULL,
	[phone] [nvarchar](max) NULL,
	[mobile] [nvarchar](max) NULL,
	[Occupation] [nvarchar](max) NULL,
	[PanNumber] [nvarchar](max) NULL,
	[AadharNumber] [nvarchar](max) NULL,
	[Date_of_Birth] [date] NULL,
	[Guardname] [nvarchar](max) NULL,
	[Guardpan] [nvarchar](max) NULL,
	[Guardpan_DOB] [date] NULL,
	[GuardLocaladd] [nvarchar](max) NULL,
	[account_type] [nvarchar](max) NULL,
	[Annual_Income] [nvarchar](max) NULL,
	[KycStatus] [nvarchar](500) NULL,
	[ReqNominee] [nvarchar](500) NULL,
	[CAF_Basic_Status] [bit] NULL,
	[CAF_Status] [bit] NULL,
	[profilePic] [nvarchar](max) NULL,
	[createdDate] [datetime] NULL,
	[updatedDate] [datetime] NULL,
	[email] [nvarchar](max) NULL,
	[sameAddress] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

USE [FP]
GO
/****** Object:  Table [dbo].[fp_SubsidaryPaymentDetails]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_SubsidaryPaymentDetails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Customer_id] [nvarchar](max) NULL,
	[Email_id] [nvarchar](max) NULL,
	[Amount] [nvarchar](max) NULL,
	[Bank] [nvarchar](max) NULL,
	[Bankid] [nvarchar](max) NULL,
	[Currency] [nvarchar](max) NULL,
	[Fundtype] [nvarchar](max) NULL,
	[Invertortype] [nvarchar](max) NULL,
	[Transactiontype] [nvarchar](max) NULL,
	[Requestdatetime] [nvarchar](max) NULL,
	[Paymentstate] [nvarchar](max) NULL,
	[Authstate] [nvarchar](max) NULL,
	[Requestdata] [nvarchar](max) NULL,
	[Responsedata] [nvarchar](max) NULL,
	[Createddate] [datetime] NULL,
	[Updateddate] [datetime] NULL,
	[Status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

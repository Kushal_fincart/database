USE [FP]
GO
/****** Object:  Table [dbo].[profilelistSheet$]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[profilelistSheet$](
	[ProfileID] [nvarchar](255) NULL,
	[FirstApplicant] [nvarchar](255) NULL,
	[SecondApplicant] [nvarchar](255) NULL,
	[ThirdApplicant] [nvarchar](255) NULL,
	[GroupLeader] [nvarchar](255) NULL,
	[FirstPAN] [nvarchar](255) NULL,
	[SecondPAN] [nvarchar](255) NULL,
	[ThirdPAN] [nvarchar](255) NULL,
	[MOH] [nvarchar](255) NULL
) ON [PRIMARY]

GO

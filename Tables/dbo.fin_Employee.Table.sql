USE [FP]
GO
/****** Object:  Table [dbo].[fin_Employee]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fin_Employee](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EmpCode] [varchar](100) NOT NULL,
	[DepId] [varchar](100) NOT NULL,
	[RoleId] [varchar](100) NOT NULL,
	[ReportToEmpCode] [nvarchar](max) NULL,
	[ReportToEmail] [nvarchar](100) NULL,
	[Name] [nvarchar](500) NULL,
	[UserID] [nvarchar](200) NULL,
	[Pwd] [nvarchar](200) NULL,
	[Address] [nvarchar](500) NULL,
	[City] [nvarchar](100) NULL,
	[Pin] [nvarchar](10) NULL,
	[State] [nvarchar](100) NULL,
	[Country] [nvarchar](100) NULL,
	[Phone] [nvarchar](100) NULL,
	[EmergencyPhone] [nvarchar](100) NULL,
	[OfficeEmail] [nvarchar](100) NULL,
	[PersonalEmail] [nvarchar](100) NULL,
	[DOB] [datetime] NULL,
	[DOJ] [datetime] NULL,
	[DOR] [datetime] NULL,
	[Salary] [nvarchar](max) NULL,
	[BloodGroup] [nvarchar](10) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedByEmail] [nvarchar](1000) NULL,
	[Status] [bit] NULL,
	[ModuleCode] [varchar](50) NULL,
	[SubModuleCode] [varchar](50) NULL,
 CONSTRAINT [pk_employee] PRIMARY KEY CLUSTERED 
(
	[EmpCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[fin_Employee] ADD  CONSTRAINT [df_password]  DEFAULT ('fincart@123') FOR [Pwd]
GO
ALTER TABLE [dbo].[fin_Employee] ADD  CONSTRAINT [df_UpdatedByEmail]  DEFAULT ('Admin@fincart.com') FOR [UpdatedByEmail]
GO
ALTER TABLE [dbo].[fin_Employee] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[fin_Employee]  WITH CHECK ADD  CONSTRAINT [fk_Employee] FOREIGN KEY([DepId])
REFERENCES [dbo].[fin_Departments] ([Code])
GO
ALTER TABLE [dbo].[fin_Employee] CHECK CONSTRAINT [fk_Employee]
GO
ALTER TABLE [dbo].[fin_Employee]  WITH CHECK ADD  CONSTRAINT [fk_employeeRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[fin_EmployeeRole] ([Code])
GO
ALTER TABLE [dbo].[fin_Employee] CHECK CONSTRAINT [fk_employeeRole]
GO

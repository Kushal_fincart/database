USE [FP]
GO
/****** Object:  Table [dbo].[fin_documents]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fin_documents](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userId] [nvarchar](500) NOT NULL,
	[memberId] [nvarchar](200) NOT NULL,
	[fileName] [nvarchar](max) NOT NULL,
	[filePath] [nvarchar](max) NOT NULL,
	[docType] [varchar](200) NULL,
	[createDate] [datetime] NULL,
	[status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

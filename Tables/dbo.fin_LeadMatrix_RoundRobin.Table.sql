USE [FP]
GO
/****** Object:  Table [dbo].[fin_LeadMatrix_RoundRobin]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_LeadMatrix_RoundRobin](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[empCode] [bigint] NULL,
	[updatedDate] [datetime] NULL,
	[status] [bit] NULL
) ON [PRIMARY]

GO

USE [FP]
GO
/****** Object:  Table [dbo].[fin_Questions]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_Questions](
	[QID] [int] IDENTITY(1,1) NOT NULL,
	[QCode] [nvarchar](max) NULL,
	[GCode] [nvarchar](max) NULL,
	[QDesc] [nvarchar](max) NULL,
	[AType] [int] NULL,
	[QCreatedByEmail] [nvarchar](500) NULL,
	[QCreatedDatetime] [datetime] NULL,
	[QUpdatedByEmail] [nvarchar](500) NULL,
	[QUpdatedDatetime] [datetime] NULL,
	[QStatus] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

USE [FP]
GO
/****** Object:  Table [dbo].[fin_App_Version]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_App_Version](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DeviceId] [int] NULL,
	[Version] [nvarchar](20) NULL,
	[Mandatory] [bit] NULL,
	[Msg] [nvarchar](max) NULL,
	[CreatedByEmail] [nvarchar](200) NULL,
	[CreatedDatetime] [datetime] NULL,
	[UpdatedByEmail] [nvarchar](200) NULL,
	[UpdatedDatetime] [datetime] NULL,
	[Status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

USE [FP]
GO
/****** Object:  Table [dbo].[fp_EmailSetting]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_EmailSetting](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Host] [nvarchar](max) NULL,
	[Port] [nvarchar](max) NULL,
	[email] [nvarchar](max) NULL,
	[pass] [nvarchar](max) NULL,
	[email_from] [nvarchar](max) NULL,
	[is_active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

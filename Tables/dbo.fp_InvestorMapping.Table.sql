USE [FP]
GO
/****** Object:  Table [dbo].[fp_InvestorMapping]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_InvestorMapping](
	[Code] [numeric](10, 0) NOT NULL,
	[ClientCode] [numeric](10, 0) NULL,
	[FamilyMemberCode] [numeric](10, 0) NULL,
	[Bid] [varchar](5) NULL,
	[Cid] [varchar](8) NULL,
	[CidName] [varchar](50) NULL,
	[EntryDate] [datetime] NULL,
	[EditDate] [datetime] NULL,
	[EditedBy] [varchar](40) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE [FP]
GO
/****** Object:  Table [dbo].[fin_EKyc]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_EKyc](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[basic_ID] [bigint] NOT NULL,
	[sess_id] [nvarchar](max) NOT NULL,
	[pan] [nvarchar](max) NULL,
	[email] [nvarchar](max) NULL,
	[intermediary_id] [nvarchar](max) NULL,
	[err_code] [nvarchar](max) NULL,
	[err_desc] [nvarchar](max) NULL,
	[kyc_statusCode] [nvarchar](max) NULL,
	[IT_PAN_NAME] [nvarchar](max) NULL,
	[AADHAAR_NAME] [nvarchar](max) NULL,
	[FATHER_NAME] [nvarchar](max) NULL,
	[GENDER] [nvarchar](max) NULL,
	[MARITAL_STATUS] [nvarchar](max) NULL,
	[DOB] [nvarchar](max) NULL,
	[NATIONALITY] [nvarchar](max) NULL,
	[PEKRN] [nvarchar](max) NULL,
	[AADHAAR_NO] [nvarchar](max) NULL,
	[RESIDENTIAL_STATUS] [nvarchar](max) NULL,
	[ADDRESS1] [nvarchar](max) NULL,
	[ADDRESS2] [nvarchar](max) NULL,
	[ADDRESS3] [nvarchar](max) NULL,
	[CITY] [nvarchar](max) NULL,
	[STATE] [nvarchar](max) NULL,
	[PINCODE] [nvarchar](max) NULL,
	[MOBILE_NO] [nvarchar](max) NULL,
	[TELOFF] [nvarchar](max) NULL,
	[TELRES] [nvarchar](max) NULL,
	[TELFAX] [nvarchar](max) NULL,
	[APP_MAIDEN_NAME] [nvarchar](max) NULL,
	[APP_MOTHER_NAME] [nvarchar](max) NULL,
	[APP_OCCU] [nvarchar](max) NULL,
	[ADDRESS_TYPE] [nvarchar](max) NULL,
	[entrydate] [datetime] NOT NULL,
	[updateddate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

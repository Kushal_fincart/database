USE [FP]
GO
/****** Object:  Table [dbo].[fin_GoalType]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_GoalType](
	[GTID] [int] IDENTITY(1,1) NOT NULL,
	[GTName] [nvarchar](500) NULL,
	[GTStatus] [int] NULL
) ON [PRIMARY]

GO

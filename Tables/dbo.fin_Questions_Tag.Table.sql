USE [FP]
GO
/****** Object:  Table [dbo].[fin_Questions_Tag]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_Questions_Tag](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QCode] [nvarchar](max) NULL,
	[GCode] [nvarchar](max) NULL,
	[BrowserId] [nvarchar](500) NULL,
	[BrowserIp] [nvarchar](500) NULL,
	[CreatedByEmail] [nvarchar](500) NULL,
	[CreatedByMobile] [nvarchar](50) NULL,
	[CreatedDatetime] [datetime] NULL,
	[Answer] [nvarchar](max) NULL,
	[Device] [nvarchar](max) NULL,
	[Device_Version] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

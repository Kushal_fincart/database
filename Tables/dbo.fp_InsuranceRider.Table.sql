USE [FP]
GO
/****** Object:  Table [dbo].[fp_InsuranceRider]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_InsuranceRider](
	[Code] [numeric](10, 0) NOT NULL,
	[InsCode] [numeric](10, 0) NULL,
	[RiderType] [numeric](10, 0) NULL,
	[SumAssured] [numeric](10, 0) NULL,
	[EntryDate] [datetime] NULL,
	[EditDate] [datetime] NULL,
	[EditedBy] [varchar](40) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE [FP]
GO
/****** Object:  Table [dbo].[Sheet1$]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sheet1$](
	[UserID] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[Pwd] [varchar](50) NULL,
	[Category] [varchar](20) NULL,
	[ClientStatus] [varchar](5) NULL,
	[Mobile1] [varchar](10) NULL,
	[Mobile2] [varchar](10) NULL,
	[Email1] [varchar](50) NULL,
	[Email2] [varchar](50) NULL,
	[country] [varchar](25) NULL,
	[Source1] [varchar](20) NULL,
	[Source2] [varchar](20) NULL,
	[OpType1] [varchar](10) NULL,
	[OpType2] [varchar](10) NULL,
	[SiteAC] [char](1) NULL,
	[Remarks] [nvarchar](max) NULL,
	[SystemDate] [datetime] NULL,
	[AllocateDate] [datetime] NULL,
	[LeadType] [numeric](10, 0) NULL,
	[plannercode] [numeric](10, 0) NULL,
	[plock] [char](1) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

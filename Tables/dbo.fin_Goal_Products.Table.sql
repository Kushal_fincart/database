USE [FP]
GO
/****** Object:  Table [dbo].[fin_Goal_Products]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_Goal_Products](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GoalCode] [nvarchar](500) NULL,
	[RiskCode] [nvarchar](100) NULL,
	[TimeCode] [nvarchar](100) NULL,
	[AmountCode] [nvarchar](100) NULL,
	[ProductCode] [nvarchar](500) NULL,
	[CreatedBy] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](500) NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[Status] [int] NULL
) ON [PRIMARY]

GO

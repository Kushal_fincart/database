USE [FP]
GO
/****** Object:  Table [dbo].[fin_LeadAllocationHistory]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fin_LeadAllocationHistory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[leadCode] [bigint] NOT NULL,
	[AssignTo] [varchar](max) NULL,
	[AssignBy] [varchar](max) NULL,
	[assignedDate] [datetime] NULL,
	[reason] [nvarchar](max) NULL,
	[status] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

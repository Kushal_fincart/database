USE [FP]
GO
/****** Object:  Table [dbo].[fp_RiskPer]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_RiskPer](
	[Risk] [varchar](30) NULL,
	[Equity] [numeric](6, 2) NULL,
	[Debt] [numeric](6, 2) NULL,
	[Gold] [numeric](6, 2) NULL,
	[Cash] [numeric](6, 2) NULL,
	[ROR] [numeric](6, 2) NULL,
	[SrNo] [numeric](10, 0) NOT NULL,
	[RiskDescription] [varchar](500) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE [FP]
GO
/****** Object:  Table [dbo].[fp_Campaign]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_Campaign](
	[C_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[C_Var] [nvarchar](max) NULL,
	[C_Key] [nvarchar](max) NULL,
	[C_Name] [nvarchar](max) NULL,
	[C_Route] [nvarchar](max) NULL,
	[C_ClientSatusCode] [nvarchar](max) NULL,
	[C_CreatedBy] [nvarchar](max) NULL,
	[C_CreatedByRole] [nvarchar](max) NULL,
	[C_CreatedDate] [datetime] NULL,
	[C_UpdatedDate] [datetime] NULL,
	[C_Status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

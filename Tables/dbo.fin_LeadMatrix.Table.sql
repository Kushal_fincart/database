USE [FP]
GO
/****** Object:  Table [dbo].[fin_LeadMatrix]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_LeadMatrix](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[empCode] [nvarchar](max) NULL,
	[priority] [int] NULL,
	[maxLead] [bigint] NULL,
	[currentLead] [bigint] NULL,
	[Isnext] [bit] NULL,
	[updatedDate] [datetime] NULL,
	[status] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

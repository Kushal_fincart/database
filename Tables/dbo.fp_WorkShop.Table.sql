USE [FP]
GO
/****** Object:  Table [dbo].[fp_WorkShop]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_WorkShop](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[CourseName] [varchar](100) NULL,
	[CourseDate] [datetime] NULL,
	[CourseTime] [varchar](50) NULL,
	[Location] [varchar](100) NULL,
	[Price] [varchar](5) NULL,
	[CourseDescription] [varchar](200) NULL,
	[SpeakerName] [varchar](30) NULL,
	[SpeakerDetails] [varchar](50) NULL,
	[VideoURL1] [varchar](200) NULL,
	[VideoURL2] [varchar](200) NULL,
	[WSStatus] [char](1) NULL,
	[EditDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

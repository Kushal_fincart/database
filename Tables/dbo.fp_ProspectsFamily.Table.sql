USE [FP]
GO
/****** Object:  Table [dbo].[fp_ProspectsFamily]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_ProspectsFamily](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[GroupCode] [numeric](10, 0) NULL,
	[Name] [varchar](50) NULL,
	[Gender] [char](1) NULL,
	[DOB] [datetime] NULL,
	[Mobile1] [varchar](10) NULL,
	[Relation] [numeric](10, 0) NULL,
	[FinancialDependent] [char](1) NULL,
	[HealthHistory] [varchar](50) NULL,
	[Age] [numeric](10, 0) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

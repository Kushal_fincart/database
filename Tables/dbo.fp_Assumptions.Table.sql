USE [FP]
GO
/****** Object:  Table [dbo].[fp_Assumptions]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_Assumptions](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[Name] [varchar](30) NULL,
	[ValuePer] [numeric](6, 2) NULL,
	[SrNo] [numeric](10, 0) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

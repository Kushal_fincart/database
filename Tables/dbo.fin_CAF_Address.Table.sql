USE [FP]
GO
/****** Object:  Table [dbo].[fin_CAF_Address]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_CAF_Address](
	[address_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[basic_ID] [bigint] NOT NULL,
	[prm_Address] [nvarchar](max) NULL,
	[corp_Address] [nvarchar](max) NULL,
	[city] [nvarchar](max) NULL,
	[state] [nvarchar](max) NULL,
	[country] [nvarchar](max) NULL,
	[pincode] [nvarchar](max) NULL,
	[CAF_Address_Status] [bit] NULL,
	[createdDate] [datetime] NULL,
	[updatedDate] [datetime] NULL,
	[corp_city] [nvarchar](max) NULL,
	[corp_state] [nvarchar](max) NULL,
	[corp_country] [nvarchar](max) NULL,
	[corp_pincode] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

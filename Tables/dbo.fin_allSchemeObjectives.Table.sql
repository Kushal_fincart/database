USE [FP]
GO
/****** Object:  Table [dbo].[fin_allSchemeObjectives]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_allSchemeObjectives](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[objective] [nvarchar](max) NULL,
	[objective_Name] [nvarchar](max) NULL,
	[status] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

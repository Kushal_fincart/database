USE [FP]
GO
/****** Object:  Table [dbo].[fin_App_Device]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fin_App_Device](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Device] [varchar](200) NULL,
	[CreatedByEmail] [nvarchar](200) NULL,
	[CreatedDatetime] [datetime] NULL,
	[UpdatedByEmail] [nvarchar](200) NULL,
	[UpdatedDatetime] [datetime] NULL,
	[Status] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE [FP]
GO
/****** Object:  Table [dbo].[fin_allFundSchemes]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_allFundSchemes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[fscode] [nvarchar](max) NULL,
	[scode] [nvarchar](max) NULL,
	[Org_Scheme] [nvarchar](max) NULL,
	[exlcode] [nvarchar](max) NULL,
	[objective] [nvarchar](max) NULL,
	[type] [nvarchar](max) NULL,
	[yr1return] [float] NULL,
	[annualSD1yr] [float] NULL,
	[exitload] [nvarchar](max) NULL,
	[expenseratio] [float] NULL,
	[mininitialinv] [nvarchar](max) NULL,
	[fundmanager] [nvarchar](max) NULL,
	[FundActive] [nvarchar](max) NULL,
	[SchemeActive] [nvarchar](max) NULL,
	[yr3return] [float] NULL,
	[inception] [float] NULL,
	[fcode] [nvarchar](max) NULL,
	[Close_Ended] [nvarchar](max) NULL,
	[incart] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

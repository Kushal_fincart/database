USE [FP]
GO
/****** Object:  Table [dbo].[fp_PaymentDetail]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_PaymentDetail](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[ClientCode] [numeric](10, 0) NULL,
	[PaymentDate] [datetime] NULL,
	[Amount] [numeric](10, 0) NULL,
	[Instrument] [numeric](10, 0) NULL,
	[Drawer] [varchar](30) NULL,
	[Drawee] [varchar](30) NULL,
	[Payer] [varchar](30) NULL,
	[Payee] [varchar](30) NULL,
	[PaymentMode] [varchar](30) NULL,
	[PlannerCode] [numeric](10, 0) NULL,
	[EntryDate] [datetime] NULL,
	[EditDate] [datetime] NULL,
	[EditBy] [varchar](30) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE [FP]
GO
/****** Object:  Table [dbo].[fp_Employee]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_Employee](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EmpCode] [nvarchar](max) NOT NULL,
	[DepId] [nvarchar](500) NULL,
	[RoleId] [nvarchar](500) NULL,
	[ReportToEmpCode] [nvarchar](max) NULL,
	[ReportToEmail] [nvarchar](100) NULL,
	[Name] [nvarchar](500) NULL,
	[UserID] [nvarchar](200) NULL,
	[Pwd] [nvarchar](200) NULL,
	[Address] [nvarchar](500) NULL,
	[City] [nvarchar](100) NULL,
	[Pin] [nvarchar](10) NULL,
	[State] [nvarchar](100) NULL,
	[Country] [nvarchar](100) NULL,
	[Phone] [nvarchar](100) NULL,
	[EmergencyPhone] [nvarchar](100) NULL,
	[OfficeEmail] [nvarchar](100) NULL,
	[PersonalEmail] [nvarchar](100) NULL,
	[DOB] [datetime] NULL,
	[DOJ] [datetime] NULL,
	[DOR] [datetime] NULL,
	[Salary] [nvarchar](max) NULL,
	[BloodGroup] [nvarchar](10) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedByEmail] [nvarchar](1000) NULL,
	[Status] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[fp_Employee] ADD  DEFAULT ((1)) FOR [Status]
GO

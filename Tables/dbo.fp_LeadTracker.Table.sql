USE [FP]
GO
/****** Object:  Table [dbo].[fp_LeadTracker]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_LeadTracker](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TrackerVar] [nvarchar](max) NULL,
	[TrackerVal] [nvarchar](max) NULL,
	[TrackerRoute] [nvarchar](max) NULL,
	[TrackerIp] [nvarchar](max) NULL,
	[TrackingDate] [datetime] NULL,
	[TrackingStatus] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[fp_LeadTracker] ADD  DEFAULT (getdate()) FOR [TrackingDate]
GO

USE [FP]
GO
/****** Object:  Table [dbo].[fin_IPLocation]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_IPLocation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[IPAddress] [nvarchar](max) NULL,
	[CountryCode] [nvarchar](max) NULL,
	[CountryName] [nvarchar](max) NULL,
	[RegionCode] [nvarchar](max) NULL,
	[Region] [nvarchar](max) NULL,
	[CityCode] [nvarchar](max) NULL,
	[CityName] [nvarchar](max) NULL,
	[ISP] [nvarchar](max) NULL,
	[Organization] [nvarchar](max) NULL,
	[Latitude] [nvarchar](max) NULL,
	[Longitude] [nvarchar](max) NULL,
	[ZipCode] [nvarchar](max) NULL,
	[TimeZone] [nvarchar](max) NULL,
	[Status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

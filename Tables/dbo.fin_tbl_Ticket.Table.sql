USE [FP]
GO
/****** Object:  Table [dbo].[fin_tbl_Ticket]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_tbl_Ticket](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TicketId] [nvarchar](10) NULL,
	[Subject] [nvarchar](max) NULL,
	[Query] [nvarchar](max) NULL,
	[Attachment] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[AssignTo] [nvarchar](100) NULL,
	[Status] [nvarchar](10) NULL,
	[Priority] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[TAT] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[LastUpdatedBy] [nvarchar](100) NULL,
 CONSTRAINT [PK_fin_tbl_Tickets] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

USE [FP]
GO
/****** Object:  Table [dbo].[citylist]    Script Date: 29-05-2018 18:40:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[citylist](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[city] [nvarchar](max) NULL,
	[code] [nvarchar](200) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

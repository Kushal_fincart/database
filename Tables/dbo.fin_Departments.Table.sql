USE [FP]
GO
/****** Object:  Table [dbo].[fin_Departments]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fin_Departments](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](100) NOT NULL,
	[Name] [nvarchar](1000) NOT NULL,
	[CreatorEmail] [nvarchar](1000) NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [pk_Departments] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[fin_Departments] ADD  CONSTRAINT [DF_CreatorEmail]  DEFAULT ('Admin@fincart.com') FOR [CreatorEmail]
GO
ALTER TABLE [dbo].[fin_Departments] ADD  CONSTRAINT [DF_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[fin_Departments] ADD  CONSTRAINT [DF_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[fin_Departments] ADD  CONSTRAINT [DF_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

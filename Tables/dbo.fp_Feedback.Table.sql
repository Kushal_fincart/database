USE [FP]
GO
/****** Object:  Table [dbo].[fp_Feedback]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_Feedback](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[ClientCode] [numeric](10, 0) NULL,
	[FName] [varchar](50) NULL,
	[FTitle] [varchar](100) NULL,
	[FDescription] [varchar](500) NULL,
	[FEmail] [varchar](50) NULL,
	[QueryType] [char](10) NULL,
	[EntryDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

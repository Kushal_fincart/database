USE [FP]
GO
/****** Object:  Table [dbo].[fp_FreeSchemeMatrix]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_FreeSchemeMatrix](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[RiskCode] [numeric](10, 0) NULL,
	[FromMonth] [numeric](4, 0) NULL,
	[ToMonth] [numeric](4, 0) NULL,
	[Fcode] [varchar](6) NULL,
	[SCode] [varchar](6) NULL,
	[Exlcode] [varchar](10) NULL,
	[SName] [varchar](100) NULL,
	[Allocation] [numeric](4, 0) NULL,
	[Bid] [numeric](10, 0) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE [FP]
GO
/****** Object:  Table [dbo].[fp_Subscription]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_Subscription](
	[Code] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](50) NULL,
	[SubscribeDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

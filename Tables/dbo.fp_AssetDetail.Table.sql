USE [FP]
GO
/****** Object:  Table [dbo].[fp_AssetDetail]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_AssetDetail](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[AssetCode] [numeric](10, 0) NULL,
	[ClientCode] [numeric](10, 0) NULL,
	[AssetName] [varchar](50) NULL,
	[CurrentValue] [numeric](10, 0) NULL,
	[PurchasePrice] [numeric](10, 0) NULL,
	[AccquisitionDate] [datetime] NULL,
	[FamilyMemberCode] [numeric](10, 0) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

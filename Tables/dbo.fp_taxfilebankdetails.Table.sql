USE [FP]
GO
/****** Object:  Table [dbo].[fp_taxfilebankdetails]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_taxfilebankdetails](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[email_id] [nvarchar](max) NULL,
	[primary_account] [bit] NULL,
	[account_id] [nvarchar](max) NULL,
	[bank_name] [nvarchar](max) NULL,
	[ifsc] [nvarchar](max) NULL,
	[account_number] [nvarchar](max) NULL,
	[account_type] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[Status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

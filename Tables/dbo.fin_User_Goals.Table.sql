USE [FP]
GO
/****** Object:  Table [dbo].[fin_User_Goals]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_User_Goals](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UUID] [nvarchar](max) NULL,
	[UserID] [nvarchar](200) NULL,
	[RelationID] [nvarchar](max) NULL,
	[GoalCode] [nvarchar](5) NULL,
	[TypeCode] [nvarchar](5) NULL,
	[People] [nvarchar](5) NULL,
	[Location] [nvarchar](100) NULL,
	[Place] [nvarchar](100) NULL,
	[MonthlyAmount] [nvarchar](1000) NULL,
	[Amount] [nvarchar](1000) NULL,
	[Start] [nvarchar](5) NULL,
	[End] [nvarchar](5) NULL,
	[Duration] [nvarchar](5) NULL,
	[Risk] [nvarchar](5) NULL,
	[InvestmentType] [nvarchar](5) NULL,
	[GetAmount] [nvarchar](1000) NULL,
	[InvestAmount] [nvarchar](1000) NULL,
	[ProductList] [nvarchar](100) NULL,
	[ActivatedDatetime] [datetime] NULL,
	[CompletedDatetime] [datetime] NULL,
	[CreatedByEmail] [nvarchar](200) NULL,
	[CreatedByUUID] [nvarchar](max) NULL,
	[CreatedDatetime] [datetime] NULL,
	[UpdatedByEmail] [nvarchar](200) NULL,
	[UpdatedByUUID] [nvarchar](max) NULL,
	[UpdatedDatetime] [datetime] NULL,
	[Status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

USE [FP]
GO
/****** Object:  Table [dbo].[fp_Income]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_Income](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[ClientCode] [numeric](10, 0) NULL,
	[Source] [numeric](10, 0) NULL,
	[IncomeYearly] [numeric](18, 0) NULL,
	[IncomeMonthly] [numeric](18, 0) NULL,
	[IncomePeriod] [varchar](20) NULL,
	[MiscIncomeName] [varchar](30) NULL,
	[EntryDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

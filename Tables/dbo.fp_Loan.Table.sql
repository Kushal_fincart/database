USE [FP]
GO
/****** Object:  Table [dbo].[fp_Loan]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_Loan](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[ClientCode] [numeric](10, 0) NULL,
	[FamilyMemberCode] [numeric](10, 0) NULL,
	[Lender] [varchar](40) NULL,
	[LoanInsured] [char](1) NULL,
	[LoanAmount] [numeric](10, 0) NULL,
	[TypeofInterest] [varchar](40) NULL,
	[DisbursementDate] [datetime] NULL,
	[RepaymentStartDate] [datetime] NULL,
	[PreclosureAmount] [numeric](10, 0) NULL,
	[PreclosureYear] [datetime] NULL,
	[PreclosureOption] [varchar](40) NULL,
	[SelfOccupied] [char](1) NULL,
	[AdvancePaymentPaid] [numeric](10, 0) NULL,
	[Remarks] [varchar](200) NULL,
	[LoanTaken] [numeric](10, 0) NULL,
	[OriginalAmount] [numeric](10, 0) NULL,
	[RateofInterest] [numeric](10, 0) NULL,
	[LoanType] [varchar](10) NULL,
	[EMI] [numeric](10, 0) NULL,
	[LoanTenure] [numeric](10, 0) NULL,
	[OutStandingBalance] [numeric](10, 0) NULL,
	[StartDate] [datetime] NULL,
	[EntryDate] [datetime] NULL,
	[EditDate] [datetime] NULL,
	[EditedBy] [varchar](40) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE [FP]
GO
/****** Object:  Table [dbo].[fin_EmployeeRole]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fin_EmployeeRole](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](100) NOT NULL,
	[DepCode] [varchar](100) NOT NULL,
	[Role] [nvarchar](1000) NOT NULL,
	[Level] [int] NOT NULL,
	[CreatorEmail] [nvarchar](1000) NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [pk_EmployeeRole] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[fin_EmployeeRole] ADD  CONSTRAINT [df_creatorEmailId]  DEFAULT ('Admin@fincart.com') FOR [CreatorEmail]
GO
ALTER TABLE [dbo].[fin_EmployeeRole] ADD  CONSTRAINT [df_ActiveStatus]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[fin_EmployeeRole] ADD  CONSTRAINT [DF_RoleCreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[fin_EmployeeRole] ADD  CONSTRAINT [DF_RoleUpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[fin_EmployeeRole]  WITH CHECK ADD  CONSTRAINT [fk_Department] FOREIGN KEY([DepCode])
REFERENCES [dbo].[fin_Departments] ([Code])
GO
ALTER TABLE [dbo].[fin_EmployeeRole] CHECK CONSTRAINT [fk_Department]
GO

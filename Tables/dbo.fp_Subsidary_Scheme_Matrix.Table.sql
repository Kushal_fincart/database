USE [FP]
GO
/****** Object:  Table [dbo].[fp_Subsidary_Scheme_Matrix]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_Subsidary_Scheme_Matrix](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NAME] [nvarchar](500) NULL,
	[CODE] [nvarchar](100) NULL,
	[CREATEDDATE] [datetime] NOT NULL,
	[STATUS] [int] NULL
) ON [PRIMARY]

GO

USE [FP]
GO
/****** Object:  Table [dbo].[fp_WorkShopRegister]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_WorkShopRegister](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[FullName] [varchar](50) NULL,
	[City] [varchar](20) NULL,
	[CountryCode] [varchar](5) NULL,
	[Email] [varchar](50) NULL,
	[Mobile] [varchar](10) NULL,
	[CourseCode] [numeric](10, 0) NULL,
	[InfoFrom] [varchar](20) NULL,
	[InfoCancellation] [varchar](20) NULL,
	[RegisterDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

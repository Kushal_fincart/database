USE [FP]
GO
/****** Object:  Table [dbo].[fin_scheme_validDates]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_scheme_validDates](
	[scode] [nvarchar](max) NOT NULL,
	[Day] [nvarchar](max) NULL,
	[Date] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

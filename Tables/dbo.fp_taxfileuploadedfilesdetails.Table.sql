USE [FP]
GO
/****** Object:  Table [dbo].[fp_taxfileuploadedfilesdetails]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_taxfileuploadedfilesdetails](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[email_id] [nvarchar](max) NULL,
	[isPassword] [bit] NULL,
	[addcmt] [bit] NULL,
	[file_ID] [nvarchar](max) NULL,
	[fileStream] [nvarchar](max) NULL,
	[fileName] [nvarchar](max) NULL,
	[fileType] [nvarchar](max) NULL,
	[file_MIME] [nvarchar](max) NULL,
	[fileID_Encrypted] [nvarchar](max) NULL,
	[errorMessage] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[Status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

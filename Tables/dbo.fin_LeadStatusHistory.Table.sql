USE [FP]
GO
/****** Object:  Table [dbo].[fin_LeadStatusHistory]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_LeadStatusHistory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[leadCode] [bigint] NOT NULL,
	[leadType] [bigint] NULL,
	[empCode] [nvarchar](max) NULL,
	[StatusDate] [datetime] NULL,
	[status] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

USE [FP]
GO
/****** Object:  Table [dbo].[fp_Departments]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_Departments](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](1000) NOT NULL,
	[Name] [nvarchar](1000) NOT NULL,
	[CreatorEmail] [nvarchar](1000) NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL
) ON [PRIMARY]

GO

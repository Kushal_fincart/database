USE [FP]
GO
/****** Object:  Table [dbo].[fp_Insurance]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_Insurance](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[ClientCode] [numeric](10, 0) NULL,
	[FamilyMemberCode] [numeric](10, 0) NULL,
	[Product] [char](3) NULL,
	[InsType] [numeric](10, 0) NULL,
	[PlanType] [numeric](10, 0) NULL,
	[PolicyIssuer] [varchar](50) NULL,
	[PolicyName] [varchar](50) NULL,
	[PolicyNumber] [varchar](20) NULL,
	[NomineeName] [numeric](10, 0) NULL,
	[ProposerName] [numeric](10, 0) NULL,
	[SumAssured] [numeric](15, 0) NULL,
	[CurrentValue] [numeric](15, 0) NULL,
	[PolicyStartDate] [datetime] NULL,
	[FundValue] [numeric](10, 2) NULL,
	[AsOnDate] [datetime] NULL,
	[MaturityDate] [datetime] NULL,
	[PremiumMode] [char](15) NULL,
	[Premium] [numeric](10, 0) NULL,
	[PolicyTerm] [numeric](10, 0) NULL,
	[PremiumPaymentTerm] [numeric](10, 0) NULL,
	[Remarks] [varchar](500) NULL,
	[EntryDate] [datetime] NULL,
	[EditDate] [datetime] NULL,
	[EditedBy] [varchar](40) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

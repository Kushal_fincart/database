USE [FP]
GO
/****** Object:  Table [dbo].[nileshdata]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[nileshdata](
	[ClientName] [varchar](50) NULL,
	[comment] [nvarchar](max) NULL,
	[UserId] [varchar](50) NULL,
	[mobile] [varchar](10) NULL,
	[Campaign] [nvarchar](max) NULL,
	[LeadType] [nvarchar](max) NULL,
	[lastActivityDate] [datetime] NULL,
	[createdDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

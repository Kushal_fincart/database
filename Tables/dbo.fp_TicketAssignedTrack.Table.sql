USE [FP]
GO
/****** Object:  Table [dbo].[fp_TicketAssignedTrack]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_TicketAssignedTrack](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TicketId] [nvarchar](max) NOT NULL,
	[AssignToEmail] [nvarchar](1000) NOT NULL,
	[AssignByEmail] [nvarchar](1000) NOT NULL,
	[ReportToEmail] [nvarchar](1000) NULL,
	[AssignedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

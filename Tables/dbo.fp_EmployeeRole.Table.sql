USE [FP]
GO
/****** Object:  Table [dbo].[fp_EmployeeRole]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_EmployeeRole](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](1000) NOT NULL,
	[DepCode] [nvarchar](1000) NOT NULL,
	[Role] [nvarchar](1000) NOT NULL,
	[Level] [int] NOT NULL,
	[CreatorEmail] [nvarchar](1000) NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL
) ON [PRIMARY]

GO

USE [FP]
GO
/****** Object:  Table [dbo].[fin_Other_Transactions_Details]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_Other_Transactions_Details](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[OtherTxnID] [bigint] NOT NULL,
	[userId] [nvarchar](max) NOT NULL,
	[memberId] [nvarchar](max) NOT NULL,
	[profileId] [nvarchar](max) NULL,
	[goalCode] [nvarchar](max) NULL,
	[fundCode] [nvarchar](max) NULL,
	[schemeCode] [nvarchar](max) NULL,
	[toSchemeCode] [nvarchar](max) NULL,
	[scheme_name] [nvarchar](max) NULL,
	[toscheme_name] [nvarchar](max) NULL,
	[invest_profile] [nvarchar](max) NULL,
	[entryDate] [datetime] NULL,
	[units] [nvarchar](max) NULL,
	[divOption] [nvarchar](max) NULL,
	[folioNo] [nvarchar](max) NULL,
	[navDate] [datetime] NULL,
	[startDate] [datetime] NULL,
	[endDate] [datetime] NULL,
	[noOfInstallment] [int] NULL,
	[amount] [nvarchar](max) NULL,
	[frequency] [nvarchar](max) NULL,
	[type] [nvarchar](max) NULL,
	[bank] [nvarchar](max) NULL,
	[branch] [nvarchar](max) NULL,
	[accountNo] [nvarchar](max) NULL,
	[mandateId] [nvarchar](max) NULL,
	[mDate] [nvarchar](max) NULL,
	[UserGoalId] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

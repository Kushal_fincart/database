USE [FP]
GO
/****** Object:  Table [dbo].[goalDataSheet]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[goalDataSheet](
	[GoalName] [nvarchar](255) NULL,
	[OtherGoalName] [nvarchar](255) NULL,
	[TimeHorizon] [nvarchar](255) NULL,
	[GetAmount] [nvarchar](255) NULL,
	[InvestAmount] [nvarchar](255) NULL,
	[FundName] [nvarchar](255) NULL,
	[hideFundCode] [nvarchar](255) NULL,
	[SchemeName] [nvarchar](255) NULL,
	[SchemeCode] [nvarchar](255) NULL,
	[TotalCurrentValue] [nvarchar](255) NULL,
	[SIPAmount] [nvarchar](255) NULL,
	[SIPDate] [nvarchar](255) NULL,
	[SIPStartDate] [datetime] NULL,
	[FolioNo] [nvarchar](255) NULL,
	[ClientEmail] [nvarchar](255) NULL,
	[ClientName] [nvarchar](255) NULL,
	[ChildName1] [nvarchar](255) NULL,
	[ChildName2] [nvarchar](255) NULL,
	[ChildName3] [nvarchar](255) NULL,
	[Child1Age] [nvarchar](255) NULL,
	[Child2Age] [nvarchar](255) NULL,
	[Child3Age] [nvarchar](255) NULL,
	[Child1Gender] [nvarchar](255) NULL,
	[Child2Gender] [nvarchar](255) NULL,
	[Child3Gender] [nvarchar](255) NULL,
	[RetirementAge ] [nvarchar](255) NULL
) ON [PRIMARY]

GO

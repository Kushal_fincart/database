USE [FP]
GO
/****** Object:  Table [dbo].[fp_Parameter]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_Parameter](
	[CompanyName] [varchar](50) NULL,
	[ContactName] [varchar](50) NULL,
	[UserID] [varchar](50) NULL,
	[Pwd] [varchar](20) NULL,
	[Address] [varchar](100) NULL,
	[City] [varchar](25) NULL,
	[Pin] [varchar](6) NULL,
	[State] [varchar](25) NULL,
	[Mobile1] [varchar](10) NULL,
	[Mobile2] [varchar](10) NULL,
	[Email] [varchar](100) NULL,
	[AlternateEmail] [varchar](100) NULL,
	[Website] [varchar](150) NULL,
	[FPLink] [varchar](150) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

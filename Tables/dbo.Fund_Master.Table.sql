USE [FP]
GO
/****** Object:  Table [dbo].[Fund_Master]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Fund_Master](
	[CODE] [varchar](6) NULL,
	[Name] [varchar](50) NULL,
	[EditDate] [datetime] NULL,
	[Active] [char](1) NULL,
	[Manager] [varchar](30) NULL,
	[FactSheet_URL] [varchar](255) NULL,
	[ViewStatus] [char](1) NULL,
	[CIN] [varchar](50) NULL,
	[AssetName] [varchar](150) NULL,
	[Address1] [varchar](150) NULL,
	[Address2] [varchar](150) NULL,
	[Address3] [varchar](150) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[PIN] [varchar](10) NULL,
	[TollFreeNo1] [varchar](25) NULL,
	[TollFreeNo2] [varchar](25) NULL,
	[TelNo1] [varchar](25) NULL,
	[TelNo2] [varchar](25) NULL,
	[FaxNo1] [varchar](25) NULL,
	[FaxNo2] [varchar](25) NULL,
	[Email1] [varchar](100) NULL,
	[Email2] [varchar](100) NULL,
	[WebSite] [varchar](100) NULL,
	[Registrar] [varchar](3) NULL,
	[NSEAMCCODE] [varchar](10) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

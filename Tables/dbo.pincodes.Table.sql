USE [FP]
GO
/****** Object:  Table [dbo].[pincodes]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pincodes](
	[PostOfficeName] [nvarchar](255) NULL,
	[Pincode] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[District] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL
) ON [PRIMARY]

GO

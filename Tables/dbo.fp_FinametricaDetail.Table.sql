USE [FP]
GO
/****** Object:  Table [dbo].[fp_FinametricaDetail]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fp_FinametricaDetail](
	[Code] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[ClientCode] [numeric](10, 0) NULL,
	[FileName] [varchar](100) NULL,
	[PlannerCode] [numeric](10, 0) NULL,
	[RiskStatus] [numeric](10, 0) NULL,
	[RiskFromDate] [datetime] NULL,
	[RiskToDate] [datetime] NULL,
	[EntryDate] [datetime] NULL,
	[EditDate] [datetime] NULL,
	[EditBy] [varchar](30) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

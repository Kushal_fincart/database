USE [FP]
GO
/****** Object:  Table [dbo].[fin_AnswerType]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_AnswerType](
	[ATID] [int] IDENTITY(1,1) NOT NULL,
	[ATName] [nvarchar](500) NULL,
	[ATDesc] [nvarchar](max) NULL,
	[ATCreatedByEmail] [nvarchar](500) NULL,
	[ATCreatedDatetime] [datetime] NULL,
	[ATUpdatedByEmail] [nvarchar](500) NULL,
	[ATUpdatedDatetime] [datetime] NULL,
	[ATStatus] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

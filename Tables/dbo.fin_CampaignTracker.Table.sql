USE [FP]
GO
/****** Object:  Table [dbo].[fin_CampaignTracker]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fin_CampaignTracker](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](max) NULL,
	[TrackerCode] [nvarchar](max) NULL,
	[UUID] [nvarchar](max) NULL,
	[IpAddress] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

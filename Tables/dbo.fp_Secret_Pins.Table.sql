USE [FP]
GO
/****** Object:  Table [dbo].[fp_Secret_Pins]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_Secret_Pins](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Pin] [nvarchar](max) NULL,
	[PinFor] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[Status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

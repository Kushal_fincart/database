USE [FP]
GO
/****** Object:  Table [dbo].[fp_TicketComments]    Script Date: 29-05-2018 18:40:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fp_TicketComments](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TicketId] [nvarchar](max) NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[CommentorName] [nvarchar](1000) NOT NULL,
	[CommentorEmail] [nvarchar](1000) NOT NULL,
	[Attachment] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[IsDeleted] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

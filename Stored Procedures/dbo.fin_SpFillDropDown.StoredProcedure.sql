USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_SpFillDropDown]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[fin_SpFillDropDown]
@UserID [nvarchar](500)=null,
@AssignUserID [nvarchar](500)=null,
@DepId [nvarchar](500)=null,
@RoleId [nvarchar](500)=null,
@Action[varchar](100)
as
begin

if(@Action='DEPARTMENT')
begin
select * from fin_Departments where Id in (1,2,3)
end

if(@Action='ROLE')
begin
declare @lvl int 
Set @lvl=(select Level from fin_EmployeeRole where Code=@RoleId)
select * from fin_EmployeeRole where DepCode=@DepId and Status=1 and Level>=@lvl
end

if(@Action='ROLENOTBYLEVEL')
begin
select * from fin_EmployeeRole where DepCode=@DepId and Status=1
end

if(@Action='EMPLOYEE')
begin
select * from fin_Employee where roleid=@RoleId and UserID!=@UserID
end

if(@Action='REPORTING')
begin
declare @lvl1 int 
Set @lvl1=(select Level from fin_EmployeeRole where Code=(select roleid from fin_Employee where UserID=@AssignUserID and Status=1))
select * from fin_Employee where roleid in (select code from fin_EmployeeRole where DepCode=@DepId and Level<@lvl1)
end
if(@Action='NEWREPORTING')
begin

declare @Level int 
set @Level=(select Level from fin_EmployeeRole WHERE Status=1 and Code=@RoleId and DepCode=@DepId)
select emp.EmpCode,emp.Name,empRole.Level from fin_Employee emp inner join fin_EmployeeRole empRole on emp.RoleId=empRole.Code where emp.DepId=@DepId and emp.Status=1 and empRole.Level<@Level;
end

end



GO

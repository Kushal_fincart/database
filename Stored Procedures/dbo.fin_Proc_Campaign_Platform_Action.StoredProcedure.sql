USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_Campaign_Platform_Action]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO











CREATE PROCEDURE [dbo].[fin_Proc_Campaign_Platform_Action] @id BIGINT =null

	,@platform_name NVARCHAR(max)=null

	,@platform_desc NVARCHAR(max)=null

	,@staticURL NVARCHAR(max)=null

	,@status VARCHAR(5)=1

	,@Action NVARCHAR(50)=null

	,@EmpCode NVARCHAR(max)=null
	,@IsDripMktEnable VARCHAR(5)=1
	,@IsAutoAllocate bit=1
	,@errormessage INT = NULL OUTPUT

AS

BEGIN

	

	IF (@Action = 'INSERT')

	BEGIN

		INSERT INTO fin_campaign_platform (

			platform_name

			,platform_desc,staticurl

			,createdby

			,createddate,Status,IsDripMktEnable,UpdatedBy,UpdatedDate,IsAutoAllocate

			)

		VALUES (

			rtrim(ltrim(@platform_name))

			,rtrim(ltrim(@platform_desc)),rtrim(ltrim(@staticURL))

			,rtrim(ltrim(@EmpCode))

			,getdate(),@status,@IsDripMktEnable,rtrim(ltrim(@EmpCode)),GETDATE(),@IsAutoAllocate

			)

	END

	ELSE IF (@Action = 'UPDATE')

	BEGIN

		UPDATE fin_campaign_platform

		SET platform_name = rtrim(ltrim(@platform_name)),platform_desc=rtrim(ltrim(@platform_desc)),staticURL=rtrim(ltrim(@staticURL))

			,updatedby = rtrim(ltrim(@EmpCode))

			,updateddate = GETDATE(),Status=@status,IsDripMktEnable=@IsDripMktEnable,IsAutoAllocate=@IsAutoAllocate

		WHERE id = @id



		

	END

	ELSE IF EXISTS (
				SELECT *
				FROM fin_Mapping_Camp_Plat
				WHERE Platform_Id = @id
				)
				Begin
				SET @errormessage = 0
				end
	else
	BEGIN

		DELETE

		FROM fin_campaign_platform

		WHERE id = @id



		SET @errormessage = 1

	END

END






GO

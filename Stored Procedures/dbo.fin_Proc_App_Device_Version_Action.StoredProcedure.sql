USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_App_Device_Version_Action]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[fin_Proc_App_Device_Version_Action] @Id BIGINT = NULL
	,@DeviceId INT = 0
	,@Device [varchar] (200) = NULL
	,@Version [nvarchar] (20) = NULL
	,@Mandatory BIT = 0
	,@Msg [nvarchar] (max)=NULL
	,@CreatedByEmail [nvarchar] (200) = NULL
	,@UpdatedByEmail [nvarchar] (200) = NULL
	,@Status INT = 1
	,@Action [nvarchar] (max)
AS
BEGIN
	--- DML FOR APP DEVICE MASTER---START----
	IF (@Action = 'INSERT_DEVICE')
	BEGIN
		IF NOT EXISTS (
				SELECT *
				FROM fin_App_Device
				WHERE Device = @Device
				)
		BEGIN
			INSERT INTO fin_App_Device (
				Device
				,CreatedByEmail
				,CreatedDatetime
				,UpdatedByEmail
				,UpdatedDatetime
				,[Status]
				)
			VALUES (
				rtrim(ltrim(@Device))
				,rtrim(ltrim(@CreatedByEmail))
				,getdate()
				,rtrim(ltrim(@UpdatedByEmail))
				,getdate()
				,@Status
				)
		END
	END

	IF (@Action = 'UPDATE_DEVICE')
	BEGIN
		UPDATE fin_App_Device
		SET Device = rtrim(ltrim(@Device))
			,UpdatedByEmail = rtrim(ltrim(@UpdatedByEmail))
			,UpdatedDatetime = getdate()
			,[Status] = @Status
		WHERE id = @Id
	END

	IF (@Action = 'SELECT_DEVICE_ALL')
	BEGIN
		SELECT [Id]
			,rtrim(ltrim([Device])) AS [Device]
			,rtrim(ltrim([CreatedByEmail])) AS [CreatedByEmail]
			,[CreatedDatetime]
			,rtrim(ltrim([UpdatedByEmail])) AS [UpdatedByEmail]
			,[UpdatedDatetime]
			,CASE 
				WHEN isnull(STATUS, 0) = 0
					THEN 'Off'
				ELSE 'On'
				END AS STATUS
		FROM fin_App_Device
	END

	IF (@Action = 'SELECT_DEVICE_BYID')
	BEGIN
		SELECT [Id]
			,rtrim(ltrim([Device])) AS [Device]
			,rtrim(ltrim([CreatedByEmail])) AS [CreatedByEmail]
			,[CreatedDatetime]
			,rtrim(ltrim([UpdatedByEmail])) AS [UpdatedByEmail]
			,[UpdatedDatetime]
			,CASE 
				WHEN isnull(STATUS, 0) = 0
					THEN 'Off'
				ELSE 'On'
				END AS STATUS
		FROM fin_App_Device
		WHERE id = @Id
	END

	IF (@Action = 'DELETE_DEVICE')
	BEGIN
		DELETE
		FROM fin_App_Device
		WHERE id = @Id
	END

	--- DML FOR APP DEVICE MASTER---END----
	--- DML FOR APP VERSION UPDATE---START---
	IF (@Action = 'INSERT_APP_VERSION')
	BEGIN
		IF NOT EXISTS (
				SELECT *
				FROM fin_App_Version
				WHERE rtrim(ltrim(DeviceId)) = rtrim(ltrim(@DeviceId))
					AND rtrim(ltrim([Version])) = rtrim(ltrim(@Version))
				)
		BEGIN
			INSERT INTO fin_App_Version (
				DeviceId
				,[Version]
				,Mandatory
				,Msg
				,CreatedByEmail
				,CreatedDatetime
				,UpdatedByEmail
				,UpdatedDatetime
				,[Status]
				)
			VALUES (
				rtrim(ltrim(@DeviceId))
				,rtrim(ltrim(@Version))
				,@Mandatory
				,rtrim(ltrim(@Msg))
				,rtrim(ltrim(@CreatedByEmail))
				,getdate()
				,rtrim(ltrim(@UpdatedByEmail))
				,getdate()
				,@Status
				)
		END
	END

	IF (@Action = 'UPDATE_APP_VERSION')
	BEGIN
		UPDATE fin_App_Version
		SET DeviceId = rtrim(ltrim(@DeviceId))
			,[Version] = rtrim(ltrim(@Version))
			,Mandatory = @Mandatory
			,Msg=rtrim(ltrim(@Msg))
			,UpdatedByEmail = rtrim(ltrim(@UpdatedByEmail))
			,UpdatedDatetime = getdate()
			,[Status] = @Status
		WHERE id = @Id
	END

	IF (@Action = 'SELECT_APP_VERSION_ALL')
	BEGIN
		SELECT AV.Id
			,rtrim(ltrim(AV.DeviceId)) AS DeviceId
			,rtrim(ltrim(AD.Device)) AS Device
			,rtrim(ltrim(AV.Version)) AS Version
			,AV.Mandatory
			,rtrim(ltrim(AV.Msg)) AS Msg
			,rtrim(ltrim(AV.CreatedByEmail)) AS CreatedByEmail
			,AV.CreatedDatetime
			,rtrim(ltrim(AV.UpdatedByEmail)) AS UpdatedByEmail
			,AV.UpdatedDatetime
			,CASE 
				WHEN isnull(av.STATUS, 0) = 0
					THEN 'Off'
				ELSE 'On'
				END AS STATUS
		FROM fin_App_Device AS AD
		INNER JOIN fin_App_Version AS AV ON AD.Id = AV.Id
	END

	IF (@Action = 'SELECT_APP_VERSION_BYID')
	BEGIN
		SELECT AV.Id
			,rtrim(ltrim(AV.DeviceId)) AS DeviceId
			,rtrim(ltrim(AD.Device)) AS Device
			,rtrim(ltrim(AV.Version)) AS Version
			,AV.Mandatory
			,rtrim(ltrim(AV.Msg)) AS Msg
			,rtrim(ltrim(AV.CreatedByEmail)) AS CreatedByEmail
			,AV.CreatedDatetime
			,rtrim(ltrim(AV.UpdatedByEmail)) AS UpdatedByEmail
			,AV.UpdatedDatetime
			,CASE 
				WHEN isnull(av.STATUS, 0) = 0
					THEN 'Off'
				ELSE 'On'
				END AS STATUS
		FROM fin_App_Device AS AD
		INNER JOIN fin_App_Version AS AV ON AD.Id = AV.Id
		WHERE AV.id = @Id
	END

	IF (@Action = 'SELECT_APP_VERSION_BY_VERSION_DEVICEID')
	BEGIN
		DECLARE @curr_version [nvarchar] (20)
			,@curr_deviceid [int]

		SET @curr_version = REPLACE(@Version, '.', '')
		SET @curr_deviceid = @DeviceId

		SELECT *
		FROM fin_App_Version
		WHERE rtrim(ltrim(DeviceId)) = rtrim(ltrim(@curr_deviceid))
			AND CONVERT(INT, REPLACE([Version], '.', '')) > CONVERT(INT, @curr_version)
	END

	IF (@Action = 'DELETE_APP_VERSION')
	BEGIN
		DELETE
		FROM fin_App_Version
		WHERE id = @Id
	END
			--- DML FOR APP VERSION UPDATE---END---
END

GO

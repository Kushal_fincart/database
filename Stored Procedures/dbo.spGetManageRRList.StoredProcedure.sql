USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[spGetManageRRList]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--[spGetManageRRList] AllList


CREATE PROCEDURE [dbo].[spGetManageRRList] @Action VARCHAR(30)

	,@RoleID int

AS

BEGIN

	IF (@Action = 'AllList' and @RoleID=0)

	BEGIN

		SELECT a.Id,rtrim(ltrim(a.EmpCode)) AS EmpCode

			,rtrim(ltrim(a.Name)) AS Name

			,isnull(b.maxLead, 0) AS MaxLead

			,CASE 

				WHEN isnull(b.STATUS, 0) = 0

					THEN 'Off'

				ELSE 'On'

				END AS Status

			,isnull(b.priority, '') AS Priority

		FROM fin_Employee a

		LEFT JOIN fin_LeadMatrix b ON rtrim(ltrim(a.EmpCode)) = rtrim(ltrim(b.empCode))

	END
	else
	begin
	SELECT a.Id,rtrim(ltrim(a.EmpCode)) AS EmpCode

			,rtrim(ltrim(a.Name)) AS Name

			,isnull(b.maxLead, 0) AS MaxLead

			,CASE 

				WHEN isnull(b.STATUS, 0) = 0

					THEN 'Off'

				ELSE 'On'

				END AS Status

			,isnull(b.priority, '') AS Priority

		FROM fin_Employee a

		LEFT JOIN fin_LeadMatrix b ON rtrim(ltrim(a.EmpCode)) = rtrim(ltrim(b.empCode)) 
		where rtrim(ltrim(a.Id))=@RoleID
	end

END


GO

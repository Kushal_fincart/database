USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_trackerDomain_Action]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[fin_Proc_trackerDomain_Action] @id BIGINT =null
	,@domainname NVARCHAR(max)=null
	,@staticname NVARCHAR(max)=null
	,@status VARCHAR(5)=1
	,@Action NVARCHAR(50)=null
	,@EmpCode NVARCHAR(max)=null
AS
BEGIN
	

	IF (@Action = 'INSERT')
	BEGIN
		INSERT INTO fin_tracker_domain (
			domain_name
			,static_name
			,createdby
			,createddate,status,UpdatedBy,UpdatedDate
			)
		VALUES (
			rtrim(ltrim(@domainname))
			,rtrim(ltrim(@staticname))
			,rtrim(ltrim(@EmpCode))
			,getdate(),@status,rtrim(ltrim(@EmpCode)),GETDATE()
			)


	END
	ELSE IF (@Action = 'UPDATE')
	BEGIN
		UPDATE fin_tracker_domain
		SET domain_name = rtrim(ltrim(@domainname)),static_name=rtrim(ltrim(@staticname))
			,updatedby = rtrim(ltrim(@EmpCode))
			,updateddate = GETDATE(),status=@status
		WHERE id = @id

		SELECT 1
	END
	ELSE
	BEGIN
		DELETE
		FROM fin_tracker_domain
		WHERE id = @id

		SELECT 1
	END
END




GO

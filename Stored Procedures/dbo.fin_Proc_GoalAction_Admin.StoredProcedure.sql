USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_GoalAction_Admin]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[fin_Proc_GoalAction_Admin]
@GCode nvarchar(max)=null,
@GName nvarchar (max)=null,
@GDesc nvarchar (max)=null,
@GCreatedByEmail nvarchar (500)=null,
@GCreatedDatetime nvarchar (50)=null,
@GUpdatedByEmail nvarchar (500)=null,
@GUpdatedDatetime nvarchar (50)=null,
@GStatus int=1

as
begin

select GCode,GName,GDesc,GCreatedByEmail,GCreatedDatetime,GUpdatedByEmail,GUpdatedDatetime,GStatus from [dbo].[fin_Goal]
end

GO

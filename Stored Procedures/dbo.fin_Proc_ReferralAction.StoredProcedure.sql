USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_ReferralAction]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[fin_Proc_ReferralAction]
	@FROMEMAIL [nvarchar](max)=NULL,
	@TONAME [nvarchar](max)=NULL,
	@TOEMAIL [nvarchar](max)=NULL,
	@TOMOBILE [nvarchar](max)=NULL,
	@RCODE [nvarchar](max)=NULL,
	@STATUS [int]=1,
	@ACTION [varchar](500)
as
begin
	if(@ACTION='ADD')
		begin
			insert [dbo].[fin_Referral] select @FROMEMAIL,@TONAME,@TOEMAIL,@TOMOBILE,@RCODE,GETDATE(),GETDATE(),@STATUS
			
			SELECT ISNULL(NULLIF(name, ''), 'User') from fp_prospectS where userid =@FROMEMAIL
		end

	if(@ACTION='VIEW')
		begin
			select * from [dbo].[fin_Referral] where FROMEMAIL=@FROMEMAIL order by CREATEDDATETIME desc
		end
end
GO

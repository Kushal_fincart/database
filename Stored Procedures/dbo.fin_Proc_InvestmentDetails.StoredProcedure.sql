USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_InvestmentDetails]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[fin_Proc_InvestmentDetails]
@userid_for_cursor nvarchar(max)=null,
@fcode nvarchar(max)=null,
@scode nvarchar(max)=null,
@Action [nvarchar](max)
as
Begin
if(@Action='GetGoalSchemeWise')
begin
--User final transaction for each goal details
       create table #CartTempTable
       (
       userId nvarchar(max),
       memberId nvarchar(max),
       profileId nvarchar(max),
       goalCode nvarchar(max),
       fundCode nvarchar(max),
       schemeCode nvarchar(max),
	   foliono nvarchar(max),
       amount nvarchar(max),
       goalname nvarchar(max),
       UserGoalId bigint
       )

       declare @memberId nvarchar(max),@profileId nvarchar(max),@userid nvarchar(500),@goalCode nvarchar(max),@goalname nvarchar(max),
       @fundCode nvarchar(max),@schemeCode nvarchar(max),@foliono nvarchar(max),@toSchemeCode nvarchar(max),@amount nvarchar(max),@type nvarchar(max),
       @startDate datetime,@noOfInstallment int,@UserGoalId bigint,@noofmonth int

       DECLARE investment_cursor CURSOR FOR  
       --cursor loop for lumpsum/instasip/sip for each investment in each goal
       SELECT userId,memberId,profileId,goalCode,fundCode,schemeCode,foliono,amount,type,startDate,noOfInstallment,UserGoalId
       FROM [dbo].[fin_Cart] where UserID=@userid_for_cursor and isnull(goalcode,'')!='' and isnull(UserGoalId,'')!='' and status=1
       OPEN investment_cursor  
       FETCH NEXT FROM investment_cursor INTO @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,@amount,@type,@startDate,@noOfInstallment,@UserGoalId  
       WHILE @@FETCH_STATUS = 0  
       BEGIN  
       --print 'k'
                     if exists(select * from #CartTempTable where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId)
                                  begin
                                         if(@type='L')
                                                begin
                                                update #CartTempTable set amount=CONVERT(float,amount)+CONVERT(float,@amount) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                                end
                                         else if(@type='I')
                                                begin
                                                set @noofmonth=  (select DateDiff(mm,@startDate, DateAdd(mm, 1,getdate())))
                                                --print @noofmonth
                                                if(@noofmonth<1)
                                                       begin
                                                       update #CartTempTable set amount=CONVERT(float,amount)+CONVERT(float,@amount)where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                                       end
                                                else if(@noofmonth<=@noOfInstallment)
                                                       begin
                                                       update #CartTempTable set amount=CONVERT(float,amount)+(CONVERT(float,@amount)*@noofmonth)where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                                       end
                                                else
                                                       begin
                                                       update #CartTempTable set amount=CONVERT(float,amount)+(CONVERT(float,@amount)*@noOfInstallment) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                                       end
                                                end
                                         else if(@type='S')
                                                begin
                                                set @noofmonth=  (select DateDiff(mm,@startDate, DateAdd(mm, 1,getdate())))
                                                --print @noofmonth
                                                --if(@noofmonth<1)
                                                --     begin
                                                --     update #CartTempTable set amount='0' where fundCode=@fundCode and schemeCode=@schemeCode and UserGoalId=@UserGoalId
                                                --     end
                                                --else 
                                                if(@noofmonth>0 and @noofmonth<=@noOfInstallment)
                                                       begin
                                                       update #CartTempTable set amount=CONVERT(float,amount)+(CONVERT(float,@amount)*@noofmonth)where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                                       end
                                                else if(@noofmonth>@noOfInstallment)
                                                       begin
                                                       update #CartTempTable set amount=CONVERT(float,amount)+(CONVERT(float,@amount)*@noOfInstallment) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                                       end
                                                end
                                  end
                     else
                                  begin
                                  if(@goalCode='FG12')
                                  begin
                                         set    @goalname=(select top 1 name+'s Study' from [dbo].[fin_User_Relation] where relationid in(select relationid from [dbo].[fin_User_Goals] where id=@UserGoalId))
                                  end
                                  else if(@goalCode='FG13')
                                  begin
                                         set    @goalname=(select top 1 name+'s Wedding' from [dbo].[fin_User_Relation] where relationid in(select relationid from [dbo].[fin_User_Goals] where id=@UserGoalId))
                                  end
                                  else if(@goalCode='FG14')
                                  begin
                                         set    @goalname=(select Place from [dbo].[fin_User_Goals] where id=@UserGoalId)
                                  end
                                  else
                                  begin
                                         set    @goalname= (select Gname from [dbo].[fin_Goal] where GCode in((select GoalCode from [dbo].[fin_User_Goals] where id=@UserGoalId)))
                                  end
                                  
                                  insert #CartTempTable select @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,@amount,@goalname,@UserGoalId
                                  end
                     FETCH NEXT FROM investment_cursor INTO @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,@amount,@type,@startDate,@noOfInstallment,@UserGoalId  
       END  

       CLOSE investment_cursor  
       DEALLOCATE investment_cursor 

       DECLARE investment_cursor CURSOR FOR  
       --cursor loop for switch/stp/redemption/swp for each investment in each goal
       SELECT userId,memberId,profileId,goalCode,fundCode,schemeCode,foliono,toSchemeCode,amount,type,startDate,noOfInstallment,UserGoalId
       FROM [dbo].[fin_Other_Transactions_Details] where UserID=@userid_for_cursor and isnull(goalcode,'')!='' and isnull(UserGoalId,'')!=''
       OPEN investment_cursor  
       FETCH NEXT FROM investment_cursor INTO @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,@toSchemeCode,@amount,@type,@startDate,@noOfInstallment,@UserGoalId  

       WHILE @@FETCH_STATUS = 0  
       BEGIN  
              if(@type='SWITCH')
                     begin
                           if exists(select * from #CartTempTable where fundCode=@fundCode and schemeCode=@toSchemeCode and foliono=@foliono and UserGoalId=@UserGoalId)
                                  begin
                                  update #CartTempTable set amount=CONVERT(float,amount)+CONVERT(float,@amount) where fundCode=@fundCode and schemeCode=@toSchemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                  end
                           else
                                  begin
                                  if(@goalCode='FG12')
                                  begin
                                         set    @goalname=(select top 1 name+'s Study' from [dbo].[fin_User_Relation] where relationid in(select relationid from [dbo].[fin_User_Goals] where id=@UserGoalId))
                                  end
                                  else if(@goalCode='FG13')
                                  begin
                                         set    @goalname=(select top 1 name+'s Wedding' from [dbo].[fin_User_Relation] where relationid in(select relationid from [dbo].[fin_User_Goals] where id=@UserGoalId))
                                  end
                                  else if(@goalCode='FG14')
                                  begin
                                         set    @goalname=(select Place from [dbo].[fin_User_Goals] where id=@UserGoalId)
                                  end
                                  else
                                  begin
                                         set    @goalname= (select Gname from [dbo].[fin_Goal] where GCode in((select GoalCode from [dbo].[fin_User_Goals] where id=@UserGoalId)))
                                  end
                                  insert #CartTempTable select @userId,@memberId,@profileId,@goalCode,@fundCode,@toSchemeCode,@foliono,@amount,@goalname,@UserGoalId
                                  end
                           if exists(select * from #CartTempTable where fundCode=@fundCode and schemeCode=@schemeCode and UserGoalId=@UserGoalId)
                                  begin
                                  update #CartTempTable set amount=CONVERT(float,amount)-CONVERT(float,@amount) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                  end
                     end
              else if(@type='STP')
                     begin
                           set @noofmonth=  (select DateDiff(mm,@startDate, DateAdd(mm, 1,getdate())))
                           if exists(select * from #CartTempTable where fundCode=@fundCode and schemeCode=@toSchemeCode and foliono=@foliono and UserGoalId=@UserGoalId)
                                  begin
                                                if(@noofmonth>0 and @noofmonth<=@noOfInstallment)
                                                begin
                                                update #CartTempTable set amount=CONVERT(float,amount)+(CONVERT(float,@amount)*@noofmonth) where fundCode=@fundCode and schemeCode=@toSchemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                                end
                                         else  if(@noofmonth>@noOfInstallment)
                                                begin
                                                update #CartTempTable set amount=CONVERT(float,amount)+(CONVERT(float,@amount)*@noOfInstallment) where fundCode=@fundCode and schemeCode=@toSchemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                                end
                                  end
                           else
                                  begin
                                         if(@noofmonth>0 and @noofmonth<=@noOfInstallment)
                                                begin
                                                                     if(@goalCode='FG12')
                                                       begin
                                                              set    @goalname=(select top 1 name+'s Study' from [dbo].[fin_User_Relation] where relationid in(select relationid from [dbo].[fin_User_Goals] where id=@UserGoalId))
                                                       end
                                                       else if(@goalCode='FG13')
                                                       begin
                                                              set    @goalname=(select top 1 name+'s Wedding' from [dbo].[fin_User_Relation] where relationid in(select relationid from [dbo].[fin_User_Goals] where id=@UserGoalId))
                                                       end
                                                       else if(@goalCode='FG14')
                                                       begin
                                                              set    @goalname=(select Place from [dbo].[fin_User_Goals] where id=@UserGoalId)
                                                       end
                                                       else
                                                       begin
                                                              set    @goalname= (select Gname from [dbo].[fin_Goal] where GCode in((select GoalCode from [dbo].[fin_User_Goals] where id=@UserGoalId)))
                                                       end
                                                       insert #CartTempTable select @userId,@memberId,@profileId,@goalCode,@fundCode,@toSchemeCode,@foliono,@amount,@goalname,@UserGoalId
                                                end
                                         else  if(@noofmonth>@noOfInstallment)
                                                begin
                                                                                  if(@goalCode='FG12')
                                                       begin
                                                              set    @goalname=(select top 1 name+'s Study' from [dbo].[fin_User_Relation] where relationid in(select relationid from [dbo].[fin_User_Goals] where id=@UserGoalId))
                                                       end
                                                       else if(@goalCode='FG13')
                                                       begin
                                                              set    @goalname=(select top 1 name+'s Wedding' from [dbo].[fin_User_Relation] where relationid in(select relationid from [dbo].[fin_User_Goals] where id=@UserGoalId))
                                                       end
                                                       else if(@goalCode='FG14')
                                                       begin
                                                              set    @goalname=(select Place from [dbo].[fin_User_Goals] where id=@UserGoalId)
                                                       end
                                                       else
                                                       begin
                                                              set    @goalname= (select Gname from [dbo].[fin_Goal] where GCode in((select GoalCode from [dbo].[fin_User_Goals] where id=@UserGoalId)))
                                                       end
                                                       insert #CartTempTable select @userId,@memberId,@profileId,@goalCode,@fundCode,@toSchemeCode,@amount,@goalname,@UserGoalId
                                                end
                                  end
                           if exists(select * from #CartTempTable where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId)
                                  begin
                                         if(@noofmonth>0 and @noofmonth<=@noOfInstallment)
                                                begin
                                                       update #CartTempTable set amount=CONVERT(float,amount)-(CONVERT(float,@amount)*@noofmonth) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                                end
                                         else  if(@noofmonth>@noOfInstallment)
                                                begin
                                                       update #CartTempTable set amount=CONVERT(float,amount)-(CONVERT(float,@amount)*@noOfInstallment) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                                end
                                  end
                     end
              else if(@type='R')
                     begin
                           if exists(select * from #CartTempTable where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId)
                                  begin
                                  update #CartTempTable set amount=CONVERT(float,amount)-CONVERT(float,@amount) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                  end
                     end
              else if(@type='SWP')
                     begin
                           set @noofmonth=  (select DateDiff(mm,@startDate, DateAdd(mm, 1,getdate())))
                           if exists(select * from #CartTempTable where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId)
                                  begin
                                         if(@noofmonth>0 and @noofmonth<=@noOfInstallment)
                                                begin
                                                       update #CartTempTable set amount=CONVERT(float,amount)-(CONVERT(float,@amount)*@noofmonth) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                                end
                                         else  if(@noofmonth>@noOfInstallment)
                                                begin
                                                       update #CartTempTable set amount=CONVERT(float,amount)-(CONVERT(float,@amount)*@noOfInstallment) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
                                                end
                                  end
                     end
                     FETCH NEXT FROM investment_cursor INTO @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,@toSchemeCode,@amount,@type,@startDate,@noOfInstallment,@UserGoalId  
       END  

       CLOSE investment_cursor  

       DEALLOCATE investment_cursor 

	   if(@fcode='0' and @scode='0')
		   begin
		   select * from #CartTempTable
		   end
	   else
		   begin
		   select * from #CartTempTable where fundCode=@fcode and schemeCode=@scode
		   end
       
	   drop table #CartTempTable
end
End 

GO

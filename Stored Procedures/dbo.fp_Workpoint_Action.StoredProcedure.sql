USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fp_Workpoint_Action]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

















CREATE PROCEDURE [dbo].[fp_Workpoint_Action] @Id BIGINT = NULL
	,@userid [nvarchar] (max) = NULL
	,@pass [nvarchar] (max) = NULL
	,@LEADCODE BIGINT = NULL
	,@EMPCODE NVARCHAR(max) = NULL
	,@REASSIGNTO NVARCHAR(max) = NULL
	,@REASON NVARCHAR(max) = NULL
	,@COMMENT NVARCHAR(max) = NULL
	,@ACTIVITYTYPEID BIGINT = NULL
	,@LEADTYPEID INT = NULL
	,@ACTIONDATETIME DATETIME = NULL
	,@LEADSTATUS VARCHAR(max) = NULL
	,@CREATEDDATE DATETIME = NULL
	,@ASSIGNEDDATE DATETIME = NULL
	,@LASTACTIVITYDATE DATETIME = NULL
	,@PageIndex INT = 0
	,--FOR PAGING
	@PageSize INT = 0
	,--FOR PAGING
	@DATEFROM DATETIME = ''
	,--- FOR DYNAMIC QUERY
	@DATETO DATETIME = ''
	,--- FOR DYNAMIC QUERY
	@FILTERBY1 NVARCHAR(max) = NULL
	,--- FOR DYNAMIC QUERY
	@FILTERBY2 NVARCHAR(max) = NULL
	,--- FOR DYNAMIC QUERY
	@FILTERBYNAME NVARCHAR(max) = NULL
	,--- FOR DYNAMIC QUERY
	@FILTERBYEMAIL NVARCHAR(max) = NULL
	,--- FOR DYNAMIC QUERY
	@FILTERBYMOBILE NVARCHAR(max) = NULL
	,--- FOR DYNAMIC QUERY
	@FILTERBYCAMPAIGN NVARCHAR(max) = NULL
	,--- FOR DYNAMIC QUERY
	@RM NVARCHAR(max) = NULL
	,@Name NVARCHAR(max) = NULL
	,@depcode nvarchar(100)=null,
	--@BULKLEADTBL fin_Type_BulkLeadTbl READONLY,     --- FOR BULK LEAD ACTIONS
	@Action [varchar] (100)
	,@ASSIGNEDTOEMPCODE VARCHAR(max) = NULL OUTPUT
	,@RecordCount BIGINT = NULL OUTPUT
AS
BEGIN
	DECLARE @Conditions VARCHAR(MAX) = '' --- FOR DYNAMIC QUERY
	DECLARE @EmpQuery VARCHAR(MAX) = '' --- FOR DYNAMIC QUERY
	DECLARE @TestQuery NVARCHAR(MAX) = NULL --- FOR DYNAMIC QUERY
	DECLARE @ParmDefinition NVARCHAR(max) = NULL --- FOR DYNAMIC QUERY
	DECLARE @LTypeId INT = NULL
	DECLARE @CurrentLead INT = NULL
	DECLARE @isAdmin INT = NULL

	IF (@Action = 'LOGIN')
	BEGIN
		SELECT emp.empcode
			,emp.name
			,emp.userid
			,emp.pwd
			,rl.ROLE
		FROM fin_Employee emp
		INNER JOIN fin_EmployeeRole rl ON emp.RoleId = rl.Code
		WHERE emp.userid = @userid
			AND emp.Pwd = @pass
			AND emp.STATUS = 1
	END

	IF (@Action = 'SELECT_LEADS_PAGED')
	BEGIN
		IF (
				@EMPCODE IS NOT NULL
				AND @EMPCODE != ''
				)
		BEGIN
			SELECT @isAdmin = count(*)
			FROM fin_Employee e
			INNER JOIN fin_Departments d ON e.DepId = Code
				AND d.Code = 'DEP2018416185931'
				AND EmpCode = @EMPCODE

			IF (@isAdmin <= 0)
			BEGIN
				SET @EmpQuery = ' and lm.empcode =''' + @EMPCODE + ''''
			END
			ELSE
				SET @EmpQuery = ''
		END
		ELSE
		BEGIN
			SET @EmpQuery = ''
		END

		IF (
				@DATEFROM IS NOT NULL
				AND @DATEFROM != ''
				AND @DATETO IS NOT NULL
				AND @DATETO != ''
				AND @FILTERBY1 IS NOT NULL
				AND @FILTERBY1 != ''
				)
		BEGIN
			IF (@FILTERBY1 = 'BY_CREATEDDATE')
			BEGIN
				SET @Conditions = @Conditions + ' and CONVERT(date, lm.createdDate) between ''' + CAST(CONVERT(DATE, @DATEFROM) AS NVARCHAR(500)) + ''' and ''' + CAST(CONVERT(DATE, @DATETO) AS NVARCHAR(500)) + ''''
			END
			ELSE
			BEGIN
				SET @Conditions = @Conditions + ' and CONVERT(date, lm.lastActivityDate) between ''' + CAST(CONVERT(DATE, @DATEFROM) AS NVARCHAR(500)) + ''' and ''' + CAST(CONVERT(DATE, @DATETO) AS NVARCHAR(500)) + ''''
			END
		END

		IF (
				@FILTERBY2 IS NOT NULL
				AND @FILTERBY2 != ''
				)
		BEGIN
			IF (@FILTERBY2 = 'FOLLOW UP')
			BEGIN
				SET @Conditions = @Conditions + ' and lm.leadTypeId in (13,18,19)';
			END
			ELSE
			BEGIN
				IF (@FILTERBY2 = 'ASSIGNED')
				BEGIN
					SET @Conditions = @Conditions + ' and lm.leadTypeId in (12,15)';
				END
				else
				Begin
					SET @Conditions = @Conditions + ' and lm.leadTypeId= CASE WHEN @FILTERBY2=''INTERESTED'' then 18 WHEN 
					@FILTERBY2=''CONVERTED'' THEN 14 WHEN @FILTERBY2=''NEW'' THEN 11 WHEN @FILTERBY2=''DEAD'' THEN 16 WHEN @FILTERBY2=''READY TO INVEST'' 
					THEN 19 WHEN @FILTERBY2=''ON HOLD'' THEN 17 WHEN @FILTERBY2=''ASSIGNED'' THEN 12 WHEN @FILTERBY2=''FOLLOW UP'' THEN 13 WHEN @FILTERBY2=''RE-ASSIGNED'' THEN 15 ELSE 0 END'
				end
			END
		END

		IF (
				@FILTERBYNAME IS NOT NULL
				AND @FILTERBYNAME != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.name like ''%' + @FILTERBYNAME + '%'''
		END

		IF (
				@FILTERBYEMAIL IS NOT NULL
				AND @FILTERBYEMAIL != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.userid like ''%' + @FILTERBYEMAIL + '%'''
		END

		IF (
				@FILTERBYMOBILE IS NOT NULL
				AND @FILTERBYMOBILE != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.mobile1 like ''%' + @FILTERBYMOBILE + '%'''
		END

		IF (
				@RM IS NOT NULL
				AND @RM != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and lm.empCode = ''' + @RM + ''''
		END

		IF (
				@FILTERBYCAMPAIGN IS NOT NULL
				AND @FILTERBYCAMPAIGN != ''
				)
		BEGIN
			IF (@FILTERBYCAMPAIGN != 'NULL')
			BEGIN
				SET @Conditions = @Conditions + ' and fp.Clientstatus = ''' + @FILTERBYCAMPAIGN + ''''
			END
			ELSE
			BEGIN
				SET @Conditions = @Conditions + ' and fp.Clientstatus IS NULL'
			END
		END

		SET @ParmDefinition = N'@FILTERBY2 nvarchar (max), @RecordCount BIGINT OUTPUT, @PageIndex INT,  @PageSize INT';
		SET @TestQuery = 'SET @RecordCount = (select count(*) FROM fin_leadmapping lm join fp_prospects fp on lm.leadcode = fp.code where status = 1 ' + @EmpQuery + @Conditions + '); ' + ' WITH MyCte AS 
(
    select lm.id,lm.leadcode,lm.empCode,fp.Name as ClientName,fp.UserId,fp.Pwd,fp.mobile1 as mobile,(CASE WHEN fp.Clientstatus like ''FG%'' then fg.GDesc WHEN fp.Clientstatus like 
	''C%'' THEN cmp.Name ELSE ''Direct Registration'' END) as Campaign,ltm.Name as LeadType,emp.Name as Planner, lm.lastActivityDate ,lm.createdDate ,ROW_NUMBER() OVER (ORDER BY lm.createdDate desc) RowNumber from fin_leadmapping lm left join fp_Prospects fp on lm.leadCode=fp.Code join [dbo].[fin_Employee] emp on lm.empcode = emp.empcode join fin_LeadTypeMaster ltm on lm.leadTypeId=ltm.leadTypeId left join fin_campaign cmp on fp.clientstatus= cmp.ClientStatusCode And fp.Clientstatus like ''C%'' left join fin_Goal fg on fp.clientstatus=fg.Gcode And fp.Clientstatus like ''FG%'' where lm.status = 1 ' + @EmpQuery + 
			@Conditions + ' ) SELECT  *
FROM    MyCte WHERE   MyCte.RowNumber BETWEEN(@PageIndex -1) *  @PageSize +  1 AND(((@PageIndex -1) * @PageSize + 1) +  @PageSize) - 1;

select @RecordCount as total; '

		--print @TestQuery
		EXECUTE sp_executesql @TestQuery
			,@ParmDefinition
			,@FILTERBY2 = @FILTERBY2
			,@RecordCount = @RecordCount OUTPUT
			,@PageIndex = @PageIndex
			,@PageSize = @PageSize;
	END

	IF (@Action = 'SELECT_CLIENTS_PAGED')
	BEGIN
		IF (
				@EMPCODE IS NOT NULL
				AND @EMPCODE != ''
				)
		BEGIN
			SELECT @isAdmin = count(*)
			FROM fin_Employee e
			INNER JOIN fin_Departments d ON e.DepId = Code
				AND d.Code = 'DEP2018416185931'
				AND EmpCode = @EMPCODE

			IF (@isAdmin <= 0)
			BEGIN
				SET @EmpQuery = ' and lm.empcode =''' + @EMPCODE + ''''
			END
			ELSE
			BEGIN
				SET @EmpQuery = ''
			END
		END
		ELSE
		BEGIN
			SET @EmpQuery = ''
		END

		IF (
				@DATEFROM IS NOT NULL
				AND @DATEFROM != ''
				AND @DATETO IS NOT NULL
				AND @DATETO != ''
				AND @FILTERBY1 IS NOT NULL
				AND @FILTERBY1 != ''
				)
		BEGIN
			IF (@FILTERBY1 = 'BY_CREATEDDATE')
			BEGIN
				SET @Conditions = @Conditions + ' and CONVERT(date, lm.createdDate) between ''' + CAST(CONVERT(DATE, @DATEFROM) AS NVARCHAR(500)) + ''' and ''' + CAST(CONVERT(DATE, @DATETO) AS NVARCHAR(500)) + ''''
			END
			ELSE
			BEGIN
				SET @Conditions = @Conditions + ' and CONVERT(date, lm.lastActivityDate) between ''' + CAST(CONVERT(DATE, @DATEFROM) AS NVARCHAR(500)) + ''' and ''' + CAST(CONVERT(DATE, @DATETO) AS NVARCHAR(500)) + ''''
			END
		END

		IF (
				@FILTERBY2 IS NOT NULL
				AND @FILTERBY2 != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and lm.leadTypeId= CASE WHEN @FILTERBY2=''INTERESTED'' then 18 WHEN 
				@FILTERBY2=''CONVERTED'' THEN 14 WHEN @FILTERBY2=''NEW'' THEN 11 WHEN @FILTERBY2=''DEAD'' THEN 16 WHEN @FILTERBY2=''READY TO INVEST'' 
				THEN 19 WHEN @FILTERBY2=''ON HOLD'' THEN 17 WHEN @FILTERBY2=''ASSIGNED'' THEN 12 WHEN @FILTERBY2=''FOLLOW UP'' THEN 13 WHEN @FILTERBY2=''RE-ASSIGNED'' 
				THEN 15 ELSE 0 END'
		END

		IF (
				@FILTERBYNAME IS NOT NULL
				AND @FILTERBYNAME != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.name like ''%' + @FILTERBYNAME + '%'''
		END

		IF (
				@FILTERBYEMAIL IS NOT NULL
				AND @FILTERBYEMAIL != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.userid like ''%' + @FILTERBYEMAIL + '%'''
		END

		IF (
				@FILTERBYMOBILE IS NOT NULL
				AND @FILTERBYMOBILE != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.mobile1 like ''%' + @FILTERBYMOBILE + '%'''
		END

		IF (
				@FILTERBYCAMPAIGN IS NOT NULL
				AND @FILTERBYCAMPAIGN != ''
				)
		BEGIN
			IF (@FILTERBYCAMPAIGN != 'NULL')
			BEGIN
				SET @Conditions = @Conditions + ' and fp.Clientstatus = ''' + @FILTERBYCAMPAIGN + ''''
			END
			ELSE
			BEGIN
				SET @Conditions = @Conditions + ' and fp.Clientstatus IS NULL'
			END
		END

		SET @ParmDefinition = N'@FILTERBY2 nvarchar (max), @RecordCount BIGINT OUTPUT, @PageIndex INT,  @PageSize INT';
		SET @TestQuery = 'SET @RecordCount = (select count(*) FROM fin_leadmapping lm join fp_prospects fp on lm.leadcode = fp.code where lm.leadTypeId=14 and status = 1 ' + @EmpQuery + @Conditions + '); ' + 
			' WITH MyCte AS 
(
    select lm.id,lm.leadcode,lm.empCode,fp.Name as ClientName,fp.UserId,fp.Pwd,fp.mobile1 as mobile,(CASE WHEN fp.Clientstatus like ''FG%'' then fg.GDesc WHEN fp.Clientstatus like ''C%'' THEN cmp.Name ELSE ''Direct Registration'' END) as Campaign,ltm.Name as LeadType,emp.Name as Planner,
	lm.lastActivityDate,lm.createdDate,ROW_NUMBER() OVER (ORDER BY lm.createdDate desc) RowNumber from fin_leadmapping lm left join fp_Prospects fp on lm.leadCode=fp.Code join [dbo].[fin_Employee] emp on lm.empcode = emp.empcode join fin_LeadTypeMaster ltm on lm.leadTypeId=ltm.leadTypeId left join fin_campaign cmp on fp.clientstatus= cmp.ClientStatusCode And fp.Clientstatus like ''C%'' left join fin_Goal fg on fp.clientstatus=fg.Gcode And fp.Clientstatus like ''FG%'' where lm.status = 1 and ltm.leadTypeId=14 ' + @EmpQuery + @Conditions + 
			' ) SELECT  *
FROM    MyCte WHERE   MyCte.RowNumber BETWEEN(@PageIndex -1) *  @PageSize +  1 AND(((@PageIndex -1) * @PageSize + 1) +  @PageSize) - 1;

select @RecordCount as total; '

		--print @TestQuery
		EXECUTE sp_executesql @TestQuery
			,@ParmDefinition
			,@FILTERBY2 = @FILTERBY2
			,@RecordCount = @RecordCount OUTPUT
			,@PageIndex = @PageIndex
			,@PageSize = @PageSize;
	END

	IF (@Action = 'CountByLeadType')
	BEGIN
		SELECT @isAdmin = count(*)
		FROM fin_Employee e
		INNER JOIN fin_Departments d ON e.DepId = Code
			AND d.Code = 'DEP2018416185931'
			AND EmpCode = @EMPCODE

		IF (@isAdmin <= 0)
		BEGIN
			SELECT lt.Name
				,isnull(base.TotalRecords, 0) AS TotalRecords
			FROM fin_LeadTypeMaster lt
			LEFT JOIN (
				SELECT lt.leadTypeId
					,lt.Name
					,count(lm.Id) AS TotalRecords
				FROM fin_LeadMapping lm
				INNER JOIN fin_LeadTypeMaster lt ON lm.leadTypeId = lt.leadTypeId
				WHERE empCode = @EMPCODE
				GROUP BY lt.Name
					,lt.leadTypeId
				) AS base ON lt.leadTypeId = base.leadTypeId
		END
		ELSE
		BEGIN
			SELECT lt.Name
				,isnull(base.TotalRecords, 0) AS TotalRecords
			FROM fin_LeadTypeMaster lt
			LEFT JOIN (
				SELECT lt.leadTypeId
					,lt.Name
					,count(lm.Id) AS TotalRecords
				FROM fin_LeadMapping lm
				INNER JOIN fin_LeadTypeMaster lt ON lm.leadTypeId = lt.leadTypeId
				GROUP BY lt.Name
					,lt.leadTypeId
				) AS base ON lt.leadTypeId = base.leadTypeId
		END
	END

	IF (@Action = 'SELECT_ALL_LEADS')
	BEGIN
		IF (
				@EMPCODE IS NOT NULL
				AND @EMPCODE != ''
				)
		BEGIN
			SET @EmpQuery = ' and lm.empcode =''' + @EMPCODE + ''''
		END
		ELSE
		BEGIN
			SET @EmpQuery = ''
		END

		IF (
				@DATEFROM IS NOT NULL
				AND @DATEFROM != ''
				AND @DATETO IS NOT NULL
				AND @DATETO != ''
				AND @FILTERBY1 IS NOT NULL
				AND @FILTERBY1 != ''
				)
		BEGIN
			IF (@FILTERBY1 = 'BY_CREATEDDATE')
			BEGIN
				SET @Conditions = @Conditions + ' and CONVERT(date, createdDate) between ''' + CAST(CONVERT(DATE, @DATEFROM) AS NVARCHAR(500)) + ''' and ''' + CAST(CONVERT(DATE, @DATETO) AS NVARCHAR(500)) + ''''
			END
			ELSE
			BEGIN
				SET @Conditions = @Conditions + ' and CONVERT(date, lastActivityDate) between ''' + CAST(CONVERT(DATE, @DATEFROM) AS NVARCHAR(500)) + ''' and ''' + CAST(CONVERT(DATE, @DATETO) AS NVARCHAR(500)) + ''''
			END
		END

		IF (
				@FILTERBY2 IS NOT NULL
				AND @FILTERBY2 != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and lm.leadTypeId= CASE WHEN @FILTERBY2=''INTERESTED'' then 18 WHEN 
				@FILTERBY2=''CONVERTED'' THEN 14 WHEN @FILTERBY2=''NEW'' THEN 11 WHEN @FILTERBY2=''DEAD'' THEN 16 WHEN @FILTERBY2=''READY TO INVEST'' 
				THEN 19 WHEN @FILTERBY2=''ON HOLD'' THEN 17 WHEN @FILTERBY2=''ASSIGNED'' THEN 12 WHEN @FILTERBY2=''FOLLOW UP'' THEN 13 WHEN @FILTERBY2=''RE-ASSIGNED'' THEN 15 ELSE 0 END'
		END

		IF (
				@FILTERBYNAME IS NOT NULL
				AND @FILTERBYNAME != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.name like ''%' + @FILTERBYNAME + '%'''
		END

		IF (
				@FILTERBYEMAIL IS NOT NULL
				AND @FILTERBYEMAIL != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.userid like ''%' + @FILTERBYEMAIL + '%'''
		END

		IF (
				@FILTERBYMOBILE IS NOT NULL
				AND @FILTERBYMOBILE != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.mobile1 like ''%' + @FILTERBYMOBILE + '%'''
		END

		IF (
				@FILTERBYCAMPAIGN IS NOT NULL
				AND @FILTERBYCAMPAIGN != ''
				)
		BEGIN
			IF (@FILTERBYCAMPAIGN != 'NULL')
			BEGIN
				SET @Conditions = @Conditions + ' and fp.Clientstatus = ''' + @FILTERBYCAMPAIGN + ''''
			END
			ELSE
			BEGIN
				SET @Conditions = @Conditions + ' and fp.Clientstatus IS NULL'
			END
		END

		SET @ParmDefinition = N'@FILTERBY2 nvarchar (max)';
		SET @TestQuery = ' WITH MyCte AS 
(
    select lm.id,lm.leadcode,lm.empCode,fp.Name as ClientName,fp.UserId,fp.mobile1 as mobile,(CASE WHEN fp.Clientstatus like ''FG%'' then fg.GDesc WHEN fp.Clientstatus like ''C%'' THEN cmp.Name ELSE ''Direct Registration'' END) as Campaign, ltm.Name as LeadType,emp.Name as Planner, lm.lastActivityDate,lm.createdDate,ROW_NUMBER() OVER (ORDER BY lm.createdDate desc) RowNumber from fin_leadmapping lm left join fp_Prospects fp on lm.leadCode=fp.Code join [dbo].[fin_Employee] emp on lm.empcode = emp.empcode join fin_LeadTypeMaster ltm on lm.leadTypeId=ltm.leadTypeId left join fin_campaign cmp on fp.clientstatus= cmp.ClientSatusCode And fp.Clientstatus like ''C%'' left join fin_Goal fg on fp.clientstatus=fg.Gcode And fp.Clientstatus like ''FG%'' where lm.status = 1 ' + @EmpQuery + @Conditions + ' ) SELECT  *
FROM  MyCte'

		EXECUTE sp_executesql @TestQuery
			,@ParmDefinition
			,@FILTERBY2 = @FILTERBY2;
	END

	IF (@Action = 'SELECT_LEADTYPE_MASTER')
	BEGIN
		SELECT DISTINCT name
		FROM fin_LeadTypeMaster
	END

	IF (@Action = 'SELECT_GOAL_MASTER')
	BEGIN
		SELECT DISTINCT GName AS name
			,GCode AS code
		FROM fin_Goal
		WHERE gtype = 1
	END

	IF (@Action = 'SELECT_LEADTYPE_COUNT')
	BEGIN
		SELECT [12] AS ASSIGNED
			,[15] AS REASSIGNED
			,[18] AS INTERESTED
			,[17] AS ONHOLD
			,[13] AS FOLLOWUP
			,[14] AS CONVERTED
			,[19] AS READYTOINVEST
			,[16] AS DEAD
		FROM (
			SELECT empCode
				,leadTypeId
				,STATUS
			FROM fin_LeadMapping
			) src
		pivot(count(leadTypeId) FOR leadTypeId IN (
					[12]
					,[15]
					,[18]
					,[17]
					,[13]
					,[14]
					,[19]
					,[16]
					)) piv
		WHERE empcode = @EMPCODE
			AND STATUS = 1;
	END

	IF (@Action = 'SELECT_CAMPAIGN_MASTER')
	BEGIN
		SELECT DISTINCT Name AS name
			,ClientStatusCode AS code
		FROM fin_Campaign
	END

	IF (@Action = 'SELECT_ACTIVITYTYPE_MASTER')
	BEGIN
		SELECT *
		FROM fin_ActivityTypeMaster
		WHERE activityid != 0
			AND STATUS = 1
	END

	IF (@Action = 'SELECT_LEAD_ACTIVITY_MONITOR')
	BEGIN
		IF (
				@LEADCODE != ''
				AND @LEADCODE IS NOT NULL
				)
		BEGIN
			SELECT lam.Id
				,lam.leadCode
				,lam.empCode
				,lam.comment
				,aty.name AS activity
				,aty.activityId
				,lam.actionDate
				,emp.Name AS actionTakenBy
				,Convert(DATE, lam.nextActionDate) AS nextActionDate
				,CONVERT(VARCHAR(5), lam.nextActionDate, 108) AS Actiontime
			FROM [fin_LeadActivityMonitor] lam
			LEFT JOIN fin_ActivityTypeMaster aty ON lam.activityId = aty.activityId
			LEFT JOIN fin_Employee emp ON lam.empCode = emp.empcode
			WHERE leadcode = @LEADCODE
			ORDER BY Convert(DATETIME, actionDate) DESC
		END
	END

	IF (@Action = 'SELECT_ALL_EMPLOYEE')
	BEGIN
		IF (
				@EMPCODE IS NOT NULL
				AND @EMPCODE != ''
				)
		BEGIN
			SELECT *
			FROM fin_Employee
			WHERE EmpCode != 'FIN01032013001'
		END
		ELSE
		BEGIN
			SELECT *
			FROM fin_Employee
		END
	END

	IF (@Action = 'SELECT_ALL_REMINDERS')
	BEGIN
		IF (
				@EMPCODE IS NOT NULL
				AND @EMPCODE != ''
				)
		BEGIN
			SELECT AM.id
				,ATM.Name AS Activity
				,(
					CASE 
						WHEN FP.Name IS NULL
							OR FP.Name = ''
							THEN 'User'
						ELSE FP.Name
						END
					) AS Name
				,FP.Userid
				,FP.mobile1 AS mobile
				,AM.comment
				,AM.NextActionDate
			FROM fin_LeadActivityMonitor AM
			LEFT JOIN fp_Prospects FP ON AM.leadCode = FP.Code
			LEFT JOIN fin_ActivityTypeMaster ATM ON AM.activityId = ATM.activityId
			WHERE empcode = @EMPCODE
				AND AM.activityId IN (
					1
					,4
					,5
					)
				AND AM.STATUS = 1
				AND IsReminder = 1
			ORDER BY nextActionDate DESC
		END
	END

	IF (@Action = 'SELECT_TOP20_REMINDERS')
	BEGIN
		IF (
				@EMPCODE IS NOT NULL
				AND @EMPCODE != ''
				)
		BEGIN
			SELECT TOP 20 AM.id
				,ATM.Name AS Activity
				,(
					CASE 
						WHEN FP.Name IS NULL
							OR FP.Name = ''
							THEN 'User'
						ELSE FP.Name
						END
					) AS Name
				,FP.Userid
				,FP.mobile1 AS mobile
				,AM.comment
				,AM.NextActionDate
			FROM fin_LeadActivityMonitor AM
			LEFT JOIN fp_Prospects FP ON AM.leadCode = FP.Code
			LEFT JOIN fin_ActivityTypeMaster ATM ON AM.activityId = ATM.activityId
			WHERE empcode = @EMPCODE
				AND AM.activityId IN (
					1
					,4
					,5
					)
				AND AM.STATUS = 1
				AND IsReminder = 1
			ORDER BY nextActionDate DESC
		END
	END

	IF (@Action = 'SELECT_TOTAL_REMINDERS')
	BEGIN
		IF (
				@EMPCODE IS NOT NULL
				AND @EMPCODE != ''
				)
		BEGIN
			SELECT count(*) AS total
			FROM fin_LeadActivityMonitor
			WHERE empcode = @EMPCODE
				AND activityId IN (
					1
					,4
					,5
					)
				AND STATUS = 1
				AND IsReminder = 1
		END
	END

	IF (@Action = 'SELECT_CLIENTCAF_BY_EMAIL')
	BEGIN
		IF (
				@userid IS NOT NULL
				AND @userid != ''
				)
		BEGIN
			SELECT *
			FROM [dbo].[fin_CAF_BasicDetails]
			WHERE userid = @userid
		END
	END

	IF (@Action = 'UPDATE_LEAD_STATUS')
	BEGIN
		IF (
				@LEADSTATUS IS NOT NULL
				AND @LEADSTATUS != ''
				)
		BEGIN
			SET @LTypeId = CASE 
					WHEN @LEADSTATUS = 'NEW'
						THEN 11
					WHEN @LEADSTATUS = 'ASSIGNED'
						THEN 12
					WHEN @LEADSTATUS = 'FOLLOW UP'
						THEN 13
					WHEN @LEADSTATUS = 'CONVERTED'
						THEN 14
					WHEN @LEADSTATUS = 'RE-ASSIGNED'
						THEN 15
					WHEN @LEADSTATUS = 'DEAD'
						THEN 16
					WHEN @LEADSTATUS = 'ON HOLD'
						THEN 17
					WHEN @LEADSTATUS = 'INTERESTED'
						THEN 18
					WHEN @LEADSTATUS = 'READY TO INVEST'
						THEN 19
					ELSE NULL
					END

			IF (@LTypeId IS NOT NULL)
			BEGIN
				UPDATE fin_leadmapping
				SET leadTypeId = @LTypeId
					,lastActivityDate = getdate()
				WHERE leadCode = @LEADCODE

				INSERT INTO fin_LeadStatusHistory (
					leadCode
					,leadType
					,empCode
					,StatusDate
					,STATUS
					)
				VALUES (
					@LEADCODE
					,@LTypeId
					,@EMPCODE
					,getdate()
					,1
					)

				INSERT INTO fin_LeadActivityMonitor (
					leadCode
					,empCode
					,comment
					,activityId
					,actionDate
					,nextActionDate
					,STATUS
					)
				VALUES (
					@LEADCODE
					,@EMPCODE
					,'Lead Status Changed By ' + (
						SELECT name
						FROM fin_Employee
						WHERE empcode = @EMPCODE
						)
					,2
					,getdate()
					,getdate()
					,1
					)

				IF (
						@LEADSTATUS = 'CONVERTED'
						OR @LEADSTATUS = 'DEAD'
						)
				BEGIN
					IF (@EMPCODE != 'FIN01032013001') --CHECK IF UPDATER IS NOT ADMIN
					BEGIN
						IF EXISTS (
								SELECT *
								FROM fin_LeadMatrix
								WHERE Isnext = 1
									AND empcode = 'FIN01032013001'
								)
						BEGIN
							UPDATE fin_LeadMatrix
							SET currentLead = currentLead - 1
								,STATUS = 1
								,isnext = 1
							WHERE empcode = @EMPCODE
								AND currentLead > 0

							UPDATE fin_LeadMatrix
							SET isnext = 0
							WHERE empcode = 'FIN01032013001'
						END
						ELSE
						BEGIN
							UPDATE fin_LeadMatrix
							SET currentLead = currentLead - 1
								,STATUS = 1
							WHERE empcode = @EMPCODE
								AND currentLead > 0
						END
					END
				END

				SELECT 1 AS result
			END
			ELSE
			BEGIN
				SELECT 0 AS result
			END
		END
	END

	IF (@Action = 'REMINDER_ACTIVITY_DONE')
	BEGIN
		IF (
				@ID IS NOT NULL
				AND @ID != ''
				)
		BEGIN
			UPDATE fin_leadActivityMonitor
			SET isReminder = 0
			WHERE id = @ID

			SELECT 1 AS result
		END
		ELSE
			SELECT 0 AS result
	END

	IF (@Action = 'DELETE_ACTIVITYMONITOR')
	BEGIN
		IF (
				@Id IS NOT NULL
				AND @Id != ''
				)
		BEGIN
			IF (
					@LEADCODE IS NOT NULL
					AND @LEADCODE != ''
					)
			BEGIN
				DELETE
				FROM [fin_LeadActivityMonitor]
				WHERE leadcode = @LEADCODE
					AND Id = @Id

				SELECT 1 AS result
			END
			ELSE
			BEGIN
				SELECT 0 AS result
			END
		END
	END

	IF (@Action = 'LEAD_REASSIGN')
	BEGIN
		IF (
				@REASSIGNTO IS NOT NULL
				AND @REASSIGNTO != ''
				)
		BEGIN
			IF (
					@LEADCODE IS NOT NULL
					AND @LEADCODE != ''
					)
			BEGIN
				UPDATE fin_leadmapping
				SET empcode = @REASSIGNTO
					,assignedDate = getdate()
					,lastActivityDate = getdate()
					,leadTypeId = 15
				WHERE leadCode = @LEADCODE

				INSERT INTO fin_LeadStatusHistory (
					leadCode
					,leadType
					,empCode
					,StatusDate
					,STATUS
					)
				VALUES (
					@LEADCODE
					,15
					,@EMPCODE
					,getdate()
					,1
					)

				INSERT INTO fin_LeadAllocationHistory (
					leadCode
					,AssignTo
					,AssignBy
					,assignedDate
					,reason
					,STATUS
					)
				VALUES (
					@LEADCODE
					,@REASSIGNTO
					,@EMPCODE
					,getdate()
					,@REASON
					,1
					)

				INSERT INTO fin_LeadActivityMonitor (
					leadCode
					,empCode
					,comment
					,activityId
					,actionDate
					,nextActionDate
					,STATUS
					)
				VALUES (
					@LEADCODE
					,@EMPCODE
					,'Re-Assigned to ' + (
						SELECT name
						FROM fin_Employee
						WHERE empcode = @REASSIGNTO
						) + ' By ' + (
						SELECT name
						FROM fin_Employee
						WHERE empcode = @EMPCODE
						)
					,3
					,getdate()
					,getdate()
					,1
					)

				IF (@EMPCODE != 'FIN01032013001') --CHECK IF RE-ASSIGNER IS NOT ADMIN
				BEGIN
					IF EXISTS (
							SELECT *
							FROM fin_LeadMatrix
							WHERE Isnext = 1
								AND empcode = 'FIN01032013001'
							)
					BEGIN
						-- DECREMENT ASSIGNER CURRENT LEAD CAP WITH MOVE ISNEXT TO----
						UPDATE fin_LeadMatrix
						SET currentLead = currentLead - 1
							,STATUS = 1
							,isnext = 1
						WHERE empcode = @EMPCODE
							AND currentLead > 0

						UPDATE fin_LeadMatrix
						SET isnext = 0
						WHERE empcode = 'FIN01032013001'

						-- INCREMENT ASSIGNEE CURRENT LEAD CAP----
						UPDATE fin_LeadMatrix
						SET currentLead = currentLead + 1
						WHERE empcode = @REASSIGNTO
					END
					ELSE
					BEGIN
						-- DECREMENT ASSIGNER CURRENT LEAD CAP----
						UPDATE fin_LeadMatrix
						SET currentLead = currentLead - 1
							,STATUS = 1
						WHERE empcode = @EMPCODE
							AND currentLead > 0

						-- INCREMENT ASSIGNEE CURRENT LEAD CAP----
						UPDATE fin_LeadMatrix
						SET currentLead = currentLead + 1
						WHERE empcode = @REASSIGNTO
					END
				END

				SELECT 1 AS result
			END
			ELSE
			BEGIN
				SELECT 0 AS result
			END
		END
	END

	IF (@Action = 'CREATE_ACTIVITY')
	BEGIN
		IF (
				@ACTIVITYTYPEID IS NOT NULL
				AND @ACTIVITYTYPEID != ''
				)
		BEGIN
			IF (
					@EMPCODE IS NOT NULL
					AND @EMPCODE != ''
					)
			BEGIN
				IF (
						@LEADCODE IS NOT NULL
						AND @LEADCODE != ''
						)
				BEGIN
					DECLARE @ISREMINDER BIT = 0;

					SET @ISREMINDER = (
							CASE 
								WHEN @ACTIVITYTYPEID IN (
										1
										,4
										,5
										)
									THEN 1
								ELSE 0
								END
							)

					INSERT INTO fin_LeadActivityMonitor (
						leadCode
						,empCode
						,comment
						,activityId
						,actionDate
						,nextActionDate
						,STATUS
						,IsReminder
						)
					VALUES (
						@LEADCODE
						,@EMPCODE
						,@COMMENT
						,@ACTIVITYTYPEID
						,getdate()
						,@ACTIONDATETIME
						,1
						,@ISREMINDER
						)

					SELECT 1 AS result
				END
				ELSE
				BEGIN
					SELECT 0 AS result
				END
			END
		END
	END

	--if(@Action='BULK_LEAD_REASSIGN')
	----Begin
	----if(@REASSIGNTO is not null and @REASSIGNTO !='')
	----	Begin
	----		IF EXISTS (select * from @BULKLEADTBL)
	----			Begin
	----				update fin_leadmapping set empcode=@REASSIGNTO,assignedDate=getdate(),
	----				lastActivityDate=getdate(),leadTypeId=15 where leadCode = @LEADCODE
	----				insert into fin_LeadStatusHistory(leadCode,leadType,empCode,StatusDate,status)
	----													values(@LEADCODE,15,@EMPCODE,getdate(),1)
	----				insert into fin_LeadAllocationHistory (leadCode,AssignTo,AssignBy,assignedDate,reason,status)
	----													values(@LEADCODE,@REASSIGNTO,@EMPCODE,getdate(),@REASON,1)
	----				insert into fin_LeadActivityMonitor (leadCode,empCode,comment,activityId,actionDate,nextActionDate,status)
	----				values(@LEADCODE,@EMPCODE,'Re-Assigned to '+(select name from fin_Employee where empcode = @REASSIGNTO) +' By ' + 
	----				(select name from fin_Employee where empcode = @EMPCODE),3,getdate(),getdate(),1)
	----				Select 1 as result
	----			End
	----		Else 
	----		Begin Select 0 as result 
	----		End
	----	End
	----End
	IF (@Action = 'AUTO_ALLOCATE_LEADBYMATRIX')
	BEGIN
		DECLARE @currECode NVARCHAR(max)
		DECLARE @nextECode NVARCHAR(max)
		DECLARE @currPrio BIGINT
		DECLARE @topPrio BIGINT
		DECLARE @lastPrio INT
		DECLARE @currRecord INT
		DECLARE @adminECode NVARCHAR(max)

		IF EXISTS (
				SELECT *
				FROM fin_LeadMatrix
				)
		BEGIN
			--GET ADMIN EMPCODE--
			SELECT @lastPrio = max(priority)
			FROM fin_leadmatrix

			SELECT @adminECode = empcode
			FROM fin_leadmatrix
			WHERE priority = @lastPrio

			IF EXISTS (
					SELECT *
					FROM fin_LeadMatrix
					WHERE isnext = 1
					) --FOR FIRST TIME TO START R-ROBIN
			BEGIN
				--CHECK IF PLANNER IS NOT ADMIN--	
				IF EXISTS (
						SELECT *
						FROM fin_LeadMatrix
						WHERE STATUS = 1
							AND currentLead < maxlead
							AND empcode != @adminECode
						)
				BEGIN
					SELECT @currECode = empCode
					FROM fin_LeadMatrix
					WHERE STATUS = 1
						AND isnext = 1
						AND currentLead < maxlead
					ORDER BY priority ASC

					IF (@currECode IS NOT NULL)
						AND (LEN(@currECode) > 0)
					BEGIN
						-- LOGIC TO GET NEXT PRIORITY PLANNER --STARTS
						SELECT TOP 1 @topPrio = priority
						FROM fin_leadmatrix
						ORDER BY priority ASC

						SELECT @currPrio = priority
						FROM fin_leadmatrix
						WHERE empcode = @currECode

						SELECT @currRecord = id
						FROM fin_leadmatrix
						WHERE empcode = @currECode

						IF (@lastPrio = @currPrio)
						BEGIN
							SET @currPrio = 0;
						END

						IF EXISTS (
								SELECT *
								FROM fin_leadmatrix
								WHERE priority > @currPrio
									AND STATUS = 1
									AND currentLead < maxLead
								)
						BEGIN
							SELECT TOP 1 @nextECode = empcode
							FROM fin_leadmatrix
							WHERE priority > @currPrio
								AND STATUS = 1
								AND currentLead < maxLead
							ORDER BY priority ASC
								--print @nextECode
						END
						ELSE
						BEGIN
							SET @nextECode = (
									SELECT TOP 1 empcode
									FROM fin_leadmatrix
									WHERE STATUS = 1
										AND currentLead < maxLead
									ORDER BY priority ASC
									)
						END

						-- LOGIC TO GET NEXT PRIORITY PLANNER --ENDS
						IF (@nextECode IS NOT NULL)
							AND (LEN(@nextECode) > 0)
						BEGIN
							--print 'next code  - '+ cast(@nextECode as varchar)
							--print '@currECode code  - '+ cast(@currECode as varchar)
							BEGIN TRANSACTION

							UPDATE fin_LeadMatrix
							SET Isnext = 0
							WHERE empcode = @currECode

							UPDATE fin_LeadMatrix
							SET currentLead = currentLead + 1
							WHERE empcode = @currECode

							UPDATE fin_LeadMatrix
							SET STATUS = 0
							WHERE empcode = @currECode
								AND currentLead >= maxLead

							IF (
									(
										SELECT Count(*)
										FROM fin_LeadMatrix
										WHERE STATUS = 1
										) = 1
									)
							BEGIN
								UPDATE fin_LeadMatrix
								SET Isnext = 1
								WHERE empcode = @adminECode
							END
							ELSE
							BEGIN
								UPDATE fin_LeadMatrix
								SET Isnext = 1
								WHERE empcode = @nextECode
							END

							--LeadType (11 for NEW , 12 for ASSIGNED------
							INSERT INTO fin_LeadMapping (
								leadCode
								,empCode
								,leadTypeId
								,createdDate
								,assignedDate
								,lastActivityDate
								,STATUS
								)
							VALUES (
								@LEADCODE
								,@currECode
								,12
								,getdate()
								,getdate()
								,getdate()
								,1
								)

							INSERT INTO fin_LeadStatusHistory (
								leadCode
								,leadType
								,empCode
								,StatusDate
								,STATUS
								)
							VALUES (
								@LEADCODE
								,11
								,@currECode
								,getdate()
								,1
								)

							INSERT INTO fin_LeadStatusHistory (
								leadCode
								,leadType
								,empCode
								,StatusDate
								,STATUS
								)
							VALUES (
								@LEADCODE
								,12
								,@currECode
								,getdate()
								,1
								)

							INSERT INTO fin_LeadAllocationHistory (
								leadCode
								,AssignTo
								,AssignBy
								,assignedDate
								,reason
								,STATUS
								)
							VALUES (
								@LEADCODE
								,@currECode
								,'AUTO'
								,getdate()
								,'Auto Allocated By System'
								,1
								)

							INSERT INTO fin_LeadActivityMonitor (
								leadCode
								,empCode
								,comment
								,activityId
								,actionDate
								,nextActionDate
								,STATUS
								)
							VALUES (
								@LEADCODE
								,@currECode
								,'Auto Allocated By System'
								,0
								,getdate()
								,getdate()
								,1
								)

							SET @ASSIGNEDTOEMPCODE = @currECode

							COMMIT TRANSACTION
						END
					END
				END
				ELSE
				BEGIN
					----ALLOCATE ALL TO ADMIN---
					BEGIN TRANSACTION

					--LeadType (11 for NEW , 12 for ASSIGNED------
					INSERT INTO fin_LeadMapping (
						leadCode
						,empCode
						,leadTypeId
						,createdDate
						,assignedDate
						,lastActivityDate
						,STATUS
						)
					VALUES (
						@LEADCODE
						,@adminECode
						,12
						,getdate()
						,getdate()
						,getdate()
						,1
						)

					INSERT INTO fin_LeadStatusHistory (
						leadCode
						,leadType
						,empCode
						,StatusDate
						,STATUS
						)
					VALUES (
						@LEADCODE
						,11
						,@adminECode
						,getdate()
						,1
						)

					INSERT INTO fin_LeadStatusHistory (
						leadCode
						,leadType
						,empCode
						,StatusDate
						,STATUS
						)
					VALUES (
						@LEADCODE
						,12
						,@adminECode
						,getdate()
						,1
						)

					INSERT INTO fin_LeadAllocationHistory (
						leadCode
						,AssignTo
						,AssignBy
						,assignedDate
						,reason
						,STATUS
						)
					VALUES (
						@LEADCODE
						,@adminECode
						,'AUTO'
						,getdate()
						,'Auto Allocated By System'
						,1
						)

					INSERT INTO fin_LeadActivityMonitor (
						leadCode
						,empCode
						,comment
						,activityId
						,actionDate
						,nextActionDate
						,STATUS
						)
					VALUES (
						@LEADCODE
						,@adminECode
						,'Auto Allocated By System'
						,0
						,getdate()
						,getdate()
						,1
						)

					SET @ASSIGNEDTOEMPCODE = @adminECode

					COMMIT TRANSACTION
				END
			END
			ELSE
			BEGIN
				--Print 'FIRST TIME RROBIN'
				--====GET INITIAL PLANNER TO START ROUND ROBIN FOR FIRST TIME===---
				SELECT TOP 1 @currECode = empcode
				FROM fin_leadmatrix
				ORDER BY priority ASC

				-- LOGIC TO GET NEXT PRIORITY PLANNER --STARTS
				SELECT TOP 1 @topPrio = priority
				FROM fin_leadmatrix
				ORDER BY priority ASC

				SELECT @currPrio = priority
				FROM fin_leadmatrix
				WHERE empcode = @currECode

				SELECT @currRecord = id
				FROM fin_leadmatrix
				WHERE empcode = @currECode

				IF (@lastPrio = @currPrio)
				BEGIN
					SET @currPrio = 0;
				END

				IF EXISTS (
						SELECT *
						FROM fin_leadmatrix
						WHERE priority > @currPrio
							AND STATUS = 1
							AND currentLead < maxLead
						)
				BEGIN
					SELECT TOP 1 @nextECode = empcode
					FROM fin_leadmatrix
					WHERE priority > @currPrio
						AND STATUS = 1
						AND currentLead < maxLead
					ORDER BY priority ASC
						--print @nextECode
				END
				ELSE
				BEGIN
					SELECT TOP 1 @nextECode = empcode
					FROM fin_leadmatrix
					WHERE STATUS = 1
						AND currentLead < maxLead
					ORDER BY priority ASC
						--print @nextECode
				END

				-- LOGIC TO GET NEXT PRIORITY PLANNER --ENDS
				--print 'step - LOGIC TO GET NEXT PRIORITY PLANNER'
				BEGIN TRANSACTION

				UPDATE fin_LeadMatrix
				SET Isnext = 1
				WHERE empcode = @nextECode

				UPDATE fin_LeadMatrix
				SET Isnext = 0
				WHERE empcode = @currECode

				UPDATE fin_LeadMatrix
				SET currentLead = currentLead + 1
				WHERE empcode = @currECode

				IF (
						(
							SELECT Count(*)
							FROM fin_LeadMatrix
							WHERE STATUS = 1
							) = 1
						)
				BEGIN
					UPDATE fin_LeadMatrix
					SET Isnext = 1
					WHERE empcode = @adminECode
				END
				ELSE
				BEGIN
					UPDATE fin_LeadMatrix
					SET Isnext = 1
					WHERE empcode = @nextECode
				END

				--LeadType (11 for NEW , 12 for ASSIGNED------
				INSERT INTO fin_LeadMapping (
					leadCode
					,empCode
					,leadTypeId
					,createdDate
					,assignedDate
					,lastActivityDate
					,STATUS
					)
				VALUES (
					@LEADCODE
					,@currECode
					,12
					,getdate()
					,getdate()
					,getdate()
					,1
					)

				INSERT INTO fin_LeadStatusHistory (
					leadCode
					,leadType
					,empCode
					,StatusDate
					,STATUS
					)
				VALUES (
					@LEADCODE
					,11
					,@currECode
					,getdate()
					,1
					)

				INSERT INTO fin_LeadStatusHistory (
					leadCode
					,leadType
					,empCode
					,StatusDate
					,STATUS
					)
				VALUES (
					@LEADCODE
					,12
					,@currECode
					,getdate()
					,1
					)

				INSERT INTO fin_LeadAllocationHistory (
					leadCode
					,AssignTo
					,AssignBy
					,assignedDate
					,reason
					,STATUS
					)
				VALUES (
					@LEADCODE
					,@currECode
					,'AUTO'
					,getdate()
					,'Auto Allocated By System'
					,1
					)

				INSERT INTO fin_LeadActivityMonitor (
					leadCode
					,empCode
					,comment
					,activityId
					,actionDate
					,nextActionDate
					,STATUS
					)
				VALUES (
					@LEADCODE
					,@currECode
					,'Auto Allocated By System'
					,0
					,getdate()
					,getdate()
					,1
					)

				SET @ASSIGNEDTOEMPCODE = @currECode

				COMMIT TRANSACTION
			END
		END
	END

	IF (@Action = 'SelectAllRM')
	BEGIN
		SELECT EmpCode
			,Name
		FROM fin_employee
		WHERE DepId = 'DEP2016112313947'
			AND RoleId = 'ERL2016112315525'
	END

	IF (@Action = 'Select_LeadActivityType')
	BEGIN
		SELECT *
		FROM fin_ActivityTypeMaster
		WHERE activityid != 0
			AND STATUS = 1
	END

	IF (@Action = 'Select_LeadType')
	BEGIN
		SELEct *
		FROM fin_LeadTypeMaster
		WHERE leadTypeId IN (
				13
				,16
				,17,14
				)
	END

	IF (@Action = 'Select_DepartmentList')
	BEGIN
		SELECT *
		FROM fin_Departments
		WHERE STATUS = 1;
	END

	IF (@Action = 'Select_IsDepartmentExist')
	BEGIN
		SELECT *
		FROM fin_Departments
		WHERE rtrim(ltrim(name)) = rtrim(ltrim(@Name));
	END

	IF (@Action = 'Select_CountryList')
	BEGIN
		SELECT *
		FROM countrylist
	END

	IF (@Action = 'Select_EmployeeRoleList')
	BEGIN
		SELECT Id
			,Code
			,DepCode
			,DeptName
			,ROLE
			,LEVEL
			,CreatorEmail
			,STATUS
			,CreatedDate
			,UpdatedDate
		FROM vw_ManageRoleList
		WHERE STATUS = 1;
	END

	IF (@Action = 'Select_EmpRoleByID')
	BEGIN
		SELECT Id
			,Code
			,DepCode
			,DeptName
			,ROLE
			,LEVEL
			,CreatorEmail
			,STATUS
			,CreatedDate
			,UpdatedDate
		FROM vw_ManageRoleList
		WHERE Id = @Id
			AND STATUS = 1;
	END

	IF (@Action = 'Select_GoalList')
	BEGIN
		SELECT GName AS name
			,GCode AS code
		FROM fin_Goal
		WHERE gtype = 1
	END

	if(@Action='Select_IsEmployeeSupport')
	begin
	    if exists (select count(e.UserID)  from fin_Departments d inner join fin_Employee e on ltrim(rtrim(d.Code))=rtrim(ltrim(e.DepId)) inner join fin_EmployeeRole r 
	    on rtrim(ltrim(e.RoleId))=rtrim(ltrim(r.Code)) where rtrim(ltrim(r.Role)) like '%support%' and UserID=@userid)
		begin
		 select 1
		end
		else
		begin
		 select 0
		end

	end
	IF (@Action = 'Select_EmployeeRoleListByDept')
	BEGIN
		SELECT Id
			,Code
			,DepCode
			,DeptName
			,ROLE
			,LEVEL
			,CreatorEmail
			,STATUS
			,CreatedDate
			,UpdatedDate
		FROM vw_ManageRoleList
		WHERE STATUS = 1  and DepCode=@depcode;
	END
	-----------this department list for ticketing system where department list will show excluding login user dept------------------
	IF (@Action = 'Select_TktDepartmentListByOther')
	BEGIN
		select d.Code,d.Name from fin_Departments d where Code not in(select e.DepId  from fin_Employee e where UserID=@userid) and d.Status=1 and d.Name not like '%admin%'
	END
	--------------------------------end----------------------------------------------------
	------------------------------this designation list for ticketing system where designation list will show related to login user dept--------------------------
	IF (@Action = 'Select_TktDesignationListBySame')
	BEGIN
		select distinct  Code,Role from fin_EmployeeRole where DepCode  in(select e.DepId  from fin_Employee e where UserID=@userid)
	END
	---------------------------------end----------------------------------------------------------------------------
	------------------------------this employee list for ticketing system where employee list will show on selected designation-----------------------------
	IF (@Action = 'Select_TktEmployeeListBySame')
	BEGIN
		select UserID,Name from fin_employee e inner join  fin_EmployeeRole r  on e.RoleId=r.Code
		WHERE r.Code=@userid---------------here @userid is the role id
	END
	---------------------------------end----------------------------------------------------------------------------
	IF (@Action = 'Select_TktReportTolistBySame')
	BEGIN
		select top 1 r.Id,rtrim(ltrim(ReportToEmail)) as ReportToEmail from fin_Employee r where r.UserID in(
		select  e.UserID  from fin_Employee e 
		WHERE  e.UserID=@userid)

		
	END
END















GO

USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[AddUpdateDeleteDepartments]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[AddUpdateDeleteDepartments]
 @DepId int=0,
@Name varchar(30)='',
@DepCode varchar(50)='',
@Action varchar(50)=null
as
begin



if(@Action='INSERT')
begin
insert into fin_Departments(Code,Name) values(
'DEP'+CONVERT(varchar,DATEPART(yy,getdate()))+
CONVERT(varchar,datepart(mm,getdate())) +
CONVERT(varchar,datepart(dd,getdate()))+
CONVERT(varchar,datepart(HH,getdate()))+
CONVERT(varchar,datepart(MI,getdate()))+
CONVERT(varchar,datepart(SS,getdate())),
@Name
)
end
if(@Action='Delete')
begin

update fin_Departments set Status=0 where Id=@DepID

end

if(@Action='Update')
begin
update fin_Departments set Code=@DepCode,Name=@Name where Id=@DepId
end
end


GO

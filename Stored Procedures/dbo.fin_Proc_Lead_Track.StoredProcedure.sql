USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_Lead_Track]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[fin_Proc_Lead_Track]
	
	@TrackerVar nvarchar(500)=null,

	@TrackerVal nvarchar(500)=null,

	@TrackerRoute nvarchar(500)=null,

	@TrackerIp nvarchar(500)=null,

	@CL_UserId nvarchar(500)=null,

	@CL_Campaign_Key nvarchar(500)=null,

	@CL_Goal nvarchar(500)=null,

	@action nvarchar(500)=null

as

begin
if(@action='landing_page_user_track')
	begin
	declare @track int ,@userip_count_track int
		set @track=(select count(*) from fp_LeadTracker where TrackerVar=@TrackerVar and TrackerVal=@TrackerVal and TrackerRoute=@TrackerRoute and TrackerIp=@TrackerIp)
		set @userip_count_track=(select count(*) from fin_IPLocation where IPAddress=@TrackerIp)
		if(@track<1)
			begin
				insert into fp_LeadTracker(TrackerVar,TrackerVal,TrackerRoute,TrackerIp,TrackingStatus) 
				values(@TrackerVar,@TrackerVal,@TrackerRoute,@TrackerIp,1)
			end
		if(@userip_count_track<1)
			begin
				insert into fin_IPLocation(IPAddress)values(@TrackerIp)
			end
	end

if(@action='landing_page_user_register')
	begin
		declare @user int ,@userip_count int
		set @user=(select count(*) from fp_Campaign_Leads where CL_UserId=@CL_UserId and CL_Campaign_Key=@CL_Campaign_Key)
		set @userip_count=(select count(*) from fin_IPLocation where IPAddress=@TrackerIp)
		if(@user<1)
			begin
				insert into fp_Campaign_Leads(CL_UserId,CL_Campaign_Key,CL_CreatedDate,CL_UpdatedDate,CL_Status,CL_IP)
				values(@CL_UserId,@CL_Campaign_Key,getdate(),getdate(),1,@TrackerIp)
			end
		if(@userip_count<1)
			begin
				insert into fin_IPLocation(IPAddress)values(@TrackerIp)
			end
	end

if(@action='landing_page_usergoal_save')
	begin
		
		insert into [fin_landingpage_usergoals](userid,campaign_key,goalcode,[status])
				values(@CL_UserId,@CL_Campaign_Key,@CL_Goal,1)
end

End
GO

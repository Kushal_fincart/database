USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_MigrateGoalDataInvestwell]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[fin_Proc_MigrateGoalDataInvestwell]
@Action [nvarchar](max)
as
Begin
if(@Action='MIGRATE')
begin

DECLARE @tmpOtherTrans TABLE (schemefrom nvarchar(max) NOT NULL,
schemeto nvarchar(max) NULL,
calamount nvarchar(max) NOT NULL,
foliono nvarchar(max) NULL
)


declare
@GoalName nvarchar(max),
@OtherGoalName nvarchar(max),
@TimeHorizon nvarchar(max),
@GetAmount nvarchar(max),
@InvestAmount nvarchar(max),
@FundName nvarchar(max),
@hideFundCode nvarchar(max),
@SchemeName nvarchar(max),
@SchemeCode nvarchar(max),
@TotalCurrentValue nvarchar(max),
@sipAmount nvarchar(max),
@sipDate nvarchar(max),
@sipStartDate nvarchar(max),
@Foliono nvarchar(max),
@ClientEmail nvarchar(max),
@ClientName nvarchar(max),
@ChildName1 nvarchar(max),
@ChildName2 nvarchar(max),
@ChildName3 nvarchar(max),
@Child1Age nvarchar(max),
@Child2Age nvarchar(max),
@Child3Age nvarchar(max),
@Child1Gender nvarchar(max),
@Child2Gender nvarchar(max),
@Child3Gender nvarchar(max),
@RetirementAge nvarchar(max)


DECLARE goal_cursor CURSOR FOR

  SELECT GoalName,
OtherGoalName,
TimeHorizon,
GetAmount,
InvestAmount,
FundName,
hideFundCode,
SchemeName,
SchemeCode,
TotalCurrentValue,
sipAmount,
sipDate,
sipStartDate,
Foliono,
ClientEmail,
ClientName,
ChildName1,
ChildName2,
ChildName3,
Child1Age,
Child2Age,
Child3Age,
Child1Gender,
Child2Gender,
Child3Gender,
RetirementAge
       FROM [dbo].[goalDataSheet] order by clientEmail asc

       OPEN goal_cursor
       FETCH NEXT FROM goal_cursor INTO
      @GoalName,
@OtherGoalName,
@TimeHorizon,
@GetAmount,
@InvestAmount,
@FundName,
@hideFundCode,
@SchemeName,
@SchemeCode,
@TotalCurrentValue,
@sipAmount,
@sipDate,
@sipStartDate,
@Foliono,
@ClientEmail,
@ClientName,
@ChildName1,
@ChildName2,
@ChildName3,
@Child1Age,
@Child2Age,
@Child3Age,
@Child1Gender,
@Child2Gender,
@Child3Gender,
@RetirementAge
     
       WHILE @@FETCH_STATUS = 0
       BEGIN
       Declare @USERGOAL_ID bigint
       Declare @NXTSIPDATE date
       Declare @FINALENDDATE date
       DECLARE @CHILDCOUNT INT = 0;
       DECLARE @CHILDNAME nvarchar(max);
       DECLARE @CHILDAGE nvarchar(max);
       DECLARE @CHILDGENDER nvarchar(max);          
       DECLARE @GUID nvarchar(max);
       DECLARE @AMOUNTCODE nvarchar(max);
       DECLARE @TIMECODE nvarchar(max);
       DECLARE @PRODUCTLIST nvarchar(max);

       DECLARE @OTHRGOALNAME nvarchar(max);
       DECLARE @OTHRGOALCODE nvarchar(max);

       DECLARE @FINALTOTALCURRENTVALUE INT;
       DECLARE @STPTOTALAMT INT;
       DECLARE @SWITCHTOTALAMT INT;
       DECLARE @RDMTTOTALAMT INT;
       DECLARE @SWPTOTALAMT INT;

       DECLARE @STPFROMSCHEME nvarchar(max);
       DECLARE @SWPFROMSCHEME nvarchar(max);
       DECLARE @SWITCHFROMSCHEME nvarchar(max);
       DECLARE @RDMPTFROMSCHEME nvarchar(max);

       DECLARE @STPTOSCHEME nvarchar(max);
       DECLARE @SWPTOSCHEME nvarchar(max);
       DECLARE @SWITCHTOSCHEME nvarchar(max);
       DECLARE @RDMPTTOSCHEME nvarchar(max);

       DECLARE @CHKDATE date;
       DECLARE @YEAR varchar(20);
       DECLARE @MONTH varchar(20);
       DECLARE @DAY varchar(20);

                            SET @CHILDCOUNT = 0;
                            SET @CHILDNAME = NULL;
                            SET @CHILDAGE = NULL;
                            SET @CHILDGENDER = NULL;

                    PRINT 'GOAL ==>>'+@GoalName
                    PRINT 'FUNDCODE-'+@hideFundCode+ ' SCHEMECODE-'+@SchemeCode +' FOLOI-'+ @FolioNo
                    --IF NOT EXISTS(select * from fin_Other_Transactions_Details where userid=@ClientEmail)
                    BEGIN
                    BEGIN----GET CHILD DETAILS----
                    IF(@ChildName1 IS NOT NULL AND @ChildName1 !='')
                        BEGIN
                            SET @CHILDCOUNT = @CHILDCOUNT + 1;
                            SET @CHILDNAME = @ChildName1;
        SET @CHILDAGE = @Child1Age;
                            SET @CHILDGENDER = @Child1Gender;

                                IF(@ChildName2 IS NOT NULL AND @ChildName2 !='')
           BEGIN
                   SET @CHILDCOUNT = @CHILDCOUNT + 1;
                                        SET @CHILDNAME = @ChildName2;
                                        SET @CHILDAGE = @Child2Age;
                                        SET @CHILDGENDER = @Child2Gender;
                                        IF(@ChildName3 IS NOT NULL AND @ChildName3 !='')
                                            BEGIN
                                                    SET @CHILDCOUNT = @CHILDCOUNT + 1;
                                                    SET @CHILDNAME = @ChildName3;
                                                    SET @CHILDAGE = @Child3Age;
                                                    SET @CHILDGENDER = @Child3Gender;
                                                END
                                    END
                        END

                    --GET PRODUCT LIST---
                     SET @AMOUNTCODE = 'F1';
                     
                     IF (CAST(@InvestAmount AS INT) <= 10000)
                      BEGIN
                        SET @AMOUNTCODE = 'F1';
                      END
                     ELSE IF(CAST(@InvestAmount AS INT) <= 20000)
                      BEGIN
                        SET @AMOUNTCODE = 'F2';
                      END
                     ELSE
                        BEGIN
                        SET @AMOUNTCODE = 'F4';
                        END
                     IF (CAST(@TimeHorizon AS float) <= 1)
                      BEGIN
                        SET @TIMECODE = '0';
                      END
                     ELSE IF(CAST(@TimeHorizon AS float) <= 3)
                      BEGIN
                        SET @TIMECODE = '1';
                      END
                     ELSE IF(CAST(@TimeHorizon AS float) <= 5)
                      BEGIN
                        SET @TIMECODE = '3';
                      END
                     ELSE IF(CAST(@TimeHorizon AS float) <= 10)
                      BEGIN
                        SET @TIMECODE = '5';
                      END
                     ELSE
                        BEGIN
                        SET @TIMECODE = '10';
                        END
                     
                     
                     IF((select Count(*) from [dbo].[fin_Goal_Products] where GoalCode in (Select gcode from fin_goal where gname = @GoalName))>0)
                        BEGIN
                        PRINT 'TST-1'
                            SET @PRODUCTLIST = (select ProductCode from [dbo].[fin_Goal_Products] where GoalCode in(Select gcode from fin_goal where gname = @GoalName) and
                             RiskCode='M' and TimeCode=@TIMECODE and AmountCode=@AMOUNTCODE) + '~' + @InvestAmount
                        END
                     ELSE
                        BEGIN
                        PRINT 'TST-2'
                        PRINT 'TCODE -'+ @TimeCode +' '+ 'AMTCODE-'+@AmountCode +' InvstAmt - '+@InvestAmount
                            SET @PRODUCTLIST =(select ProductCode from [dbo].[fin_Goal_Products] where GoalCode='FG4' and
                            RiskCode='M' and TimeCode=@TimeCode and AmountCode=@AmountCode) + '~' + @InvestAmount
                            PRINT @PRODUCTLIST
                        END
                    END

                  
                   
                    IF NOT EXISTS(select top 1 * from fin_User where UserID=@ClientEmail) -----IF NOT EXIST IN fin_User
                    Begin
                    PRINT 'TST-3'
                   

                    insert into fin_User(UUID,UserID,Name,Mobile,Age,Gender,MaritialStatus,AnnualIncome,MonthlyExpence,
RetirementAge,LifeExpentency,Spouse,ChildCount,Emi,EmiAmount,Loans,CreatedByEmail,
CreatedByUUID,CreatedDatetime,UpdatedByEmail,UpdatedByUUID,UpdatedDatetime,
Status)
 select NULL,@ClientEmail,@ClientName,(select mobile1 from fp_prospects where userid = @ClientEmail),
(select DATEDIFF(hour,date_of_birth,GETDATE())/8766 from fin_CAF_BasicDetails where userid=@ClientEmail and upper(REPLACE(clientName, ' ', '')) = upper(REPLACE(@ClientName, ' ', ''))),
(select CASE WHEN Gender='F' THEN '002' WHEN Gender='M' THEN '001' ELSE NULL END from fp_prospects where UserID=@ClientEmail),
'001',
(select CASE WHEN BS.Annual_income=31 THEN '100000' WHEN BS.Annual_income=32 THEN '300000' WHEN BS.Annual_income=33 THEN '700000' WHEN BS.Annual_income=34 THEN '1700000' WHEN BS.Annual_income=35 THEN '5000000' WHEN BS.Annual_income=36 THEN 'ABOVE 10000000' ELSE 0 END from fin_CAF_BasicDetails BS where BS.userid=@ClientEmail and upper(REPLACE(BS.clientName, ' ', '')) = upper(REPLACE(@ClientName, ' ', ''))),
'0',(CASE WHEN @RetirementAge IS NOT NULL AND @RetirementAge!='' AND @RetirementAge != '0' THEN @RetirementAge ELSE 60 END),80,
0,@CHILDCOUNT,0,'0',0,@ClientEmail,NULL,getdate(),@ClientEmail,NULL,getdate(),1

                    PRINT 'ERR'
                    End
                    ELSE
                    BEGIN
                            IF(@CHILDNAME IS NOT NULL OR @CHILDNAME !='')
                            BEGIN
                            PRINT 'TST-4'
                                IF NOT EXISTS(select * from fin_User_Relation where userid = @ClientEmail and name=@CHILDNAME)
                                BEGIN update fin_User set ChildCount= ChildCount + @CHILDCOUNT where userid=@ClientEmail END
                            END
                    END
                   
                   
                   
                    --BELOW FLOW FOR CHECK DUPLICATE ENTRY IN CART AND GOAL TABLE
                    IF NOT EXISTS(select * from [dbo].[fin_User_Goals] ug join fin_cart fc on ug.userid=fc.userid where ug.userid = @ClientEmail
                    and fc.fundcode = @hideFundCode and fc.schemeCode=@SchemeCode and fc.folioNo = @FolioNo and ug.GoalCode in (Select gcode from fin_goal where gname = @GoalName) and fc.amount = @TotalCurrentValue and fc.firstsipDate = @sipStartDate and fc.status = 1)
                    BEGIN


                   


                    --***********CHECK IF USER HAS RECORDS IN OTHER TRANASTION TABLE***********************************
                    IF EXISTS(select * from [dbo].[fin_Other_Transactions_Details] where userid = @ClientEmail)
                        BEGIN
                           
                            --******************************** SECOND CURSOR*******************************************
                            --******************************* STARTS HERER*********************************************
                            PRINT 'INNER CURSOR START'
                            Declare @OTDOtherTxnID nvarchar(max),
                            @OTDuserId  nvarchar(max),
                            @OTDmemberId  nvarchar(max),
                            @OTDprofileId  nvarchar(max),
                            @OTDgoalCode nvarchar(max),
                            @OTDfundCode nvarchar(max),
                            @OTDschemeCode nvarchar(max),
                            @OTDtoSchemeCode nvarchar(max),
                            @OTDscheme_name nvarchar(max),
                            @OTDtoscheme_name nvarchar(max),
                            @OTDinvest_profile nvarchar(max),
                            @OTDentryDate nvarchar(max),
                            @OTDunits nvarchar(max),
                            @OTDdivOption nvarchar(max),
                            @OTDfolioNo nvarchar(max),
                            @OTDnavDate nvarchar(max),
                            @OTDstartDate nvarchar(max),
                            @OTDendDate nvarchar(max),
                    @OTDnoOfInstallment nvarchar(max),
                            @OTDamount nvarchar(max),
                            @OTDfrequency nvarchar(max),
                            @OTDtype nvarchar(max),
                            @OTDbank nvarchar(max),
                            @OTDbranch nvarchar(max),
                            @OTDaccountNo nvarchar(max),
                            @OTDmandateId nvarchar(max),
                            @OTDmDate nvarchar(max),
                            @OTDUserGoalId nvarchar(max)
                            DECLARE CART_CURSOR CURSOR FOR

                                select
                                OtherTxnID,
                                userId,
                                memberId,
                                profileId,
                                goalCode,
                                fundCode,
                                schemeCode,
                                toSchemeCode,
                                scheme_name,
                                toscheme_name,
                                invest_profile,
                                entryDate,
                                units,
                                divOption,
                                folioNo,
                                navDate,
                                startDate,
                                endDate,
                                noOfInstallment,
                                amount,
                                frequency,
                                type,
                                bank,
                                branch,
                                accountNo,
                                mandateId,
                                mDate,
                                UserGoalId from [fin_Other_Transactions_Details] where userid = @ClientEmail and schemeCode=@SchemeCode
                            OPEN CART_CURSOR
                            FETCH NEXT FROM CART_CURSOR INTO
                                        @OTDOtherTxnID,
                                @OTDuserId,
                                @OTDmemberId,
                                @OTDprofileId,
                                @OTDgoalCode,
                                @OTDfundCode,
                                @OTDschemeCode,
                                @OTDtoSchemeCode,
                                @OTDscheme_name,
                                @OTDtoscheme_name,
                                @OTDinvest_profile,
                                @OTDentryDate,
                                @OTDunits,
                                @OTDdivOption,
                                @OTDfolioNo,
                                @OTDnavDate,
                                @OTDstartDate,
                                @OTDendDate,
                                @OTDnoOfInstallment,
                                @OTDamount,
                                @OTDfrequency,
                                @OTDtype,
                                @OTDbank,
                                @OTDbranch,
                                @OTDaccountNo,
                                @OTDmandateId,
                                @OTDmDate,
                                @OTDUserGoalId
                            WHILE @@FETCH_STATUS = 0
                                BEGIN
                               
                                PRINT 'LOOP RUN FOR SCHEMECODE-->'+@SchemeCode   
                                    --############ STP CHECK #########
                            IF (@OTDtype='STP')
                                BEGIN
                               
                                        DECLARE @STPDONE INT;
                                       
                                        PRINT 'STP FLOW ENTR'

                            SET @YEAR = year(getdate())
                                        SET @MONTH = month(getdate())
                                        SET @DAY = @OTDmDate

                                        SET @CHKDATE = Cast((@YEAR + '-' + @MONTH  + '-' + @DAY) as date)
                                       
                                        PRINT '--------BELOW DATE DIFF-----'
                                        PRINT getdate() --2017-10-31
                                        PRINT @CHKDATE  --2017-10-15
                                        PRINT '--------END OF DATE DIFF-----'

										SET @STPDONE = (select CONVERT(INT,DATEDIFF(MONTH, @OTDstartDate, getdate())))


                                        IF (getdate() > @CHKDATE)
                                                Begin
                                                SET @STPDONE = (select CONVERT(INT,DATEDIFF(MONTH, @OTDstartDate, @CHKDATE)+1))
                                                End

                                            ELSE
                                                BEGIN 
                                                SET @STPDONE = (select CONVERT(INT,DATEDIFF(MONTH, @OTDstartDate, @CHKDATE)-1))
                                                END

                                        SET @STPTOTALAMT = (SELECT CONVERT(INT,CONVERT(INT, @OTDamount) * @STPDONE))
                                        SET @STPFROMSCHEME = @OTDschemeCode
                                        SET @STPTOSCHEME  = @OTDtoSchemeCode

                                        PRINT 'STPDONE AMT-->'+ CONVERT(nvarchar,@STPTOTALAMT)
                                END
                            ELSE
                            BEGIN
                                SET @STPTOTALAMT = 0;
                            END
                            --########## END OF STP


                            --############ SWITCH CHECK #########
                            IF (@OTDtype='SWITCH')
                                BEGIN
                                       
                                        SET @SWITCHTOTALAMT = CAST(FLOOR(@OTDamount) as INT)

                                        SET @SWITCHFROMSCHEME  = @OTDschemeCode
                                        SET @SWITCHTOSCHEME   = @OTDtoSchemeCode
                                END
                            ELSE
                            BEGIN
                                SET @SWITCHTOTALAMT = 0;
                            END
                            --########## END OF SWITCH

                            --############ SWP CHECK #########
                            IF (@OTDtype='SWP')
                                BEGIN
                               
                                        DECLARE @SWPDONE INT;
                                       
                                        PRINT 'SWP FLOW ENTR'

                                        SET @YEAR = year(getdate())
                                        SET @MONTH = month(getdate())
                                        SET @DAY = @OTDmDate

                                        SET @CHKDATE = Cast((@YEAR + '-' + @MONTH  + '-' + @DAY) as date)
                                        PRINT '--------BELOW DATE DIFF-----'
                                        PRINT getdate()
                                        PRINT @CHKDATE
                                        PRINT '--------END OF DATE DIFF-----'

                                        IF (getdate() > @CHKDATE)
                                                Begin
                                                SET @SWPDONE = (select CONVERT(INT,DATEDIFF(MONTH, @OTDstartDate, @CHKDATE)+1))
                                                End

                                            ELSE
                                                BEGIN 
                                                SET @SWPDONE = (select CONVERT(INT,DATEDIFF(MONTH, @OTDstartDate, @CHKDATE)-1))
                                                END

                                        SET @SWPTOTALAMT = (SELECT CONVERT(INT,CONVERT(INT, @OTDamount) * @SWPDONE))
                                        SET @SWPFROMSCHEME = @OTDschemeCode
                                        SET @SWPTOSCHEME  = @OTDtoSchemeCode

                                        PRINT 'SWPDONE AMT-->'+ CONVERT(nvarchar,@SWPTOTALAMT)
                                END
                            ELSE
                            BEGIN
                                SET @SWPTOTALAMT = 0;
                            END
                            --########## END OF SWP

                            --############ REDEMPTION CHECK #########
                            IF (@OTDtype='R')
                                BEGIN
										PRINT 'ERR----CAST'
										PRINT FLOOR(@OTDamount)
                                        SET @RDMTTOTALAMT = CAST(FLOOR(@OTDamount) as INT)

                                        SET @RDMPTFROMSCHEME   = @OTDschemeCode
                                        SET @RDMPTTOSCHEME   = @OTDtoSchemeCode
                                END
                            ELSE
                            BEGIN
                                SET @RDMTTOTALAMT = 0;
                            END
                            --########## END OF REDEMPTION

                            --CALCALUATE
                       
                        SET @FINALTOTALCURRENTVALUE = (@STPTOTALAMT + @SWITCHTOTALAMT + @RDMTTOTALAMT + @SWPTOTALAMT)
                        PRINT @FINALTOTALCURRENTVALUE
                        PRINT '-----ABOVE IS FINALTOALVALUE---AFTER ADD STP SWP RDM SWITCH'

                               
                                insert into @tmpOtherTrans(schemefrom,schemeto,calamount,folioNo) values(@OTDschemeCode,@OTDtoSchemeCode,
								@FINALTOTALCURRENTVALUE,@OTDfolioNo)
                               

                                FETCH NEXT FROM CART_CURSOR INTO @OTDOtherTxnID,
                                @OTDuserId,
                                @OTDmemberId,
                                @OTDprofileId,
                                @OTDgoalCode,
                                @OTDfundCode,
                                @OTDschemeCode,
                                @OTDtoSchemeCode,
                                @OTDscheme_name,
                                @OTDtoscheme_name,
                                @OTDinvest_profile,
                                @OTDentryDate,
                                @OTDunits,
                                @OTDdivOption,
                                @OTDfolioNo,
                                @OTDnavDate,
                                @OTDstartDate,
                                @OTDendDate,
                                @OTDnoOfInstallment,
                                @OTDamount,
                                @OTDfrequency,
                                @OTDtype,
                                @OTDbank,
                                @OTDbranch,
                                @OTDaccountNo,
                                @OTDmandateId,
                                @OTDmDate,
                                @OTDUserGoalId
                                END
                            close CART_CURSOR
                            deallocate CART_CURSOR
                           

                        END
                    ELSE
                        BEGIN
                           
                            SET @FINALTOTALCURRENTVALUE = @TotalCurrentValue
                            PRINT 'ELSE CASE --FINALAMT-->'+ CONVERT (nvarchar,@FINALTOTALCURRENTVALUE)
                        END



                       


                       

                        PRINT 'TST-5'
                        PRINT 'TST-START CHILD START'
                        --####################### CHILD GOAL #################
                        IF(@GoalName = 'ChlidStudy' OR @GoalName = 'ChlidWedding')
                                BEGIN------IF RELATION GOAL FOR CHILD----
                                   PRINT 'CHILD FLOW'
                                    -------CHILD #
                                    IF(@CHILDNAME IS NOT NULL AND @CHILDNAME != '')
                                    BEGIN
                                        IF NOT EXISTS(select * from fin_User_Relation where userid = @ClientEmail and name=@CHILDNAME)
                                        Begin
                                         PRINT 'CHILD RELATION INSERT'
                                        SET @GUID = NEWID();      
                                        -----INSERT RECORD IN FIN_RELATION TABLE
                                        insert into [fin_User_Relation](UUID,UserID,Relation,RelationID,
                                        Name,Mobile,Age,Gender,AnnualIncome,CreatedByEmail,CreatedByUUID,CreatedDatetime,
                                        UpdatedByEmail,UpdatedByUUID,UpdatedDatetime,Status) select NULL,@ClientEmail,'002',
                                        @GUID,@CHILDNAME,NULL,@CHILDAGE,(CASE WHEN @CHILDGENDER='MALE' THEN '001' WHEN
                                         @CHILDGENDER='FEMALE' THEN '002' ELSE NULL END),0,@ClientEmail,NULL,getdate(),@ClientEmail,
                                         NULL,getdate(),1
                                        End

                                        -----INSERT RECORD IN fin_User_GOAL TABLE
                                        PRINT 'CHILD USERGOAL INSERT'
                                        IF NOT EXISTS(select * from fin_User_Goals where userid = @ClientEmail and
                                        relationid in (select relationid from fin_User_Relation where name = @CHILDNAME) and goalcode in (Select gcode from fin_goal where gname = @GoalName))
                                       
                                            BEGIN --#####################
                                            PRINT 'TST-6'
                                                insert into fin_User_Goals (UUID,UserID,RelationID,GoalCode,TypeCode,People,Location,
                                                Place,MonthlyAmount,Amount,Start,[End],Duration,Risk,InvestmentType,GetAmount,InvestAmount,
                                                ProductList,ActivatedDatetime,CompletedDatetime,CreatedByEmail,CreatedByUUID,
                                                CreatedDatetime,UpdatedByEmail,UpdatedByUUID,UpdatedDatetime,Status)
                                                select NULL,@ClientEmail,
                                                (CASE WHEN @GoalName = 'ChlidStudy' OR @GoalName = 'ChlidWedding' THEN @GUID ELSE NULL END),
                                                (Select gcode from fin_goal where gname = @GoalName),(CASE WHEN @GoalName='ChlidWedding'
                                                THEN NULL ELSE '001' END),'0',(CASE WHEN @GoalName='ChlidStudy' THEN '001' ELSE NULL END),
                                                (CASE WHEN @GoalName='ChlidStudy' THEN 'USA OR CANADA' ELSE NULL END),'0',@GetAmount,'0','0',
                                                @TimeHorizon,'M',(CASE WHEN @SipAmount!='' AND @SipAmount IS NOT NULL THEN 'S' ELSE 'L' END),
                                                @GetAmount,@InvestAmount,@PRODUCTLIST,NULL,NULL,@ClientEmail,NULL,getdate(),@ClientEmail,
                                                NULL,getdate(),1

                                                SET @USERGOAL_ID = SCOPE_IDENTITY();
                                            END
                                            ELSE
                                           
                                                BEGIN
PRINT 'TST-7'
                                               
                                                SET @USERGOAL_ID = (select id from fin_User_Goals where userid = @ClientEmail and
                                                relationid in (select relationid from fin_User_Relation where name = @CHILDNAME) and goalcode in
                                                (Select gcode from fin_goal where gname = @GoalName))
                                                IF NOT EXISTS(select * from fin_User_Goals where userid = @ClientEmail and
                                                    relationid in(select relationid from fin_User_Relation where name = @CHILDNAME) and
                                                    goalcode in (Select gcode from fin_goal where gname = @GoalName) and InvestmentType='S')
                                                    BEGIN
                                                update fin_User_Goals set InvestmentType=(CASE WHEN @SipAmount!='' AND
                                                        @SipAmount IS NOT NULL THEN 'S' ELSE 'L' END) where userid = @ClientEmail and
                                                    relationid in(select relationid from fin_User_Relation where name = @CHILDNAME) and
                                                    goalcode in (Select gcode from fin_goal where gname = @GoalName)
                                                    END
                                                    PRINT 'GET FROM SCOPE'
                                                    PRINT @USERGOAL_ID
                                                END

												--*****************************************************************************************
                                                --**************LOGIC TO ADD AMOUNT TO STP/SWP/RDM/SWITCH SCHEME FROM*********************
                                        IF EXISTS(select * from @tmpOtherTrans where schemefrom = @SchemeCode and foliono = @Foliono)
                                            BEGIN
                                                
                                                PRINT @SchemeCode
                                                PRINT @TotalCurrentValue
                                               
                                                PRINT '--->>>>>'
                                                SET @FINALTOTALCURRENTVALUE =
                                                CONVERT(INT,(select top 1 calamount from @tmpOtherTrans where schemefrom = 
												@SchemeCode and foliono = @Foliono)) + CONVERT(INT,@TotalCurrentValue)
                                                
												SET @FINALTOTALCURRENTVALUE = ABS(@FINALTOTALCURRENTVALUE)
												PRINT '--BELOW SHOULD GO TO FINCART TABLE--CHILd'
                                                PRINT @FINALTOTALCURRENTVALUE
                                            END
                                            ELSE
                                                BEGIN
                                                    SET @FINALTOTALCURRENTVALUE = @TotalCurrentValue
                                                    PRINT '--BELOW SHOULD GO TO FINCART TABLE--'
                                                PRINT @FINALTOTALCURRENTVALUE
                                                END




                                        /*-----INSERT RECORD IN fin_cart TABLE--*/
                                        IF(@SipAmount!='' AND @SipAmount IS NOT NULL)
                                        Begin

                                        PRINT 'CHILD SIPAMT NOT NULL FLOW'
                                        PRINT @ClientEmail +' ' +@ClientName
                                        -----CHECK AND GET NEXT SIP DATE----
                                       
                                        IF(@SipDate IS NOT NULL AND @SipDate !='')
                                        BEGIN
                                        SET @YEAR = year(getdate())
                                        SET @MONTH = month(getdate())
                                        SET @DAY = @SipDate
                                        SET @CHKDATE = Cast((@YEAR + '-' + @MONTH  + '-' + @DAY) as date)

                                        IF (@CHKDATE < getdate())
                                                Begin 
                                                SET @NXTSIPDATE = DateAdd(month,1,@CHKDATE)  
                                                SET @FINALENDDATE = DateAdd(year,CAST(@TimeHorizon AS float),@NXTSIPDATE)
                                                End

                                            ELSE
                                                BEGIN 
                                                SET @NXTSIPDATE = @CHKDATE
                                                SET @FINALENDDATE = DateAdd(year,CAST(@TimeHorizon AS float),@NXTSIPDATE)
                                                END

                                        END   
                                        ------END OF LOGIC

                                       
                                           
                                       

                                        insert into fin_cart(userId,memberId,profileId,CartId,goalCode,fundCode,
                                        schemeCode,type,entryDate,dividendOpt,folioNo,amount,startDate,endDate,
                                        noOfInstallment,frequency,bankCode,accountNo,mandateId,firstSipDate,
                                        mDate,status,UserGoalId) Select @ClientEmail,(select memberId from fin_CAF_BasicDetails where userid =@ClientEmail
                                        and upper(REPLACE(clientName,' ',''))=upper(REPLACE(@ClientName, ' ', ''))),(select profileid from
                                        fin_InvestProfile where firstApplicant in(select memberId from fin_CAF_BasicDetails where
                                         userid = @ClientEmail and upper(REPLACE(ClientName,' ','')) = upper(REPLACE(@ClientName,' ','')))),NULL,
                                         (Select gcode from fin_goal where gname = @GoalName), @hideFundCode,@SchemeCode,'S',
                                         getdate(),'Z',@FolioNo,@SipAmount,@NXTSIPDATE,@FINALENDDATE,
                                         (DATEDIFF(mm, @NXTSIPDATE, @FINALENDDATE) +1),'Monthly',NULL,NULL,'MIGRATED DATA',
                                         @NXTSIPDATE,@SipDate,1,@USERGOAL_ID

                                         End
                                          
             
                                        PRINT 'CHILD LUMSUMP FLOW--'
                                        PRINT 'YEH GAYA-'
                                        PRINT @FINALTOTALCURRENTVALUE


                                        IF Exists(select * from fin_cart where userid = @ClientEmail and fundcode = @hideFundCode
                                         and schemeCode=@SchemeCode and folioNo = @FolioNo and type = 'L' and status=1)
                                         BEGIN
										 print 'SUBTRACT EXIST LUMSUP ----'
										 
                                            SET @FINALTOTALCURRENTVALUE = (CONVERT(INT,@FINALTOTALCURRENTVALUE) - CONVERT(INT,(select
                                            sum(CAST(amount as INT)) from fin_Cart where userid = @ClientEmail and fundcode = @hideFundCode
                                         and schemeCode=@SchemeCode and folioNo = @FolioNo and type = 'L' and status=1)))
										 
										 Declare @TMPLAMOUNTCHILD INT;
										 SET @TMPLAMOUNTCHILD = CONVERT(INT,(select
                                            sum(CAST(amount as INT)) from fin_Cart where userid = @ClientEmail and fundcode = @hideFundCode
                                         and schemeCode=@SchemeCode and folioNo = @FolioNo and type = 'L' and status=1))

										 IF(@FINALTOTALCURRENTVALUE < @TMPLAMOUNTCHILD)
										 BEGIN
										 SET @FINALTOTALCURRENTVALUE = ABS(@FINALTOTALCURRENTVALUE)
										 END

										 PRINT 'select sum(CAST(amount as INT)) from fin_Cart where userid = ' + @ClientEmail + ' and fundcode = ' + @hideFundCode +
										 ' and schemeCode=' + @SchemeCode + ' and folioNo = ' + @FolioNo + ' and type = ''L'' and status=1'
                                         END


                                            insert into fin_cart(userId,memberId,profileId,CartId,goalCode,fundCode,
                     schemeCode,type,entryDate,dividendOpt,folioNo,amount,startDate,endDate,
                                        noOfInstallment,frequency,bankCode,accountNo,mandateId,firstSipDate,
                                        mDate,status,UserGoalId) Select @ClientEmail,(select memberId from fin_CAF_BasicDetails
                                        where userid = @ClientEmail and upper(REPLACE(ClientName,' ','')) =
                                        upper(REPLACE(@ClientName,' ',''))),(select top 1 profileid from fin_InvestProfile where
                                        firstApplicant in(select memberId from fin_CAF_BasicDetails where
                                         userid = @ClientEmail and upper(REPLACE(ClientName,' ','')) = upper(REPLACE(@ClientName,' ','')))),NULL,
                                         (Select gcode from fin_goal where gname = @GoalName), @hideFundCode,@SchemeCode,'L',
                                         getdate(),'Z',@FolioNo,@FINALTOTALCURRENTVALUE,getdate(),getdate(),
                                         NULL,'Onetime',NULL,NULL,'MIGRATED DATA',
                                         NULL,NULL,1,@USERGOAL_ID
                                       
                                       PRINT 'END OF LUMPSUM'

                                       

                                       
                                    END

                                  
                                END
                                PRINT 'TST-START OTHER'
                       
                        --####################### OTHER GOAL #################
                        IF(@GoalName='Other')
                            BEGIN------IF OTHER GOAL----
                                    PRINT 'OTHER FLOW'
                                       

                                        -----INSERT RECORD IN FIN_GOAL IF GOAL NOT EXIST ELSE GET EXISTED
                                        IF NOT EXISTS(select * from fin_goal where GName = @OtherGoalName)
                                        Begin
                                        EXEC [dbo].[fin_Proc_GoalAction] @Action='ADDGOAL',@GName=@OtherGoalName,@GDesc=@OtherGoalName
                                        End
                                        SET @OTHRGOALNAME = @OtherGoalName;
                                        SET @OTHRGOALCODE = (select gcode from fin_goal where GName=@OtherGoalName);
                                       

                                        PRINT 'TST-8'
                                            /*-----INSERT RECORD IN fin_User_GOAL TABLE*/
                                        IF NOT EXISTS(select * from fin_User_Goals where userid=@ClientEmail and goalcode = 'FG14'
                                         and TypeCode in(select gcode from fin_Goal where gname=@OtherGoalName))
                                            Begin
                                               
                                                insert into fin_User_Goals (UUID,UserID,RelationID,GoalCode,TypeCode,People,Location,
                                        Place,MonthlyAmount,Amount,Start,[End],Duration,Risk,InvestmentType,GetAmount,InvestAmount,
                                        ProductList,ActivatedDatetime,CompletedDatetime,CreatedByEmail,CreatedByUUID,
                                        CreatedDatetime,UpdatedByEmail,UpdatedByUUID,UpdatedDatetime,Status)
                                       
                                        select NULL,@ClientEmail,NULL,'FG14',@OTHRGOALCODE,'0',NULL,@OTHRGOALNAME,'0',@GetAmount,'0','0',
                                        @TimeHorizon,'M',(CASE WHEN @SipAmount!='' AND @SipAmount IS NOT NULL THEN 'S'
                                        ELSE 'L' END),@GetAmount,@InvestAmount,@PRODUCTLIST,NULL,NULL,@ClientEmail,NULL,
                                        getdate(),@ClientEmail,NULL,getdate(),1
                                                SET @USERGOAL_ID = SCOPE_IDENTITY();
                                            End
                                        Else
             BEGIN
                                            
												SET @USERGOAL_ID = (select top 1 id from fin_User_Goals where TypeCode in 
												(select gcode from fin_Goal where GName=@OtherGoalName)
                                         and userid=@ClientEmail)

                                                IF NOT EXISTS(select * from fin_User_Goals where userid = @ClientEmail and goalcode = 'FG14'
                                                    and TypeCode in(select gcode from fin_Goal where gname=@OtherGoalName) and InvestmentType='S')
                                                    BEGIN
                                                        update fin_User_Goals set InvestmentType=(CASE WHEN @SipAmount!='' AND
                                                        @SipAmount IS NOT NULL THEN 'S' ELSE 'L' END) where userid = @ClientEmail and
                                                        goalcode = 'FG14' and TypeCode in(select gcode from fin_Goal where gname=@OtherGoalName)
                                                    END
													
													PRINT 'GET FROM SCOPE'
                                                    PRINT @USERGOAL_ID

                                            END

                                           
                                        --*****************************************************************************************
                                                --**************LOGIC TO ADD AMOUNT TO STP/SWP/RDM/SWITCH SCHEME FROM*********************
                                        IF EXISTS(select * from @tmpOtherTrans where schemefrom = @SchemeCode and foliono = @Foliono)
                                            BEGIN
                                                
                                                PRINT @SchemeCode
                                                PRINT @TotalCurrentValue
                                               
                                                PRINT '--->>>>>'
                                                SET @FINALTOTALCURRENTVALUE =
                                                CONVERT(INT,(select top 1 calamount from @tmpOtherTrans where schemefrom = 
												@SchemeCode and foliono = @Foliono)) + CONVERT(INT,@TotalCurrentValue)
                                                
												SET @FINALTOTALCURRENTVALUE = ABS(@FINALTOTALCURRENTVALUE)
												PRINT '--BELOW SHOULD GO TO FINCART TABLE--CHILd'
                                                PRINT @FINALTOTALCURRENTVALUE
                                            END
                                            ELSE
                                                BEGIN
                                                    SET @FINALTOTALCURRENTVALUE = @TotalCurrentValue
                                                    PRINT '--BELOW SHOULD GO TO FINCART TABLE--'
                                                PRINT @FINALTOTALCURRENTVALUE
                                                END



                                        /*#############################-----INSERT RECORD IN fin_cart TABLE--#################################*/

                                        IF(@SipAmount!='' AND @SipAmount IS NOT NULL)
                                        Begin

                                        -----CHECK AND GET NEXT SIP DATE----
                                        IF(@SipDate IS NOT NULL AND @SipDate !='')
                                        BEGIN
                                        SET @YEAR = year(getdate())
                                        SET @MONTH = month(getdate())
                                        SET @DAY = @SipDate
                                        SET @CHKDATE = (Cast((@YEAR + '-' + @MONTH  + '-' + @DAY) as date))

                                       IF (@CHKDATE < getdate())
                                                Begin 
                                                SET @NXTSIPDATE = DateAdd(month,1,@CHKDATE) 
                                                SET @FINALENDDATE = DateAdd(year,CAST(@TimeHorizon AS float),CAST(@sipStartDate as date))
                                                End

                       ELSE
                                                BEGIN 
                                                SET @NXTSIPDATE = @CHKDATE
                                                SET @FINALENDDATE = DateAdd(year,CAST(@TimeHorizon AS float),CAST(@sipStartDate as date))
                                                END
                                   
                                        ------END OF LOGIC

                                       



                                        insert into fin_cart(userId,memberId,profileId,CartId,goalCode,fundCode,
                                        schemeCode,type,entryDate,dividendOpt,folioNo,amount,startDate,endDate,
                                        noOfInstallment,frequency,bankCode,accountNo,mandateId,firstSipDate,
                                        mDate,status,UserGoalId) Select @ClientEmail,(select memberId from fin_CAF_BasicDetails where userid = @ClientEmail and
                                                upper(REPLACE(ClientName,' ','')) = upper(REPLACE(@ClientName,' ',''))),(select top 1 profileid from fin_InvestProfile
                                         where firstApplicant in(select memberId from fin_CAF_BasicDetails where
                                         userid = @ClientEmail and upper(REPLACE(ClientName,' ','')) = upper(REPLACE(@ClientName,' ','')))),NULL,
                                         'FG14', @hideFundCode,@SchemeCode,'S',getdate(),'Z',@FolioNo,@SipAmount,@NXTSIPDATE,@FINALENDDATE,
                                         (DATEDIFF(mm, @NXTSIPDATE, @FINALENDDATE) +1),'Monthly',NULL,NULL,'MIGRATED DATA',
                                         @sipStartDate,@SipDate,1,@USERGOAL_ID

                                         End
                                        END      


                                        IF Exists(select * from fin_cart where userid = @ClientEmail and fundcode = @hideFundCode
                                         and schemeCode=@SchemeCode and folioNo = @FolioNo and type = 'L' and status=1)
                                         BEGIN
										 print 'SUBTRACT EXIST LUMSUP ----'
                                            SET @FINALTOTALCURRENTVALUE = (CONVERT(INT,@FINALTOTALCURRENTVALUE) - CONVERT(INT,(select
                                            sum(CAST(amount as INT)) from fin_Cart where userid = @ClientEmail and fundcode = @hideFundCode
                                         and schemeCode=@SchemeCode and folioNo = @FolioNo and type = 'L' and status=1)))
										 
										 
										  Declare @TMPLAMOUNTOTHER INT;
										 SET @TMPLAMOUNTOTHER = CONVERT(INT,(select
                                            sum(CAST(amount as INT)) from fin_Cart where userid = @ClientEmail and fundcode = @hideFundCode
                                         and schemeCode=@SchemeCode and folioNo = @FolioNo and type = 'L' and status=1))

										 IF(@FINALTOTALCURRENTVALUE < @TMPLAMOUNTOTHER)
										 BEGIN
										 SET @FINALTOTALCURRENTVALUE = ABS(@FINALTOTALCURRENTVALUE)
										 END
										 PRINT 'select sum(CAST(amount as INT)) from fin_Cart where userid = ' + @ClientEmail + ' and fundcode = ' + @hideFundCode +
										 ' and schemeCode=' + @SchemeCode + ' and folioNo = ' + @FolioNo + ' and type = ''L'' and status=1'
                                         END


                                        insert into fin_cart(userId,memberId,profileId,CartId,goalCode,fundCode,
                                        schemeCode,type,entryDate,dividendOpt,folioNo,amount,startDate,endDate,
                                        noOfInstallment,frequency,bankCode,accountNo,mandateId,firstSipDate,
                                        mDate,status,UserGoalId) Select @ClientEmail,(select memberId from fin_CAF_BasicDetails where userid = @ClientEmail and
                                                upper(REPLACE(ClientName,' ','')) = upper(REPLACE(@ClientName,' ',''))),(select top 1 profileid from fin_InvestProfile
                                         where firstApplicant in(select memberId from fin_CAF_BasicDetails where
                                         userid = @ClientEmail and upper(REPLACE(ClientName,' ','')) = upper(REPLACE(@ClientName,' ','')))),NULL,
                                         'FG14', @hideFundCode,@SchemeCode,'L',
                                         getdate(),'Z',@FolioNo,@FINALTOTALCURRENTVALUE,getdate(),getdate(),
                                         NULL,'Onetime',NULL,NULL,'MIGRATED DATA',
                                         NULL,NULL,1,@USERGOAL_ID


                                      
                                    END
									PRINT 'TST-START NORMAL'
                                   PRINT 'GOAL NAME------>>'+@GoalName
                       
                        --####################### NORMAL GOAL #########################################################################
                        IF(@GoalName!='Other' AND @GoalName != 'ChlidStudy' AND @GoalName != 'ChlidWedding' AND @GoalName !='')
                         BEGIN------IF NORMAL GOAL----
                                    PRINT 'NORMAL GOAL FLOW'
                                       
                                        /*-----INSERT RECORD IN fin_User_GOAL TABLE*/
                                        IF NOT EXISTS(select * from fin_User_Goals where userid=@ClientEmail and GoalCode in(select gcode from fin_Goal where GName=@GoalName))
                                            Begin---#######################
                                                PRINT 'INSERT CALL FOR fin_user_goals'
                                                insert into fin_User_Goals (UUID,UserID,RelationID,GoalCode,TypeCode,People,Location,
                                        Place,MonthlyAmount,Amount,Start,[End],Duration,Risk,InvestmentType,GetAmount,InvestAmount,
                                        ProductList,ActivatedDatetime,CompletedDatetime,CreatedByEmail,CreatedByUUID,
                                        CreatedDatetime,UpdatedByEmail,UpdatedByUUID,UpdatedDatetime,Status)
                                        select NULL,@ClientEmail,NULL,(Select gcode from fin_goal where gname =
                                        @GoalName),NULL,'0',NULL,NULL,'0',@GetAmount,'0','0',@TimeHorizon,'M',
                                        (CASE WHEN @SipAmount!='' AND @SipAmount IS NOT NULL THEN 'S' ELSE 'L' END),
                                        @GetAmount,@InvestAmount,@PRODUCTLIST,NULL,NULL,@ClientEmail,NULL,getdate(),@ClientEmail,NULL,getdate(),1
                                                SET @USERGOAL_ID = SCOPE_IDENTITY();
                                            End
                                        Else
                                            BEGIN
                                            SET @USERGOAL_ID= (select id from fin_User_Goals where goalcode in
                                            (select gcode from fin_Goal where GName=@GoalName) and userid=@ClientEmail)
                                           
                                            IF NOT EXISTS(select * from fin_User_Goals where userid = @ClientEmail and GoalCode
                                            in(select gcode from fin_Goal where GName=@GoalName) and InvestmentType='S')
                                                    BEGIN
                                                        update fin_User_Goals set InvestmentType=(CASE WHEN @SipAmount!='' AND
                                                        @SipAmount IS NOT NULL THEN 'S' ELSE 'L' END) where userid = @ClientEmail and GoalCode
                                                        in(select gcode from fin_Goal where GName=@GoalName)
                                                    END

                                                    PRINT 'GET FROM SCOPE'
                                                    PRINT @USERGOAL_ID
                                         END

                                         
                                       
                                        --*****************************************************************************************
                                                --**************LOGIC TO ADD AMOUNT TO STP/SWP/RDM/SWITCH SCHEME FROM*********************
                                        IF EXISTS(select * from @tmpOtherTrans where schemefrom = @SchemeCode and foliono = @Foliono)
                                            BEGIN
                                                
                                                PRINT @SchemeCode
                                                PRINT @TotalCurrentValue
                                               
                                                PRINT '--->>>>>>>>>>>>>>>>>>>>>>>>>'PRINT @FINALTOTALCURRENTVALUE
                                                SET @FINALTOTALCURRENTVALUE =
                                                CONVERT(INT,(select top 1 calamount from @tmpOtherTrans where schemefrom = 
												@SchemeCode and foliono = @Foliono)) + CONVERT(INT,@TotalCurrentValue)
                                                
												SET @FINALTOTALCURRENTVALUE = ABS(@FINALTOTALCURRENTVALUE)
												PRINT '--BELOW SHOULD GO TO FINCART TABLE--CHILd'
                                                PRINT @FINALTOTALCURRENTVALUE
                                            END
                                            ELSE
                                                BEGIN
                                                    SET @FINALTOTALCURRENTVALUE = @TotalCurrentValue
                                                    PRINT '--BELOW SHOULD GO TO FINCART TABLE--'
                                                PRINT @FINALTOTALCURRENTVALUE
                                                END
                                       
                                       
                                        /*-----INSERT RECORD IN fin_cart TABLE--*/

                                        IF(@SipAmount!='' AND @SipAmount IS NOT NULL)
                                        Begin
                                        PRINT 'NORMAL GOAL FLOW - SIPAMT NOT NULL FLOW'
                                        -----CHECK AND GET NEXT SIP DATE----
                                        IF(@SipDate IS NOT NULL AND @SipDate !='')
                                        BEGIN
                                        SET @YEAR = year(getdate())
                                        SET @MONTH = month(getdate())
                                        SET @DAY = @SipDate
                                        SET @CHKDATE = (Cast((@YEAR + '-' + @MONTH  + '-' + @DAY) as date))

        IF (@CHKDATE < getdate())
                                                Begin 
                                                PRINT 'CHKDATE LESSTHAN'
                                                SET @NXTSIPDATE = DateAdd(month,1,@CHKDATE) 
                                                print @sipStartDate
                                                SET @FINALENDDATE = DateAdd(year,CAST(@TimeHorizon AS float),CAST(@sipStartDate as date))
                                                End

                                            ELSE
                                                BEGIN 
                                                PRINT 'CHKDATE GREATER OR EQUAL'
                                                SET @NXTSIPDATE = @CHKDATE
                                                print @sipStartDate
                                                print '@FINALENDDATE = DateAdd(year,CAST('+@TimeHorizon+' AS INT),CAST('+@sipStartDate+' as date))'
                                                SET @FINALENDDATE = DateAdd(year,CAST(@TimeHorizon AS float),CAST(@sipStartDate as date))
                                                END
                                   
                                        ------END OF LOGIC

                                       




                                        PRINT @FINALENDDATE
                                        PRINT @NXTSIPDATE
                                        PRINT 'SIP FLOW'
                                        insert into fin_cart(userId,memberId,profileId,CartId,goalCode,fundCode,
                                        schemeCode,type,entryDate,dividendOpt,folioNo,amount,startDate,endDate,
                                        noOfInstallment,frequency,bankCode,accountNo,mandateId,firstSipDate,
                                        mDate,status,UserGoalId) Select @ClientEmail,(select memberId from fin_CAF_BasicDetails where userid = @ClientEmail and
                                                upper(REPLACE(ClientName,' ','')) = upper(REPLACE(@ClientName,' ',''))),(select top 1 profileid from fin_InvestProfile
             where firstApplicant in(select memberId from fin_CAF_BasicDetails where
                                         userid = @ClientEmail and upper(REPLACE(ClientName,' ','')) = upper(REPLACE(@ClientName,' ','')))),NULL,
                                         (Select gcode from fin_goal where gname = @GoalName), @hideFundCode,@SchemeCode,'S',
                                         getdate(),'Z',@FolioNo,@SipAmount,@NXTSIPDATE,@FINALENDDATE,
                                         (DATEDIFF(mm, @NXTSIPDATE, @FINALENDDATE) +1),'Monthly',NULL,NULL,'MIGRATED DATA',
                                         @sipStartDate,@SipDate,1,@USERGOAL_ID

                                         End
                                        END      

                                        PRINT 'NORMAL GOAL FLOW - INSERT CART TABLE'
                                        PRINT @ClientEmail + '--' + @ClientName
                                       


                                        IF Exists(select * from fin_cart where userid = @ClientEmail and fundcode = @hideFundCode
                                         and schemeCode=@SchemeCode and folioNo = @FolioNo and type = 'L' and status=1)
                                         BEGIN
										 print 'SUBTRACT EXIST LUMSUP NORMAL GOAL FLOWW ----'
                                            SET @FINALTOTALCURRENTVALUE = (CONVERT(INT,@FINALTOTALCURRENTVALUE) - CONVERT(INT,(select
                                            sum(CAST(amount as INT)) from fin_Cart where userid = @ClientEmail and fundcode = @hideFundCode
                                         and schemeCode=@SchemeCode and folioNo = @FolioNo and type = 'L' and status=1)))

										 Declare @TMPLAMOUNTNORMAL INT;
										 SET @TMPLAMOUNTNORMAL = CONVERT(INT,(select
                                            sum(CAST(amount as INT)) from fin_Cart where userid = @ClientEmail and fundcode = @hideFundCode
                                         and schemeCode=@SchemeCode and folioNo = @FolioNo and type = 'L' and status=1))

										 Print 'CONDITION CHK FOR LESS NEGATIVE'
										 PRINT @FINALTOTALCURRENTVALUE
										 Print @TMPLAMOUNTNORMAL
										 IF(@FINALTOTALCURRENTVALUE < @TMPLAMOUNTNORMAL)
										 BEGIN
										 Print 'ABS() FOR AMOUNT'
										 SET @FINALTOTALCURRENTVALUE = ABS(@FINALTOTALCURRENTVALUE)
										 END

										 PRINT 'select sum(CAST(amount as INT)) from fin_Cart where userid = ' + @ClientEmail + ' and fundcode = ' + @hideFundCode +
										 ' and schemeCode=' + @SchemeCode + ' and folioNo = ' + @FolioNo + ' and type = ''L'' and status=1'
                                         END


                                        insert into fin_cart(userId,memberId,profileId,CartId,goalCode,fundCode,
                                        schemeCode,type,entryDate,dividendOpt,folioNo,amount,startDate,endDate,
                                        noOfInstallment,frequency,bankCode,accountNo,mandateId,firstSipDate,
                                        mDate,status,UserGoalId) Select @ClientEmail,(select memberId from fin_CAF_BasicDetails where userid = @ClientEmail and
                                                upper(REPLACE(ClientName,' ','')) = upper(REPLACE(@ClientName,' ',''))),(select top 1 profileid from fin_InvestProfile
                                         where firstApplicant in(select memberId from fin_CAF_BasicDetails where
                                         userid = @ClientEmail and upper(REPLACE(ClientName,' ','')) = upper(REPLACE(@ClientName,' ','')))),NULL,
                                         (Select gcode from fin_goal where gname = @GoalName), @hideFundCode,@SchemeCode,'L',
                                         getdate(),'Z',@FolioNo,@FINALTOTALCURRENTVALUE,getdate(),getdate(),
                                         NULL,'Onetime',NULL,NULL,'MIGRATED DATA',
                                         NULL,NULL,1,@USERGOAL_ID
                                        PRINT 'TST-9'
                                      
                                END      
                       



                        --******UPDATE BLANK GOALCODE AND USERGOALID IN FIN_CART TABLE
						IF EXISTS(select * from [dbo].[fin_cart] where (userid = @ClientEmail
                    and fundcode = @hideFundCode and schemeCode=@SchemeCode and folioNo = @FolioNo and status = 1) and (goalcode is null OR goalcode = '' OR
                    usergoalid is null))
                        BEGIN

                       PRINT 'UPDATE FLOW FOR BLANKGOALCODE AND USERGOALID...'
                        IF(@GoalName!='Other' AND @GoalName != 'ChlidStudy' AND @GoalName != 'ChlidWedding' AND @GoalName !='')
                            BEGIN
                                Update fin_cart set goalcode = (Select gcode from fin_goal where gname = @GoalName),
                        UserGoalId = (Select id from fin_User_Goals where userid = @ClientEmail and
                        goalcode=(Select gcode from fin_goal where gname = @GoalName)) where (userid = @ClientEmail
                    and fundcode = @hideFundCode and schemeCode=@SchemeCode and folioNo = @FolioNo and status = 1) and
                    (goalcode is null OR goalcode = '' OR usergoalid is null)
                            END
                       
                        IF(@GoalName='Other')
                            BEGIN
                                Update fin_cart set goalcode = 'FG14',UserGoalId = @USERGOAL_ID where (userid = @ClientEmail
                    and fundcode = @hideFundCode and schemeCode=@SchemeCode and folioNo = @FolioNo and status = 1) and
                    (goalcode is null OR goalcode = '' OR usergoalid is null)
                            END
                       
                        IF(@GoalName = 'ChlidStudy' OR @GoalName = 'ChlidWedding')
                            BEGIN
                                Update fin_cart set goalcode = (Select gcode from fin_goal where gname = @GoalName),
                                UserGoalId = @USERGOAL_ID where (userid = @ClientEmail
                    and fundcode = @hideFundCode and schemeCode=@SchemeCode and folioNo = @FolioNo and status = 1) and
                    (goalcode is null OR goalcode = '' OR usergoalid is null)
                            END

                        END

                    END
                    END
              
    FETCH NEXT FROM goal_cursor INTO
            @GoalName,
@OtherGoalName,
@TimeHorizon,
@GetAmount,
@InvestAmount,
@FundName,
@hideFundCode,
@SchemeName,
@SchemeCode,
@TotalCurrentValue,
@sipAmount,
@sipDate,
@sipStartDate,
@Foliono,
@ClientEmail,
@ClientName,
@ChildName1,
@ChildName2,
@ChildName3,
@Child1Age,
@Child2Age,
@Child3Age,
@Child1Gender,
@Child2Gender,
@Child3Gender,
@RetirementAge
END


            --LOGIC TO SUBTRACT ALL SCHEME AMOUNT IN FIN_CART (STP / SWP / REDMTION)
       
            IF EXISTS(select * from fin_cart where schemecode in(select distinct schemeto from @tmpOtherTrans where schemeto is not null) and userid=@ClientEmail)
                Begin
                   
				   PRINT 'SUBTRACTION CALLED........'
                    update fc set amount = CONVERT(INT,fc.amount) - CONVERT(INT,ot.calamount) from fin_cart fc JOIN @tmpOtherTrans ot
                    on fc.schemeCode = ot.schemeto and fc.foliono=ot.foliono where fc.userid=@ClientEmail and fc.status = 1 and fc.type = 'L'

                    
				    update otdtls set goalcode=fc.goalcode,usergoalid=fc.usergoalid from fin_Other_Transactions_Details otdtls JOIN fin_cart fc
                    on otdtls.userid=fc.userId and otdtls.schemecode =  fc.schemecode where otdtls.userId = @ClientEmail
				   
					PRINT 'EMAIL='+@ClientEmail 
					select * from @tmpOtherTrans
                   
                END
				--delete from fin_cart where userId=@ClientEmail and amount <= 0
                
CLOSE goal_cursor
DEALLOCATE goal_cursor
End
END


GO

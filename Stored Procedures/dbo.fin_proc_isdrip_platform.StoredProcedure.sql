USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_proc_isdrip_platform]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[fin_proc_isdrip_platform](@platformidlist [UT_fin_isdrip_platform] readonly)
as
begin


SELECT count(*) as Isdrip
	FROM fin_Campaign_Platform a where ID in(select platformid from @platformidlist)  and a.IsDripMktEnable=1
			
end

GO

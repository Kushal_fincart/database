USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[TimelineDetails]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 create proc [dbo].[TimelineDetails]
 @UserId varchar(50)=''
 as
 begin
 SELECT fp.Name,fp.UserID,gl.GoalCode,gl.Amount as 'Current_Amount',gl.GetAmount as 'Expected_Amount',
 gl.InvestAmount,cast(gl.CreatedDatetime as Date) as 'Created_Date',(gl.Duration +' year' )as 'TimePeriod',fngoal.GDesc from 
 fp_Prospects fp join fin_User_Goals gl on fp.UserID=gl.UserID join 
 fin_Goal fngoal on gl.GoalCode=fngoal.GCode where fp.UserID=@UserId
 end
 
GO

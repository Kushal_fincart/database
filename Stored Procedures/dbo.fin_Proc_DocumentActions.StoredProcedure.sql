USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_DocumentActions]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[fin_Proc_DocumentActions]
@USERID nvarchar (max)=null,
@MEMBERID nvarchar (max)=null,
@FILENAME nvarchar (max)=null,
@FILEPATH nvarchar (max)=null,
@DOCTYPE varchar(200)=null,
@ID int=0,
@ACTION [varchar](500)
as
begin
	if(@ACTION='ADD')
	begin
		insert into [dbo].[fin_documents] select @USERID,@MEMBERID,@FILENAME,@FILEPATH,@DOCTYPE,getdate(),1
	end

	if(@ACTION='REMOVE')
	begin
		delete from [dbo].[fin_documents] where Id = @ID
	end

	if(@ACTION='VIEW')
	begin
		Select * from [dbo].[fin_documents] where userId = @USERID
	end
end



GO

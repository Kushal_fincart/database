USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_proc_insert_emailer]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[fin_proc_insert_emailer] (
	@emailer_list [UT_fin_emailerlist] readonly
	,@platformid_list UT_fin_Campign_platform readonly
	,@campid BIGINT
	)
AS
BEGIN
	INSERT INTO fin_EmailerList (
		Email
		,UserCode
		,CreatedDate,UpdatedDate
		)
	SELECT email
		,usercode
		,getdate(),GETDATE()
	FROM @emailer_list
	WHERE email NOT IN (
			SELECT email
			FROM fin_EmailerList
			)

	INSERT INTO fin_CampaignLeads (
		Email
		,CreatedDate,UpdatedDate
		)
	SELECT email
		,GETDATE(),GETDATE()
	FROM @emailer_list
	WHERE email NOT IN (
			SELECT email
			FROM fin_CampaignLeads
			)

	DECLARE @myflag AS INT
	DECLARE @Totalemail AS INT

	SELECT @Totalemail = count(*)
	FROM @emailer_list

	DECLARE @cntemail AS INT
	DECLARE @currentemail AS VARCHAR(50)

	SET @cntemail = 1

	DECLARE @totalplatform AS INT

	SELECT @totalplatform = count(*)
	FROM @platformid_list

	DECLARE @cntplat AS INT
	DECLARE @currentplat AS BIGINT
	DECLARE @currentplattrackercode AS VARCHAR(max)

	SET @cntplat = 1

	WHILE (@cntplat <= @totalplatform)
	BEGIN
		SELECT @currentplat = Platformid
		FROM @platformid_list
		WHERE rowid = @cntplat

		SELECT @currentplattrackercode = Track_Code
		FROM fin_Mapping_Camp_Plat
		WHERE Platform_Id = @currentplat
			AND Camp_Id = @campid

		WHILE (@cntemail <= @Totalemail)
		BEGIN
			SELECT @currentemail = email
			FROM @emailer_list
			WHERE rowid = @cntemail

			SELECT @myflag = count(*)
			FROM fin_CampaignLeadHistory
			WHERE Email = @currentemail
				AND TrackerCode = @currentplattrackercode

			IF (@myflag = 0)
			BEGIN
				INSERT INTO fin_CampaignLeadHistory (
					Email
					,TrackerCode
					,CreatedDate
					,CampaignCount,UpdatedDate
					)
				VALUES (
					@currentemail
					,@currentplattrackercode
					,GETDATE()
					,1,GETDATE()
					)
			END
			ELSE
			BEGIN
				UPDATE fin_CampaignLeadHistory
				SET CampaignCount = CampaignCount + 1,UpdatedDate=getdate()
				WHERE Email = @currentemail
					AND TrackerCode = @currentplattrackercode
			END

			SET @cntemail = @cntemail + 1
		END

		SET @cntemail = 1
		SET @cntplat = @cntplat + 1
	END
END

GO

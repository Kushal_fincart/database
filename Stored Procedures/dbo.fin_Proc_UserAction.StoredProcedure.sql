USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_UserAction]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[fin_Proc_UserAction]
	
	@name nvarchar(500)=null,

	@userid nvarchar(500)=null,

	@password nvarchar(500)=null,

	@mobile nvarchar(500)=null,

	@clientstatus nvarchar(100)=null,

	@UUID nvarchar(max)=null,

	@Age int=null,
	
	@Gender varchar(5)=null,
	
	@MaritialStatus varchar(5)=null,
	
	@AnnualIncome nvarchar(200)=null,
	
	@MonthlyExpence nvarchar(200)=null,
	
	@RetirementAge int=null,
	
	@LifeExpentency int=null,
	
	@Spouse bit=null,
	
	@ChildCount int=null,
	
	@Emi bit=null,
	
	@EmiAmount nvarchar(200)=null,
	
	@Loans bit=null,

	@LoansTbl fin_Type_LoansTbl READONLY,

	@UserRelationTbl fin_Type_UserRelationTbl READONLY,

	@UserGoalsTbl fin_Type_UserGoalsTbl READONLY,

	@companyCode nvarchar(max) = null,
	
	@companyName nvarchar(max) = null,
	
	@goalCode nvarchar(max) = null,
	@action nvarchar(500)=null

	

as

begin

DECLARE @ASSIGNEDTOEMPCODE nvarchar(max)
DECLARE @ASSIGNED_TOEMPCODE nvarchar(max)
DECLARE @LEAD_CODE bigint

if(@action='UserExistCount')

	begin

		select count(*) as exist from [dbo].[fp_Prospects] where UserID=@userid

	end

if(@action='ValidateUser')

	begin

		select Code,Name,UserID,Pwd,ClientStatus,Mobile1 from [dbo].[fp_Prospects] where UserID=@userid and Pwd=@password

	end
if(@action='GetUserPassword')

	begin

		select Pwd from [dbo].[fp_Prospects] where UserID=@userid

	end
if(@action='GET_USER_NAME')

	begin

		select name from [dbo].[fp_Prospects] where UserID=@userid

	end

if(@action='GetUserDetails_Basic')

	begin

		select Code,Name,UserID,Pwd,ClientStatus,Mobile1 from [dbo].[fp_Prospects] where UserID=@userid and Pwd=@password

	end

if(@action='RegisterUser')

	begin

		declare @user int 

		set @user=(select count(userid) from fp_Prospects where userid=@userid)

		if(@user<1)

			begin
				BEGIN TRAN
					insert into fp_Prospects(Name,userid,Pwd,mobile1,email1,clientStatus,SystemDate)

					values(@name,@userid,@password,@mobile,@userid,@clientstatus,getdate())

					--insert into [dbo].[fin_Exclusive_Company] (userId,companyCode,companyName,[status])
					--values (@userid,@companyCode,@companyName,1)
						
						
						Execute fin_Proc_CafAction @action='INSERT_CAF_INIT_RECORD',@USERID=@userid,@MEMBERID=@userid,
						@GROUPLEADER=@userid,@PASSWORD=@password,@PHONE=@mobile,@MOBILE=@mobile,@CLIENTNAME=@name

						SET @LEAD_CODE = (select max(code) from fp_prospects)

						IF(@userid not like '%testentry%')		
						
						BEGIN	
							EXECUTE fp_Workpoint_Action @Id=0,@userid=0,@pass=null,@LEADCODE=@LEAD_CODE,
						@EMPCODE= null,@LEADTYPEID= null,@CREATEDDATE= null,@ASSIGNEDDATE= null,
						@LASTACTIVITYDATE= null,@Action='AUTO_ALLOCATE_LEADBYMATRIX',@ASSIGNEDTOEMPCODE = @ASSIGNEDTOEMPCODE OUTPUT

							SET @ASSIGNED_TOEMPCODE = @ASSIGNEDTOEMPCODE

						
							declare @plannercode int
							declare @plannerName varchar(max)
							Set @plannercode = (select code from fp_Planner where userid in
							(select userid from [fp_Employee] where empcode = @ASSIGNED_TOEMPCODE))
							Set @plannerName = (select PlannerName from fp_Planner where userid in
							(select userid from [fp_Employee] where empcode = @ASSIGNED_TOEMPCODE))

							--TEMP CODE FOR OLD LMS TO WORK-------
							update fp_Prospects set PlannerCode=@plannercode,AllocateDate=getdate(),
							OperatorCode=16, LeadType=135 where Code=@LEAD_CODE
							update fp_Prospects set PLock='Y' where Code=@LEAD_CODE
							insert into fp_ActivityMonitor values(@plannercode,@LEAD_CODE,'Allocated to Planner '+@plannerName,124,
							getdate(),null,null,'Admin',null,getdate(),null,null)

						END
						
						select 1 result
						
						-- CALL LEAD MATRIX----- 
						SELECT top 1 CASE p.Name WHEN '' THEN 'New Lead' ELSE p.Name END as ClientName, p.UserID as ClientEmail, 
						p.Mobile1 as ClientMobile, e.Name AS EmpName, e.UserID AS EmpEmail, e.Phone as EmpMobile, 
						lah.AssignBy,LM.assignedDate FROM  fin_LeadMapping LM Join fp_Prospects p on LM.LeadCode=p.Code 
						JOIN fp_Employee e on LM.empcode = e.EmpCode join fin_LeadAllocationHistory lah on 
						lah.leadcode = LM.LeadCode where LM.empCode = @ASSIGNED_TOEMPCODE order by lm.createdDate desc

					
				COMMIT TRAN

			end

		else

			begin

				select 0 result

			end

		end
if(@action='UpdateNameStatus')
	begin

	if((select count(*) from fp_Prospects where userid=@userid and isnull(Name,'')='')>0)
		begin
			update fp_Prospects set Name=@name where userid=@userid
		end
	if((select count(*) from fp_Prospects where userid=@userid and isnull(clientstatus,'')='')>0)
		begin
			update fp_Prospects set clientstatus=@clientstatus where userid=@userid
		end

	end
if(@action='UpdateGoalUser')
	begin
			   IF NOT EXISTS (SELECT * FROM fin_user 
						   WHERE userid = @userid)
				   begin
						
						insert fin_user select @UUID,@userid,@name,@mobile,@age,'','',@AnnualIncome,'',60,80,0,0,0,'0',0,@userid,@UUID,getdate(),@userid,@UUID,getdate(),1
				   end

	if((select count(*) from fin_User where userid=@userid and isnull(Name,'')='')>0)
		begin
			update fin_User set Name=@name where userid=@userid
		end
	if((select count(*) from fin_User where userid=@userid and Age<1)>0)
		begin
			update fin_User set Age=@Age where userid=@userid
		end
	if((select count(*) from fin_User where userid=@userid and AnnualIncome<1)>0)
		begin
			update fin_User set AnnualIncome=@AnnualIncome where userid=@userid
		end
	end
--if(@action='UpdateProfile')
--	begin
--		update fin_User set Name=@name,Age=@Age,AnnualIncome=@AnnualIncome where userid=@userid
--	end
if(@action='ForgotPassword')  

	begin  
		select Code,Name,UserID,Pwd from [dbo].[fp_Prospects] where UserID=@userid  
	end  

if(@action='ResetPassword')  

	begin  
	 
		UPDATE fp_Prospects SET pwd=@password WHERE UserID=@userid
		SELECT ISNULL(NULLIF(name, ''), 'User') from fp_prospectS where userid =@userid
	end  

if(@action='SaveGoal')

begin
	set @mobile=(select mobile1 from fp_Prospects where userid=@userid)
	if((select count(*) from fp_Prospects where userid=@userid and isnull(Name,'')='')>0)
		begin
			update fp_Prospects set Name=@name where userid=@userid
		end
		declare @user_count int 
		set @user_count=(select count(userid) from fin_User where userid=@userid)
		if(@user_count<1)
			begin
				insert [dbo].[fin_User] select @UUID,@userid,@name,@mobile,@Age,@Gender,@MaritialStatus,
				@AnnualIncome,@MonthlyExpence,@RetirementAge,@LifeExpentency,@Spouse,@ChildCount,@Emi,
				@EmiAmount,@Loans,@userid,@UUID,getdate(),@userid,@UUID,getdate(),1

				insert fin_User_Loans select @UUID,@userid,LoanID,@userid,@UUID,getdate(),@userid,@UUID,
				getdate(),1 from @LoansTbl
	
				insert fin_User_Relation select @UUID,@userid,Relation,RelationID,Name,Mobile,Age,Gender,
				AnnualIncome,@userid,@UUID,getdate(),@userid,@UUID,getdate(),1 from @UserRelationTbl

				insert fin_User_Goals select @UUID,@userid,RelationID,GoalCode,TypeCode,People,Location,
				Place,MonthlyAmount,Amount,Start,[End],CAST(Duration AS NVARCHAR(5)),Risk,InvestmentType,GetAmount,InvestAmount,
				CASE 
				WHEN ProductList is null or ProductList = '' 
				THEN (SELECT [dbo].[fin_fun_GetProductList](GoalCode,Risk,Duration,InvestAmount)) 
				ELSE ProductList END,
				null,null,@userid,@UUID,getdate(),@userid,@UUID,getdate(),1 from @UserGoalsTbl
			end
end

if(@action='SaveSingleGoal')

begin

	
	insert fin_User_Relation select @UUID,@userid,Relation,RelationID,Name,Mobile,Age,Gender,
	AnnualIncome,@userid,@UUID,getdate(),@userid,@UUID,getdate(),1 from @UserRelationTbl

		   IF NOT EXISTS (SELECT * FROM fin_user 
						   WHERE userid = @userid)
				   begin
						
						insert fin_user select @UUID,@userid,@name,@mobile,@age,@gender,@MaritialStatus,@AnnualIncome,@MonthlyExpence,@RetirementAge,@LifeExpentency,@Spouse,@ChildCount,@Emi,@EmiAmount,@Loans,@userid,@UUID,getdate(),@userid,@UUID,getdate(),1
				   end
	
	update fin_User set ChildCount=(select count(*) from fin_User_Relation where Relation = '002' and userid = @userid) where userid = @userid
	update fin_User set annualIncome = @AnnualIncome where userid = @userid

	insert fin_User_Goals select @UUID,@userid,RelationID,GoalCode,TypeCode,People,Location,
	Place,MonthlyAmount,Amount,Start,[End],CAST(Duration AS NVARCHAR(5)),Risk,InvestmentType,GetAmount,InvestAmount,
	CASE 
	WHEN ProductList is null or ProductList = '' 
	THEN (SELECT [dbo].[fin_fun_GetProductList](GoalCode,Risk,Duration,InvestAmount)) 
	ELSE ProductList END,
	null,null,@userid,@UUID,getdate(),@userid,@UUID,getdate(),1 from @UserGoalsTbl

end

if(@action='FetchGoal')

begin
	declare @userid_for_cursor nvarchar(max)
	set @userid_for_cursor=@userid

	--User details
    select * from [dbo].[fin_User] where UserID = @userid
	
	--User loan details
	select * from [dbo].[fin_User_Loans] where UserID = @userid

	--User own goal details
	select * from [dbo].[fin_User_Goals] where UserID = @userid and RelationID is null

	--User relation goal details
	select UR.*,UG.ID as UGID,UG.UUID as UGUUID,UG.UserID as UGUserID,UG.GoalCode as UGGoalCode,UG.TypeCode as UGTypeCode,UG.People as UGPeople,
	UG.Location as UGLocation,UG.Place as UGPlace,UG.MonthlyAmount as UGMonthlyAmount,UG.Amount as UGAmount,
	UG.Start as UGStart,UG.[End] as UGEnd,UG.Duration as UGDuration,UG.Risk as UGRisk,UG.InvestmentType as UGInvestmentType,
	UG.GetAmount as UGGetAmount,UG.InvestAmount as UGInvestAmount,UG.ProductList as UGProductList,UG.ActivatedDatetime as UGActivatedDatetime,
	UG.CompletedDatetime as UGCompletedDatetime,UG.CreatedDatetime as UGCreatedDatetime,UG.UpdatedDatetime as UGUpdatedDatetime,UG.[Status] as UGStatus
	from [dbo].[fin_User_Relation] UR left join [fin_User_Goals] UG on UR.RelationID=UG.RelationID where UR.UserID = @userid and ur.relation <> '001'

	--User Cart transaction for each goal details
	--select * from [dbo].[fin_Cart]  where UserID = @userid and status=1
	--User other transaction for each goal details
	--select * from [dbo].[fin_Other_Transactions_Details] where UserID = @userid

	--User final transaction for each goal details
	create table #CartTempTable
	(
	userId nvarchar(max),
	memberId nvarchar(max),
	profileId nvarchar(max),
	goalCode nvarchar(max),
	fundCode nvarchar(max),
	schemeCode nvarchar(max),
	foliono nvarchar(max),
	amount float,
	UserGoalId bigint
	)

	declare @memberId nvarchar(max),@profileId nvarchar(max),
	@fundCode nvarchar(max),@schemeCode nvarchar(max),@foliono nvarchar(max),@toSchemeCode nvarchar(max),@amount nvarchar(max),@type nvarchar(max),
	@startDate datetime,@noOfInstallment int,@UserGoalId bigint,@noofmonth int

	DECLARE investment_cursor CURSOR FOR  
	--cursor loop for lumpsum/instasip/sip for each investment in each goal
	SELECT userId,memberId,profileId,goalCode,fundCode,schemeCode,foliono,amount,type,startDate,noOfInstallment,UserGoalId
	FROM [dbo].[fin_Cart] where UserID=@userid_for_cursor and status=1 
	OPEN investment_cursor  
	FETCH NEXT FROM investment_cursor INTO @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,@amount,@type,@startDate,@noOfInstallment,@UserGoalId  
	WHILE @@FETCH_STATUS = 0  
	BEGIN  
	--print 'k'
			if exists(select * from #CartTempTable where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId)
					begin
						if(@type='L')
							begin
							update #CartTempTable set amount=CONVERT(float,amount)+CONVERT(float,@amount) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
							end
						else if(@type='I')
							begin
							set @noofmonth=  (select DateDiff(mm,@startDate, DateAdd(mm, 1,getdate())))
							--print @noofmonth
							if(@noofmonth<1)
								begin
								update #CartTempTable set amount=CONVERT(float,amount)+CONVERT(float,@amount)where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
								end
							else if(@noofmonth<=@noOfInstallment)
								begin
								update #CartTempTable set amount=CONVERT(float,amount)+(CONVERT(float,@amount)*@noofmonth)where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
								end
							else
								begin
								update #CartTempTable set amount=CONVERT(float,amount)+(CONVERT(float,@amount)*@noOfInstallment) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
								end
							end
						else if(@type='S')
							begin
							set @noofmonth=  (select DateDiff(mm,@startDate, DateAdd(mm, 1,getdate())))
							--print @noofmonth
							--if(@noofmonth<1)
							--	begin
							--	update #CartTempTable set amount='0' where fundCode=@fundCode and schemeCode=@schemeCode and UserGoalId=@UserGoalId
							--	end
							--else 
							if(@noofmonth>0 and @noofmonth<=@noOfInstallment)
								begin
								update #CartTempTable set amount=CONVERT(float,amount)+(CONVERT(float,@amount)*@noofmonth)where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
								end
							else if(@noofmonth>@noOfInstallment)
								begin
								update #CartTempTable set amount=CONVERT(float,amount)+(CONVERT(float,@amount)*@noOfInstallment) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
								end
							end
					end
			else
					begin
					--insert #CartTempTable select @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@amount,@UserGoalId
					if(@type='L')
						begin
						insert #CartTempTable select @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,CONVERT(float,@amount),@UserGoalId
						end
					else if(@type='I')
							begin
							set @noofmonth=  (select DateDiff(mm,@startDate, DateAdd(mm, 1,getdate())))
							--print @noofmonth
							if(@noofmonth<1)
								begin
								insert #CartTempTable select @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,CONVERT(float,@amount),@UserGoalId
								end
							else if(@noofmonth<=@noOfInstallment)
								begin
								insert #CartTempTable select @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,CONVERT(float,@amount)*@noofmonth,@UserGoalId
								end
							else
								begin
								insert #CartTempTable select @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,CONVERT(float,@amount)*@noOfInstallment,@UserGoalId
								end
							end
						else if(@type='S')
							begin
							set @noofmonth=  (select DateDiff(mm,@startDate, DateAdd(mm, 1,getdate())))
							--print @noofmonth
							--if(@noofmonth<1)
							--	begin
							--	update #CartTempTable set amount='0' where fundCode=@fundCode and schemeCode=@schemeCode and UserGoalId=@UserGoalId
							--	end
							--else 
							if(@noofmonth>0 and @noofmonth<=@noOfInstallment)
								begin
								insert #CartTempTable select @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,CONVERT(float,@amount)*@noofmonth,@UserGoalId
								end
							else if(@noofmonth>@noOfInstallment)
								begin
								insert #CartTempTable select @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,CONVERT(float,@amount)*@noOfInstallment,@UserGoalId
								end
							end
					end
			FETCH NEXT FROM investment_cursor INTO @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,@amount,@type,@startDate,@noOfInstallment,@UserGoalId  
	END  

	CLOSE investment_cursor  
	DEALLOCATE investment_cursor 

	DECLARE investment_cursor CURSOR FOR  
	--cursor loop for switch/stp/redemption/swp for each investment in each goal
	SELECT userId,memberId,profileId,goalCode,fundCode,schemeCode,foliono,toSchemeCode,amount,type,startDate,noOfInstallment,UserGoalId
	FROM [dbo].[fin_Other_Transactions_Details] where UserID=@userid_for_cursor
	OPEN investment_cursor  
	FETCH NEXT FROM investment_cursor INTO @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,@toSchemeCode,@amount,@type,@startDate,@noOfInstallment,@UserGoalId  

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		if(@type='SWITCH')
			begin
				if exists(select * from #CartTempTable where fundCode=@fundCode and schemeCode=@toSchemeCode and foliono=@foliono and UserGoalId=@UserGoalId)
					begin
					update #CartTempTable set amount=CONVERT(float,amount)+CONVERT(float,@amount) where fundCode=@fundCode and schemeCode=@toSchemeCode and foliono=@foliono and UserGoalId=@UserGoalId
					end
				else
					begin
					insert #CartTempTable select @userId,@memberId,@profileId,@goalCode,@fundCode,@toSchemeCode,@foliono,@amount,@UserGoalId
					end
				if exists(select * from #CartTempTable where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId)
					begin
					update #CartTempTable set amount=CONVERT(float,amount)-CONVERT(float,@amount) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
					end
			end
		else if(@type='STP')
			begin
				set @noofmonth=  (select DateDiff(mm,@startDate, DateAdd(mm, 1,getdate())))
				if exists(select * from #CartTempTable where fundCode=@fundCode and schemeCode=@toSchemeCode and foliono=@foliono and UserGoalId=@UserGoalId)
					begin
							if(@noofmonth>0 and @noofmonth<=@noOfInstallment)
							begin
							update #CartTempTable set amount=CONVERT(float,amount)+(CONVERT(float,@amount)*@noofmonth) where fundCode=@fundCode and schemeCode=@toSchemeCode and foliono=@foliono and UserGoalId=@UserGoalId
							end
						else  if(@noofmonth>@noOfInstallment)
							begin
							update #CartTempTable set amount=CONVERT(float,amount)+(CONVERT(float,@amount)*@noOfInstallment) where fundCode=@fundCode and schemeCode=@toSchemeCode and foliono=@foliono and UserGoalId=@UserGoalId
							end
					end
				else
					begin
						if(@noofmonth>0 and @noofmonth<=@noOfInstallment)
							begin
								insert #CartTempTable select @userId,@memberId,@profileId,@goalCode,@fundCode,@toSchemeCode,@foliono,@amount,@UserGoalId
							end
						else  if(@noofmonth>@noOfInstallment)
							begin
								insert #CartTempTable select @userId,@memberId,@profileId,@goalCode,@fundCode,@toSchemeCode,@foliono,@amount,@UserGoalId
							end
					end
				if exists(select * from #CartTempTable where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId)
					begin
						if(@noofmonth>0 and @noofmonth<=@noOfInstallment)
							begin
								update #CartTempTable set amount=CONVERT(float,amount)-(CONVERT(float,@amount)*@noofmonth) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
							end
						else  if(@noofmonth>@noOfInstallment)
							begin
								update #CartTempTable set amount=CONVERT(float,amount)-(CONVERT(float,@amount)*@noOfInstallment) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
							end
					end
			end
		else if(@type='R')
			begin
				if exists(select * from #CartTempTable where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId)
					begin
					update #CartTempTable set amount=CONVERT(float,amount)-CONVERT(float,@amount) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
					end
			end
		else if(@type='SWP')
			begin
				set @noofmonth=  (select DateDiff(mm,@startDate, DateAdd(mm, 1,getdate())))
				if exists(select * from #CartTempTable where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId)
					begin
						if(@noofmonth>0 and @noofmonth<=@noOfInstallment)
							begin
								update #CartTempTable set amount=CONVERT(float,amount)-(CONVERT(float,@amount)*@noofmonth) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
							end
						else  if(@noofmonth>@noOfInstallment)
							begin
								update #CartTempTable set amount=CONVERT(float,amount)-(CONVERT(float,@amount)*@noOfInstallment) where fundCode=@fundCode and schemeCode=@schemeCode and foliono=@foliono and UserGoalId=@UserGoalId
							end
					end
			end
			FETCH NEXT FROM investment_cursor INTO @userId,@memberId,@profileId,@goalCode,@fundCode,@schemeCode,@foliono,@toSchemeCode,@amount,@type,@startDate,@noOfInstallment,@UserGoalId  
	END  

	CLOSE investment_cursor  

	DEALLOCATE investment_cursor 

	select * from #CartTempTable

	drop table #CartTempTable

	select * from [dbo].[fin_InvestProfile] where groupleader in(select top 1 groupleader from [dbo].[fin_CAF_BasicDetails] where userid=@userid)

end

if(@action='UpdateGoal')

begin

    update [dbo].[fin_User] set Name=@name,Mobile=@mobile,Age=@Age,Gender=@Gender,MaritialStatus=@MaritialStatus,
	AnnualIncome=@AnnualIncome,MonthlyExpence=@MonthlyExpence,RetirementAge=@RetirementAge,
	LifeExpentency=@LifeExpentency,Spouse=@Spouse,ChildCount=@ChildCount,Emi=@Emi,
	EmiAmount=@EmiAmount,Loans=@Loans,UpdatedDatetime=getdate()
	where UserID=@userid

	if((select count(*) from @LoansTbl)>0)

	begin
	delete from fin_User_Loans where UserID=@userid
	insert fin_User_Loans select @UUID,@userid,LoanID,@userid,@UUID,getdate(),@userid,@UUID,
	getdate(),1 from @LoansTbl
	end

	if((select count(*) from @UserRelationTbl)>0)

	begin
	update UR set UR.Name=URT.Name,UR.Mobile=URT.Mobile,UR.Age=URT.Age,UR.Gender=URT.Gender,UR.AnnualIncome=URT.AnnualIncome,UR.UpdatedDatetime=getdate() 
	from [dbo].[fin_User_Relation] as UR join @UserRelationTbl URT on isnull(UR.RelationID,'')=isnull(URT.RelationID,'') and UR.UserID=@userid
	end

	if((select count(*) from @UserGoalsTbl)>0)

	begin
	update UG set UG.TypeCode=UGT.TypeCode,UG.People=UGT.People,UG.Location=UGT.Location,UG.Place=UGT.Place,UG.MonthlyAmount=UGT.MonthlyAmount,UG.Amount=UGT.Amount,UG.Start=UGT.Start,
	UG.[End]=UGT.[End],UG.Duration=UGT.Duration,UG.Risk=UGT.Risk,UG.InvestmentType=UGT.InvestmentType,UG.GetAmount=UGT.GetAmount,UG.InvestAmount=UGT.InvestAmount,
	UG.ProductList=(SELECT [dbo].[fin_fun_GetProductList](UGT.GoalCode,UGT.Risk,UGT.Duration,UGT.InvestAmount)),UG.UpdatedDatetime=getdate()
	from [dbo].[fin_User_Goals] as UG join @UserGoalsTbl UGT 
	on isnull(UG.RelationID,'')=isnull(UGT.RelationID,'') and UG.GoalCode=UGT.GoalCode and UG.GoalCode!='FG14' and UG.UserID=@userid

	update UG set UG.People=UGT.People,UG.Location=UGT.Location,UG.Place=UGT.Place,UG.MonthlyAmount=UGT.MonthlyAmount,UG.Amount=UGT.Amount,UG.Start=UGT.Start,
	UG.[End]=UGT.[End],UG.Duration=UGT.Duration,UG.Risk=UGT.Risk,UG.InvestmentType=UGT.InvestmentType,UG.GetAmount=UGT.GetAmount,UG.InvestAmount=UGT.InvestAmount,
	UG.ProductList=(SELECT [dbo].[fin_fun_GetProductList](UGT.GoalCode,UGT.Risk,UGT.Duration,UGT.InvestAmount)),UG.UpdatedDatetime=getdate()
	from [dbo].[fin_User_Goals] as UG join @UserGoalsTbl UGT 
	on isnull(UG.RelationID,'')=isnull(UGT.RelationID,'') and UG.GoalCode='FG14' and UG.TypeCode=UGT.TypeCode and UG.UserID=@userid

	end

end

if(@action = 'SelectChildInGoal')
begin
select userid,name,relationID,age,Gender from  [dbo].[fin_User_Relation] where isnull(relationid,0) not in (
select relationID from [dbo].[fin_User_Goals] where goalcode = @goalCode and userid = @userid) and userid = @userid and Relation=002

select userid,name,relationID,age,Gender from  [dbo].[fin_User_Relation] where isnull(relationid,0) in (
select relationID from [dbo].[fin_User_Goals] where goalcode = @goalCode and userid = @userid) and userid = @userid and Relation=002
end

end
GO

USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_InvestAction]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[fin_Proc_InvestAction]
@GoalCode nvarchar (500)=null,
@RiskCode nvarchar (100)=null,
@TimeCode nvarchar (100)=null,
@AmountCode nvarchar (100)=null,
@ProductCode nvarchar (500)=null,
@action varchar(500)
as
begin
	if(@action='PRODUCTID')
		begin
			if((select Count(*) from [dbo].[fin_Goal_Products] where GoalCode=@GoalCode)>0)
				begin
					select ProductCode from [dbo].[fin_Goal_Products] where GoalCode=@GoalCode and RiskCode=@RiskCode and TimeCode=@TimeCode and AmountCode=@AmountCode
				end
			else
				begin
				select ProductCode from [dbo].[fin_Goal_Products] where GoalCode='FG4' and RiskCode=@RiskCode and TimeCode=@TimeCode and AmountCode=@AmountCode
				end
		end
end
GO

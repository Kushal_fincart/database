USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_Campaign_Action]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[fin_Proc_Campaign_Action] @id BIGINT = NULL
	,@name NVARCHAR(max) = NULL
	,@key NVARCHAR(max) = NULL
	,@status BIT = 1
	,@Action NVARCHAR(50) = NULL
	,@EmpCode NVARCHAR(max) = NULL
	,@errormessage INT = NULL OUTPUT
AS
BEGIN
	DECLARE @currentid AS BIGINT
	DECLARE @clientstcd NVARCHAR(max)

	IF (@Action = 'INSERT')
	BEGIN
		INSERT INTO fin_campaign (
			name
			,[key]
			,createdby
			,createddate,UpdatedBy,UpdatedDate
			)
		VALUES (
			rtrim(ltrim(@name))
			,rtrim(ltrim(@key))
			,rtrim(ltrim(@EmpCode))
			,getdate(),rtrim(ltrim(@EmpCode)),GETDATE()
			)

		SET @currentid = SCOPE_IDENTITY()
		SET @clientstcd = 'C' + CONVERT(VARCHAR(max), @currentid)

		UPDATE fin_campaign
		SET clientstatuscode = rtrim(ltrim(@clientstcd))
			,[status] = @status
		WHERE id = @currentid
	END
	ELSE IF (@Action = 'UPDATE')
	BEGIN
		UPDATE fin_campaign
		SET name = rtrim(ltrim(@name))
			,updatedby = rtrim(ltrim(@EmpCode))
			,updateddate = GETDATE()
			,STATUS = @status
		WHERE id = @id
	END
	ELSE IF EXISTS (
				SELECT *
				FROM fin_Mapping_Camp_Plat
				WHERE Camp_Id = @id
				)
		BEGIN
			SET @errormessage = 0
		END
		ELSE
		BEGIN
			DELETE
			FROM fin_campaign
			WHERE id = @id

		SET @errormessage = 1
		END
END



GO

USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Workpoint_Action_revised]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE Proc [dbo].[fin_Workpoint_Action_revised]
@Id bigint=null,
@userid [nvarchar](max)=null,
@pass [nvarchar](max)=null,
@LEADCODE bigint = null,
@EMPCODE nvarchar(max) = null,
@REASSIGNTO nvarchar(max) = null,
@REASON nvarchar(max) = null,
@COMMENT nvarchar(max) = null,
@ACTIVITYTYPEID bigint=null, 
@LEADTYPEID int = null,
@ACTIONDATETIME datetime = null,
@LEADSTATUS varchar(max) = null,
@CREATEDDATE datetime = null,
@ASSIGNEDDATE datetime = null,
@LASTACTIVITYDATE datetime = null,
@PageIndex INT = 0,					--FOR PAGING
@PageSize INT =0,					--FOR PAGING
@DATEFROM datetime = '',			--- FOR DYNAMIC QUERY
@DATETO datetime ='',			--- FOR DYNAMIC QUERY
@FILTERBY1 nvarchar(max) = null,	--- FOR DYNAMIC QUERY
@FILTERBY2 nvarchar(max) = null,	--- FOR DYNAMIC QUERY
@FILTERBYNAME nvarchar(max) = null,	--- FOR DYNAMIC QUERY
@FILTERBYEMAIL nvarchar(max) = null,	--- FOR DYNAMIC QUERY
@FILTERBYMOBILE nvarchar(max) = null,	--- FOR DYNAMIC QUERY
@FILTERBYCAMPAIGN nvarchar(max) = null,	--- FOR DYNAMIC QUERY

--@BULKLEADTBL fin_Type_BulkLeadTbl READONLY,     --- FOR BULK LEAD ACTIONS
@Action[varchar](100),
@ASSIGNEDTOEMPCODE varchar(max) = null OUTPUT,
@RecordCount BIGINT=null OUTPUT
as
begin
Declare @Conditions VARCHAR(MAX) = ''		--- FOR DYNAMIC QUERY
Declare @EmpQuery VARCHAR(MAX) = ''		--- FOR DYNAMIC QUERY
Declare @TestQuery NVARCHAR(MAX) = NULL	--- FOR DYNAMIC QUERY
Declare @ParmDefinition nvarchar(max) = null  --- FOR DYNAMIC QUERY
Declare @LTypeId int = null
Declare @CurrentLead int = null
if(@Action='LOGIN')
begin
select emp.empcode,emp.name,emp.userid,emp.pwd,rl.Role from fp_Employee emp join fp_employeeRole rl  on emp.RoleId = rl.Code where emp.userid = @userid and emp.Pwd = @pass and emp.status = 1
end

if(@Action='SELECT_LEADS_PAGED')
begin

	IF(@EMPCODE is not null and @EMPCODE != '')
		Begin
			set @EmpQuery = ' and lm.empcode =''' + @EMPCODE + ''''
			
		End
	Else 
		Begin
			set @EmpQuery = ''
			
		End
	
 if (@DATEFROM IS NOT NULL and @DATEFROM != '' and @DATETO IS NOT NULL and @DATETO != '' and @FILTERBY1 IS NOT NULL and @FILTERBY1 != '')
 Begin
 
 
		If(@FILTERBY1 = 'BY_CREATEDDATE')
			Begin
			 set @Conditions = @Conditions + ' and CONVERT(date, lm.createdDate) between ''' + CAST(CONVERT (DATE, @DATEFROM) as nvarchar(500))  
			 + ''' and ''' + CAST(CONVERT (DATE, @DATETO) as nvarchar(500)) +''''
			
			End
		Else
			Begin
			set @Conditions = @Conditions + ' and CONVERT(date, lm.lastActivityDate) between ''' + CAST(CONVERT (DATE, @DATEFROM)
			 as nvarchar(500)) + ''' and ''' + CAST(CONVERT (DATE, @DATETO) as nvarchar(500)) +''''
			End

		
 End
	
	If(@FILTERBY2 IS NOT NULL and @FILTERBY2 != '') 
		 Begin
			
				set @Conditions = @Conditions + ' and lm.leadTypeId= CASE WHEN @FILTERBY2=''INTERESTED'' then 18 WHEN 
				@FILTERBY2=''CONVERTED'' THEN 14 WHEN @FILTERBY2=''NEW'' THEN 11 WHEN @FILTERBY2=''DEAD'' THEN 16 WHEN @FILTERBY2=''READY TO INVEST'' 
				THEN 19 WHEN @FILTERBY2=''ON HOLD'' THEN 17 WHEN @FILTERBY2=''ASSIGNED'' THEN 12 WHEN @FILTERBY2=''FOLLOW UP'' THEN 13 WHEN @FILTERBY2=''RE-ASSIGNED'' THEN 15 ELSE 0 END'
				 
		End	


		If(@FILTERBYNAME IS NOT NULL and @FILTERBYNAME != '') 
		 Begin
			
				set @Conditions = @Conditions + ' and fp.name like ''%' + @FILTERBYNAME + '%'''
				 
		 End	

		 If(@FILTERBYEMAIL IS NOT NULL and @FILTERBYEMAIL != '') 
		 Begin
			
				set @Conditions = @Conditions + ' and fp.userid like ''%' + @FILTERBYEMAIL + '%'''
				 
		 End	

		 If(@FILTERBYMOBILE IS NOT NULL and @FILTERBYMOBILE != '') 
		 Begin
			
				set @Conditions = @Conditions + ' and fp.mobile1 like ''%' + @FILTERBYMOBILE + '%'''
				 
		 End
		 
		 If(@FILTERBYCAMPAIGN IS NOT NULL and @FILTERBYCAMPAIGN != '') 
		 Begin
			IF(@FILTERBYCAMPAIGN != 'NULL')
				BEGIN
					set @Conditions = @Conditions + ' and fp.Clientstatus = ''' + @FILTERBYCAMPAIGN + ''''
				END

			ELSE 
				BEGIN
					set @Conditions = @Conditions + ' and fp.Clientstatus IS NULL'
				END
		 End		


 SET @ParmDefinition = N'@FILTERBY2 nvarchar (max), @RecordCount BIGINT OUTPUT, @PageIndex INT,  @PageSize INT';
 
 Set @TestQuery = 'SET @RecordCount = (select count(*) FROM fin_leadmapping lm join fp_prospects fp on lm.leadcode = fp.code where status = 1 ' 
 + @EmpQuery + @Conditions + '); ' +

 ' WITH MyCte AS 
(
    select lm.id,lm.leadcode,lm.empCode,fp.Name as ClientName,fp.UserId,fp.mobile1 as mobile,(CASE WHEN fp.Clientstatus like ''FG%'' then fg.GDesc WHEN fp.Clientstatus like ''C%'' THEN cmp.C_Name ELSE ''Direct Registration'' END) as Campaign, ltm.Name as LeadType,emp.Name as Planner, lm.lastActivityDate,lm.createdDate,ROW_NUMBER() OVER (ORDER BY lm.createdDate desc) RowNumber from fin_leadmapping lm left join fp_Prospects fp on lm.leadCode=fp.Code join [dbo].[fp_Employee] emp on lm.empcode = emp.empcode join fin_LeadTypeMaster ltm on lm.leadTypeId=ltm.leadTypeId left join fp_Campaign cmp on fp.clientstatus= cmp.C_ClientSatusCode And fp.Clientstatus like ''C%'' left join fin_Goal fg on fp.clientstatus=fg.Gcode And fp.Clientstatus like ''FG%'' where lm.status = 1 ' + @EmpQuery + @Conditions + ' ) SELECT  *
FROM    MyCte WHERE   MyCte.RowNumber BETWEEN(@PageIndex -1) *  @PageSize +  1 AND(((@PageIndex -1) * @PageSize + 1) +  @PageSize) - 1;

select @RecordCount as total; '


 
 --print @TestQuery
 EXECUTE sp_executesql @TestQuery, @ParmDefinition,@FILTERBY2=@FILTERBY2, @RecordCount = @RecordCount OUTPUT, @PageIndex = @PageIndex, @PageSize = @PageSize;
 



end

if(@Action='SELECT_ALL_LEADS')
begin


IF(@EMPCODE is not null and @EMPCODE != '')
		Begin
			set @EmpQuery = ' and lm.empcode =''' + @EMPCODE + ''''
		End
	Else 
		Begin
			set @EmpQuery = ''
		End
	
 IF (@DATEFROM IS NOT NULL and @DATEFROM != '' and @DATETO IS NOT NULL and @DATETO != '' and @FILTERBY1 IS NOT NULL and @FILTERBY1 != '')
 Begin
 
		If(@FILTERBY1 = 'BY_CREATEDDATE')
			Begin
			 set @Conditions = @Conditions + ' and CONVERT(date, createdDate) between ''' + CAST(CONVERT (DATE, @DATEFROM) as nvarchar(500))  
			 + ''' and ''' + CAST(CONVERT (DATE, @DATETO) as nvarchar(500)) +''''
			
			End
		Else
			Begin
			set @Conditions = @Conditions + ' and CONVERT(date, lastActivityDate) between ''' + CAST(CONVERT (DATE, @DATEFROM)
			 as nvarchar(500)) + ''' and ''' + CAST(CONVERT (DATE, @DATETO) as nvarchar(500)) +''''
			End
 End

	If(@FILTERBY2 IS NOT NULL and @FILTERBY2 != '') 
		 Begin
			
				set @Conditions = @Conditions + ' and lm.leadTypeId= CASE WHEN @FILTERBY2=''INTERESTED'' then 18 WHEN 
				@FILTERBY2=''CONVERTED'' THEN 14 WHEN @FILTERBY2=''NEW'' THEN 11 WHEN @FILTERBY2=''DEAD'' THEN 16 WHEN @FILTERBY2=''READY TO INVEST'' 
				THEN 19 WHEN @FILTERBY2=''ON HOLD'' THEN 17 WHEN @FILTERBY2=''ASSIGNED'' THEN 12 WHEN @FILTERBY2=''FOLLOW UP'' THEN 13 WHEN @FILTERBY2=''RE-ASSIGNED'' THEN 15 ELSE 0 END'
				 
		End	

			If(@FILTERBYNAME IS NOT NULL and @FILTERBYNAME != '') 
		 Begin
			
				set @Conditions = @Conditions + ' and fp.name like ''%' + @FILTERBYNAME + '%'''
				 
		 End	

		 If(@FILTERBYEMAIL IS NOT NULL and @FILTERBYEMAIL != '') 
		 Begin
			
				set @Conditions = @Conditions + ' and fp.userid like ''%' + @FILTERBYEMAIL + '%'''
				 
		 End	

		 If(@FILTERBYMOBILE IS NOT NULL and @FILTERBYMOBILE != '') 
		 Begin
			
				set @Conditions = @Conditions + ' and fp.mobile1 like ''%' + @FILTERBYMOBILE + '%'''
				 
		 End
		 
		 If(@FILTERBYCAMPAIGN IS NOT NULL and @FILTERBYCAMPAIGN != '') 
		 Begin
			IF(@FILTERBYCAMPAIGN != 'NULL')
				BEGIN
					set @Conditions = @Conditions + ' and fp.Clientstatus = ''' + @FILTERBYCAMPAIGN + ''''
				END

			ELSE 
				BEGIN
					set @Conditions = @Conditions + ' and fp.Clientstatus IS NULL'
				END
		 End	

 SET @ParmDefinition = N'@FILTERBY2 nvarchar (max)';

 Set @TestQuery = ' WITH MyCte AS 
(
    select lm.id,lm.leadcode,lm.empCode,fp.Name as ClientName,fp.UserId,fp.mobile1 as mobile,(CASE WHEN fp.Clientstatus like ''FG%'' then fg.GDesc WHEN fp.Clientstatus like ''C%'' THEN cmp.C_Name ELSE ''Direct Registration'' END) as Campaign, ltm.Name as LeadType,emp.Name as Planner, lm.lastActivityDate,lm.createdDate,ROW_NUMBER() OVER (ORDER BY lm.createdDate desc) RowNumber from fin_leadmapping lm left join fp_Prospects fp on lm.leadCode=fp.Code join [dbo].[fp_Employee] emp on lm.empcode = emp.empcode join fin_LeadTypeMaster ltm on lm.leadTypeId=ltm.leadTypeId left join fp_Campaign cmp on fp.clientstatus= cmp.C_ClientSatusCode And fp.Clientstatus like ''C%'' left join fin_Goal fg on fp.clientstatus=fg.Gcode And fp.Clientstatus like ''FG%'' where lm.status = 1 ' + @EmpQuery + @Conditions + ' ) SELECT  *
FROM  MyCte'


 EXECUTE sp_executesql @TestQuery, @ParmDefinition,@FILTERBY2=@FILTERBY2;

end
if(@Action='SELECT_LEADTYPE_MASTER')
begin

select distinct name from fin_LeadTypeMaster

end

if(@Action='SELECT_GOAL_MASTER')
begin

select distinct GName as name,GCode as code from fin_Goal where gtype=1

end
if(@Action='SELECT_LEADTYPE_COUNT')
Begin
select [12] as ASSIGNED, [15] as REASSIGNED, [18] as INTERESTED , [17] as ONHOLD , [13] as FOLLOWUP , [14] as CONVERTED , [19] as READYTOINVEST , [16] as DEAD 
from 
(
  select empCode,leadTypeId,status
  from fin_LeadMapping
) src 
pivot
(
  count(leadTypeId)
  for leadTypeId in ([12],[15],[18],[17],[13],[14],[19],[16])
) piv where empcode=@EMPCODE and status =1;
End
if(@Action='SELECT_CAMPAIGN_MASTER')
begin

select distinct C_Name as name,C_ClientSatusCode as code from fp_Campaign

end

if(@Action='SELECT_ACTIVITYTYPE_MASTER')
begin

select * from fin_ActivityTypeMaster where activityid != 0 and status = 1

end 
if(@Action='SELECT_LEAD_ACTIVITY_MONITOR')
begin
	if(@LEADCODE != '' and @LEADCODE IS NOT NULL)
	Begin
	select lam.Id,lam.leadCode,lam.empCode,lam.comment,aty.name as activity,aty.activityId,lam.actionDate,emp.Name as actionTakenBy,Convert		(date,lam.nextActionDate) as nextActionDate, CONVERT(VARCHAR(5),lam.nextActionDate,108) as Actiontime from [fin_LeadActivityMonitor] lam LEFT OUTER JOIN fin_ActivityTypeMaster aty on lam.activityId = aty.activityId LEFT OUTER JOIN fp_Employee emp on lam.empCode = emp.empcode where leadcode = @LEADCODE order by Convert(datetime,actionDate) desc 
	End
end
if(@Action='SELECT_ALL_EMPLOYEE')
begin

		IF(@EMPCODE is not null and @EMPCODE != '')
		Begin
			select * from fp_Employee where EmpCode != 'FIN01032013001'
		End
	Else 
		Begin
			select * from fp_Employee where EmpCode != @EMPCODE
			
		End

end


if(@Action='SELECT_ALL_REMINDERS')
begin

		IF(@EMPCODE is not null and @EMPCODE != '')
		Begin
			select AM.id, ATM.Name as Activity, (CASE WHEN FP.Name IS NULL OR FP.Name = '' THEN 'User' ELSE FP.Name END) 
			as Name, FP.Userid,FP.mobile1 as mobile, AM.comment,AM.NextActionDate from fin_LeadActivityMonitor AM 
			LEFT OUTER JOIN fp_Prospects FP on AM.leadCode = FP.Code LEFT OUTER JOIN fin_ActivityTypeMaster ATM on 
			AM.activityId = ATM.activityId where empcode=@EMPCODE and AM.activityId in (1,4,5) and AM.status = 1
			 and IsReminder = 1 order by nextActionDate desc

			
		End

	
end
if(@Action='SELECT_TOP20_REMINDERS')
begin

		IF(@EMPCODE is not null and @EMPCODE != '')
		Begin
			select top 20 AM.id, ATM.Name as Activity, (CASE WHEN FP.Name IS NULL OR FP.Name = '' THEN 'User' ELSE FP.Name END) 
			as Name, FP.Userid,FP.mobile1 as mobile, AM.comment,AM.NextActionDate from fin_LeadActivityMonitor AM 
			LEFT OUTER JOIN fp_Prospects FP on AM.leadCode = FP.Code LEFT OUTER JOIN fin_ActivityTypeMaster ATM on 
			AM.activityId = ATM.activityId where empcode=@EMPCODE and AM.activityId in (1,4,5) and AM.status = 1
			 and IsReminder = 1 order by nextActionDate desc

			
		End
	
end
if(@Action='SELECT_TOTAL_REMINDERS')
begin

		IF(@EMPCODE is not null and @EMPCODE != '')
		Begin
			select count(*) as total from fin_LeadActivityMonitor where empcode=@EMPCODE and activityId in (1,4,5) and status = 1
			 and IsReminder = 1 
			
		End
	
end
if(@Action='SELECT_CLIENTCAF_BY_EMAIL')
begin

		IF(@userid is not null and @userid != '')
		Begin
			select * from [dbo].[fin_CAF_BasicDetails] where userid = @userid
		End
	
end
if(@Action='UPDATE_LEAD_STATUS')
Begin

if(@LEADSTATUS is not null and @LEADSTATUS !='')
	Begin
		
		SET @LTypeId = CASE WHEN @LEADSTATUS = 'NEW' THEN 11 WHEN @LEADSTATUS = 'ASSIGNED' THEN 12 WHEN @LEADSTATUS = 'FOLLOW UP' THEN 13 WHEN @LEADSTATUS = 'CONVERTED' THEN 14 WHEN @LEADSTATUS = 'RE-ASSIGNED' THEN 15 WHEN @LEADSTATUS = 'DEAD' THEN 16 WHEN @LEADSTATUS = 'ON HOLD' THEN 17 WHEN @LEADSTATUS = 'INTERESTED' THEN 18 WHEN @LEADSTATUS = 'READY TO INVEST' THEN 19 ELSE NULL END

		IF (@LTypeId is not null)
			Begin
				
				update fin_leadmapping set leadTypeId=@LTypeId,lastActivityDate=getdate() where leadCode = @LEADCODE

				insert into fin_LeadStatusHistory(leadCode,leadType,empCode,StatusDate,status)
													values(@LEADCODE,@LTypeId,@EMPCODE,getdate(),1)
				
				insert into fin_LeadActivityMonitor (leadCode,empCode,comment,activityId,actionDate,nextActionDate,status)
				values(@LEADCODE,@EMPCODE,'Lead Status Changed By ' + (select name from fp_employee where empcode = @EMPCODE),
				2,getdate(),getdate(),1)

					if(@LEADSTATUS='CONVERTED' OR @LEADSTATUS='DEAD')
						Begin
							if (@EMPCODE != 'FIN01032013001') --CHECK IF UPDATER IS NOT ADMIN
								Begin
									if Exists(select * from fin_LeadMatrix where Isnext=1 and empcode = 'FIN01032013001')
										Begin
											update fin_LeadMatrix set currentLead = currentLead - 1,status=1,isnext=1 
											where empcode = @EMPCODE and currentLead > 0
											
											update fin_LeadMatrix set isnext=0 where empcode = 'FIN01032013001' 	
										End
									Else
										Begin
										update fin_LeadMatrix set currentLead = currentLead - 1,status=1
										where empcode = @EMPCODE and currentLead > 0		
										End
								End
						End
				Select 1 as result
			End
		Else 
		Begin Select 0 as result 
		End

	End
End
if(@Action='REMINDER_ACTIVITY_DONE')
Begin

if(@ID is not null and @ID !='')
	Begin
		update fin_leadActivityMonitor set isReminder=0 where id = @ID
		
				Select 1 as result
	End
	ELSE Select 0 as result
End

if(@Action='DELETE_ACTIVITYMONITOR')
Begin

if(@Id is not null and @Id !='')
	Begin
		
	

		IF (@LEADCODE is not null and @LEADCODE != '')
			Begin
				
				delete from [fin_LeadActivityMonitor] where leadcode = @LEADCODE and Id = @Id
				Select 1 as result
			End
		Else 
		Begin Select 0 as result 
		End

	End
End

if(@Action='LEAD_REASSIGN')
Begin

if(@REASSIGNTO is not null and @REASSIGNTO !='')
	Begin
		
		

		IF (@LEADCODE is not null and @LEADCODE != '')
			Begin
				

				update fin_leadmapping set empcode=@REASSIGNTO,assignedDate=getdate(),
				lastActivityDate=getdate(),leadTypeId=15 where leadCode = @LEADCODE

				insert into fin_LeadStatusHistory(leadCode,leadType,empCode,StatusDate,status)
													values(@LEADCODE,15,@EMPCODE,getdate(),1)

				insert into fin_LeadAllocationHistory (leadCode,AssignTo,AssignBy,assignedDate,reason,status)
													values(@LEADCODE,@REASSIGNTO,@EMPCODE,getdate(),@REASON,1)
				
				insert into fin_LeadActivityMonitor (leadCode,empCode,comment,activityId,actionDate,nextActionDate,status)
				
				values(@LEADCODE,@EMPCODE,'Re-Assigned to '+(select name from fp_employee where empcode = @REASSIGNTO) +' By ' + 
				(select name from fp_employee where empcode = @EMPCODE),3,getdate(),getdate(),1)

				if (@EMPCODE != 'FIN01032013001') --CHECK IF RE-ASSIGNER IS NOT ADMIN
								Begin
									if Exists(select * from fin_LeadMatrix where Isnext=1 and empcode = 'FIN01032013001')
										Begin
											-- DECREMENT ASSIGNER CURRENT LEAD CAP WITH MOVE ISNEXT TO----
											update fin_LeadMatrix set currentLead = currentLead - 1,status=1,isnext=1 
											where empcode = @EMPCODE and currentLead > 0
											
											update fin_LeadMatrix set isnext=0 where empcode = 'FIN01032013001' 
											
											-- INCREMENT ASSIGNEE CURRENT LEAD CAP----
											update fin_LeadMatrix set currentLead = currentLead + 1
											where empcode = @REASSIGNTO		
										End
									Else
										Begin
											-- DECREMENT ASSIGNER CURRENT LEAD CAP----
											update fin_LeadMatrix set currentLead = currentLead - 1,status=1
											where empcode = @EMPCODE and currentLead > 0		
											
											-- INCREMENT ASSIGNEE CURRENT LEAD CAP----
											update fin_LeadMatrix set currentLead = currentLead + 1
											where empcode = @REASSIGNTO		
										End
								End

				Select 1 as result
			End
		Else 
		Begin Select 0 as result 
		End

	End
End

if(@Action='CREATE_ACTIVITY')
Begin

if(@ACTIVITYTYPEID is not null and @ACTIVITYTYPEID !='')
	Begin
		
		IF (@EMPCODE is not null and @EMPCODE != '')
			Begin
					IF (@LEADCODE is not null and @LEADCODE != '')
						Begin
							
							DECLARE @ISREMINDER bit = 0;
							SET @ISREMINDER = (CASE WHEN @ACTIVITYTYPEID in (1,4,5) THEN 1 ELSE 0 END)
							
							insert into fin_LeadActivityMonitor(leadCode,empCode,comment,activityId,actionDate,
							nextActionDate,status,IsReminder)values(@LEADCODE,@EMPCODE,@COMMENT,@ACTIVITYTYPEID,
							getdate(),@ACTIONDATETIME,1,@ISREMINDER)


							Select 1 as result
						 End
					Else 
						 Begin Select 0 as result End
			End

	End
End
--if(@Action='BULK_LEAD_REASSIGN')
----Begin

----if(@REASSIGNTO is not null and @REASSIGNTO !='')
----	Begin
		
----		IF EXISTS (select * from @BULKLEADTBL)
----			Begin
				

----				update fin_leadmapping set empcode=@REASSIGNTO,assignedDate=getdate(),
----				lastActivityDate=getdate(),leadTypeId=15 where leadCode = @LEADCODE

----				insert into fin_LeadStatusHistory(leadCode,leadType,empCode,StatusDate,status)
----													values(@LEADCODE,15,@EMPCODE,getdate(),1)

----				insert into fin_LeadAllocationHistory (leadCode,AssignTo,AssignBy,assignedDate,reason,status)
----													values(@LEADCODE,@REASSIGNTO,@EMPCODE,getdate(),@REASON,1)
				
----				insert into fin_LeadActivityMonitor (leadCode,empCode,comment,activityId,actionDate,nextActionDate,status)
				
----				values(@LEADCODE,@EMPCODE,'Re-Assigned to '+(select name from fp_employee where empcode = @REASSIGNTO) +' By ' + 
----				(select name from fp_employee where empcode = @EMPCODE),3,getdate(),getdate(),1)


----				Select 1 as result
----			End
----		Else 
----		Begin Select 0 as result 
----		End

----	End
----End



if(@Action='AUTO_ALLOCATE_LEADBYMATRIX')
begin
declare @currECode nvarchar(max)
declare @nextECode nvarchar(max)
declare @currPrio bigint 
declare @topPrio bigint
declare @lastPrio int
declare @currRecord int
declare @adminECode nvarchar(max)
IF EXISTS (SELECT * FROM fin_LeadMatrix)
	
	BEGIN
		--GET ADMIN EMPCODE--
		select @lastPrio = max(priority) from fin_leadmatrix
		select @adminECode = empcode from fin_leadmatrix where priority = @lastPrio
		

		if EXISTS(select * from fin_LeadMatrix where isnext=1)--FOR FIRST TIME TO START R-ROBIN
			Begin
				--CHECK IF PLANNER IS NOT ADMIN--	
				if EXISTS(select * from fin_LeadMatrix where status = 1 and currentLead < maxlead and empcode != @adminECode)
					
					Begin
						
						select @currECode = empCode from fin_LeadMatrix where status = 1 and isnext=1 and currentLead < maxlead 
						order by priority asc																													IF (@currECode IS NOT NULL) AND (LEN(@currECode) > 0)
							BEGIN
								
								-- LOGIC TO GET NEXT PRIORITY PLANNER --STARTS
								select top 1 @topPrio = priority from fin_leadmatrix order by priority asc
								select @currPrio = priority from fin_leadmatrix where empcode = @currECode 
								select @currRecord = id from fin_leadmatrix where empcode = @currECode 
								IF(@lastPrio = @currPrio)
							Begin set @currPrio = 0; end
								if EXISTS(select * FROM fin_leadmatrix where priority > @currPrio and 
								status=1 and currentLead < maxLead)

									Begin
									SELECT top 1 @nextECode = empcode 
									FROM fin_leadmatrix where priority > @currPrio and status=1 and currentLead < maxLead order by priority asc
									--print @nextECode
								End

								Else
									Begin
										SET @nextECode =(select top 1 empcode 
										FROM fin_leadmatrix where status=1 and currentLead < maxLead order by priority asc)
										
									End
							-- LOGIC TO GET NEXT PRIORITY PLANNER --ENDS

								IF (@nextECode IS NOT NULL) AND (LEN(@nextECode) > 0)
									Begin
									
									--print 'next code  - '+ cast(@nextECode as varchar)
									--print '@currECode code  - '+ cast(@currECode as varchar)
										BEGIN TRAN
											update fin_LeadMatrix set Isnext = 0 where empcode = @currECode
											update fin_LeadMatrix set currentLead = currentLead + 1 where empcode = @currECode
											update fin_LeadMatrix set status = 0 where empcode = @currECode and currentLead >= maxLead
											if((select Count(*) from fin_LeadMatrix where status = 1) = 1)
												Begin 
													update fin_LeadMatrix set Isnext = 1 where empcode = @adminECode
												End
											Else
												Begin
													update fin_LeadMatrix set Isnext = 1 where empcode = @nextECode
												End
													--LeadType (11 for NEW , 12 for ASSIGNED------
													insert into fin_LeadMapping (leadCode,empCode,leadTypeId,
													createdDate,assignedDate,lastActivityDate,status)
													values(@LEADCODE,@currECode,12,getdate(),getdate(),getdate(),1)

													insert into fin_LeadStatusHistory(leadCode,leadType,empCode,
													StatusDate,status)
													values(@LEADCODE,11,@currECode,getdate(),1)

													insert into fin_LeadStatusHistory(leadCode,leadType,empCode,
													StatusDate,status)
													values(@LEADCODE,12,@currECode,getdate(),1)

													insert into fin_LeadAllocationHistory (leadCode,AssignTo,AssignBy,
													assignedDate,reason,status)
													values(@LEADCODE,@currECode,'AUTO',getdate(),'Auto Allocated By System',1)

													insert into fin_LeadActivityMonitor (leadCode,empCode,comment,activityId,
													actionDate,nextActionDate,status)
													values(@LEADCODE,@currECode,'Auto Allocated By System',0,getdate(),getdate(),1)

													SET @ASSIGNEDTOEMPCODE = @currECode

										
										COMMIT TRAN
									End
								
							
							END
					End
				ELSE
					BEGIN 
						----ALLOCATE ALL TO ADMIN---
						
							BEGIN TRAN
									--LeadType (11 for NEW , 12 for ASSIGNED------
													insert into fin_LeadMapping (leadCode,empCode,leadTypeId,
													createdDate,assignedDate,lastActivityDate,status)
													values(@LEADCODE,@adminECode,12,getdate(),getdate(),getdate(),1)

													insert into fin_LeadStatusHistory(leadCode,leadType,empCode,
													StatusDate,status)
													values(@LEADCODE,11,@adminECode,getdate(),1)

													insert into fin_LeadStatusHistory(leadCode,leadType,empCode,
													StatusDate,status)
													values(@LEADCODE,12,@adminECode,getdate(),1)

													insert into fin_LeadAllocationHistory (leadCode,AssignTo,AssignBy,
													assignedDate,reason,status)
													values(@LEADCODE,@adminECode,'AUTO',getdate(),'Auto Allocated By System',1)

													insert into fin_LeadActivityMonitor (leadCode,empCode,comment,activityId,
													actionDate,nextActionDate,status)
													values(@LEADCODE,@adminECode,'Auto Allocated By System',0,getdate(),getdate(),1)

													SET @ASSIGNEDTOEMPCODE = @adminECode
							COMMIT TRAN
						
					END
			End	
		
		Else
			Begin 
				--Print 'FIRST TIME RROBIN'
			--====GET INITIAL PLANNER TO START ROUND ROBIN FOR FIRST TIME===---
			SELECT top 1 @currECode = empcode FROM fin_leadmatrix order by priority asc

			-- LOGIC TO GET NEXT PRIORITY PLANNER --STARTS
			select top 1 @topPrio = priority from fin_leadmatrix order by priority asc
			select @currPrio = priority from fin_leadmatrix where empcode = @currECode 
			select @currRecord = id from fin_leadmatrix where empcode = @currECode 
			IF(@lastPrio = @currPrio)
			Begin set @currPrio = 0; end
			if EXISTS(select * FROM fin_leadmatrix where priority > @currPrio and status=1 and currentLead < maxLead)
				Begin
					SELECT top 1 @nextECode = empcode 
					FROM fin_leadmatrix where priority > @currPrio and status=1 and currentLead < maxLead order by priority asc
					--print @nextECode
				End

				Else
					Begin
						SELECT top 1 @nextECode = empcode 
						FROM fin_leadmatrix where status=1 and currentLead < maxLead order by priority asc
						--print @nextECode
					End
			-- LOGIC TO GET NEXT PRIORITY PLANNER --ENDS

			--print 'step - LOGIC TO GET NEXT PRIORITY PLANNER'
			
					BEGIN TRAN
											update fin_LeadMatrix set Isnext = 1 where empcode = @nextECode
											update fin_LeadMatrix set Isnext = 0 where empcode = @currECode
											update fin_LeadMatrix set currentLead = currentLead + 1 where empcode = @currECode
											if((select Count(*) from fin_LeadMatrix where status = 1) = 1)
												Begin 
													update fin_LeadMatrix set Isnext = 1 where empcode = @adminECode
												End
											Else
												Begin
													update fin_LeadMatrix set Isnext = 1 where empcode = @nextECode
												End
													--LeadType (11 for NEW , 12 for ASSIGNED------
													insert into fin_LeadMapping (leadCode,empCode,leadTypeId,
													createdDate,assignedDate,lastActivityDate,status)
													values(@LEADCODE,@currECode,12,getdate(),getdate(),getdate(),1)

													insert into fin_LeadStatusHistory(leadCode,leadType,empCode,
													StatusDate,status)
													values(@LEADCODE,11,@currECode,getdate(),1)

													insert into fin_LeadStatusHistory(leadCode,leadType,empCode,
													StatusDate,status)
													values(@LEADCODE,12,@currECode,getdate(),1)

													insert into fin_LeadAllocationHistory (leadCode,AssignTo,AssignBy,
													assignedDate,reason,status)
													values(@LEADCODE,@currECode,'AUTO',getdate(),'Auto Allocated By System',1)

													insert into fin_LeadActivityMonitor (leadCode,empCode,comment,activityId,
													actionDate,nextActionDate,status)
													values(@LEADCODE,@currECode,'Auto Allocated By System',0,getdate(),getdate(),1)

													SET @ASSIGNEDTOEMPCODE = @currECode


										
					COMMIT TRAN




			
			End
		
		
	END



end
End 



GO

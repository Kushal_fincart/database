USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[AddUpdateRRParam]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[AddUpdateRRParam] @Id int

	,@priority INT

	,@maxlead INT

	,@status varchar(5)

	,@Action VARCHAR(50) = NULL

AS

BEGIN


declare @empcode varchar(50)

select @empcode=rtrim(ltrim(EmpCode)) from fin_employee where id=@Id

	IF (@Action = 'UPDATE')

	BEGIN

		IF EXISTS (

				SELECT empCode

				FROM dbo.fin_LeadMatrix

				WHERE empCode = @empcode

				)

		BEGIN

			UPDATE fin_LeadMatrix

			SET priority = @priority

				,STATUS = @status

				,maxLead = @maxlead

			WHERE empCode = @empcode;
			select 1

		END

		ELSE

		BEGIN

			INSERT INTO fin_LeadMatrix (

				empCode

				,priority

				,maxLead

				,currentLead

				,Isnext

				,updatedDate

				,STATUS

				)

			VALUES (

				@empcode

				,@priority

				,@maxlead

				,0

				,0

				,GETDATE()

				,@status

				)
				select 2

		END

	END

END


GO

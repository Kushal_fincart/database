USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_proc_spGetExcelList]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[fin_proc_spGetExcelList](@emailer_list [UT_fin_emailerlist] readonly,@platformid bigint,@campid bigint)

as

begin

declare @currentplattrackercode nvarchar(max)

SELECT @currentplattrackercode = Track_Code

		FROM fin_Mapping_Camp_Plat

		WHERE Platform_Id = @platformid

			AND Camp_Id = @campid



SELECT email

		,usercode

		,@currentplattrackercode as TrackCode

	FROM fin_EmailerList where Email in(select email from @emailer_list)

			

end

GO

USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_SelectAllCafByBasicId]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[fin_Proc_SelectAllCafByBasicId]
@BASICID Bigint=0,
@Action [nvarchar](max)
as
Begin
if(@Action='SELECT_ALL_CAF_BY_BASICID')
begin

	Declare @SADDRESSID nvarchar (max)
	SET @SADDRESSID = (select sameAddress from fin_CAF_BasicDetails where Basic_id = @BASICID)

select BD.Basic_id, BD.userid,BD.memberId,BD.userPass,BD.groupLeader,BD.clientName,BD.phone,BD.mobile,BD.Occupation,BD.PanNumber,BD.AadharNumber,BD.Date_of_Birth,BD.Guardname,BD.Guardpan,BD.Guardpan_DOB,BD.GuardLocaladd,BD.account_type,BD.Annual_Income,BD.KycStatus,BD.ReqNominee,
BD.CAF_Status,BD.CAF_Basic_Status,BD.profilePic,BD.email,BD.sameAddress from fin_CAF_BasicDetails BD where BD.Basic_id = @BASICID

select ADRS.prm_Address,ADRS.corp_Address,ADRS.city,ADRS.state,ADRS.country,
ADRS.pincode,ADRS.corp_city,ADRS.corp_state,ADRS.corp_country,ADRS.corp_pincode,
ADRS.CAF_Address_Status from fin_CAF_Address ADRS where ADRS.basic_ID = @BASICID

select BNK.Bank_name,BNK.NameAsPerBank,BNK.Bank_Address,BNK.Acc_No,BNK.Branch,BNK.Bankcity,BNK.Acc_Type,BNK.Micr,BNK.Ifsc,
BNK.CAF_Bank_Status from fin_CAF_BankDetails BNK where BNK.basic_ID = @BASICID

select NOM.nominee_ID,NOM.Guardian_name,NOM.Guardian_pan,NOM.Nominee_Dob,NOM.Nominee_name,NOM.[percent],NOM.Relation,NOM.CAF_Nominee_Status 
from fin_CAF_Nominee NOM where NOM.basic_ID = @BASICID order by [nominee_ID] asc

select FTCA.place_of_birth,FTCA.Country_of_Birth,FTCA.Pol_Exp_Person,FTCA.[Country_Tax_Res1],FTCA.[CountryPayer_Id_no],FTCA.[Idenif_Type],FTCA.[Country_Tax_Res2],
FTCA.[CountryPayer_Id_no2],FTCA.[Idenif_Type2],FTCA.[Country_Tax_Res3],FTCA.[CountryPayer_Id_no3],FTCA.[Idenif_Type3],FTCA.[Source_Wealth],FTCA.[isTaxOther],FTCA.CAF_Fatca_Status from fin_CAF_FATCA FTCA 
where FTCA.basic_ID = @BASICID



End
End 


GO

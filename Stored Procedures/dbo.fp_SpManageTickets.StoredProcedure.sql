USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fp_SpManageTickets]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[fp_SpManageTickets]
@Id bigint=null,
@TicketId [nvarchar](max)=null,
@Sub [nvarchar](max)=null,
@Tat [nvarchar](500)=null,
@Query [nvarchar](max)=null,
@Attachment [nvarchar](max)=null,
@DepId [nvarchar](500)=null,
@RoleId [nvarchar](500)=null,
@CreatorEmail [nvarchar](1000)=null,
@AssignerRoleId [nvarchar](500)=null,
@AssignToEmail [nvarchar](1000)=null,
@ReportToEmail [nvarchar](1000)=null,
@UpdatedByEmail [nvarchar](1000)=null,
@Status int=null,
@Priority int=null,
@Action[varchar](100)
as
begin

if(@Action='DELETE')
begin
delete from [dbo].[fp_TicketSystem] where TicketId=@TicketId
delete from [dbo].[fp_TicketAssignedTrack] where TicketId=@TicketId 
delete from [dbo].[fp_TicketComments] where TicketId=@TicketId
end

if(@Action='INSERT')
begin
declare @tid [nvarchar](max)
set @tid=(select 'FN'+
convert(varchar,datepart(yyyy, getdate()))+
convert(varchar,datepart(MM, getdate()))+
convert(varchar,datepart(dd, getdate()))+
convert(varchar,datepart(hour, getdate()))+
convert(varchar,datepart(minute, getdate()))+
convert(varchar,datepart(second, getdate())))
insert fp_TicketSystem select @tid,@Query,@Attachment,@DepId,@RoleId,
@CreatorEmail,@AssignerRoleId,@CreatorEmail,@AssignToEmail,@ReportToEmail,@CreatorEmail,@Status,@Priority,getdate(),getdate(),@Sub,@Tat
insert fp_TicketAssignedTrack select @tid,@AssignToEmail,@CreatorEmail,@ReportToEmail,getdate()
end

if(@Action='SELECTBYTICKETID')
begin
select T.id,T.TicketId,T.Subject,datediff(second,getdate(),T.TAT) as TAT,T.TAT as TatDate,T.Query,Coalesce(NullIf(rtrim(T.Attachment),''),'/Admin/TicketAttachments/1_noimage.jpg') as Attachment,D.name as DepName,T.DepId,ER.Role,T.RoleId,T.AssignerRoleId,
EC.Name as CreatorName,T.CreatorEmail,AB.Name as AssignByName,T.AssignByEmail,AT.Name as AssignToName,T.AssignToEmail,
RT.Name as ReportToName,T.ReportToEmail,T.Status,T.Priority,T.OpenDate,T.UpdateDate,T.UpdatedByEmail
from [dbo].[fp_TicketSystem] as T 
join [dbo].[fp_Departments] as D on T.Depid=D.code
join [dbo].[fp_EmployeeRole] as ER on T.RoleId=ER.code
join [dbo].[fp_Employee] as EC on T.CreatorEmail=EC.UserID
join [dbo].[fp_Employee] as AB on T.AssignByEmail=AB.UserID
join [dbo].[fp_Employee] as AT on T.AssignToEmail=AT.UserID
join [dbo].[fp_Employee] as RT on T.ReportToEmail=RT.UserID
where T.TicketId=@TicketId 
order by Status,Priority
end

if(@Action='SELECTBYREPORTTOEMAIL')
begin
select T.id,T.TicketId,T.Subject,datediff(second,getdate(),T.TAT) as TAT,T.TAT as TatDate,T.Query,Coalesce(NullIf(rtrim(T.Attachment),''),'/Admin/TicketAttachments/1_noimage.jpg') as Attachment,D.name as DepName,T.DepId,ER.Role,T.RoleId,T.AssignerRoleId,
EC.Name as CreatorName,T.CreatorEmail,AB.Name as AssignByName,T.AssignByEmail,AT.Name as AssignToName,T.AssignToEmail,
RT.Name as ReportToName,T.ReportToEmail,T.Status,T.Priority,T.OpenDate,T.UpdateDate,T.UpdatedByEmail
from [dbo].[fp_TicketSystem] as T 
join [dbo].[fp_Departments] as D on T.Depid=D.code
join [dbo].[fp_EmployeeRole] as ER on T.RoleId=ER.code
join [dbo].[fp_Employee] as EC on T.CreatorEmail=EC.UserID
join [dbo].[fp_Employee] as AB on T.AssignByEmail=AB.UserID
join [dbo].[fp_Employee] as AT on T.AssignToEmail=AT.UserID
join [dbo].[fp_Employee] as RT on T.ReportToEmail=RT.UserID
where T.ReportToEmail=@ReportToEmail
order by Status,Priority
end

if(@Action='SELECTBYASSIGNTOEMAIL')
begin
select T.id,T.TicketId,T.Subject,datediff(second,getdate(),T.TAT) as TAT,T.TAT as TatDate,T.Query,Coalesce(NullIf(rtrim(T.Attachment),''),'/Admin/TicketAttachments/1_noimage.jpg') as Attachment,D.name as DepName,T.DepId,ER.Role,T.RoleId,T.AssignerRoleId,
EC.Name as CreatorName,T.CreatorEmail,AB.Name as AssignByName,T.AssignByEmail,AT.Name as AssignToName,T.AssignToEmail,
RT.Name as ReportToName,T.ReportToEmail,T.Status,T.Priority,T.OpenDate,T.UpdateDate,T.UpdatedByEmail
from [dbo].[fp_TicketSystem] as T 
join [dbo].[fp_Departments] as D on T.Depid=D.code
join [dbo].[fp_EmployeeRole] as ER on T.RoleId=ER.code
join [dbo].[fp_Employee] as EC on T.CreatorEmail=EC.UserID
join [dbo].[fp_Employee] as AB on T.AssignByEmail=AB.UserID
join [dbo].[fp_Employee] as AT on T.AssignToEmail=AT.UserID
join [dbo].[fp_Employee] as RT on T.ReportToEmail=RT.UserID
where T.AssignToEmail=@AssignToEmail
order by Status,Priority
end

if(@Action='SELECTALL')
begin
select T.id,T.TicketId,T.Subject,datediff(second,getdate(),T.TAT) as TAT,T.TAT as TatDate,T.Query,Coalesce(NullIf(rtrim(T.Attachment),''),'/Admin/TicketAttachments/1_noimage.jpg') as Attachment,D.name as DepName,T.DepId,ER.Role,T.RoleId,T.AssignerRoleId,
EC.Name as CreatorName,T.CreatorEmail,AB.Name as AssignByName,T.AssignByEmail,AT.Name as AssignToName,T.AssignToEmail,
RT.Name as ReportToName,T.ReportToEmail,T.Status,T.Priority,T.OpenDate,T.UpdateDate,T.UpdatedByEmail
from [dbo].[fp_TicketSystem] as T 
join [dbo].[fp_Departments] as D on T.Depid=D.code
join [dbo].[fp_EmployeeRole] as ER on T.RoleId=ER.code
join [dbo].[fp_Employee] as EC on T.CreatorEmail=EC.UserID
join [dbo].[fp_Employee] as AB on T.AssignByEmail=AB.UserID
join [dbo].[fp_Employee] as AT on T.AssignToEmail=AT.UserID
join [dbo].[fp_Employee] as RT on T.ReportToEmail=RT.UserID
order by Status,Priority
end

if(@Action='UPDATE')
begin
Update fp_TicketSystem 
Set Subject=@Sub,TAT=@Tat,Query=@Query,Attachment=@Attachment,UpdatedByEmail=@UpdatedByEmail,
Status=@Status,Priority=@Priority,UpdateDate=getdate() where Id=@Id
--DepId=@DepId,RoleId=@RoleId,
end

if(@Action='REASSIGN')
begin
Update fp_TicketSystem 
Set AssignerRoleId=@AssignerRoleId,AssignByEmail=@UpdatedByEmail,AssignToEmail=@AssignToEmail,ReportToEmail=@ReportToEmail,
UpdatedByEmail=@UpdatedByEmail,UpdateDate=getdate() where Id=@Id
insert fp_TicketAssignedTrack select @TicketId,@AssignToEmail,@UpdatedByEmail,@ReportToEmail,getdate()
end

end
GO

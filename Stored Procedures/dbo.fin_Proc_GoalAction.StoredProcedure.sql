USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_GoalAction]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[fin_Proc_GoalAction]



@GName nvarchar (max)=null,



@GDesc nvarchar (max)=null,



@GType int=2,



@GBrowserId nvarchar (500)=null,



@GBrowserIp nvarchar (500)=null,



@GCreatedByEmail nvarchar (500)=null,



@GCreatedByMobile nvarchar (50)=null,



@GUpdatedByEmail nvarchar (500)=null,



@GUpdatedByMobile nvarchar (50)=null,



@GStatus int=1,



@action varchar(500)



as



begin



	if(@action='ADDGOAL')



		begin



			declare @GCode nvarchar(max), @GNum int, @record int



			--check for goal if already in database



			set @record=(select COUNT(*) from fin_Goal where GName=@GName)

			set @GCode= (select GCode from fin_Goal where GName=@GName)

			if(@record=0)



				begin



					set @GNum=(select top 1 convert(int,REPLACE(GCode,'FG','')) as GCode from fin_Goal order by GCode desc)+1



					set @GCode ='FG'+ convert(varchar,@GNum)



					--print @GCode



					insert into fin_Goal(GCode,GName,GDesc,GType,GBrowserId,GBrowserIp,GCreatedByEmail,GCreatedByMobile,GCreatedDatetime,GUpdatedByEmail,GUpdatedByMobile,GUpdatedDatetime,GStatus)



					values(@GCode,@GName,@GDesc,@GType,@GBrowserId,@GBrowserIp,@GCreatedByEmail,@GCreatedByMobile,getdate(),@GUpdatedByEmail,@GUpdatedByMobile,getdate(),@GStatus)



				end

				select @GCode as GoalCode

		end



	if(@action='ALLGOAL')

		begin

			select * from [dbo].[fin_Goal]

		end



	if(@action='FINCARTGOAL')

		begin

			select * from [dbo].[fin_Goal] where GType=1

		end



	if(@action='OTHERGOAL')

		begin

			if(@GName is null or @GName='')

				begin

					select * from [dbo].[fin_Goal] where GType=2

				end

			else

				begin

					select * from [dbo].[fin_Goal] where GType=2 and GName like ''+@GName+'%'

				end

		end

end
GO

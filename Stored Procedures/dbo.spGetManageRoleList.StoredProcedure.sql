USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[spGetManageRoleList]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spGetManageRoleList]
@Action varchar(30),
@RoleID int=0
as
 begin
 if(@Action='AllList')
 begin
 select  Id,Code,DepCode,DeptName,Role,Level,CreatorEmail,Status,CreatedDate,UpdatedDate from vw_ManageRoleList where Status=1;
 end
 if(@Action='SelectedList')
 begin
 select  Id,Code,DepCode,DeptName,Role,Level,CreatorEmail,Status,CreatedDate,UpdatedDate from vw_ManageRoleList where Id=@RoleID and  Status=1;
 end

 end
GO

USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_fin_Mapping_Camp_Plat_Action]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO










CREATE PROCEDURE [dbo].[fin_Proc_fin_Mapping_Camp_Plat_Action] @id BIGINT =null

	,@camp_id bigint=null

	,@platform_id NVARCHAR(max)=null

	,@track_code NVARCHAR(max)=null

	,@status VARCHAR(5)=1

	,@Action NVARCHAR(50)=null

	,@EmpCode NVARCHAR(max)=null

AS

BEGIN

	

	IF (@Action = 'INSERT')

	BEGIN

		INSERT INTO fin_Mapping_Camp_Plat (

			Camp_Id

			,Platform_Id,Track_Code

			,createdby

			,createddate,Status,UpdatedBy,UpdatedDate

			)

		VALUES (

			rtrim(ltrim(@camp_id))

			,rtrim(ltrim(@platform_id)),rtrim(ltrim(@track_code))

			,rtrim(ltrim(@EmpCode))

			,getdate(),@status,rtrim(ltrim(@EmpCode)),GETDATE()

			)

	END

	ELSE IF (@Action = 'UPDATE')

	BEGIN

		UPDATE fin_Mapping_Camp_Plat

		SET camp_id = rtrim(ltrim(@camp_id)),Platform_Id=rtrim(ltrim(@platform_id)),Track_Code=rtrim(ltrim(@track_code))

			,updatedby = rtrim(ltrim(@EmpCode))

			,updateddate = GETDATE(),Status=@status

		WHERE id = @id



		SELECT 1

	END

	ELSE

	BEGIN

		DELETE

		FROM fin_Mapping_Camp_Plat

		WHERE id = @id



		SELECT 1

	END

END





GO

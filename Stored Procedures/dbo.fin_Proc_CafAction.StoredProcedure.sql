USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_CafAction]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[fin_Proc_CafAction]
@ID bigint=null,
@USERID [nvarchar](500)=null,
@PASSWORD [nvarchar](500)=null,
@MEMBERID [nvarchar](max)=null,
@GROUPLEADER [nvarchar](max)=null,
@TITLE [nvarchar](500)=null,
@CLIENTNAME [nvarchar](max)=null,
@PHONE [nvarchar](max)=null,
@MOBILE [nvarchar](max)=null,
@OCCUPATION [nvarchar](max)=null,
@PANNUMBER [nvarchar](max)=null,
@AADHARNUMBER [nvarchar](max)=null,
@DATEOFBIRTH date=null,
@GUARDNAME [nvarchar](max)=null,
@GUARDPAN [nvarchar](max)=null,
@GUARDPANDOB date =null,
@GUARDLOCALADD [nvarchar](max)=null,
@ACCOUNTTYPE [nvarchar](max)=null,
@ANNUALINCOME [nvarchar](max)=null,
@KYCSTATUS [nvarchar](500)=null,
@REQNOMINEE [nvarchar](500)=null,
@CAFBASICSTATUS bit=0,
@CAFSTATUS bit=0,
@PROFILEPIC [nvarchar](max)=null,
@EMAIL [nvarchar](max)=null,
@SAMEADDRESS nvarchar (max)= NULL,
-------ADDRESS----
@ADDRESS [nvarchar](max)=null,
@CITY [nvarchar](max)=null,
@STATE [nvarchar](max)=null,
@COUNTRY [nvarchar](max)=null,
@PINCODE [nvarchar](max)=null,
@CORSPADDRESS [nvarchar](max)=null,
@CORSCITY [nvarchar](max)=null,
@CORSSTATE [nvarchar](max)=null,
@CORSCOUNTRY [nvarchar](max)=null,
@CORSPINCODE [nvarchar](max)=null,
@CAFADDRESSSTATUS bit=0,

---BANK----
@BANKNAME [nvarchar](max)=null,
@NAMEASPERBANK [nvarchar](max)=null,
@BANKADDRESS [nvarchar](max)=null,
@BANKACCNO [nvarchar](max)=null,
@BANKBRANCH [nvarchar](max)=null,
@BANKCITY [nvarchar](max)=null,
@BANKACCTYPE [nvarchar](max)=null,
@BANKMICR [nvarchar](max)=null,
@BANKIFSC [nvarchar](max)=null,
@CAFBANKSTATUS bit=0,

----NOMINEE-----
@NOMNAME [nvarchar](max)=null,
@NOMRELATION [nvarchar](max)=null,
@NOMDOB date=null,
@NOMPERCENT int=0,
@NOMGUARDNAME [nvarchar](max)=null,
@NOMGUARDPAN [nvarchar](max)=null,
@CAFNOMINEESTATUS bit=0,
@NOMID Bigint=0,

------FATCA------
@FATCAPLACEOFBIRTH [nvarchar](max)=null,
@FATCACOUNTRYOFBIRTH [nvarchar](max)=null,
@FATCAPOLEXPPERSON [nvarchar](max)=null,
@FATCACOUNTRYTAXRES1 [nvarchar](max)=null,
@FATCACOUNTRYPAYIDNO1 [nvarchar](max)=null,
@FATCAIDENTYPE1 [nvarchar](max)=null,
@FATCACOUNTRYTAXRES2 [nvarchar](max)=null,
@FATCACOUNTRYPAYIDNO2 [nvarchar](max)=null,
@FATCAIDENTYPE2 [nvarchar](max)=null,
@FATCACOUNTRYTAXRES3 [nvarchar](max)=null,
@FATCACOUNTRYPAYIDNO3 [nvarchar](max)=null, 
@FATCAIDENTYPE3 [nvarchar](max)=null, 
@FATCASOURCEWEALTH [nvarchar](max)=null,
@ISTAXOTHER [nvarchar](max)=null,
@CAFFATCASTATUS bit=0,

@BASICID Bigint=0,
@INVSTACCOUNTSTBL fin_Type_InvestmentAccounts READONLY,
@Action [nvarchar](max)
as
Begin

DECLARE @BASIC_ID BIGINT;
DECLARE @GRPLEADER [nvarchar](max);
 
if(@Action='INSERT_CAF_INIT_RECORD')
begin
	IF NOT EXISTS(select * from fin_CAF_BasicDetails where userid = @USERID)
	Begin
		insert into fin_CAF_BasicDetails(userid, memberId, userPass, groupLeader, title, clientName,phone,mobile,
		Occupation,PanNumber,AadharNumber,Date_of_Birth, Guardname, Guardpan, Guardpan_DOB, GuardLocaladd, account_type,			        Annual_Income, kycStatus, ReqNominee,CAF_Basic_Status, CAF_Status,profilePic,createdDate,updatedDate,email,sameAddress) select @USERID,@MEMBERID,@PASSWORD,
		@GROUPLEADER,@TITLE,@CLIENTNAME,@PHONE,@MOBILE,@OCCUPATION,@PANNUMBER,@AADHARNUMBER,@DATEOFBIRTH,@GUARDNAME,@GUARDPAN
		,@GUARDPANDOB,@GUARDLOCALADD,@ACCOUNTTYPE,@ANNUALINCOME,@KYCSTATUS,@REQNOMINEE,@CAFBASICSTATUS,@CAFSTATUS,@PROFILEPIC,getdate(),getdate(),@EMAIL,@SAMEADDRESS
		 
		 SET @BASIC_ID = scope_identity();

		insert into fin_CAF_Address([basic_ID],[prm_Address],[corp_Address],[city],[state],[country],[pincode],
		[corp_city],[corp_state],[corp_country],[corp_pincode],[CAF_Address_Status],createdDate,updatedDate) 
		select @BASIC_ID,@ADDRESS,@CORSPADDRESS,@CITY,@STATE,@COUNTRY,@PINCODE,@CORSCITY,@CORSSTATE,@CORSCOUNTRY,@CORSPINCODE,
		@CAFADDRESSSTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_BankDetails]([basic_ID],[Bank_name],[NameAsPerBank],[Bank_Address],[Acc_No],
		[Branch],[Bankcity],[Acc_Type],[Micr],[Ifsc],[CAF_Bank_Status],createdDate,updatedDate) 
		select @BASIC_ID,@BANKNAME,@NAMEASPERBANK,@BANKADDRESS,@BANKACCNO,@BANKBRANCH,@BANKCITY,@BANKACCTYPE,
		@BANKMICR,@BANKIFSC,@CAFBANKSTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_Nominee] ([basic_ID],[Nominee_name],[Relation],[Nominee_Dob],[percent],
		[Guardian_name],[Guardian_pan],[CAF_Nominee_Status],createdDate,updatedDate) select @BASIC_ID,@NOMNAME,@NOMRELATION,@NOMDOB,
		@NOMPERCENT,@NOMGUARDNAME,@NOMGUARDPAN,@CAFNOMINEESTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_FATCA] ([basic_ID],[Place_of_Birth],[Country_of_Birth],[Pol_Exp_Person],
		[Country_Tax_Res1],[CountryPayer_Id_no],[Idenif_Type],[Country_Tax_Res2],[CountryPayer_Id_no2],
		[Idenif_Type2],[Country_Tax_Res3],[CountryPayer_Id_no3],[Idenif_Type3],[Source_Wealth],
		[isTaxOther],[CAF_Fatca_Status],createdDate,updatedDate) select 
		@BASIC_ID,@FATCAPLACEOFBIRTH,@FATCACOUNTRYOFBIRTH,@FATCAPOLEXPPERSON,@FATCACOUNTRYTAXRES1,
		@FATCACOUNTRYPAYIDNO1,@FATCAIDENTYPE1,@FATCACOUNTRYTAXRES2,@FATCACOUNTRYPAYIDNO2, @FATCAIDENTYPE2,
		 @FATCACOUNTRYTAXRES3, @FATCACOUNTRYPAYIDNO3, @FATCAIDENTYPE3, @FATCASOURCEWEALTH,@ISTAXOTHER,@CAFFATCASTATUS,getdate(),getdate()

		
	End
	

end
if(@Action='SAVE_CAF_BASIC')
begin
		IF (@BASICID is NULL OR @BASICID = '' OR @BASICID = 0)
			BEGIN
				
				SET @GRPLEADER = (select memberId from fin_CAF_BasicDetails as bs where bs.userid=@USERID
									 and bs.memberid=bs.groupLeader)
				insert into fin_CAF_BasicDetails(userid, memberId, userPass, groupLeader, title, clientName,phone,mobile,
		Occupation,PanNumber,AadharNumber,Date_of_Birth, Guardname, Guardpan, Guardpan_DOB, GuardLocaladd, account_type,			        Annual_Income, kycStatus, ReqNominee,CAF_Basic_Status, CAF_Status,profilePic,createdDate,updatedDate,email,sameAddress) select @USERID,@MEMBERID,@PASSWORD,
		@GRPLEADER,@TITLE,@CLIENTNAME,@PHONE,@MOBILE,@OCCUPATION,@PANNUMBER,@AADHARNUMBER,@DATEOFBIRTH,@GUARDNAME,@GUARDPAN
		,@GUARDPANDOB,@GUARDLOCALADD,@ACCOUNTTYPE,@ANNUALINCOME,@KYCSTATUS,@REQNOMINEE,@CAFBASICSTATUS,@CAFSTATUS,@PROFILEPIC,getdate(),getdate(),@EMAIL,@SAMEADDRESS
				

				SET @BASIC_ID = scope_identity();



		insert into fin_CAF_Address([basic_ID],[prm_Address],[corp_Address],[city],[state],[country],[pincode],
		[corp_city],[corp_state],[corp_country],[corp_pincode],[CAF_Address_Status],createdDate,updatedDate) 
		select @BASIC_ID,@ADDRESS,@CORSPADDRESS,@CITY,@STATE,@COUNTRY,@PINCODE,@CORSCITY,@CORSSTATE,@CORSCOUNTRY,@CORSPINCODE,
		@CAFADDRESSSTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_BankDetails]([basic_ID],[Bank_name],[NameAsPerBank],[Bank_Address],[Acc_No],
		[Branch],[Bankcity],[Acc_Type],[Micr],[Ifsc],[CAF_Bank_Status],createdDate,updatedDate) 
		select @BASIC_ID,@BANKNAME,@NAMEASPERBANK,@BANKADDRESS,@BANKACCNO,@BANKBRANCH,@BANKCITY,@BANKACCTYPE,
		@BANKMICR,@BANKIFSC,@CAFBANKSTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_Nominee] ([basic_ID],[Nominee_name],[Relation],[Nominee_Dob],[percent],
		[Guardian_name],[Guardian_pan],[CAF_Nominee_Status],createdDate,updatedDate) select @BASIC_ID,@NOMNAME,@NOMRELATION,@NOMDOB,
		@NOMPERCENT,@NOMGUARDNAME,@NOMGUARDPAN,@CAFNOMINEESTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_FATCA] ([basic_ID],[Place_of_Birth],[Country_of_Birth],[Pol_Exp_Person],
		[Country_Tax_Res1],[CountryPayer_Id_no],[Idenif_Type],[Country_Tax_Res2],[CountryPayer_Id_no2],
		[Idenif_Type2],[Country_Tax_Res3],[CountryPayer_Id_no3],[Idenif_Type3],[Source_Wealth],
		[isTaxOther],[CAF_Fatca_Status],createdDate,updatedDate) select 
		@BASIC_ID,@FATCAPLACEOFBIRTH,@FATCACOUNTRYOFBIRTH,@FATCAPOLEXPPERSON,@FATCACOUNTRYTAXRES1,
		@FATCACOUNTRYPAYIDNO1,@FATCAIDENTYPE1,@FATCACOUNTRYTAXRES2,@FATCACOUNTRYPAYIDNO2, @FATCAIDENTYPE2,
		 @FATCACOUNTRYTAXRES3, @FATCACOUNTRYPAYIDNO3, @FATCAIDENTYPE3, @FATCASOURCEWEALTH,@ISTAXOTHER,@CAFFATCASTATUS,getdate(),getdate()
			
			EXECUTE fin_Proc_SelectAllCafByBasicId @Action='SELECT_ALL_CAF_BY_BASICID',@BASICID=@BASIC_ID

			END
		
		ELSE
			BEGIN
				update fin_CAF_BasicDetails set title=@TITLE,clientName=@CLIENTNAME,phone=@PHONE,mobile=@MOBILE,
				Occupation=@OCCUPATION,PanNumber=@PANNUMBER,AadharNumber=@AADHARNUMBER,
				Date_of_Birth=@DATEOFBIRTH,Guardname=@GUARDNAME,Guardpan=@GUARDPAN,
				Guardpan_DOB=@GUARDPANDOB,GuardLocaladd=@GUARDLOCALADD,account_type=@ACCOUNTTYPE,
				Annual_Income=@ANNUALINCOME,kycStatus=@KYCSTATUS,ReqNominee=@REQNOMINEE,
				CAF_Basic_Status=@CAFBASICSTATUS,CAF_Status=@CAFSTATUS,profilePic=@PROFILEPIC,updatedDate=getdate(),email=@EMAIL
				 where
				basic_id = @BASICID

				EXECUTE fin_Proc_SelectAllCafByBasicId @Action='SELECT_ALL_CAF_BY_BASICID',@BASICID=@BASICID
			END
		
		

		
End

if(@Action='SAVE_CAF_ADDRESS')
begin

		IF (@BASICID is NULL OR @BASICID = '' OR @BASICID = 0)
			BEGIN
					SET @GRPLEADER = (select memberId from fin_CAF_BasicDetails as bs where bs.userid=@USERID
									 and bs.memberid=bs.groupLeader)
							
							
			insert into fin_CAF_BasicDetails(userid, memberId, userPass, groupLeader, title, clientName,phone,mobile,
		Occupation,PanNumber,AadharNumber,Date_of_Birth, Guardname, Guardpan, Guardpan_DOB, GuardLocaladd, account_type,			        Annual_Income, kycStatus, ReqNominee,CAF_Basic_Status, CAF_Status,profilePic,createdDate,updatedDate,email,sameAddress) select @USERID,@MEMBERID,@PASSWORD,@GRPLEADER,@TITLE,@CLIENTNAME,@PHONE,@MOBILE,@OCCUPATION,@PANNUMBER,@AADHARNUMBER,@DATEOFBIRTH,@GUARDNAME,@GUARDPAN,@GUARDPANDOB,@GUARDLOCALADD,@ACCOUNTTYPE,@ANNUALINCOME,@KYCSTATUS,@REQNOMINEE,@CAFBASICSTATUS,@CAFSTATUS,@PROFILEPIC,getdate(),getdate(),@EMAIL,@SAMEADDRESS
				

				SET @BASIC_ID = scope_identity();

				IF(@SAMEADDRESS IS NOT NULL AND @SAMEADDRESS !='')
					Begin
					
					insert into fin_CAF_Address([basic_ID],[prm_Address],[corp_Address],[city],[state],[country],[pincode],
					[corp_city],[corp_state],[corp_country],[corp_pincode],[CAF_Address_Status],createdDate,updatedDate) 
					select @BASIC_ID,@ADDRESS,@CORSPADDRESS,@CITY,@STATE,@COUNTRY,@PINCODE,@CORSCITY,@CORSSTATE,@CORSCOUNTRY,@CORSPINCODE,
					@CAFADDRESSSTATUS,getdate(),getdate() from fin_CAF_Address where basic_ID=@SAMEADDRESS


					End
				Else
					Begin
						insert into fin_CAF_Address([basic_ID],[prm_Address],[corp_Address],[city],[state],[country],[pincode],
		[corp_city],[corp_state],[corp_country],[corp_pincode],[CAF_Address_Status],createdDate,updatedDate) 
		select @BASIC_ID,@ADDRESS,@CORSPADDRESS,@CITY,@STATE,@COUNTRY,@PINCODE,@CORSCITY,@CORSSTATE,@CORSCOUNTRY,@CORSPINCODE,
		@CAFADDRESSSTATUS,getdate(),getdate()
						
					End


		insert into [dbo].[fin_CAF_BankDetails]([basic_ID],[Bank_name],[NameAsPerBank],[Bank_Address],[Acc_No],
		[Branch],[Bankcity],[Acc_Type],[Micr],[Ifsc],[CAF_Bank_Status],createdDate,updatedDate) 
		select @BASIC_ID,@BANKNAME,@NAMEASPERBANK,@BANKADDRESS,@BANKACCNO,@BANKBRANCH,@BANKCITY,@BANKACCTYPE,
		@BANKMICR,@BANKIFSC,@CAFBANKSTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_Nominee] ([basic_ID],[Nominee_name],[Relation],[Nominee_Dob],[percent],
		[Guardian_name],[Guardian_pan],[CAF_Nominee_Status],createdDate,updatedDate) select @BASIC_ID,@NOMNAME,@NOMRELATION,@NOMDOB,
		@NOMPERCENT,@NOMGUARDNAME,@NOMGUARDPAN,@CAFNOMINEESTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_FATCA] ([basic_ID],[Place_of_Birth],[Country_of_Birth],[Pol_Exp_Person],
		[Country_Tax_Res1],[CountryPayer_Id_no],[Idenif_Type],[Country_Tax_Res2],[CountryPayer_Id_no2],
		[Idenif_Type2],[Country_Tax_Res3],[CountryPayer_Id_no3],[Idenif_Type3],[Source_Wealth],
		[isTaxOther],[CAF_Fatca_Status],createdDate,updatedDate) select 
		@BASIC_ID,@FATCAPLACEOFBIRTH,@FATCACOUNTRYOFBIRTH,@FATCAPOLEXPPERSON,@FATCACOUNTRYTAXRES1,
		@FATCACOUNTRYPAYIDNO1,@FATCAIDENTYPE1,@FATCACOUNTRYTAXRES2,@FATCACOUNTRYPAYIDNO2, @FATCAIDENTYPE2,
		 @FATCACOUNTRYTAXRES3, @FATCACOUNTRYPAYIDNO3, @FATCAIDENTYPE3, @FATCASOURCEWEALTH,@ISTAXOTHER,@CAFFATCASTATUS,getdate(),getdate()	
			EXECUTE fin_Proc_SelectAllCafByBasicId @Action='SELECT_ALL_CAF_BY_BASICID',@BASICID=@BASIC_ID
			End
			
		ELSE
			BEGIN

				IF(@SAMEADDRESS IS NOT NULL AND @SAMEADDRESS !='')
					Begin	
					
						update T1 set  T1.[prm_Address]=T2.[prm_Address],
						T1.[corp_Address]=T2.[corp_Address],
						T1.[city]=T2.[city],
						T1.[state]=T2.[state],
						T1.[country]=T2.[country],
						T1.[pincode]=T2.[pincode],
						T1.[corp_city]=T2.[corp_city],
						T1.[corp_state]=T2.[corp_state],
						T1.[corp_country]=T2.[corp_country],
						T1.[corp_pincode]=T2.[corp_pincode],
						T1.[CAF_Address_Status]=T2.[CAF_Address_Status],
						T1.updatedDate=getdate()
						from fin_CAF_Address T1 INNER JOIN  fin_CAF_Address T2 ON T1.basic_ID = @BASICID
						And T2.basic_ID = @SAMEADDRESS
						
					
					End
				Else
					Begin
					
						update fin_CAF_Address set  [prm_Address]=@ADDRESS,[corp_Address]=@ADDRESS,[city]=@CITY,[state]=@STATE,
						[country]=@COUNTRY,[pincode]=@PINCODE,
						[corp_city]=@CORSCITY,[corp_state]=@CORSSTATE,
						[corp_country]=@CORSCOUNTRY,[corp_pincode]=@CORSPINCODE,
						[CAF_Address_Status]=@CAFADDRESSSTATUS,updatedDate=getdate() 
						where basic_id = @BASICID
					End
				update fin_CAF_BasicDetails set AadharNumber=@AADHARNUMBER, sameAddress=@SAMEADDRESS  where basic_id = @BASICID

				EXECUTE fin_Proc_SelectAllCafByBasicId @Action='SELECT_ALL_CAF_BY_BASICID',@BASICID=@BASICID
			END	

	
end
if(@Action='SAVE_CAF_BANK')
begin
		
		IF (@BASICID is NULL OR @BASICID = '' OR @BASICID = 0)
			BEGIN
					SET @GRPLEADER = (select memberId from fin_CAF_BasicDetails as bs where bs.userid=@USERID
									 and bs.memberid=bs.groupLeader)
								insert into fin_CAF_BasicDetails(userid, memberId, userPass, groupLeader, title, clientName,phone,mobile,
		Occupation,PanNumber,AadharNumber,Date_of_Birth, Guardname, Guardpan, Guardpan_DOB, GuardLocaladd, account_type,			        Annual_Income, kycStatus, ReqNominee,CAF_Basic_Status, CAF_Status,profilePic,createdDate,updatedDate,email,sameAddress) select @USERID,@MEMBERID,@PASSWORD,
		@GRPLEADER,@TITLE,@CLIENTNAME,@PHONE,@MOBILE,@OCCUPATION,@PANNUMBER,@AADHARNUMBER,@DATEOFBIRTH,@GUARDNAME,@GUARDPAN
		,@GUARDPANDOB,@GUARDLOCALADD,@ACCOUNTTYPE,@ANNUALINCOME,@KYCSTATUS,@REQNOMINEE,@CAFBASICSTATUS,@CAFSTATUS,@PROFILEPIC,getdate(),getdate(),@EMAIL,@SAMEADDRESS
				

				SET @BASIC_ID = scope_identity();

		insert into fin_CAF_Address([basic_ID],[prm_Address],[corp_Address],[city],[state],[country],[pincode],
		[corp_city],[corp_state],[corp_country],[corp_pincode],[CAF_Address_Status],createdDate,updatedDate) 
		select @BASIC_ID,@ADDRESS,@CORSPADDRESS,@CITY,@STATE,@COUNTRY,@PINCODE,@CORSCITY,@CORSSTATE,@CORSCOUNTRY,@CORSPINCODE,
		@CAFADDRESSSTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_BankDetails]([basic_ID],[Bank_name],[NameAsPerBank],[Bank_Address],[Acc_No],
		[Branch],[Bankcity],[Acc_Type],[Micr],[Ifsc],[CAF_Bank_Status],createdDate,updatedDate) 
		select @BASIC_ID,@BANKNAME,@NAMEASPERBANK,@BANKADDRESS,@BANKACCNO,@BANKBRANCH,@BANKCITY,@BANKACCTYPE,
		@BANKMICR,@BANKIFSC,@CAFBANKSTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_Nominee] ([basic_ID],[Nominee_name],[Relation],[Nominee_Dob],[percent],
		[Guardian_name],[Guardian_pan],[CAF_Nominee_Status],createdDate,updatedDate) select @BASIC_ID,@NOMNAME,@NOMRELATION,@NOMDOB,
		@NOMPERCENT,@NOMGUARDNAME,@NOMGUARDPAN,@CAFNOMINEESTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_FATCA] ([basic_ID],[Place_of_Birth],[Country_of_Birth],[Pol_Exp_Person],
		[Country_Tax_Res1],[CountryPayer_Id_no],[Idenif_Type],[Country_Tax_Res2],[CountryPayer_Id_no2],
		[Idenif_Type2],[Country_Tax_Res3],[CountryPayer_Id_no3],[Idenif_Type3],[Source_Wealth],
		[isTaxOther],[CAF_Fatca_Status],createdDate,updatedDate) select 
		@BASIC_ID,@FATCAPLACEOFBIRTH,@FATCACOUNTRYOFBIRTH,@FATCAPOLEXPPERSON,@FATCACOUNTRYTAXRES1,
		@FATCACOUNTRYPAYIDNO1,@FATCAIDENTYPE1,@FATCACOUNTRYTAXRES2,@FATCACOUNTRYPAYIDNO2, @FATCAIDENTYPE2,
		 @FATCACOUNTRYTAXRES3, @FATCACOUNTRYPAYIDNO3, @FATCAIDENTYPE3, @FATCASOURCEWEALTH,@ISTAXOTHER,@CAFFATCASTATUS,getdate(),getdate()

			
			EXECUTE fin_Proc_SelectAllCafByBasicId @Action='SELECT_ALL_CAF_BY_BASICID',@BASICID=@BASIC_ID
			End
			
		ELSE
			BEGIN
					update fin_CAF_BankDetails set [Bank_name]=@BANKNAME,[NameAsPerBank]=@NAMEASPERBANK,
					[Bank_Address]=@BANKADDRESS,[Acc_No]=@BANKACCNO,[Branch]=@BANKBRANCH,
					[Bankcity]=@BANKCITY,[Acc_Type]=@BANKACCTYPE,[Micr]=@BANKMICR,[Ifsc]=@BANKIFSC,
					[CAF_Bank_Status]=@CAFBANKSTATUS,updatedDate=getdate() where basic_id = @BASICID

		EXECUTE fin_Proc_SelectAllCafByBasicId @Action='SELECT_ALL_CAF_BY_BASICID',@BASICID=@BASICID
			END	

	

end
if(@Action='SAVE_CAF_NOMINEE')
begin
		IF (@BASICID is NULL OR @BASICID = '' OR @BASICID = 0)
			BEGIN

			IF NOT EXISTS(select * from fin_CAF_BasicDetails where userid = @USERID)
				Begin

					SET @GRPLEADER = (select memberId from fin_CAF_BasicDetails as bs where bs.userid=@USERID
									 and bs.memberid=bs.groupLeader)
				
				insert into fin_CAF_BasicDetails(userid, memberId, userPass, groupLeader, title, clientName,phone,mobile,
		Occupation,PanNumber,AadharNumber,Date_of_Birth, Guardname, Guardpan, Guardpan_DOB, GuardLocaladd, account_type,			        Annual_Income, kycStatus, ReqNominee,CAF_Basic_Status, CAF_Status,profilePic,createdDate,updatedDate,email,sameAddress) select @USERID,@MEMBERID,@PASSWORD,
		@GRPLEADER,@TITLE,@CLIENTNAME,@PHONE,@MOBILE,@OCCUPATION,@PANNUMBER,@AADHARNUMBER,@DATEOFBIRTH,@GUARDNAME,@GUARDPAN
		,@GUARDPANDOB,@GUARDLOCALADD,@ACCOUNTTYPE,@ANNUALINCOME,@KYCSTATUS,@REQNOMINEE,@CAFBASICSTATUS,@CAFSTATUS,@PROFILEPIC,getdate(),getdate(),@EMAIL,@SAMEADDRESS

					SET @BASIC_ID = scope_identity();

				insert into fin_CAF_Address([basic_ID],[prm_Address],[corp_Address],[city],[state],[country],[pincode],
		[corp_city],[corp_state],[corp_country],[corp_pincode],[CAF_Address_Status],createdDate,updatedDate) 
		select @BASIC_ID,@ADDRESS,@CORSPADDRESS,@CITY,@STATE,@COUNTRY,@PINCODE,@CORSCITY,@CORSSTATE,@CORSCOUNTRY,@CORSPINCODE,
		@CAFADDRESSSTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_BankDetails]([basic_ID],[Bank_name],[NameAsPerBank],[Bank_Address],[Acc_No],
		[Branch],[Bankcity],[Acc_Type],[Micr],[Ifsc],[CAF_Bank_Status],createdDate,updatedDate) 
		select @BASIC_ID,@BANKNAME,@NAMEASPERBANK,@BANKADDRESS,@BANKACCNO,@BANKBRANCH,@BANKCITY,@BANKACCTYPE,
		@BANKMICR,@BANKIFSC,@CAFBANKSTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_Nominee] ([basic_ID],[Nominee_name],[Relation],[Nominee_Dob],[percent],
		[Guardian_name],[Guardian_pan],[CAF_Nominee_Status],createdDate,updatedDate) select @BASIC_ID,@NOMNAME,@NOMRELATION,@NOMDOB,
		@NOMPERCENT,@NOMGUARDNAME,@NOMGUARDPAN,@CAFNOMINEESTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_FATCA] ([basic_ID],[Place_of_Birth],[Country_of_Birth],[Pol_Exp_Person],
		[Country_Tax_Res1],[CountryPayer_Id_no],[Idenif_Type],[Country_Tax_Res2],[CountryPayer_Id_no2],
		[Idenif_Type2],[Country_Tax_Res3],[CountryPayer_Id_no3],[Idenif_Type3],[Source_Wealth],
		[isTaxOther],[CAF_Fatca_Status],createdDate,updatedDate) select 
		@BASIC_ID,@FATCAPLACEOFBIRTH,@FATCACOUNTRYOFBIRTH,@FATCAPOLEXPPERSON,@FATCACOUNTRYTAXRES1,
		@FATCACOUNTRYPAYIDNO1,@FATCAIDENTYPE1,@FATCACOUNTRYTAXRES2,@FATCACOUNTRYPAYIDNO2, @FATCAIDENTYPE2,
		 @FATCACOUNTRYTAXRES3, @FATCACOUNTRYPAYIDNO3, @FATCAIDENTYPE3, @FATCASOURCEWEALTH,@ISTAXOTHER,@CAFFATCASTATUS,getdate(),getdate()


				End
			Else 
			Begin 
					SET @BASIC_ID = (select basic_id from fin_CAF_BasicDetails where userid=@USERID) 

					insert into [dbo].[fin_CAF_Nominee] ([basic_ID],[Nominee_name],[Relation],[Nominee_Dob],[percent],
			[Guardian_name],[Guardian_pan],[CAF_Nominee_Status],createdDate,updatedDate) select @BASIC_ID,@NOMNAME,@NOMRELATION,@NOMDOB,
			@NOMPERCENT,@NOMGUARDNAME,@NOMGUARDPAN,@CAFNOMINEESTATUS,getdate(),getdate()
			End

			
			
			
			EXECUTE fin_Proc_SelectAllCafByBasicId @Action='SELECT_ALL_CAF_BY_BASICID',@BASICID=@BASIC_ID
			End
			
		ELSE
			BEGIN
			PRINT 'FLOW BASICID EXIST'
				IF(@NOMID is null OR @NOMID = 0 And @NOMNAME is not null and @NOMNAME != '')
					Begin
						
						PRINT 'FLOW NOMID NULL'
						PRINT @BASICID
						insert into [dbo].[fin_CAF_Nominee] ([basic_ID],[Nominee_name],[Relation],[Nominee_Dob],[percent],
			[Guardian_name],[Guardian_pan],[CAF_Nominee_Status],createdDate,updatedDate) select @BASICID,@NOMNAME,@NOMRELATION,@NOMDOB,
			@NOMPERCENT,@NOMGUARDNAME,@NOMGUARDPAN,@CAFNOMINEESTATUS,getdate(),getdate()
						
						update [dbo].[fin_CAF_BasicDetails] set ReqNominee = @REQNOMINEE where basic_id = @BASICID

					End
				Else
					Begin
					PRINT 'FLOW UPDATE'
						update fin_CAF_Nominee set [Nominee_name]=@NOMNAME,[Relation]=@NOMRELATION,
						[Nominee_Dob]=@NOMDOB,[percent]=@NOMPERCENT,[Guardian_name]=@NOMGUARDNAME,
						[Guardian_pan]=@NOMGUARDPAN,[CAF_Nominee_Status]= @CAFNOMINEESTATUS,updatedDate=getdate() 
						where basic_id = @BASICID and nominee_ID=@NOMID

						update [dbo].[fin_CAF_BasicDetails] set ReqNominee = @REQNOMINEE where basic_id = @BASICID
					End
				

				
			END	
		EXECUTE fin_Proc_SelectAllCafByBasicId @Action='SELECT_ALL_CAF_BY_BASICID',@BASICID=@BASICID
end

if(@Action='SAVE_CAF_FATCA')
begin
	
	IF (@BASICID is NULL OR @BASICID = '' OR @BASICID = 0)
			BEGIN
					SET @GRPLEADER = (select memberId from fin_CAF_BasicDetails as bs where bs.userid=@USERID
									 and bs.memberid=bs.groupLeader)
				
			insert into fin_CAF_BasicDetails(userid, memberId, userPass, groupLeader, title, clientName,phone,mobile,
		Occupation,PanNumber,AadharNumber,Date_of_Birth, Guardname, Guardpan, Guardpan_DOB, GuardLocaladd, account_type, Annual_Income, kycStatus, ReqNominee,CAF_Basic_Status, CAF_Status,profilePic,createdDate,updatedDate,email,sameAddress) select @USERID,@MEMBERID,@PASSWORD,
		@GRPLEADER,@TITLE,@CLIENTNAME,@PHONE,@MOBILE,@OCCUPATION,@PANNUMBER,@AADHARNUMBER,@DATEOFBIRTH,@GUARDNAME,@GUARDPAN
		,@GUARDPANDOB,@GUARDLOCALADD,@ACCOUNTTYPE,@ANNUALINCOME,@KYCSTATUS,@REQNOMINEE,@CAFBASICSTATUS,@CAFSTATUS,@PROFILEPIC,getdate(),getdate(),@EMAIL,@SAMEADDRESS
				

				SET @BASIC_ID = scope_identity();

		insert into fin_CAF_Address([basic_ID],[prm_Address],[corp_Address],[city],[state],[country],[pincode],
		[corp_city],[corp_state],[corp_country],[corp_pincode],[CAF_Address_Status],createdDate,updatedDate) 
		select @BASIC_ID,@ADDRESS,@CORSPADDRESS,@CITY,@STATE,@COUNTRY,@PINCODE,@CORSCITY,@CORSSTATE,@CORSCOUNTRY,@CORSPINCODE,
		@CAFADDRESSSTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_BankDetails]([basic_ID],[Bank_name],[NameAsPerBank],[Bank_Address],[Acc_No],
		[Branch],[Bankcity],[Acc_Type],[Micr],[Ifsc],[CAF_Bank_Status],createdDate,updatedDate) 
		select @BASIC_ID,@BANKNAME,@NAMEASPERBANK,@BANKADDRESS,@BANKACCNO,@BANKBRANCH,@BANKCITY,@BANKACCTYPE,
		@BANKMICR,@BANKIFSC,@CAFBANKSTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_Nominee] ([basic_ID],[Nominee_name],[Relation],[Nominee_Dob],[percent],
		[Guardian_name],[Guardian_pan],[CAF_Nominee_Status],createdDate,updatedDate) select @BASIC_ID,@NOMNAME,@NOMRELATION,@NOMDOB,
		@NOMPERCENT,@NOMGUARDNAME,@NOMGUARDPAN,@CAFNOMINEESTATUS,getdate(),getdate()

		insert into [dbo].[fin_CAF_FATCA] ([basic_ID],[Place_of_Birth],[Country_of_Birth],[Pol_Exp_Person],
		[Country_Tax_Res1],[CountryPayer_Id_no],[Idenif_Type],[Country_Tax_Res2],[CountryPayer_Id_no2],
		[Idenif_Type2],[Country_Tax_Res3],[CountryPayer_Id_no3],[Idenif_Type3],[Source_Wealth],
		[isTaxOther],[CAF_Fatca_Status],createdDate,updatedDate) select 
		@BASIC_ID,@FATCAPLACEOFBIRTH,@FATCACOUNTRYOFBIRTH,@FATCAPOLEXPPERSON,@FATCACOUNTRYTAXRES1,
		@FATCACOUNTRYPAYIDNO1,@FATCAIDENTYPE1,@FATCACOUNTRYTAXRES2,@FATCACOUNTRYPAYIDNO2, @FATCAIDENTYPE2,
		 @FATCACOUNTRYTAXRES3, @FATCACOUNTRYPAYIDNO3, @FATCAIDENTYPE3, @FATCASOURCEWEALTH,@ISTAXOTHER,@CAFFATCASTATUS,getdate(),getdate()
			
			EXECUTE fin_Proc_SelectAllCafByBasicId @Action='SELECT_ALL_CAF_BY_BASICID',@BASICID=@BASIC_ID
			End
			
		ELSE
			BEGIN
				update fin_CAF_FATCA set [Place_of_Birth]=@FATCAPLACEOFBIRTH,[Country_of_Birth]=@FATCACOUNTRYOFBIRTH,
				[Pol_Exp_Person]=@FATCAPOLEXPPERSON,[Country_Tax_Res1]=@FATCACOUNTRYTAXRES1,
				[CountryPayer_Id_no]=@FATCACOUNTRYPAYIDNO1,[Idenif_Type]=@FATCAIDENTYPE1,[Country_Tax_Res2]=@FATCACOUNTRYTAXRES2,
				[CountryPayer_Id_no2]=@FATCACOUNTRYPAYIDNO2,[Idenif_Type2]=@FATCAIDENTYPE2,[Country_Tax_Res3]=@FATCACOUNTRYTAXRES3,
				[CountryPayer_Id_no3]=@FATCACOUNTRYPAYIDNO3, [Idenif_Type3]=@FATCAIDENTYPE3, [Source_Wealth]=@FATCASOURCEWEALTH,
				[CAF_Fatca_Status]=@CAFFATCASTATUS,[isTaxOther]=@ISTAXOTHER,updatedDate=getdate() where basic_id = @BASICID

				EXECUTE fin_Proc_SelectAllCafByBasicId @Action='SELECT_ALL_CAF_BY_BASICID',@BASICID=@BASICID
			END	
	
end
if(@ACtion='SAVE_PROFILE_PIC')
 Begin
	update fin_CAF_BasicDetails set profilePic = @PROFILEPIC where basic_ID = @BASICID
 End
if(@Action='SELECT_ALL_CAF')
begin
	
	Declare @SADDRESSID nvarchar (max)
	SET @SADDRESSID = (select sameAddress from fin_CAF_BasicDetails where Basic_id = @BASICID)

		select BD.Basic_id, BD.userid,BD.memberId,BD.userPass,BD.groupLeader,BD.clientName,BD.phone,BD.mobile,BD.Occupation,BD.PanNumber,BD.AadharNumber,BD.Date_of_Birth,BD.Guardname,BD.Guardpan,BD.Guardpan_DOB,BD.GuardLocaladd,BD.account_type,BD.Annual_Income,BD.KycStatus,BD.ReqNominee,
BD.CAF_Status,BD.CAF_Basic_Status,BD.profilePic,BD.email,BD.sameAddress from fin_CAF_BasicDetails BD where BD.Basic_id = @BASICID

select ADRS.prm_Address,ADRS.corp_Address,ADRS.city,ADRS.state,ADRS.country,
ADRS.pincode,ADRS.corp_city,ADRS.corp_state,ADRS.corp_country,ADRS.corp_pincode,
ADRS.CAF_Address_Status from fin_CAF_Address ADRS where ADRS.basic_ID = @BASICID

select BNK.Bank_name,BNK.NameAsPerBank,BNK.Bank_Address,BNK.Acc_No,BNK.Branch,BNK.Bankcity,BNK.Acc_Type,BNK.Micr,BNK.Ifsc,
BNK.CAF_Bank_Status from fin_CAF_BankDetails BNK where BNK.basic_ID = @BASICID

select NOM.nominee_ID,NOM.Guardian_name,NOM.Guardian_pan,NOM.Nominee_Dob,NOM.Nominee_name,NOM.[percent],NOM.Relation,NOM.CAF_Nominee_Status 
from fin_CAF_Nominee NOM where NOM.basic_ID = @BASICID order by [nominee_ID] asc

select FTCA.place_of_birth,FTCA.Country_of_Birth,FTCA.Pol_Exp_Person,FTCA.[Country_Tax_Res1],FTCA.[CountryPayer_Id_no],FTCA.[Idenif_Type],FTCA.[Country_Tax_Res2],
FTCA.[CountryPayer_Id_no2],FTCA.[Idenif_Type2],FTCA.[Country_Tax_Res3],FTCA.[CountryPayer_Id_no3],FTCA.[Idenif_Type3],FTCA.[Source_Wealth],FTCA.[isTaxOther],FTCA.CAF_Fatca_Status from fin_CAF_FATCA FTCA 
where FTCA.basic_ID = @BASICID



End
if(@Action='REMOVE_NOMINEE')
Begin
 delete from [dbo].[fin_CAF_Nominee] where basic_id=@BASICID
 update [fin_CAF_BasicDetails] set reqNominee='N' where basic_id=@BASICID
 EXECUTE fin_Proc_SelectAllCafByBasicId @Action='SELECT_ALL_CAF_BY_BASICID',@BASICID=@BASICID
End
if(@Action='CHECK_CAF_EXIST')
Begin
select Count(*) from [dbo].[fin_CAF_BasicDetails] where memberId = @MEMBERID
End
if(@Action='SELECT_VIEWPROFILE')
Begin

select DISTINCT CB.basic_id,CB.account_type,CB.memberId,CB.groupLeader,CB.ClientName,CB.profilePic,CB.pannumber,
CB.Guardpan,CB.KycStatus,CB.ReqNominee,CB.Caf_status as iscafcomplete,CB.CAF_basic_status as ismemberbasic,CA.CAF_Address_Status as ismemberaddress,CBNK.CAF_Bank_Status as ismemberbank,CNM.CAF_Nominee_Status 
as ismembernominee,CFT.CAF_Fatca_status as ismemberfatca from fin_CAF_BasicDetails CB LEFT JOIN fin_CAF_Address CA on CB.basic_ID = CA.basic_ID LEFT JOIN fin_CAF_BankDetails CBNK on CB.basic_ID = CBNK.basic_ID LEFT JOIN fin_CAF_FATCA CFT on CB.basic_ID = CFT.basic_ID LEFT JOIN fin_CAF_Nominee CNM on CB.basic_ID = CNM.basic_id  where CB.userid = @USERID order by basic_ID asc



 select count(*) as totalInvestAcc from fin_InvestProfile where groupleader in (select groupLeader from [fin_CAF_BasicDetails] where userid = @USERID)

 select profileId from fin_InvestProfile where groupleader in (select groupLeader from [fin_CAF_BasicDetails] where userid = @USERID)


End

IF(@action = 'Update_CafMemberid_CafStatus')
Begin
			IF Exists(select * from fin_CAF_BasicDetails where userid=@USERID and memberId=@USERID and 
			groupLeader=@USERID and Basic_id = @BASICID)
				Begin
					update fin_CAF_BasicDetails set memberId=@MEMBERID,groupLeader=@MEMBERID,CAF_Status=1 where 
					userid=@USERID and memberId=@USERID and groupLeader=@USERID and Basic_id = @BASICID
				End
			Else
				Begin
				update fin_CAF_BasicDetails set memberId=@MEMBERID,CAF_Status=1 where Basic_id = @BASICID
				End
	
End
IF(@action = 'UPDATE_KYCSTATUS')
Begin
update fin_CAF_BasicDetails set KycStatus=@KYCSTATUS where Basic_id = @BASICID
End
if(@Action='CREATE_INVESTMENT_ACCOUNT')
Begin

insert into fin_InvestProfile(profileid,
FirstApplicant,
SecondApplicant,
ThirdApplicant,
GroupLeader,
FirstPAN,
SecondPAN,
ThirdPAN,
MOH,
Invst_Status,
createdDate) select distinct ProfileId,FirstApplicant,SecondApplicant,ThirdApplicant,groupleader,FirstPAN,SecondPAN,ThirdPAN,
HoldingMode,1,getdate() from @INVSTACCOUNTSTBL where profileId not in(select distinct Profileid from fin_InvestProfile)


End
IF(@action = 'SELECT_GROUPLEADER_BASICID_BYUSERID')
Begin
			
			select basic_id from fin_CAF_BasicDetails where userid = @USERID and memberid=groupLeader
			
	
End

IF(@action = 'SELECT_COUNTRY')
Begin
select name from countrylist order by name asc
	
End

IF(@action = 'SELECT_CITY')
Begin
select city from citylist order by city asc
	
End

IF(@action = 'SELECT_STATE')
Begin
select state from statelist order by state asc
	
End

------#####################################################################
IF(@action = 'SELECT_COUNTRY_VALUEBY_NAME')
Begin
select code from countrylist where name = @COUNTRY
	
End

IF(@action = 'SELECT_CITY_VALUEBY_NAME')
Begin
select code from citylist where city = @CITY
	
End

IF(@action = 'SELECT_STATE_VALUEBY_NAME')
Begin
select code from statelist where state = @STATE
	
End


IF(@action = 'SELECT_MEMBERID_BY_BASICID')
Begin
select memberId from fin_CAF_BasicDetails where basic_ID=@ID
	
End
IF(@action = 'SELECT_CITY_STATE_DISTRICT_BYPINCODE')
Begin
select distinct city,state,district from pincodes where pincode=@PINCODE
	
End


End 


GO

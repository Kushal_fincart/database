USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_QuestionAction]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[fin_Proc_QuestionAction]

@QCode nvarchar (max)=null,

@GCode nvarchar (max)=null,

@BrowserId nvarchar (500)=null,

@BrowserIp nvarchar (500)=null,

@CreatedByEmail nvarchar (500)=null,

@CreatedByMobile nvarchar (50)=null,

@Answer nvarchar (max)=null,

@Device nvarchar (max)=null,

@Device_Version nvarchar (max)=null,

@action varchar (500)

as

begin

	if(@action='TAGQUESTION')

		begin

		declare @record1 int, @record2 int, @record3 int

		set @record1=0

		set @record2=0

		set @record3=0

		

		--check for user if answering same question with same browser

		set @record1=(select COUNT(*) from fin_Questions_Tag where QCode=@QCode and GCode=@GCode and BrowserId=@BrowserId)



		--check for user if answering same question with same email

		if(@CreatedByEmail is not null or @CreatedByEmail='')

			begin

				set @record2=(select COUNT(*) from fin_Questions_Tag where QCode=@QCode and GCode=@GCode and CreatedByEmail=@CreatedByEmail)

			end



		--check for user if answering same question with same mobile

		if(@CreatedByMobile is not null or @CreatedByMobile='')

			begin

				set @record3=(select COUNT(*) from fin_Questions_Tag where QCode=@QCode and GCode=@GCode and CreatedByMobile=@CreatedByMobile)

			end



		if(@record1=0 and @record2=0 and @record3=0 and @QCode is not null and @QCode!='' and @GCode is not null and @GCode!='' and @BrowserId is not null and @BrowserId!='' and @Answer is not null and @Answer!='' and @Device is not null and @Device!='')

			begin

				insert into fin_Questions_Tag(QCode,GCode,BrowserId,BrowserIp,CreatedByEmail,CreatedByMobile,CreatedDatetime,Answer,Device,Device_Version)

				values(@QCode,@GCode,@BrowserId,@BrowserIp,Case When @CreatedByEmail = '' then null else @CreatedByEmail end,Case When @CreatedByMobile = '' then null else @CreatedByMobile end,getdate(),@Answer,@Device,@Device_Version)

			end



		end

	if(@action='ALLQUESTION')
		begin
			select * from [dbo].[fin_Questions]
		end

	if(@action='QUESTIONBYGOALCODE')
		begin
			select * from [dbo].[fin_Questions] where GCode=@GCode
		end
end
GO

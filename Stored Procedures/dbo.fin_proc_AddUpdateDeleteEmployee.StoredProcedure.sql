USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_proc_AddUpdateDeleteEmployee]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[fin_proc_AddUpdateDeleteEmployee]
@Action varchar(50)=null,
@Id int=0,
@EmpCode varchar(100)=null,
@Name varchar(50)=null,
@DeptId varchar(100)=null,
@RoleId varchar(100)=null,
@Salary varchar(50)=null,
@DOB DATE=NULL,
@DOJ DATE=NULL,
@DOR DATE=NULL,
@Country varchar(50)=null,
@State varchar(50)=null,
@City varchar(50)=null,
@ReportToEmail varchar(100)=null,
@Address varchar(500)=null,
@Pin varchar(10)=null,
@Phone varchar(10)=null,
@Off_Mail varchar(50)=null,
@Per_Mail varchar(50)=null,
@BloodGroup varchar(5)=null,
@ModuleCode varchar(50)=null,
@isRemoveModule varchar(10)=null,
@EmpModuleType fin_EmpModuleType READONLY
as
begin
if(@Action='ADDEMPLOYEE')
BEGIN
declare @email varchar(100),@newEmpCode varchar(500);

set @email=(select UserID from fin_Employee where EmpCode=@ReportToEmail)
set @newEmpCode='FIN'+substring(convert(nvarchar(8),getdate(),112),7,2)+substring(convert(nvarchar(6),getdate(),112),5,2)+convert(varchar,datepart(yyyy,getdate()))+

CASE
    WHEN len(convert(varchar,(select top 1 convert(int,RIGHT(EmpCode ,3))from fin_Employee order by 1 desc)+1)) = 1 
	THEN '00'+convert(varchar,(select top 1 convert(int,RIGHT(EmpCode ,3))from fin_Employee order by 1 desc)+1)
    WHEN len(convert(varchar,(select top 1 convert(int,RIGHT(EmpCode ,3))from fin_Employee order by 1 desc))+1) = 2 
	THEN '0'+convert(varchar,(select top 1 convert(int,RIGHT(EmpCode ,3))from fin_Employee order by 1 desc)+1)
    ELSE convert(varchar,(select top 1 convert(int,RIGHT(EmpCode ,3))from fin_Employee order by 1 desc)+1)
END

insert into fin_Employee(EmpCode,DepId,RoleId,ReportToEmpCode,ReportToEmail,Name,UserID,Address,City,Pin,State,Country,Phone,
OfficeEmail,PersonalEmail,DOB,DOJ,Salary,BloodGroup,CreatedDate,UpdatedDate) VALUES(@newEmpCode,@DeptId,@RoleId,@ReportToEmail,@email,@Name,@Off_Mail,@Address,@City,@Pin,@State,@Country,@Phone,@Off_Mail,@Per_Mail,@DOB,@DOJ,@Salary,@BloodGroup,GETDATE(),GETdATE());


insert into fin_ModuleAssignToEmployee(EmpCode,ModuleID) select  @newEmpCode,ModuleID from @EmpModuleType

END
if(@Action='DELETE')
BEGIN
update fin_Employee set Status=0 where Id=@Id;
END
if(@Action='UPDATE')
begin
declare @email_update varchar(100);

set @email_update=(select UserID from fin_Employee where EmpCode=@ReportToEmail)
update fin_Employee set Name=@Name,DepId=@DeptId,RoleId=@RoleId,ReportToEmpCode=@ReportToEmail,ReportToEmail=@email_update,Salary=@Salary,DOB=@DOB,DOJ=@DOJ,DOR=@DOJ,Country=@Country,
State=@State,City=@City,Address=@Address,Pin=@Pin,Phone=@Phone,OfficeEmail=@Off_Mail,PersonalEmail=@Per_Mail,BloodGroup=@BloodGroup WHERE EmpCode=@EmpCode;
 delete from  fin_ModuleAssignToEmployee where ModuleID not in (select ModuleID from @EmpModuleType) and EmpCode=@EmpCode;
 
 insert into fin_ModuleAssignToEmployee(EmpCode,ModuleID)  select  @EmpCode,ModuleID from @EmpModuleType
 Except
 select  @EmpCode,ModuleID from fin_ModuleAssignToEmployee where EmpCode = @EmpCode

end
end



GO

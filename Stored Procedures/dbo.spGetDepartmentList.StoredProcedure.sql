USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[spGetDepartmentList]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spGetDepartmentList]
@DepartmentID int=0, 
@DepartmentName varchar(50)='',
@Action varchar(500)
as
 begin
 if(@Action='DepartmentList')
 begin
 select * from fin_Departments where Status=1;
 end
 end
GO

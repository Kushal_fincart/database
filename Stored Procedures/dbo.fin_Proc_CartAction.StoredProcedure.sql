USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_CartAction]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[fin_Proc_CartAction]
	(
	@USERID nvarchar(max)= NULL,
	@MEMBERID nvarchar(max)=NULL,
	@PROFILEID nvarchar(max)=NULL,
	@GOALCODE nvarchar(max)=NULL,
	@FUNDCODE nvarchar(max)=NULL,
	@SCHEMECODE nvarchar(max)= NULL,
	@TYPE nvarchar(max)= NULL,
	@ENTRYDATE datetime= NULL,
	@DIVIDENTOPT nvarchar(max)=NULL,
	@FOLIONO nvarchar(max)= NULL,
	@AMOUNT nvarchar(max)=NULL,
	@STARTDATE datetime= NULL,
	@ENDDATE datetime= NULL,
	@NOOFINSTALLMENT int=0,
	@FREQUENCY nvarchar(max)=NULL,
	@BANKCODE nvarchar(max)=NULL,
	@ACCOUNTNO nvarchar(max)=NULL,
	@MANDATEID nvarchar(max)=NULL,
	@FIRSTSIPDATE datetime =NULL,
	@MDATE nvarchar(max)=NULL,
	@STATUS int=0,
	@CARTID nvarchar(max)=NULL,
	@USERGOALID bigint=NULL,

	@TOSCHEMECODE nvarchar(max)=NULL,
	@SCHEMENAME nvarchar(max)=NULL,
	@TOSCHEMENAME nvarchar(max)=NULL,
	@INVESTPROFILE nvarchar(max)=NULL,
	@UNITS nvarchar(max)=NULL,
	@NAVDATE datetime=NULL,
	@BANK nvarchar(max)=NULL,
	@BANKBRANCH nvarchar(max)=NULL,

	@ACTION varchar(500)
	)
as


	

begin
	if(@ACTION='ADD_LUMPSUM')
	begin
		insert into [dbo].fin_Cart (userId,memberId,profileId,goalCode,fundCode,schemeCode,type,entryDate,dividendOpt,folioNo,amount,bankCode,accountNo,mandateId,status,cartId,UserGoalId) 
		select @USERID,@MEMBERID,@PROFILEID,@GOALCODE,@FUNDCODE,@SCHEMECODE,@TYPE,@ENTRYDATE,@DIVIDENTOPT,@FOLIONO,@AMOUNT,@BANKCODE,@ACCOUNTNO,@MANDATEID,@STATUS,@CARTID,@USERGOALID 
	end
if(@ACTION='ADD_SIP')
	begin
		declare @tid bigint;
		set @tid = 0;

		--New code start
		-- this code added because investwell takes startdate as current date and we have to pass start date as first sip date in client transaction email
		set @STARTDATE=@FIRSTSIPDATE
		set @ENDDATE=(SELECT DATEADD(month, @NOOFINSTALLMENT-1, @STARTDATE) AS DateAdd)
		--New code end

			insert into [dbo].fin_Cart (userId,memberId,profileId,goalCode,fundCode,schemeCode,type,entryDate,dividendOpt,folioNo,amount,startDate,endDate,noOfInstallment,frequency,bankCode,accountNo,mandateId,firstSipDate,mDate,status,cartId,UserGoalId) 
			select @USERID,@MEMBERID,@PROFILEID,@GOALCODE,@FUNDCODE,@SCHEMECODE,@TYPE,@ENTRYDATE,@DIVIDENTOPT,@FOLIONO,@AMOUNT,@STARTDATE,@ENDDATE,@NOOFINSTALLMENT,@FREQUENCY,@BANKCODE ,@ACCOUNTNO,@MANDATEID,@FIRSTSIPDATE,@MDATE,@STATUS,@CARTID,@USERGOALID
			if(@TYPE = 'S')
				begin
					insert into [dbo].[fin_Cart_Transactions](InvestTxnId,userId,memberId,profileId,transDate,status,transType) 
					values(0,@USERID,@MEMBERID,@PROFILEID,@ENTRYDATE,0,@TYPE)
					set @tid =(SELECT top 1 id from [dbo].[fin_Cart_Transactions] order by id desc)
				end
			select @tid
	end
if(@ACTION='ADD_OTHER_TRANSACTION')
	begin
	declare @transid bigint;
	set @transid = 0;
		
		insert into [dbo].[fin_Cart_Transactions](InvestTxnId,userId,memberId,profileId,transDate,status,transType) 
		values(0,@USERID,@MEMBERID,@PROFILEID,@ENTRYDATE,@STATUS,@TYPE)
		set @transid =(SELECT top 1 id from [dbo].[fin_Cart_Transactions] order by id desc)

		--New code start
		-- this code added because end date should be one month less then actual @NOOFINSTALLMENT 
		if(@TYPE='SWP' OR @TYPE='STP')
		begin
		set @ENDDATE=(SELECT DATEADD(month, @NOOFINSTALLMENT-1, @STARTDATE) AS DateAdd)
		end
		--New code end

		insert into [dbo].[fin_Other_Transactions_Details] ([OtherTxnID],[userId],[memberId],[profileId],[goalCode],[fundCode],[schemeCode],[toSchemeCode],[scheme_name],[toscheme_name],[invest_profile],[entryDate],[units],[divOption],[folioNo],[navDate],[startDate],[endDate],[noOfInstallment],[amount],[frequency],[type],[bank],[branch],[accountNo],[mandateId],[mDate],[UserGoalId]) 
		select @transid,@USERID,@MEMBERID,@PROFILEID,@GOALCODE,@FUNDCODE,@SCHEMECODE,@TOSCHEMECODE,@SCHEMENAME,@TOSCHEMENAME,@INVESTPROFILE, @ENTRYDATE,@UNITS,@DIVIDENTOPT,@FOLIONO,@NAVDATE,@STARTDATE,@ENDDATE,@NOOFINSTALLMENT,@AMOUNT,@FREQUENCY,@TYPE,@BANK,@BANKBRANCH,@ACCOUNTNO,@MANDATEID,@MDATE,@USERGOALID 
		
		select @transid
		
	
		
	end
if(@ACTION='REMOVE_CART_ITEM')
	begin
		delete from [dbo].fin_Cart where cartId=@CARTID
	end
if(@ACTION='SELECT_SCHEME')
	begin
		select schemecode from [dbo].fin_Cart where schemeCode = @SCHEMECODE
	end
if(@ACTION='USER_CART_COUNT') --FOR USER CART RECORD COUNT
	begin
		select count(*) as count from [dbo].fin_Cart where userId = @USERID
	end
if(@ACTION='USER_CART_COUNT_BYTYPE')
	begin
		select count(*) from [dbo].fin_Cart where status in (0,-1) and type = @TYPE and userId = @USERID
	end
if(@ACTION='GOALCODE_BY_CARTID')
	begin
		select goalcode from [dbo].fin_Cart where cartid = @CARTID
	end
if(@ACTION='SELECT_USER_CART')
	begin

select I as Instasip, L as Lumpsum, S as Sip 
from 
(
  select userId,type,status
  from [FP].[dbo].[fin_Cart]
) src 
pivot
(
  count(type)
  for type in ([L], [I], [S])
) piv where userId=@USERID and status in (0,-1);
		
	end

IF(@ACTION='SELECT_USER_CAF_EMAIL')
Begin
select email from fin_CAF_BasicDetails where memberId=@MEMBERID
End
end	

GO

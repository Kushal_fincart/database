USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_GetScheme]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[fin_Proc_GetScheme] 
@sname [nvarchar](max)
as
begin
select FCODE as FundCode,(select Name from [dbo].[Fund_Master] where code=FCODE) as FundName,SCODE as SchemeCode,SNAME as SchemeName_Short,Org_Scheme as SchemeName_Full from [dbo].[SchMst] where sNAME like '%'+@sname+'%'
end
GO

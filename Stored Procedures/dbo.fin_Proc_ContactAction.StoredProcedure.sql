USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_ContactAction]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[fin_Proc_ContactAction]
	(
	@NAME nvarchar(500)=null,

	@EMAIL nvarchar(500)=null,

	@CONTACTNO nvarchar(500)=null,

	@STATUS int = 0,

	@ACTION nvarchar(500)

	)
as

begin
if(@action='ADD_SUBSCRIP')

	begin

		insert into [dbo].[fin_Subscription](name,email,ContactNo,[Status],SubscribeDate) 
		values (@NAME,@EMAIL,@CONTACTNO,@STATUS,GETDATE())

	end

if(@action='UPDATE_SUBSCRIP')

	begin

		update [dbo].[fin_Subscription] set  [Status] = @STATUS where email = @EMAIL 
		

	end

if(@action='COUNT_SUBSCRIP_EXIST')

	begin

		select Count(*) as exist from [dbo].[fin_Subscription] where email = @EMAIL 
		

	end


end

GO

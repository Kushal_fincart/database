USE [FP]
GO

/****** Object:  StoredProcedure [dbo].[fin_Proc_Ticket_Crud]    Script Date: 18-06-2018 11:20:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






/*
    Name:           fin_Proc_Ticket_Crud
    Author:         Kushal Agarwal
    Written:        28 May 2018
    Purpose:        This procedure is written for all user action related to Ticketing System

    Edit History:   3rd june 2018 - Kushal Agarwal
                        + Department Wise Count Action Added
						
					5th june 2018 - Kushal Agarwal
                        + one more action (Select_TicketList) added.

					8th june 2018 - Kushal Agarwal
					    + one more action (Select_TicketCountbyMenu) added.
					14th june 2018 kushal
					+ one more action for error log added
*/





















-------------This procedure is for tickrting system where we can apply crud operations and select by filters-----------------------------
ALTER PROCEDURE [dbo].[fin_Proc_Ticket_Crud] @id BIGINT=null,@LoginBy nvarchar(10)=null
	,@TicketId nvarchar(10) = NULL ----------Ticket No
	,@Subject NVARCHAR(max) = NULL
	,--------------------Subject of ticket
	@Query NVARCHAR(max) =null ------------------Query of ticket
	,@Attachment NVARCHAR(max) = NULL ----------------attached file of new ticket
	,@CreatedBy NVARCHAR(100) = NULL --------------Creator's Email id  of new ticket
	,@Status nvarchar(10) = NULL ---------Status can be open,close,
	,@priority int = 0   ---0 for low,1 for medium,1 for high
	,--------Priority can be high,low,medium
	@tat DATETIME = NULL
	,---------tat will decide by creator for same dept and support for other dept
	@lastupdatedBy NVARCHAR(100) = NULL
	,---------for every activity same will be update
	@ActivityID BIGINT = NULL
	,---------activity like create,assign,close,comment,reopen
	@ActivityBy NVARCHAR(100) = NULL
	,-----------creator of activity
	@Comment NVARCHAR(max) = NULL
	,-------------comments will be put by creator or support or assignto
	@CommentBy NVARCHAR(100) = NULL
	,----------commentby can be creator or support or assignto
	@CommentAttachment NVARCHAR(100) = NULL
	,-----------this attachment file will be attach when comment(not mandatory)
	@IsDelete BIT = NULL
	,---------it will be by default 0(false)
	@action NVARCHAR(100)
	,------------Action can be new ticket ,update ticket,close ticket and select
	@AssignTO NVARCHAR(100) = NULL
	,-----------email id of person whose ticket is assignto
	@DeptFlag NVARCHAR(10) = NULL --------deptflag can be same or other
	,@deptcode NVARCHAR(100) = NULL -------deptcode for which ticket is logged
	,@FilterBy NVARCHAR(100) = NULL --------Use for filter data when list will display
	,@ReportTo NVARCHAR(100) = NULL ---------Use for filter data by report to
	,@AssignBy NVARCHAR(100) = NULL ---------use for filter data assign by
	,@Prefixdept nvarchar(3)=null----------use for filter by department prefix
	,@Module nvarchar(100)=null,
	@Error nvarchar(max)=null,
	@StackTrace nvarchar(max)=null
	,@errormessage nvarchar(100) = NULL OUTPUT-----use for output parameter for generating error message or ticket id
AS
BEGIN
	DECLARE @SelectQuery NVARCHAR(max)
	DECLARE @Curentticketid BIGINT

	---------------------------------------------------------------START NEW TICKET INSERTION-----------------------------------------------------------------------------
	IF (@Action = 'Insert_NewTicket')
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			SELECT @TicketId = dbo.[fin_Fun_GenerateTicketNo](@deptcode) --------this function will generate new ticket no

			IF (rtrim(ltrim(@DeptFlag)) = 'Other')
			BEGIN
				--SELECT TOP 1 @AssignTO = rtrim(ltrim(e.UserID))
				--FROM fin_Departments d
				--INNER JOIN fin_Employee e ON ltrim(rtrim(d.Code)) = rtrim(ltrim(e.DepId))
				--INNER JOIN fin_EmployeeRole r ON rtrim(ltrim(e.RoleId)) = rtrim(ltrim(r.Code))
				--WHERE rtrim(ltrim(d.Code)) = rtrim(ltrim(@deptcode))
				--	AND (rtrim(ltrim(r.ROLE)) LIKE '%support%' or rtrim(ltrim(r.ROLE)) LIKE '%operat%' or rtrim(ltrim(r.ROLE)) LIKE '%support%')
            DECLARE @prefixdepartment NVARCHAR(2)
			SELECT @prefixdepartment = dbo.[fin_Fun_FindDepartment](@deptcode, 'Select_PrefixByDeptCode') --------this function will generate new ticket no
			select @AssignTO=rtrim(ltrim([SupportUserID])) from [fin_tbl_TicketSupportEmail] where deptcode=@prefixdepartment
			END

			INSERT INTO fin_tbl_Ticket (
				TicketId
				,Subject
				,Query
				,Attachment
				,CreatedBy
				,STATUS
				,Priority
				,CreatedDate
				,TAT
				,LastUpdateDate
				,LastUpdatedBy
				,AssignTo
				)
			VALUES (
				rtrim(ltrim(@TicketId))
				,rtrim(ltrim(@Subject))
				,rtrim(ltrim(@Query))
				,rtrim(ltrim(@Attachment))
				,rtrim(ltrim(@CreatedBy))
				,rtrim(ltrim(@Status))
				,@priority
				,GETDATE()
				,@tat
				,GETDATE()
				,rtrim(ltrim(@lastupdatedBy))
				,@AssignTO
				)

			SELECT @ActivityID = ID
			FROM fin_tbl_TicketActivity
			WHERE ActivityName = 'Create'

			INSERT INTO fin_tbl_TicketActivityLog (
				TicketId
				,ActivityId
				,ActivityBy
				,ActivityDate
				)
			VALUES (
				@TicketId
				,@ActivityID
				,@CreatedBy
				,GETDATE()
				)

			SELECT @ActivityID = ID
			FROM fin_tbl_TicketActivity
			WHERE ActivityName = 'Assign'

			INSERT INTO fin_tbl_TicketActivityLog (
				TicketId
				,ActivityId
				,ActivityBy
				,ActivityDate
				)
			VALUES (
				@TicketId
				,@ActivityID
				,@CreatedBy
				,GETDATE()
				)

			IF @@TRANCOUNT > 0
				COMMIT

			SET @Curentticketid = (
					SELECT IDENT_CURRENT('fin_tbl_Ticket')
					)
			SET @errormessage = (
					SELECT TicketId
					FROM fin_tbl_Ticket
					WHERE id = @Curentticketid
					)
					select @errormessage
		END TRY

		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK

			SET @errormessage = 'Error'
			select @errormessage
		END CATCH
	END

	---------------------------------------------------END NEW TICKET INSERTION-----------------------------------------------------------------------
	---------------------------------------------------START UPDATE TICKET WHEN ASSIGN-------------------------------------------------------------
	IF (@action = 'Update_TicketAssign')
	BEGIN
	BEGIN TRY
			BEGIN TRANSACTION
			if(@tat like '%1900%' or @tat is null )
			begin
			set @tat=''
			end
		UPDATE fin_tbl_Ticket
		SET TAT = @tat
			,LastUpdateDate = GETDATE()
			,LastUpdatedBy = @lastupdatedBy
			,AssignTO = @AssignTO
		WHERE TicketId = @TicketId

		SELECT @ActivityID = ID
		FROM fin_tbl_TicketActivity
		WHERE ActivityName = 'Assign'

		INSERT INTO fin_tbl_TicketActivityLog (
			TicketId
			,ActivityId
			,ActivityBy
			,ActivityDate
			)
		VALUES (
			@TicketId
			,@ActivityID
			,@ActivityBy
			,GETDATE()
			)

		
			IF @@TRANCOUNT > 0
				COMMIT
				set @errormessage='1'
				select @errormessage
			END TRY

		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK

			SET @errormessage = '0'
			select @errormessage
		END CATCH
	END

	---------------------------------------------------END UPDATE TICKET WHEN ASSIGN-------------------------------------------------------------
	---------------------------------------------------START UPDATE TICKET TAT-------------------------------------------------------------
	IF (@action = 'Update_TicketTAT')
	BEGIN
	BEGIN TRY
			BEGIN TRANSACTION
		UPDATE fin_tbl_Ticket
		SET tat = @tat
			,LastUpdateDate = GETDATE()
			,LastUpdatedBy = @lastupdatedBy
		WHERE TicketId = @TicketId

		SELECT @ActivityID = ID
		FROM fin_tbl_TicketActivity
		WHERE ActivityName = 'TatUpdate'

		INSERT INTO fin_tbl_TicketActivityLog (
			TicketId
			,ActivityId
			,ActivityBy
			,ActivityDate
			)
		VALUES (
			@TicketId
			,@ActivityID
			,@ActivityBy
			,GETDATE()
			)
			IF @@TRANCOUNT > 0
				COMMIT
				set @errormessage='1'
				select @errormessage
			END TRY

		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK

			SET @errormessage = '0'
			select @errormessage
		END CATCH
	END

	---------------------------------------------------END UPDATE TICKET WHEN CLOSE-------------------------------------------------------------
	---------------------------------------------------START UPDATE TICKET WHEN CLOSE-------------------------------------------------------------
	IF (@action = 'Update_TicketClose')
	BEGIN
	BEGIN TRY
			BEGIN TRANSACTION
		UPDATE fin_tbl_Ticket
		SET STATUS = @Status
			,LastUpdateDate = GETDATE()
			,LastUpdatedBy = @lastupdatedBy
		WHERE TicketId = @TicketId

		SELECT @ActivityID = ID
		FROM fin_tbl_TicketActivity
		WHERE ActivityName = 'Comment'

		INSERT INTO fin_tbl_TicketActivityLog (
			TicketId
			,ActivityId
			,ActivityBy
			,ActivityDate
			)
		VALUES (
			@TicketId
			,@ActivityID
			,@ActivityBy
			,GETDATE()
			)

		SELECT @ActivityID = ID
		FROM fin_tbl_TicketActivity
		WHERE ActivityName = 'Close'

		INSERT INTO fin_tbl_TicketActivityLog (
			TicketId
			,ActivityId
			,ActivityBy
			,ActivityDate
			)
		VALUES (
			@TicketId
			,@ActivityID
			,@ActivityBy
			,GETDATE()
			)

		INSERT INTO fin_tbl_TicketComment (
			TicketId
			,Comment
			,CommentBy
			,CommentDate
			,Attachment
			,IsDelete
			)
		VALUES (
			@TicketId
			,@Comment
			,@CommentBy
			,GETDATE()
			,@CommentAttachment
			,0
			)
			IF @@TRANCOUNT > 0
				COMMIT
				set @errormessage='1'
				select @errormessage
			END TRY

		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK

			SET @errormessage = '0'
			select @errormessage
		END CATCH
	END

	---------------------------------------------------END UPDATE TICKET WHEN CLOSE-------------------------------------------------------------


	if(@action='Update_TicketCommentsDelete')
	begin
	BEGIN TRY
			BEGIN TRANSACTION

			update fin_tbl_TicketComment set IsDelete=1 where ID=@id
			IF @@TRANCOUNT > 0
				COMMIT
				set @errormessage='1'
			select @errormessage
			END TRY

		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK

			SET @errormessage = '0'
			select @errormessage
		END CATCH
	end


	---------------------------------------------------START UPDATE TICKET WHEN RE-OPEN-------------------------------------------------------------
	IF (@action = 'Update_TicketReOpen')
	BEGIN
	BEGIN TRY
			BEGIN TRANSACTION
		UPDATE fin_tbl_Ticket
		SET STATUS = @Status
			,LastUpdateDate = GETDATE()
			,LastUpdatedBy = @lastupdatedBy,TAT=null
		WHERE TicketId = @TicketId

		SELECT @ActivityID = ID
		FROM fin_tbl_TicketActivity
		WHERE ActivityName = 'Comment'

		INSERT INTO fin_tbl_TicketActivityLog (
			TicketId
			,ActivityId
			,ActivityBy
			,ActivityDate
			)
		VALUES (
			@TicketId
			,@ActivityID
			,@ActivityBy
			,GETDATE()
			)

		SELECT @ActivityID = ID
		FROM fin_tbl_TicketActivity
		WHERE ActivityName = 'Reopen'

		INSERT INTO fin_tbl_TicketActivityLog (
			TicketId
			,ActivityId
			,ActivityBy
			,ActivityDate
			)
		VALUES (
			@TicketId
			,@ActivityID
			,@ActivityBy
			,GETDATE()
			)

		INSERT INTO fin_tbl_TicketComment (
			TicketId
			,Comment
			,CommentBy
			,CommentDate
			,Attachment
			,IsDelete
			)
		VALUES (
			@TicketId
			,@Comment
			,@CommentBy
			,GETDATE()
			,@CommentAttachment
			,0
			)
			IF @@TRANCOUNT > 0
				COMMIT
				set @errormessage='1'
			select @errormessage
			END TRY

		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK

			SET @errormessage = '0'
			select @errormessage
		END CATCH
	END

	---------------------------------------------------END UPDATE TICKET WHEN RE-OPEN-------------------------------------------------------------
	---------------------------------------------------START INSERT COMMENTS-------------------------------------------------------------
	IF (@action = 'Insert_Comment')
	BEGIN
	BEGIN TRY
			BEGIN TRANSACTION
		SELECT @ActivityID = ID
		FROM fin_tbl_TicketActivity
		WHERE ActivityName = 'Comment'

		INSERT INTO fin_tbl_TicketActivityLog (
			TicketId
			,ActivityId
			,ActivityBy
			,ActivityDate
			)
		VALUES (
			@TicketId
			,@ActivityID
			,@ActivityBy
			,GETDATE()
			)

		INSERT INTO fin_tbl_TicketComment (
			TicketId
			,Comment
			,CommentBy
			,CommentDate
			,Attachment
			,IsDelete
			)
		VALUES (
			@TicketId
			,@Comment
			,@CommentBy
			,GETDATE()
			,@CommentAttachment
			,0
			)
			IF @@TRANCOUNT > 0
				COMMIT
			set @errormessage='Success'
			select @errormessage
			END TRY

		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK

			SET @errormessage = 'Error'
		END CATCH
	END

	---------------------------------------------------END INSERT COMMENTS-------------------------------------------------------------
	---------------------------------------------------START TICKET LIST--------------------------------------------------------------
	IF (@action = 'Select_TicketList')
	BEGIN
		SET @SelectQuery = 'select tkt.TicketId as [Ticket ID],tkt.CreatedBy as [CreatedBy],e.name as [CreatorName],tkt.CreatedDate as [Created Date],tkt.assignto as [AssignTo],d.name as [AssigneeName],r.Role as Designation,
		d.ReportToEmail as [ReportTo],(case when CONVERT(VARCHAR(10), isnull(tkt.TAT,''''), 103) = ''01/01/1900'' then '''' else CONVERT(VARCHAR(24), isnull(tkt.TAT,''''), 121) end) as [TAT],tkt.Status as [Status],tkt.Priority as [Priority],tkt.Subject as [Subject],tkt.Query as [Query],tkt.attachment as [Attachment]
		from fin_tbl_Ticket tkt left join fin_Employee e on rtrim(ltrim(tkt.CreatedBy))=rtrim(ltrim(e.UserID))left join fin_Employee D 
		on rtrim(ltrim(d.UserID))=rtrim(ltrim(tkt.assignto)) left join fin_EmployeeRole r on rtrim(ltrim(r.Code))=rtrim(ltrim(e.RoleId)) where 1=1 '

		

		IF (rtrim(ltrim(@DeptFlag)) = 'Other')
		BEGIN
			IF (@FilterBy = 'CreatedByMe')
			BEGIN
				SELECT @prefixdept = @Prefixdept+'%'

				SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(tkt.CreatedBy))=''' + @CreatedBy + ''' and tkt.ticketId like ''' + @prefixdept + ''''
			END
			------------------------------------------------------------------------------------------------------------------------------------------------
			IF (@FilterBy = 'AssignToMe')
			BEGIN
				SELECT @prefixdept = @prefixdept+'%'

				select @deptcode=dbo.[fin_Fun_FindDepartment](@AssignTO, 'Select_DeptCodeByEmpId')

				SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(tkt.assignto))=''' + @AssignTO + ''' and rtrim(ltrim(tkt.CreatedBy))  in(select UserID from fin_Employee where DepId<>'''+ @deptcode +''') and tkt.ticketId like ''' + @prefixdept + ''''

				--SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(tkt.assignto))=''' + @AssignTO + ''' and tkt.ticketId like ''' + @prefixdept + ''''
			END
			--------------------------------------------------------------------------------------------------------------------------------------------------
			IF (@FilterBy = 'ReportToMe')
			BEGIN
				DECLARE @prefixdeptforReport NVARCHAR(3)
				SELECT @prefixdept = @Prefixdept+'%'
				select @deptcode=dbo.[fin_Fun_FindDepartment](@ReportTo, 'Select_DeptCodeByEmpId')
				SELECT @prefixdeptforReport = dbo.[fin_Fun_FindDepartment](@ReportTo, 'Select_PrefixByEmpId')+'%'
				if(@prefixdeptforReport='AD%')
				begin
				   SET @SelectQuery = @SelectQuery + ' and  tkt.ticketId   like ''' + @prefixdept + ''''
				end
				else
				begin
				SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(d.ReportToEmail))=''' + @ReportTo + ''' and rtrim(ltrim(tkt.CreatedBy))  in(select UserID from fin_Employee where DepId<>'''+ @deptcode +''') and tkt.ticketId  like ''' + @prefixdeptforReport + ''''
				end
			END
			----------------------------------------------------------------------------------------------------------------------------------------------------
			IF (@FilterBy = 'AssignByMe')
			BEGIN
				SELECT @prefixdept = @prefixdept+'%'
				select @deptcode=dbo.[fin_Fun_FindDepartment](@AssignBy, 'Select_DeptCodeByEmpId')
				SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(tkt.assignto))<>''' + @AssignBy + ''' and rtrim(ltrim(tkt.CreatedBy))  in(select UserID from fin_Employee where DepId<>'''+ @deptcode +''') and tkt.ticketId  like ''' + @prefixdept + ''''
			END
			---------------------------------------------------------------------------------------------------------------------------------------------------
		END
		ELSE
		BEGIN
			IF (@FilterBy = 'CreatedByMe')
			BEGIN
				SELECT @prefixdept = @Prefixdept+'%'

				SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(tkt.CreatedBy))=''' + @CreatedBy + ''' and tkt.ticketId  like ''' + @prefixdept + ''''
			END
			-----------------------------------------------------------------------------------------------------------------------------------------------------
			IF (@FilterBy = 'AssignToMe')
			BEGIN
				SELECT @prefixdept = @Prefixdept+'%'

				select @deptcode=dbo.[fin_Fun_FindDepartment](@AssignTO, 'Select_DeptCodeByEmpId')
				SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(tkt.assignto))=''' + @AssignTO + ''' and rtrim(ltrim(tkt.CreatedBy)) in(select UserID from fin_Employee where DepId='''+ @deptcode +''') and tkt.ticketId like ''' + @prefixdept + ''''
			END
			-------------------------------------------------------------------------------------------------------------------------------------------------------
			IF (@FilterBy = 'ReportToMe')
			BEGIN
			    
				SELECT @prefixdept = @Prefixdept+'%'
				select @deptcode=dbo.[fin_Fun_FindDepartment](@ReportTo, 'Select_DeptCodeByEmpId')
				
				SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(d.ReportToEmail))=''' + @ReportTo + ''' and rtrim(ltrim(tkt.CreatedBy))  in(select UserID from fin_Employee where DepId='''+ @deptcode +''') and tkt.ticketId  like ''' + @prefixdept + ''''
				
			END
			-------------------------------------------------------------------------------------------------------------------------------------------------------
		END
		if(@Status<>'' and @Status is not null and @Status<>'All')
		begin
		  
		 set @SelectQuery=@SelectQuery+' and tkt.status='''+ @status+ ''''
		 end
		SET @SelectQuery = @SelectQuery +' order by Status desc,tkt.CreatedDate desc'
		EXECUTE sp_executesql @SelectQuery
	END

	--------------------------------------------------------------END TICKET LIST----------------------------------------------------------------------------------
	--------------------------------------------------------------START COUNT TICKET LIST BY DEPT------------------------------------------------------------------ 
	IF (@action = 'Select_TicketCountbyDept')
	BEGIN
	SET @SelectQuery = ' SELECT Case when Dept=''OP'' then ''OPERATION''   WHEN DEPT=''SL'' THEN ''SALES'' WHEN DEPT=''IT'' THEN ''IT'' WHEN DEPT=''AC'' THEN ''ACCOUNT'' WHEN DEPT=''AD'' THEN ''ADMIN'' end AS Dept,Dept as PreFixDept, isnull([Open],0) as [Open], isnull([Close],0) as [Close],isnull([ReOpen],0) as [ReOpen],isnull([Open],0)+isnull([Close],0)+isnull([ReOpen],0) as Total FROM
                        (select isnull(count(*),0) as [NoCount],substring(ticketId,1,2) as Dept,tkt.Status from fin_tbl_Ticket tkt left join fin_Employee e on rtrim(ltrim(tkt.CreatedBy))=rtrim(ltrim(e.UserID))
                        left join fin_Employee D on rtrim(ltrim(d.UserID))=rtrim(ltrim(tkt.assignto)) left join fin_EmployeeRole r on rtrim(ltrim(r.Code))=rtrim(ltrim(e.RoleId)) where 1=1 '

		IF (rtrim(ltrim(@DeptFlag)) = 'Other')
		BEGIN
			IF (@FilterBy = 'CreatedByMe')
			BEGIN
				SELECT @prefixdept = dbo.[fin_Fun_FindDepartment](@CreatedBy, 'Select_PrefixByEmpId')+'%'

				SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(tkt.CreatedBy))=''' + @CreatedBy + ''' and tkt.ticketId not like ''' + @prefixdept + ''' group by substring(ticketId,1,2) ,tkt.Status'
			END

			-----------------------------------------------------------------------------------------------------------------------------------------
			IF (@FilterBy = 'AssignToMe')
			BEGIN
				SELECT @prefixdept = dbo.[fin_Fun_FindDepartment](@AssignTO, 'Select_PrefixByEmpId')+'%'
				select @deptcode=dbo.[fin_Fun_FindDepartment](@AssignTO, 'Select_DeptCodeByEmpId')

				SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(tkt.assignto))=''' + @AssignTO + ''' and rtrim(ltrim(tkt.CreatedBy))  in(select UserID from fin_Employee where DepId<>'''+ @deptcode +''') and tkt.ticketId like ''' + @prefixdept + ''' group by substring(ticketId,1,2) ,tkt.Status'
			END

			-------------------------------------------------------------------------------------------------------------------------------------------
			IF (@FilterBy = 'ReportToMe')
			BEGIN
				SELECT @prefixdept = dbo.[fin_Fun_FindDepartment](@ReportTo, 'Select_PrefixByEmpId')+'%'
				select @deptcode=dbo.[fin_Fun_FindDepartment](@ReportTo, 'Select_DeptCodeByEmpId')
				if(@Prefixdept='AD%')
				begin
				   SET @SelectQuery = @SelectQuery + ' and  tkt.ticketId  not like ''' + @prefixdept + ''' group by substring(ticketId,1,2) ,tkt.Status'
				end
				else
				begin
					SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(d.ReportToEmail))=''' + @ReportTo + ''' and rtrim(ltrim(tkt.CreatedBy))  in(select UserID from fin_Employee where DepId<>'''+ @deptcode +''') and tkt.ticketId  like ''' + @prefixdept + ''' group by substring(ticketId,1,2) ,tkt.Status'
				end
			END
			--------------------------------------------------------------------------------------------------------------------------------------------
			IF (@FilterBy = 'AssignByMe')
			BEGIN
				SELECT @prefixdept = dbo.[fin_Fun_FindDepartment](@AssignBy, 'Select_PrefixByEmpId')+'%'
				select @deptcode=dbo.[fin_Fun_FindDepartment](@AssignBy, 'Select_DeptCodeByEmpId')
				SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(tkt.assignto))<>''' + @AssignBy + ''' and rtrim(ltrim(tkt.CreatedBy))  in(select UserID from fin_Employee where DepId<>'''+ @deptcode +''') and tkt.ticketId  like ''' + @prefixdept + '''  group by substring(ticketId,1,2) ,tkt.Status'
			END
			-----------------------------------------------------------------------------------------------------------------------------------------------
		END
		else
		BEGIN
			IF (@FilterBy = 'CreatedByMe')
			BEGIN
				SELECT @prefixdept = dbo.[fin_Fun_FindDepartment](@CreatedBy, 'Select_PrefixByEmpId')+'%'

				SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(tkt.CreatedBy))=''' + @CreatedBy + ''' and tkt.ticketId  like ''' + @prefixdept + ''' group by substring(ticketId,1,2) ,tkt.Status'
			END
			------------------------------------------------------------------------------------------------------------------------------------------------
			IF (@FilterBy = 'AssignToMe')
			BEGIN
				SELECT @prefixdept = dbo.[fin_Fun_FindDepartment](@AssignTO, 'Select_PrefixByEmpId')+'%'
				select @deptcode=dbo.[fin_Fun_FindDepartment](@AssignTO, 'Select_DeptCodeByEmpId')
				SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(tkt.assignto))=''' + @AssignTO + ''' and rtrim(ltrim(tkt.CreatedBy)) in(select UserID from fin_Employee where DepId='''+ @deptcode +''') and tkt.ticketId like ''' + @prefixdept + ''' group by substring(ticketId,1,2) ,tkt.Status'
			END
			--------------------------------------------------------------------------------------------------------------------------------------------------
			IF (@FilterBy = 'ReportToMe')
			BEGIN
				SELECT @prefixdept = dbo.[fin_Fun_FindDepartment](@ReportTo, 'Select_PrefixByEmpId')+'%'
				select @deptcode=dbo.[fin_Fun_FindDepartment](@ReportTo, 'Select_DeptCodeByEmpId')
				SET @SelectQuery = @SelectQuery + ' and rtrim(ltrim(d.ReportToEmail))=''' + @ReportTo + ''' and rtrim(ltrim(tkt.CreatedBy)) in(select UserID from fin_Employee where DepId='''+ @deptcode +''') and tkt.ticketId  like ''' + @prefixdept + ''' group by substring(ticketId,1,2) ,tkt.Status'
			END
			--------------------------------------------------------------------------------------------------------------------------------------------------
		END
		SET @SelectQuery = @SelectQuery +') AS SourceTable PIVOT(SUM(Nocount) FOR Status IN ([Open], [Close],[ReOpen])) AS PivotTable'
		EXECUTE sp_executesql @SelectQuery
	END

	--------------------------------------------------------------END COUNT TICKET LIST BY DEPT------------------------------------------------------------------
	--------------------------------------------------------------START COUNT TICKET LIST BY MENU------------------------------------------------------------------
	IF (@action = 'Select_TicketCountbyMenu')
	BEGIN
		DECLARE @Select_TicketCountbyMenu TABLE (
			Id INT identity(1, 1)
			,Menu NVARCHAR(100)
			,TktCount INT,ParentMenu nvarchar(10)
			)

		SET @SelectQuery = 'select count(*) from fin_tbl_Ticket tkt left join fin_Employee e on rtrim(ltrim(tkt.CreatedBy))=rtrim(ltrim(e.UserID))left join fin_Employee D 
		on rtrim(ltrim(d.UserID))=rtrim(ltrim(tkt.assignto)) left join fin_EmployeeRole r on rtrim(ltrim(r.Code))=rtrim(ltrim(e.RoleId)) where 1=1 '
		set @DeptFlag='Other'
		declare @AddQuery nvarchar(max)
		IF (rtrim(ltrim(@DeptFlag)) = 'Other')
		BEGIN
			SELECT @prefixdept = dbo.[fin_Fun_FindDepartment](@CreatedBy, 'Select_PrefixByEmpId')+'%'

			SET @AddQuery = @SelectQuery + ' and rtrim(ltrim(tkt.CreatedBy))=''' + @CreatedBy + ''' and tkt.ticketId not like ''' + @prefixdept + ''''

			INSERT INTO @Select_TicketCountbyMenu (TktCount)
			EXECUTE sp_executesql @AddQuery

			UPDATE @Select_TicketCountbyMenu
			SET Menu = 'TktCreatedByMe',ParentMenu=@DeptFlag
			WHERE id = (
					SELECT MAX(id)
					FROM @Select_TicketCountbyMenu
					)

		
			------------------------------------------------------------------------------------------------------------------------------------

			select @deptcode=dbo.[fin_Fun_FindDepartment](@CreatedBy, 'Select_DeptCodeByEmpId')
			
			SET @AddQuery = @SelectQuery + ' and rtrim(ltrim(tkt.assignto))=''' + @CreatedBy + ''' and rtrim(ltrim(tkt.CreatedBy))  in(select UserID from fin_Employee where DepId<>'''+ @deptcode +''')'
			
			
			

			INSERT INTO @Select_TicketCountbyMenu (TktCount)
			EXECUTE sp_executesql @AddQuery

			UPDATE @Select_TicketCountbyMenu
			SET Menu = 'AssignToMe',ParentMenu=@DeptFlag
			WHERE id = (
					SELECT MAX(id)
					FROM @Select_TicketCountbyMenu
					)
			----------------------------------------------------------------------------------------------------------------------------------------------
			DECLARE @prefixdeptforReportN NVARCHAR(3)
				
				select @deptcode=dbo.[fin_Fun_FindDepartment](@CreatedBy, 'Select_DeptCodeByEmpId')
				
				SELECT @prefixdeptforReportN = dbo.[fin_Fun_FindDepartment](@CreatedBy, 'Select_PrefixByEmpId')+'%'
				
				if(@prefixdeptforReportN='AD%')
				begin
				   SET @AddQuery = @SelectQuery + ' '
				end
				else
				begin
				
				SET @AddQuery = @SelectQuery + ' and rtrim(ltrim(d.ReportToEmail))=''' + @CreatedBy + ''' and rtrim(ltrim(tkt.CreatedBy))  in(select UserID from fin_Employee where DepId<>'''+ @deptcode +''') '
				end
			print @AddQuery
			INSERT INTO @Select_TicketCountbyMenu (TktCount)
			EXECUTE sp_executesql @AddQuery

			UPDATE @Select_TicketCountbyMenu
			SET Menu = 'ReportToMe',ParentMenu=@DeptFlag
			WHERE id = (
					SELECT MAX(id)
					FROM @Select_TicketCountbyMenu
					)

			----------------------------------------------------------------------------------------------------------------------------------
			select @deptcode=dbo.[fin_Fun_FindDepartment](@CreatedBy, 'Select_DeptCodeByEmpId')
			SELECT @prefixdept = dbo.[fin_Fun_FindDepartment](@CreatedBy, 'Select_PrefixByEmpId')+'%'

			SET @AddQuery = @SelectQuery + ' and rtrim(ltrim(tkt.assignto))<>''' + @CreatedBy + ''' and rtrim(ltrim(tkt.CreatedBy))  in(select UserID from fin_Employee where DepId<>'''+ @deptcode +''') and tkt.ticketId  like ''' + @prefixdept + ''''

			INSERT INTO @Select_TicketCountbyMenu (TktCount)
			EXECUTE sp_executesql @AddQuery

			UPDATE @Select_TicketCountbyMenu
			SET Menu = 'AssignByMe',ParentMenu=@DeptFlag
			WHERE id = (
					SELECT MAX(id)
					FROM @Select_TicketCountbyMenu
					)




			-------------------------------------------------------------------------------------------------------------------------------------------------------
		
		END
		set @DeptFlag='Same'
		IF (rtrim(ltrim(@DeptFlag)) = 'Same')
		BEGIN
			SELECT @prefixdept = dbo.[fin_Fun_FindDepartment](@CreatedBy, 'Select_PrefixByEmpId')+'%'

			SET @AddQuery = @SelectQuery + ' and rtrim(ltrim(tkt.CreatedBy))=''' + @CreatedBy + ''' and tkt.ticketId  like ''' + @prefixdept + ''''

			INSERT INTO @Select_TicketCountbyMenu (TktCount)
			EXECUTE sp_executesql @AddQuery

			UPDATE @Select_TicketCountbyMenu
			SET Menu = 'TktCreatedByMe',ParentMenu=@DeptFlag
			WHERE id = (
					SELECT MAX(id)
					FROM @Select_TicketCountbyMenu
					)
			-----------------------------------------------------------------------------------------------------------------------------------------------------------------
			select @deptcode=dbo.[fin_Fun_FindDepartment](@CreatedBy, 'Select_DeptCodeByEmpId')
			
			SET @AddQuery = @SelectQuery + ' and rtrim(ltrim(tkt.assignto))=''' + @CreatedBy + ''' and rtrim(ltrim(tkt.CreatedBy))  in(select UserID from fin_Employee where DepId='''+ @deptcode +''')'

			INSERT INTO @Select_TicketCountbyMenu (TktCount)
			EXECUTE sp_executesql @AddQuery

			UPDATE @Select_TicketCountbyMenu
			SET Menu = 'AssignToMe',ParentMenu=@DeptFlag
			WHERE id = (
					SELECT MAX(id)
					FROM @Select_TicketCountbyMenu
				
				)
            -------------------------------------------------------------------------------------------------------------------------------------------------------------------
			
				
				select @deptcode=dbo.[fin_Fun_FindDepartment](@CreatedBy, 'Select_DeptCodeByEmpId')
				
				SELECT @prefixdeptforReportN = dbo.[fin_Fun_FindDepartment](@CreatedBy, 'Select_PrefixByEmpId')+'%'
				
				if(@prefixdeptforReportN='AD%')
				begin
				   SET @AddQuery = @SelectQuery + ' '
				end
				else
				begin
				
				SET @AddQuery = @SelectQuery + ' and rtrim(ltrim(d.ReportToEmail))=''' + @CreatedBy + ''' and rtrim(ltrim(tkt.CreatedBy))  not in(select UserID from fin_Employee where DepId<>'''+ @deptcode +''') '
				end
			print @AddQuery
			INSERT INTO @Select_TicketCountbyMenu (TktCount)
			EXECUTE sp_executesql @AddQuery

			UPDATE @Select_TicketCountbyMenu
			SET Menu = 'ReportToMe',ParentMenu=@DeptFlag
			WHERE id = (
					SELECT MAX(id)
					FROM @Select_TicketCountbyMenu
					)

			

			SELECT *
			FROM @Select_TicketCountbyMenu
		END
	END
			--------------------4------------------------------------------END COUNT TICKET LIST BY MENU------------------------------------------------------------------
			--------------------------------------------------------------START COMMENTS LIST---------------------------------------------------------------------------

	if(@action='Select_TicketCommentsList')
	begin
	 select * from fin_tbl_TicketComment  where TicketId=@TicketId  and isnull(IsDelete,0)=0 order by id desc
	end
	       ---------------------------------------------------------------END COMMENTS LIST------------------------------------------------------------------------------
	----------------------------------------------------------------START TICKET DETAILS LIST BY TICKET ID---------------------------------------------------------------
	if(@action='Select_TicketDetailsList')
	begin
	 select TicketId as [Ticket ID],CreatedDate as [Created Date],TAT as [TAT],AssignTo as [AssigneeName],CreatedBy as [CreatorName],Status as [Status],Priority as [Priority],Subject as [Subject],Query as [Query],Attachment as [Attachment] from fin_tbl_Ticket  where TicketId=@TicketId order by id desc
	end
	---------------------------------------------------------------END TICKET DETAILS LIST BY TICKET ID--------------------------------------------------------------------
	--------------------------------------------------------------SELECT TICKET ASSIGNEE BY TICKET ID-----------------------------------------------------------------------
	if(@action='Select_TicketAssignee')
	begin
	 select rtrim(ltrim(c.AssignTo)) as AssignTo,rtrim(ltrim(e.Name)) as Name from fin_tbl_Ticket c inner join fin_Employee e on c.AssignTo=e.UserID where TicketId=@TicketId
	end
	------------------------------------------------------------END TICKET ASSIGNEE BY TICKET ID---------------------------------------------------------------------------------
	------------------------------------------------------------START TICKET CREATOR BY TICKET ID ------------------------------------------------------------------------------
	if(@action='Select_TicketCreator')
	begin
	 select rtrim(ltrim(c.CreatedBy)) as CreatedBy,rtrim(ltrim(e.Name)) as Name from fin_tbl_Ticket c inner join fin_Employee e on c.CreatedBy=e.UserID where TicketId=@TicketId
	end
	----------------------------------------------------------END TICKET CREATOR BY TICKET ID-----------------------------------------------------------------------------------
	---------------------------------------------------------START TICKET CREATOR AND ASSIGNEE BY TICKET ID---------------------------------------------------------------------------------
	if(@action='Select_TicketCreatorAndAssignee')
	begin
	 select rtrim(ltrim(c.CreatedBy)) as CreatedBy,rtrim(ltrim(e.Name)) as Name,rtrim(ltrim(c.AssignTo)) as AssignTo,rtrim(ltrim(e1.Name)) as AName from 
	 fin_tbl_Ticket c inner join fin_Employee e on c.CreatedBy=e.UserID 
	 inner join fin_Employee e1 on c.AssignTo=e1.UserID
	 where TicketId=@TicketId
	end
	---------------------------------------------------------END TICKET CREATOR AND ASSIGNEE BY TICKET ID-----------------------------------------------------------------------
	---------------------------------------------------------START TICKET RM AND ASSIGNEE BY TICKET ID-------------------------------------------------------------------------
	if(@action='Select_TicketRMAndAssignee')
	begin
	 select rtrim(ltrim(e.ReportToEmail)) as RM,rtrim(ltrim(c.AssignTo)) as AssignTo,rtrim(ltrim(e.Name)) as Name from 
	 fin_tbl_Ticket c inner join fin_Employee e on c.AssignTo=e.UserID
	 where TicketId=@TicketId
	end
	---------------------------------------------------------END TICKET RM AND ASSIGNEE BY TICKET ID-------------------------------------------------------------------------

	if(@action='Insert_ErrorLog')
	begin
	  insert into [fin_tbl_ErrorLog](module,subject,error,createddate,stacktrace) values(@Module,@Subject,@Error,GETDATE(),@StackTrace)
	end
END




























GO



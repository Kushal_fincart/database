USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_GetDigitalMaster]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[fin_Proc_GetDigitalMaster] @Action VARCHAR(100)
	,@Id BIGINT = NULL
	,@Name NVARCHAR(50) = NULL
	,@campid BIGINT = NULL
	,@platformid BIGINT = NULL
AS
DECLARE @filter NVARCHAR(max)
DECLARE @Query NVARCHAR(max)

BEGIN
	-------------------------Start List of Campaign--------------------------------------
	IF (@Action = 'Select_CampaignList')
	BEGIN
		SELECT a.ID
			,rtrim(ltrim(a.name)) AS [Campaign Name]
			,rtrim(ltrim(b.Name)) AS [Created By]
			,convert(NVARCHAR, a.createddate, 109) AS [Created Date]
			,rtrim(ltrim(c.Name)) AS [Updated By]
			,a.updateddate AS [Updated Date]
			,CASE 
				WHEN isnull(a.STATUS, 0) = 0
					THEN 'Off'
				ELSE 'On'
				END AS STATUS
		FROM fin_campaign a
		INNER JOIN fin_Employee b ON rtrim(ltrim(a.createdby)) = rtrim(ltrim(b.UserID))
		LEFT JOIN fin_Employee C ON rtrim(ltrim(a.updatedby)) = rtrim(ltrim(c.UserID))
		WHERE (
				a.ID = @Id
				OR @Id IS NULL
				)
		ORDER BY id DESC
	END

	---------------End List of Campaign-----------------------------------------------------
	-------------------------Start Name of Platform--------------------------------------
	IF (@Action = 'Select_AllPlatformName')
	BEGIN
		SELECT a.ID
			,rtrim(ltrim(a.Platform_Name)) AS [Platform Name]
		FROM fin_Campaign_Platform a
		WHERE (STATUS = 1)
	END

	---------------End Name of Platform-----------------------------------------------------
	-------------------------Start Name of Campaign--------------------------------------
	IF (@Action = 'Select_AllCampaignName')
	BEGIN
		SELECT a.ID
			,rtrim(ltrim(a.name)) AS [Campaign Name]
		FROM fin_campaign a
		WHERE (STATUS = 1)
	END

	---------------End Name of Campaign-----------------------------------------------------
	---------------Start List of Campaign Platform-----------------------------------------------------
	IF (@Action = 'Select_CampaignPlatformList')
	BEGIN
		SELECT a.ID
			,rtrim(ltrim(a.Platform_Name)) AS [Platform Name]
			,rtrim(ltrim(a.Platform_Desc)) AS [Platform Description]
			,rtrim(ltrim(a.StaticURL)) AS [Static URL]
			,rtrim(ltrim(b.Name)) AS [Created By]
			,convert(NVARCHAR, a.createddate, 109) AS [Created Date]
			,rtrim(ltrim(c.Name)) AS [Updated By]
			,a.updateddate AS [Updated Date]
			,CASE 
				WHEN isnull(a.STATUS, 0) = 0
					THEN 'Off'
				ELSE 'On'
				END AS STATUS
			,CASE 
				WHEN isnull(a.IsDripMktEnable, 0) = 0
					THEN 'False'
				ELSE 'True'
				END AS chkboxIsDripMktEnable
				,CASE 

				WHEN isnull(a.IsAutoAllocate, 0) = 0

					THEN 'False'

				ELSE 'True'

				END AS IsAutoAllocate
		FROM fin_Campaign_Platform a
		INNER JOIN fin_Employee b ON rtrim(ltrim(a.createdby)) = rtrim(ltrim(b.UserID))
		LEFT JOIN fin_Employee C ON rtrim(ltrim(a.updatedby)) = rtrim(ltrim(c.UserID))
		WHERE (
				a.ID = @Id
				OR @Id IS NULL
				)
		ORDER BY id DESC
	END

	---------------End List of Campaign Platform-----------------------------------------------------
	---------------Start List of Mapping Campaign Platform-----------------------------------------------------
	IF (@Action = 'Select_Camp_PlatformMappingList')
	BEGIN
		SELECT a.ID
			,a.Camp_Id
			,a.Platform_Id
			,rtrim(ltrim(fc.Name)) AS [Campaign Name]
			,rtrim(ltrim(fcp.Platform_Name)) AS [Platform Name]
			,rtrim(ltrim(a.Track_Code)) AS [Track Code]
			,rtrim(ltrim(b.Name)) AS [Created By]
			,convert(NVARCHAR, a.createddate, 109) AS [Created Date]
			,rtrim(ltrim(c.Name)) AS [Updated By]
			,a.updateddate AS [Updated Date]
			,CASE 
				WHEN isnull(a.STATUS, 0) = 0
					THEN 'Off'
				ELSE 'On'
				END AS STATUS
		FROM fin_Mapping_Camp_Plat a
		INNER JOIN fin_Campaign fc ON fc.ID = a.Camp_Id
		INNER JOIN fin_Campaign_Platform fcp ON fcp.ID = a.Platform_Id
		INNER JOIN fin_Employee b ON rtrim(ltrim(a.createdby)) = rtrim(ltrim(b.UserID))
		LEFT JOIN fin_Employee C ON rtrim(ltrim(a.updatedby)) = rtrim(ltrim(c.UserID))
		WHERE (
				a.ID = @id
				OR @Id IS NULL
				)
		ORDER BY id DESC
	END

	---------------End List of Mapping Campaign Platform-----------------------------------------------------
	-------------------------Start List of CampaignAction--------------------------------------
	IF (@Action = 'Select_CampaignActionList')
	BEGIN
		SELECT a.ID
			,rtrim(ltrim(a.Action_Name)) AS [Action Name]
			,rtrim(ltrim(a.Action_desc)) AS [Action Description]
			,a.Weightage AS Weightage
			,rtrim(ltrim(b.Name)) AS [Created By]
			,convert(NVARCHAR, a.createddate, 109) AS [Created Date]
			,rtrim(ltrim(c.Name)) AS [Updated By]
			,a.updateddate AS [Updated Date]
			,CASE 
				WHEN isnull(a.STATUS, 0) = 0
					THEN 'Off'
				ELSE 'On'
				END AS STATUS
		FROM fin_Campaignaction a
		INNER JOIN fin_Employee b ON rtrim(ltrim(a.createdby)) = rtrim(ltrim(b.UserID))
		LEFT JOIN fin_Employee C ON rtrim(ltrim(a.updatedby)) = rtrim(ltrim(c.UserID))
		WHERE (
				a.ID = @Id
				OR @Id IS NULL
				)
		ORDER BY id DESC
	END

	---------------End List of LeadActivityAction-----------------------------------------------------
	-------------------------Start List of TrackerDomain--------------------------------------
	IF (@Action = 'Select_TrackerDomainList')
	BEGIN
		SELECT a.ID
			,rtrim(ltrim(a.domain_Name)) AS [Domain Name]
			,rtrim(ltrim(a.static_name)) AS [Static Name]
			,rtrim(ltrim(b.Name)) AS [Created By]
			,convert(NVARCHAR, a.createddate, 109) AS [Created Date]
			,rtrim(ltrim(c.Name)) AS [Updated By]
			,a.updateddate AS [Updated Date]
			,CASE 
				WHEN isnull(a.STATUS, 0) = 0
					THEN 'Off'
				ELSE 'On'
				END AS STATUS
		FROM fin_tracker_domain a
		INNER JOIN fin_Employee b ON rtrim(ltrim(a.createdby)) = rtrim(ltrim(b.UserID))
		LEFT JOIN fin_Employee C ON rtrim(ltrim(a.updatedby)) = rtrim(ltrim(c.UserID))
		WHERE (
				a.ID = @Id
				OR @Id IS NULL
				)
		ORDER BY id DESC
	END

	---------------End List of TrackerDomain-----------------------------------------------------
	IF (@Action = 'Select_TrackerDomainPartialList')
	BEGIN
		SELECT a.ID
			,rtrim(ltrim(b.Name)) AS [Created By]
			,convert(NVARCHAR, a.createddate, 109) AS [Created Date]
			,rtrim(ltrim(c.Name)) AS [Updated By]
			,a.updateddate AS [Updated Date]
		FROM fin_tracker_domain a
		INNER JOIN fin_Employee b ON rtrim(ltrim(a.createdby)) = rtrim(ltrim(b.UserID))
		LEFT JOIN fin_Employee C ON rtrim(ltrim(a.updatedby)) = rtrim(ltrim(c.UserID))
		WHERE (
				a.ID = @Id
				OR @Id IS NULL
				)
		ORDER BY id DESC
	END
END

IF (@Action = 'Select_IsCampaignExist')
BEGIN
	IF (
			@Id IS NOT NULL
			AND @id <> ''
			)
	BEGIN
		IF EXISTS (
				SELECT *
				FROM fin_campaign
				WHERE rtrim(ltrim(Name)) = rtrim(ltrim(@Name))
					AND ID <> @Id
				)
		BEGIN
			SELECT 1
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
	ELSE
	BEGIN
		IF EXISTS (
				SELECT *
				FROM fin_campaign
				WHERE rtrim(ltrim(Name)) = rtrim(ltrim(@Name))
				)
		BEGIN
			SELECT 1
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
END

IF (@Action = 'Select_IsPlatformExist')
BEGIN
	IF (
			@Id IS NOT NULL
			AND @id <> ''
			)
	BEGIN
		IF EXISTS (
				SELECT *
				FROM fin_Campaign_Platform
				WHERE rtrim(ltrim(Platform_Name)) = rtrim(ltrim(@Name))
					AND ID <> @Id
				)
		BEGIN
			SELECT 1
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
	ELSE
	BEGIN
		IF EXISTS (
				SELECT *
				FROM fin_Campaign_Platform
				WHERE rtrim(ltrim(Platform_Name)) = rtrim(ltrim(@Name))
				)
		BEGIN
			SELECT 1
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
END

IF (@Action = 'Select_IsCamp_Plat_MappingExist')
BEGIN
	IF EXISTS (
			SELECT *
			FROM fin_Mapping_Camp_Plat
			WHERE Camp_Id = @campid
				AND Platform_Id = @platformid
			)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END

IF (@Action = 'Select_IsStageExist')
BEGIN
	IF (
			@Id IS NOT NULL
			AND @id <> ''
			)
	BEGIN
		IF EXISTS (
				SELECT *
				FROM fin_Stage
				WHERE rtrim(ltrim(StageName)) = rtrim(ltrim(@Name))
					AND ID <> @Id
				)
		BEGIN
			SELECT 1
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
	ELSE
	BEGIN
		IF EXISTS (
				SELECT *
				FROM fin_Stage
				WHERE rtrim(ltrim(StageName)) = rtrim(ltrim(@Name))
				)
		BEGIN
			SELECT 1
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
END

IF (@Action = 'Select_IsCampaignActionExist')
BEGIN
	IF (
			@Id IS NOT NULL
			AND @id <> ''
			)
	BEGIN
		IF EXISTS (
				SELECT *
				FROM fin_CampaignAction
				WHERE rtrim(ltrim(Action_Name)) = rtrim(ltrim(@Name))
					AND ID <> @Id
				)
		BEGIN
			SELECT 1
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
	ELSE
	BEGIN
		IF EXISTS (
				SELECT *
				FROM fin_CampaignAction
				WHERE rtrim(ltrim(Action_Name)) = rtrim(ltrim(@Name))
				)
		BEGIN
			SELECT 1
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
END

IF (@Action = 'Select_IsTrackerExist')
BEGIN
	IF (
			@Id IS NOT NULL
			AND @id <> ''
			)
	BEGIN
		IF EXISTS (
				SELECT *
				FROM fin_tracker_domain
				WHERE rtrim(ltrim(domain_Name)) = rtrim(ltrim(@Name))
					AND ID <> @Id
				)
		BEGIN
			SELECT 1
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
	ELSE
	BEGIN
		IF EXISTS (
				SELECT *
				FROM fin_tracker_domain
				WHERE rtrim(ltrim(domain_Name)) = rtrim(ltrim(@Name))
				)
		BEGIN
			SELECT 1
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
END

IF (@Action = 'Select_StagesList')
BEGIN
	SELECT a.ID
		,rtrim(ltrim(a.stagename)) AS [Stage Name]
		,ltrim(rtrim(a.stagedescription)) AS [Description]
		,a.weightage AS [Weightage]
		,rtrim(ltrim(b.Name)) AS [Created By]
		,convert(NVARCHAR, a.createddate, 109) AS [Created Date]
		,rtrim(ltrim(c.Name)) AS [Updated By]
		,a.updateddate AS [Updated Date]
		,CASE 
			WHEN isnull(a.STATUS, 0) = 0
				THEN 'Off'
			ELSE 'On'
			END AS STATUS
	FROM fin_stage a
	INNER JOIN fin_Employee b ON rtrim(ltrim(a.createdby)) = rtrim(ltrim(b.UserID))
	LEFT JOIN fin_Employee C ON rtrim(ltrim(a.updatedby)) = rtrim(ltrim(c.UserID))
	WHERE (
			a.ID = @Id
			OR @Id IS NULL
			)
	ORDER BY id DESC
END

IF (@Action = 'Select_MapPlatformList')
BEGIN
	SELECT a.ID
		,rtrim(ltrim(a.Platform_Name)) AS [Platform Name]
	FROM fin_Campaign_Platform a
	INNER JOIN fin_Mapping_Camp_Plat b ON a.ID = b.Platform_Id
	WHERE (
			b.STATUS = 1
			AND b.Camp_Id = @Id
			)
END

IF (@Action = 'Select_IsCampaignEmailer')
BEGIN
	SELECT count(*)
	FROM fin_Campaign_Platform a
	WHERE ID = @platformid
		AND a.IsDripMktEnable = 1
END

IF (@Action = 'Select_PlatformNameById')
BEGIN
	DECLARE @campname VARCHAR(max)
	DECLARE @platname VARCHAR(max)

	SELECT @campname = a.Name
	FROM fin_Campaign a
	WHERE ID = @campid

	SELECT @platname = a.Platform_Name
	FROM fin_Campaign_Platform a
	WHERE id = @platformid

	SELECT rtrim(ltrim(@campname)) + '_' + rtrim(ltrim(@platname))
END

IF (@Action = 'Select_CampaignNameById')
BEGIN
	DECLARE @campname1 VARCHAR(max)

	SELECT @campname = a.Name
	FROM fin_Campaign a
	WHERE ID = @campid

	SELECT rtrim(ltrim(@campname))
END



GO

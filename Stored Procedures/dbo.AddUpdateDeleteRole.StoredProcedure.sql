USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[AddUpdateDeleteRole]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[AddUpdateDeleteRole]
@DepCode varchar(50)=null,
@Role varchar(50)=null,
@Level int=0,
@RoleId int=0,
@RoleCode varchar(50)=null,
@Action varchar(50)=null
as
begin

  if(@Action='INSERT')
    begin
  insert into fin_EmployeeRole(Code,DepCode,Role,Level) values(
'ERL'+CONVERT(varchar,DATEPART(yy,getdate()))+
CONVERT(varchar,datepart(mm,getdate())) +
CONVERT(varchar,datepart(dd,getdate()))+
CONVERT(varchar,datepart(HH,getdate()))+
CONVERT(varchar,datepart(MI,getdate()))+
CONVERT(varchar,datepart(SS,getdate())),@DepCode,@Role,@Level)
 end
 IF(@Action='DELETE')
 begin
 update fin_EmployeeRole set Status=0 where Id=@RoleId
 end
 if(@Action='UPDATE')
 begin
 
 if exists(SELECT Code FROM dbo.fin_EmployeeRole WHERE  Role= isnull(@Role,'') and DepCode=@DepCode and Status=1 and @Level=0)
begin
select 5
end
else
begin
update fin_EmployeeRole set Role=@Role,Level=@Level where Code=@RoleCode;
select 1
end

 end

 end


 

GO

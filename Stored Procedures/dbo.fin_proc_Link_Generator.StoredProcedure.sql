USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_proc_Link_Generator]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[fin_proc_Link_Generator](
	@platformid_list UT_fin_Campign_platform readonly
	,@campid BIGINT)
as
begin

IF OBJECT_ID('tempdb..#TempLnk') IS NOT NULL
    DROP TABLE #TempLnk


	create table #TempLnk
(
    Link nvarchar(max),Flag nvarchar(max)

)

    DECLARE @totalplatform AS INT

	SELECT @totalplatform = count(*)
	FROM @platformid_list

	DECLARE @cntplat AS INT
	DECLARE @currentplat AS BIGINT
	declare @currentplantname as varchar(max)
	Declare @IsDripMktEnable as bit
	SET @cntplat = 1

	WHILE (@cntplat <= @totalplatform)
	BEGIN
		SELECT @currentplat = Platformid
		FROM @platformid_list
		WHERE rowid = @cntplat

		select @currentplantname=rtrim(ltrim(Platform_Name)) from fin_Campaign_Platform where id=@currentplat

		SELECT @IsDripMktEnable = isnull(IsDripMktEnable,0)
		FROM fin_Campaign_Platform
		WHERE ID = @currentplat

		if(@IsDripMktEnable=1)
		begin
			insert into #TempLnk
			select @currentplantname+'|'+'<img src='+rtrim(ltrim(domain_Name))+'/'+rtrim(ltrim(static_name))+'/>','Emailer' from fin_tracker_domain

			insert into #TempLnk
			select a.Platform_Name+'|'+rtrim(ltrim(StaticURL))+'?t='+rtrim(ltrim(b.Track_Code)),'Emailer' from fin_Campaign_Platform a inner join 
			fin_Mapping_Camp_Plat b on a.ID=b.Platform_Id where a.ID=@currentplat and b.camp_id=@campid
		end
		else
		begin
		   insert into #TempLnk
			select a.Platform_Name+'|'+StaticURL+'?t='+b.Track_Code,'Social' from fin_Campaign_Platform a inner join 
			fin_Mapping_Camp_Plat b on a.ID=b.Platform_Id where a.ID=@currentplat and b.Camp_Id=@campid
		
		end

		set @cntplat=@cntplat+1
		
	end
	select * from #TempLnk		
end

GO

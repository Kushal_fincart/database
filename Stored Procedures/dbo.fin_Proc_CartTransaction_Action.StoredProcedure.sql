USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_CartTransaction_Action]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[fin_Proc_CartTransaction_Action]
	(
	@USERID nvarchar(max)=NULL,
	@MEMBERID nvarchar(max)=NULL,
	@PROFILEID nvarchar(max)=NULL,
	@TRANSDATE datetime= NULL,
	@STATUS int = 0,

	
	@CARTID nvarchar(max)=NULL,
	
	@GOALCODE nvarchar(max)=NULL,
	@FUNDCODE nvarchar(max)=NULL,
	@SCHEMECODE nvarchar(max)= NULL,
	@TYPE nvarchar(max)= NULL,
	@ENTRYDATE datetime= NULL,
	@DIVIDENTOPT nvarchar(max)=NULL,
	@FOLIONO nvarchar(max)= NULL,
	@AMOUNT nvarchar(max)=NULL,

	@BILLDESKCODE nvarchar(max)=NULL,
	@BILLAMCAMOUNT nvarchar(max)=NULL,
	@SCHEMENAME nvarchar(max)=NULL,
	@INVESTPROFILE nvarchar(max)=NULL,
	
	@STARTDATE datetime= NULL,
	@ENDDATE datetime= NULL,
	@NOOFINSTALLMENT int=0,
	@FREQUENCY nvarchar(max)=NULL,
	@BANKCODE nvarchar(max)=NULL,
	@BANKNAME nvarchar(max)=NULL,
	@BANKBRANCH nvarchar(max)=NULL,
	@ACCOUNTNO nvarchar(max)=NULL,
	@MANDATEID nvarchar(max)=NULL,
	@FIRSTSIPDATE datetime =NULL,
	@MDATE nvarchar(max)=NULL,

	@PGREFID nvarchar(max)=NULL,
	@BANKREFN nvarchar(max)=NULL,
	@BILLDESKTXNDATE nvarchar(max)=NULL,



	@TRANSID nvarchar(max)=null,
	@CARTTRANSTBL fin_Type_CartTransTbl READONLY,
	
	@ACTION varchar(500)
	)
as


	

begin
	if(@ACTION='ADD_CART_TRANSACTION')
	begin
		Declare @TID nvarchar(max);
		Declare @TRA_TYPE nvarchar(100);
		set @TRA_TYPE = ISNULL(NULLIF((select top 1 type from @CARTTRANSTBL), ''), (select top 1 type from @CARTTRANSTBL))
		
		insert into [dbo].[fin_Cart_Transactions] (InvestTxnId,userId,memberId,profileId,transdate,status,transType)
		select @TRANSID,@USERID,@MEMBERID,@PROFILEID,@TRANSDATE,@STATUS,@TRA_TYPE

		
		--old code
		--insert into [dbo].[fin_Cart_Transactions_Details]  select @TRANSID,CartId,goalCode,fundCode,schemeCode,type,entryDate,dividendOpt,folioNo,amount,billdesk_code,billamc_code,scheme_name,invest_profile,startdate,endDate,noOfInstallment,frequency,bankCode,accountNo,mandateId,firstSipDate,mDate,'',@BANKNAME,@BANKBRANCH,'','','' from @CARTTRANSTBL
		
		--new code
		-- this code added because investwell takes startdate as current date and we have to pass start date as first sip date in client transaction email
		insert into [dbo].[fin_Cart_Transactions_Details]  select @TRANSID,CartId,goalCode,fundCode,schemeCode,type,entryDate,dividendOpt,folioNo,amount,billdesk_code,billamc_code,scheme_name,invest_profile,firstSipDate,case noOfInstallment when 0 then endDate else (SELECT DATEADD(month, noOfInstallment-1, firstSipDate) AS DateAdd)end,noOfInstallment,frequency,bankCode,accountNo,mandateId,firstSipDate,mDate,'',@BANKNAME,@BANKBRANCH,'','','' from @CARTTRANSTBL

	end
if(@ACTION='UPDATE_CART_STATUS')
	begin
		update fin_Cart set status=@STATUS where CartId = @CARTID
		update [dbo].[fin_Cart_Transactions] set status=@STATUS where id = @TRANSID
	end
if(@ACTION='UPDATE_CART_TRANSACTION_STATUS')
	begin
		update [dbo].[fin_Cart_Transactions] set status = @STATUS where InvestTxnId=@TRANSID
		update [dbo].[fin_Cart_Transactions_Details] set PGRefID = @PGREFID, bankrefN = @BANKREFN, billdeskTxnDate = @BILLDESKTXNDATE where investTxnID = @TRANSID
		update fin_Cart set status=@STATUS where CartId in (select Cartid from [dbo].[fin_Cart_Transactions_Details] where InvestTxnId=@TRANSID)

		--old code start

--select f.userid,
--f.memberid,
--f.profileid,
--f.transdate,
--f.status,
--t.CartId,
--t.goalCode,
--t.fundCode,
--t.schemeCode,
--t.type,
--t.entryDate,
--t.dividendOpt,
--t.folioNo,
--t.amount,
--t.billdesk_code,
--t.billamc_code,
--t.scheme_name,
--t.invest_profile,
--t.startDate,
--t.endDate,
--t.noOfInstallment,
--t.frequency,
--t.bankCode,
--t.accountNo,
--t.mandateId,
--t.firstSipDate,
--t.mDate,
--t.bankName,
--t.bankBranch,
--p.Name,
--f.id as TxnId
--from [fin_Cart_Transactions_Details] t
--left join 
--fin_Cart_Transactions f on t.InvestTxnID = f.InvestTxnID left join
--fp_Prospects p on f.userid = p.userid
--where t.InvestTxnID = @TRANSID

	--new code start
	--here case added to startDate because need to pass entryDate instead of startDate on instasip update for generating sip series

select f.userid,
f.memberid,
f.profileid,
f.transdate,
f.status,
t.CartId,
t.goalCode,
t.fundCode,
t.schemeCode,
t.type,
t.entryDate,
t.dividendOpt,
t.folioNo,
t.amount,
t.billdesk_code,
t.billamc_code,
t.scheme_name,
t.invest_profile,
CASE t.type 
  WHEN 'I' THEN t.entryDate  
  ELSE t.startDate 
END as startDate,
t.endDate,
t.noOfInstallment,
t.frequency,
t.bankCode,
t.accountNo,
t.mandateId,
t.firstSipDate,
t.mDate,
t.bankName,
t.bankBranch,
p.Name,
f.id as TxnId,
caf.email
 from [fin_Cart_Transactions_Details] t
	left join 
 fin_Cart_Transactions f on t.InvestTxnID = f.InvestTxnID left join
 fp_Prospects p on f.userid = p.userid left join
 fin_CAF_BasicDetails caf on caf.memberId = f.memberId
	where t.InvestTxnID = @TRANSID
		
	end
if(@ACTION='SELECT_CART_TRANSACTION')
	begin
		select f.userid,f.memberid,f.profileid,f.transdate,f.status,t.CartId,t.goalCode,t.fundCode,t.schemeCode,t.type,t.entryDate,t.dividendOpt,t.folioNo,t.amount,t.billdesk_code,t.billamc_code,t.scheme_name,t.invest_profile,t.startDate,t.endDate,t.noOfInstallment,t.frequency,t.bankCode,t.accountNo,t.mandateId,t.firstSipDate,t.mDate,t.bankName,t.bankBranch from [fin_Cart_Transactions_Details] t 
		left join 
 fin_Cart_Transactions f on t.InvestTxnID = f.InvestTxnID 
	where t.InvestTxnID = @TRANSID

	end

end	



GO

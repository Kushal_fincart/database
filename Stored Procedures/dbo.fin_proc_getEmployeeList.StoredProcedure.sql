USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_proc_getEmployeeList]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[fin_proc_getEmployeeList]
@Action varchar(50)=null,
@Id int=0,
@DepId varchar(50)=null,
@AssignUserID varchar(100)=null,
@UserId varchar(50)=null,
@RoleId varchar(50)=null
as
begin
if(@Action='AllEmployee')
begin
select emp.Id,emp.EmpCode,emp.DepId,dept.Name as 'Department',emp.RoleId,emprole.Role as 'Role',emp.ReportToEmpCode,emp.ReportToEmail,
emp.Name,emp.UserID,emp.Pwd,emp.Address,emp.City,emp.Pin,emp.State,emp.Country,emp.Phone,
emp.OfficeEmail,emp.PersonalEmail,
emp.DOB,emp.DOJ,emp.DOR,emp.Salary,emp.BloodGroup,emp.CreatedDate,emp.UpdatedDate
,emp.UpdatedByEmail,emp.Status from fin_Employee emp inner join fin_Departments dept on emp.DepId=dept.Code inner join fin_EmployeeRole emprole on emp.RoleId=emprole.Code
 where emp.Status=1 order by Id desc
end
if(@Action='PartialViewEmployeeListById')
begin
select OfficeEmail,Phone,Address,BloodGroup,Convert(varchar(10),DOB,120)as DOB,convert(varchar(10),DOJ,120)as DOJ,
CONVERT(varchar(10),DOR,120)as DOR from fin_Employee  where Id=@Id and  Status=1
end
if(@Action='EmployeeById')
begin
declare @temp varchar(50);
set @temp=(select EmpCode from fin_Employee where  Id=@Id)
select emp.Id,emp.EmpCode,emp.DepId,dept.Name as 'Department',emp.RoleId,emprole.Role as 'Role',emp.ReportToEmpCode,emp.ReportToEmail,
emp.Name,emp.UserID,emp.Pwd,emp.Address,emp.City,emp.Pin,emp.State,emp.Country,emp.Phone,
emp.OfficeEmail,emp.PersonalEmail,
 CONVERT(VARCHAR(10), emp.DOB, 120) as DOB, 
 CONVERT(VARCHAR(10), emp.DOJ, 120) as DOJ,
 CONVERT(VARCHAR(10), emp.DOR, 120) as DOR,
 emp.Salary,emp.BloodGroup,emp.CreatedDate,emp.UpdatedDate
,emp.UpdatedByEmail,emp.Status,emp.ModuleCode from fin_Employee emp inner join fin_Departments dept on emp.DepId=dept.Code inner join fin_EmployeeRole emprole on emp.RoleId=emprole.Code 
 where emp.Id=@Id and emp.Status=1
 select * from fin_ModuleAssignToEmployee where EmpCode=@temp
end
if(@Action='ReportTo')
begin
declare @Level int
Set @Level=(select Level from fin_EmployeeRole where Code=(select roleid from fin_Employee where UserID=@AssignUserID and Status=1))
select emp.EmpCode,emp.Name from fin_Employee emp inner join fin_EmployeeRole empRole on emp.RoleId=empRole.Code 
where emp.DepID=@DepId and empRole.Level<@Level and emp.Status=1

end
if(@Action='UserExists')
begin
select count(Id) from fin_Employee where UserID=@UserId and Status=1
end

end


select count(Id) from fin_Employee where UserID='Tanvir.alam@fincart.com'
select * from fin_Employee where UserId like'%vivek.sharma@fincart.com%'
GO

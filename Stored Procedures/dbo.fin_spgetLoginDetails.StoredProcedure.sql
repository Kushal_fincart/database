USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_spgetLoginDetails]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[fin_spgetLoginDetails]
@UserID varchar(50)='',
@Password varchar(100)=''
as
begin

select Name,EmpCode,DepId, UserID,Pwd,RoleId,ModuleCode,SubModuleCode  from fin_Employee where UserID=@UserID and Pwd=@Password and Status=1;
exec fin_Proc_spGetModulesAndSubModules @Action='AssignModuleInSession',@UserID=@UserID
end







GO

USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_SYNCUSER_FpProspectToCAFTables]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[fin_Proc_SYNCUSER_FpProspectToCAFTables]
@action nvarchar(max)=null
AS
BEGIN
	IF(@action='SYNC_USERS')
		BEGIN
			
			declare @NAME nvarchar(max),
			@Userid nvarchar(max),
			@pwd nvarchar(max),
			@mobile1 nvarchar(max),
			@systemdate nvarchar(max)

			declare syncfpUser_record CURSOR FOR
			select  NAME,Userid,pwd,mobile1,systemdate from fp_Prospects where UserID not in(select distinct userid from fin_CAF_BasicDetails) order by systemdate desc

			OPEN syncfpUser_record
			FETCH NEXT FROM syncfpUser_record INTO
			@NAME,
			@Userid,
			@pwd,
			@mobile1,
			@systemdate	
			WHILE @@FETCH_STATUS = 0
				BEGIN

				DECLARE @FINALNAME nvarchar(max)
				IF(@NAME IS NULL OR @NAME ='')
					BEGIN 
					SET @FINALNAME = SUBSTRING(@userid,0,CHARINDEX('@',@userid, 0))  
					END
				ELSE
					BEGIN
						SET @FINALNAME = @NAME;
					END
				
				print 'insert --' + @userid + '--' + @pwd +'--'+ @mobile1 +'--'+ @FINALNAME

				Execute fin_Proc_CafAction @action='INSERT_CAF_INIT_RECORD',
				@USERID=@userid,
				@MEMBERID=@userid,
				@GROUPLEADER=@userid,
				@PASSWORD=@pwd,
				@PHONE=@mobile1,
				@MOBILE=@mobile1,
				@CLIENTNAME=@FINALNAME

				FETCH NEXT FROM syncfpUser_record INTO
			@NAME,
			@Userid,
			@pwd,
			@mobile1,
			@systemdate
				END

			CLOSE syncfpUser_record
			DEALLOCATE syncfpUser_record


		END
END




GO

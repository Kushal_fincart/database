USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fin_Proc_spGetModulesAndSubModules]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[fin_Proc_spGetModulesAndSubModules]
@Action varchar(50)=null,
@ModuleCode varchar(50)=null,
@UserID VARCHAR(50)=null
as
begin
if(@Action='gelAllModulesWithSubModules')
begin
select M.ModuleName as Module,E.ModuleName as SubModule from fin_MenuModule E inner join fin_MenuModule M on E.ParentID=M.Id;
end
if(@Action='AllModules')
begin

WITH CTE_Modules
as
(
select M.Id, M.ModuleName as  Module,E.ModuleName as SubModule from fin_MenuModule E inner join fin_MenuModule M on E.ParentID=M.Id
)
select Distinct Id, Module from CTE_Modules where Module !='Administration'
end
if(@Action='SubModuleByModul')
begin
WITH CTE_Modules
as
(
select M.ModuleName as Module,E.ModuleName as SubModule from fin_MenuModule E inner join fin_MenuModule M on E.ParentID=M.Id
)
select SubModule from CTE_Modules where Module=@ModuleCode;
end

if(@Action='AssignModuleInSession')
begin
declare @EmpCode varchar(100);
set @EmpCode=(select EmpCode from fin_Employee where  UserID=@UserID and Status=1)
select * from fin_ModuleAssignToEmployee where EmpCode=@EmpCode and Status=1
end

end







GO

USE [FP]
GO
/****** Object:  StoredProcedure [dbo].[fp_Workpoint_Lead_Export]    Script Date: 29-05-2018 18:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[fp_Workpoint_Lead_Export] @Action [varchar] (100)
	,@DATEFROM DATETIME = ''
	,--- FOR DYNAMIC QUERY
	@DATETO DATETIME = ''
	,--- FOR DYNAMIC QUERY
	@FILTERBY1 NVARCHAR(max) = NULL
	,--- FOR DYNAMIC QUERY
	@FILTERBY2 NVARCHAR(max) = NULL
	,--- FOR DYNAMIC QUERY
	@FILTERBYNAME NVARCHAR(max) = NULL
	,--- FOR DYNAMIC QUERY
	@FILTERBYEMAIL NVARCHAR(max) = NULL
	,--- FOR DYNAMIC QUERY
	@FILTERBYMOBILE NVARCHAR(max) = NULL
	,--- FOR DYNAMIC QUERY
	@FILTERBYCAMPAIGN NVARCHAR(max) = NULL
	,--- FOR DYNAMIC QUERY
	@EMPCODE NVARCHAR(max) = NULL,
	@RM nvarchar(max)=null
AS
BEGIN
	DECLARE @Conditions VARCHAR(MAX) = '' --- FOR DYNAMIC QUERY
	DECLARE @EmpQuery VARCHAR(MAX) = '' --- FOR DYNAMIC QUERY
	DECLARE @TestQuery NVARCHAR(MAX) = NULL --- FOR DYNAMIC QUERY
	DECLARE @ParmDefinition NVARCHAR(max) = NULL --- FOR DYNAMIC QUERY
	DECLARE @LTypeId INT = NULL
	DECLARE @CurrentLead INT = NULL
	DECLARE @RecordCount INT = NULL
	DECLARE @isAdmin INT = NULL

	IF (@Action = 'SELECT_LEADS_PAGED')
	BEGIN
	IF(@EMPCODE is not null and @EMPCODE != '')
	begin
		select @isAdmin=count(*) from fin_Employee e inner join fin_Departments d on e.DepId=Code and d.Code='DEP2018416185931' and EmpCode=@EMPCODE
		if(@isAdmin<=0)
			begin
				set @EmpQuery = ' and lm.empcode =''' + @EMPCODE + ''''
			end
		else
		
		set @EmpQuery=''
		
		End
	Else 
		Begin
			set @EmpQuery = ''
			
		End
		IF (
				@DATEFROM IS NOT NULL
				AND @DATEFROM != ''
				AND @DATETO IS NOT NULL
				AND @DATETO != ''
				AND @FILTERBY1 IS NOT NULL
				AND @FILTERBY1 != ''
				)
		BEGIN
			IF (@FILTERBY1 = 'BY_CREATEDDATE')
			BEGIN
				SET @Conditions = @Conditions + ' and CONVERT(date, lm.createdDate) between ''' + CAST(CONVERT(DATE, @DATEFROM) AS NVARCHAR(500)) + ''' and ''' + CAST(CONVERT(DATE, @DATETO) AS NVARCHAR(500)) + ''''
			END
			ELSE
			BEGIN
				SET @Conditions = @Conditions + ' and CONVERT(date, lm.lastActivityDate) between ''' + CAST(CONVERT(DATE, @DATEFROM) AS NVARCHAR(500)) + ''' and ''' + CAST(CONVERT(DATE, @DATETO) AS NVARCHAR(500)) + ''''
			END
		END

		IF (
				@FILTERBY2 IS NOT NULL
				AND @FILTERBY2 != ''
				)
		BEGIN
			if(@FILTERBY2='FOLLOW UP')
			begin
			set @Conditions=@Conditions + ' and lm.leadTypeId in (13,18,19)';
			end
			else
			begin
			SET @Conditions = @Conditions + ' and lm.leadTypeId= CASE WHEN @FILTERBY2=''INTERESTED'' then 18 WHEN 



				@FILTERBY2=''CONVERTED'' THEN 14 WHEN @FILTERBY2=''NEW'' THEN 11 WHEN @FILTERBY2=''DEAD'' THEN 16 WHEN @FILTERBY2=''READY TO INVEST'' 



				THEN 19 WHEN @FILTERBY2=''ON HOLD'' THEN 17 WHEN @FILTERBY2=''ASSIGNED'' THEN 12 WHEN @FILTERBY2=''FOLLOW UP'' THEN 13 WHEN @FILTERBY2=''RE-ASSIGNED'' THEN 15 ELSE 0 END'end
		END

		IF (
				@FILTERBYNAME IS NOT NULL
				AND @FILTERBYNAME != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.name like ''%' + @FILTERBYNAME + '%'''
		END

		IF (
				@FILTERBYEMAIL IS NOT NULL
				AND @FILTERBYEMAIL != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.userid like ''%' + @FILTERBYEMAIL + '%'''
		END

		IF (
				@FILTERBYMOBILE IS NOT NULL
				AND @FILTERBYMOBILE != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.mobile1 like ''%' + @FILTERBYMOBILE + '%'''
		END
		if(@RM is not null and @RM!='')
		 begin
		 set @Conditions = @Conditions + ' and lm.empCode = ''' + @RM + ''''
		 end
		IF (
				@FILTERBYCAMPAIGN IS NOT NULL
				AND @FILTERBYCAMPAIGN != ''
				)
		BEGIN
			IF (@FILTERBYCAMPAIGN != 'NULL')
			BEGIN
				SET @Conditions = @Conditions + ' and fp.Clientstatus = ''' + @FILTERBYCAMPAIGN + ''''
			END
			ELSE
			BEGIN
				SET @Conditions = @Conditions + ' and fp.Clientstatus IS NULL'
			END
		END

		SET @ParmDefinition = N'@FILTERBY2 nvarchar (max)';
		SET @TestQuery = 'WITH MyCte AS 



(



    select fp.Name as ClientName,fp.UserId as [User ID],fp.mobile1 as Mobile,(CASE WHEN fp.Clientstatus like ''FG%'' then fg.GDesc WHEN fp.Clientstatus 



	like ''C%'' THEN cmp.Name ELSE ''Direct Registration'' END) as Campaign,ltm.Name as [Lead Type],emp.Name as Planner, CONVERT(VARCHAR(10),lm.lastActivityDate,104) 

	as [Last Activity Date],

	CONVERT(VARCHAR(10),lm.createdDate,104) as [Created Date],fg.gname as GoalName from 

	fin_leadmapping lm left join fp_Prospects fp on lm.leadCode=fp.Code join [dbo].[fin_Employee] emp on lm.empcode = 



	emp.empcode join fin_LeadTypeMaster ltm on lm.leadTypeId=ltm.leadTypeId left join fin_Campaign cmp on fp.clientstatus= cmp.ClientStatusCode And fp.Clientstatus 



	like ''C%'' left join fin_Goal fg on fp.clientstatus=fg.Gcode And fp.Clientstatus like ''FG%'' where lm.status = 1 ' + @EmpQuery + @Conditions + ' ) SELECT  *



FROM    MyCte;'

		--print @TestQuery
		EXECUTE sp_executesql @TestQuery
			,@ParmDefinition
			,@FILTERBY2 = @FILTERBY2
	END

	IF (@Action = 'SELECT_CLIENTS_PAGED')
	BEGIN
		IF (
				@DATEFROM IS NOT NULL
				AND @DATEFROM != ''
				AND @DATETO IS NOT NULL
				AND @DATETO != ''
				AND @FILTERBY1 IS NOT NULL
				AND @FILTERBY1 != ''
				)
		BEGIN
			IF (@FILTERBY1 = 'BY_CREATEDDATE')
			BEGIN
				SET @Conditions = @Conditions + ' and CONVERT(date, lm.createdDate) between ''' + CAST(CONVERT(DATE, @DATEFROM) AS NVARCHAR(500)) + ''' and ''' + CAST(CONVERT(DATE, @DATETO) AS NVARCHAR(500)) + ''''
			END
			ELSE
			BEGIN
				SET @Conditions = @Conditions + ' and CONVERT(date, lm.lastActivityDate) between ''' + CAST(CONVERT(DATE, @DATEFROM) AS NVARCHAR(500)) + ''' and ''' + CAST(CONVERT(DATE, @DATETO) AS NVARCHAR(500)) + ''''
			END
		END

		IF (
				@FILTERBY2 IS NOT NULL
				AND @FILTERBY2 != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and lm.leadTypeId= CASE WHEN @FILTERBY2=''INTERESTED'' then 18 WHEN 

				@FILTERBY2=''CONVERTED'' THEN 14 WHEN @FILTERBY2=''NEW'' THEN 11 WHEN @FILTERBY2=''DEAD'' THEN 16 WHEN @FILTERBY2=''READY TO INVEST'' 

				THEN 19 WHEN @FILTERBY2=''ON HOLD'' THEN 17 WHEN @FILTERBY2=''ASSIGNED'' THEN 12 WHEN @FILTERBY2=''FOLLOW UP'' THEN 13 WHEN @FILTERBY2=''RE-ASSIGNED''

				 THEN 15 ELSE 0 END'
		END

		IF (
				@FILTERBYNAME IS NOT NULL
				AND @FILTERBYNAME != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.name like ''%' + @FILTERBYNAME + '%'''
		END

		IF (
				@FILTERBYEMAIL IS NOT NULL
				AND @FILTERBYEMAIL != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.userid like ''%' + @FILTERBYEMAIL + '%'''
		END

		IF (
				@FILTERBYMOBILE IS NOT NULL
				AND @FILTERBYMOBILE != ''
				)
		BEGIN
			SET @Conditions = @Conditions + ' and fp.mobile1 like ''%' + @FILTERBYMOBILE + '%'''
		END

		IF (
				@FILTERBYCAMPAIGN IS NOT NULL
				AND @FILTERBYCAMPAIGN != ''
				)
		BEGIN
			IF (@FILTERBYCAMPAIGN != 'NULL')
			BEGIN
				SET @Conditions = @Conditions + ' and fp.Clientstatus = ''' + @FILTERBYCAMPAIGN + ''''
			END
			ELSE
			BEGIN
				SET @Conditions = @Conditions + ' and fp.Clientstatus IS NULL'
			END
		END

		SET @ParmDefinition = N'@FILTERBY2 nvarchar (max)';
		SET @TestQuery = ' WITH MyCte AS 

(

    select lm.id,lm.leadcode,lm.empCode,fp.Name as ClientName,fp.UserId,fp.Pwd,fp.mobile1 as mobile,(CASE WHEN fp.Clientstatus like ''FG%'' then fg.GDesc WHEN fp.Clientstatus

	 like ''C%'' THEN cmp.Name ELSE ''Direct Registration'' END) as Campaign,ltm.Name as LeadType,emp.Name as Planner, lm.lastActivityDate,lm.createdDate,ROW_NUMBER()

	  OVER (ORDER BY lm.createdDate desc) RowNumber from fin_leadmapping lm left join fp_Prospects fp on lm.leadCode=fp.Code join [dbo].[fin_Employee] emp on lm.empcode = emp.empcode 

	  join fin_LeadTypeMaster ltm on lm.leadTypeId=ltm.leadTypeId left join fin_Campaign cmp on fp.clientstatus= cmp.ClientStatusCode And fp.Clientstatus like ''C%'' 

	  left join fin_Goal fg on fp.clientstatus=fg.Gcode And fp.Clientstatus like ''FG%'' where lm.status = 1 and ltm.leadTypeId=14 ' + @EmpQuery + @Conditions + ' ) SELECT  *

FROM    MyCte ;'

		--print @TestQuery
		EXECUTE sp_executesql @TestQuery
			,@ParmDefinition
			,@FILTERBY2 = @FILTERBY2;
	END
END








GO
